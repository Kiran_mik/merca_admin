import React, { Component } from 'react'
import Home from '../pages/Home'
import * as actions from '../actions'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import swal from 'sweetalert'
import { toast } from 'react-toastify';

class AdminChangePassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
      error: {
        oldPassword: '',
        newPassword: '',
        confirmPassword: ''
      },
      errors: {
      },
    }
    this.changePassword = this.changePassword.bind(this)
  }
  validataForm = () => {
    let { oldPassword, newPassword, confirmPassword, errors } = this.state;
    let formIsValid = true
    var pattern = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+/*-.])[A-Za-z\d@!@#$%^&*()_+/*-.]{6,}$/);
    if (!pattern.test(oldPassword)) {
      formIsValid = false
      if (!oldPassword || oldPassword.trim() === '') {
        errors['oldPassword'] = '* Ingrese su contraseña anterior'
      } else
        errors['oldPassword'] = '* La contraseña debe tener al menos 6 letras con mayúsculas, números y caracteres especiales '
    }

    if (!pattern.test(newPassword)) {
      formIsValid = false
      if (!newPassword || newPassword.trim() === '') {
        errors['newPassword'] = '* Introduzca su nueva contraseña'
      } else
        errors['newPassword'] = '* La contraseña debe tener 6 letras con mayúsculas, números y caracteres especiales. '
    }



    if (!confirmPassword || confirmPassword.trim() === '') {
      formIsValid = false
      errors['confirmPassword'] = '* Ingrese su contraseña de confirmación'
    } else if (!pattern.test(confirmPassword)) {
      formIsValid = false
      errors['confirmPassword'] = '* La contraseña debe tener 6 letras con mayúsculas, números y caracteres especiales. '
    }
    else if (confirmPassword != newPassword) {
      formIsValid = false
      errors['confirmPassword'] = '* Confirme que la contraseña no coincide con la contraseña'
    }
    this.setState({ errors })
    return formIsValid

  }
  changePassword(e) {
    if (this.validataForm()) {
      let token = localStorage.getItem('token')
      let { oldPassword, newPassword, confirmPassword } = this.state
      var body = { oldPassword: oldPassword, newPassword: newPassword }
      var url = '/admins/changePasswordForAdmin'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let data = response.data
        if (data.statusCode === 401) {
          toast.error(" Ingrese la contraseña anterior!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        } else if (data.statusCode === 433) {
          toast.error(" ¡La contraseña debe tener 6 letras, una letra mayúscula, una letra minúscula, un dígito y un carácter especial!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        } else if (data.statusCode === 409) {
          toast.error("Por favor, introduzca la contraseña actual correcta!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        } else if (data.statusCode === 416) {
          toast.error("¡Las contraseñas anteriores y actuales no deben ser las mismas!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        } else if (data.statusCode === 204) {
          swal({
            title: "Actualizado con éxito!",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
              this.props.history.push('/dashboard')
              // }
            })
        }
      })
    }
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }
  }


  render() {
    let { oldPassword, newPassword, confirmPassword, errors } = this.state
    return (
      <div>
        <Home>
          <div className='row'>
            <div className='col-md-12'>
              <div className='page-header'>
                <h3>Cambia la contraseña</h3>
                <ul className='breadcrumb '>
                  <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                  <li className='breadcrumb-item active'>Cambia la contraseña</li>
                </ul>
              </div>
            </div>
          </div>
          <div className='card'>
            <div className='card-header'>
              <h5 className='card-title'>Formulario de actualización de contraseña de administrador</h5>
            </div>
            <div className='card-body'>
              <form className='form-sample row'>
                <div className='profile-form'>
                  <div className='form-group row'>
                    <label className='col-form-label  col'>Contraseña anterior</label>
                    <div className='col-md-9'>
                      <input
                        className='form-control'
                        type='password'
                        name='oldPassword'
                        value={oldPassword}
                        placeholder='Ingrese la contraseña anterior'
                        onChange={this.handleChange}
                      />
                      <span className="error-block"> {errors.oldPassword} </span>
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-form-label  col'>Nueva contraseña</label>
                    <div className='col-md-9'>
                      <input
                        className='form-control'
                        type='password'
                        name='newPassword'
                        value={newPassword}
                        placeholder='Ingrese nueva clave'
                        onChange={this.handleChange}
                      />
                      <span className="error-block"> {errors.newPassword} </span>
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-form-label  col'>
                    Confirmar contraseña
                    </label>
                    <div className='col-md-9'>
                      <input
                        className='form-control'
                        type='password'
                        name='confirmPassword'
                        value={confirmPassword}
                        placeholder='Ingrese Confirmar contraseña'
                        onChange={this.handleChange}
                      />
                      <span className="error-block"> {errors.confirmPassword} </span>
                    </div>
                  </div>
                  <hr />
                  <div className='form-group'>
                    <div className='button-group-container pull-right'>
                      <button
                        type='button'
                        className='btn btn-primary'
                        onClick={this.changePassword}
                      >
                        <span>Enviar</span>
                      </button>
                      <button
                        type='button'
                        className='btn btn-outline-primary'
                        onClick={() => this.props.history.push('/dashboard')}
                      >
                        Cancelar
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </Home>
      </div>
    )
  }
}

export default withRouter(
  connect(
    null,
    actions
  )(AdminChangePassword)
)
