import React, { Component } from 'react'
import Home from '../Home'
import { API_URL, CSV_URL, EXCEL_URL } from '../../config/configs'
import axios from 'axios'
import { Select } from 'antd';
import 'antd/dist/antd.css';
import swal from 'sweetalert'
import Toggle from 'react-toggle'
import SimpleReactValidator from 'simple-react-validator'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import isAfter from "date-fns/isAfter";
import moment from 'moment'
import * as actions from '../../actions'
import { connect } from 'react-redux'
import { isEmpty } from "lodash"
import { toast } from 'react-toastify';
import { BeatLoader } from 'react-spinners';
import $ from 'jquery';
class DiscountManagement extends Component {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      best_sellers: true,
      page:1,
      pagesize:10,
      edit: true,
      discountListing1: [],
      minPrice: '',
      maxPrice: '',
      discount: '',
      selectAll: false,
      updatedMaxPrice: '',
      updatedMinPrice: '',
      updatedDiscount: '',
      errors: {
        minPrice: '',
        maxPrice: '',
        discount: '',
        updatedMaxPrice: '',
        updatedMinPrice: '',
        updatedDiscount: ''
      },
      startDate: new Date(),
      endDate: new Date(),
      multipleDelete: [],
      selectedOption: 'Seleccionar',
      minAmount: true,
      maxAmount: true,
      addedDate: true,
      discountCol: true,
      validity: true,
      status: true,
      discountFrom: '',
      discountTo: '',
      minOrder: '',
      maxOrder: '',
      statusValue: '',
      startDateValid: '',
      endDateValid: '',
      startDateAdded: '',
      endDateAdded: '',
      startDateEdit: '',
      endDateEdit: '',
      sortData: { createdAt: false, minAmount: false, maxAmount: false, discount: false, validity: false },
      sort: {},
      loading: true

    }
  }


  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.role!='Super Admin') {
      this.props.history.push('/dashboard')
    }
    this.discountListing()
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').stop().slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').stop().slideToggle();
    });

  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
    if (e.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [e.target.name]: "" })
      });
    }

  }

  // ############################## Discount Listing #############################
  discountListing = (e) => {
    var { discountFrom,page,pagesize, discountTo, minOrder, maxOrder, statusValue, startDateValid, endDateValid, startDateAdded, endDateAdded, sort } = this.state
    var data = {pagesize }
    if(e){
      data.page=1
    }else{
      data.page=page
    }
    var discount = {}
    var validity = {}
    var addedDate = {}
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (discountFrom !== '') {
      discount.minDiscount = JSON.parse(discountFrom)
    }
    if (discountFrom === '') {
      discount.minDiscount = ''
    }
    if (discountFrom !== '' && JSON.parse(discountFrom) > 100) {
      toast.error("¡No debe superar el 100%!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
    }
    if (discountTo !== '') {
      discount.maxDiscount = JSON.parse(discountTo)
    }
    if (discountTo === '') {
      discount.maxDiscount = ''
    }
    if (discountTo !== '' && JSON.parse(discountTo) > 100) {
      toast.error("¡No debe superar el 100%!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
    }
    if (!isEmpty(discount)) {
      data.discount = discount
    }
    if (minOrder !== '') {
      data.minAmount = JSON.parse(minOrder)
    }
    if (maxOrder !== '') {
      data.maxAmount = JSON.parse(maxOrder)
    }
    if (statusValue !== '') {
      data.status = statusValue
    }
    if (statusValue === '') {
      data.status = ''
    }
    if (startDateValid !== '') {
      validity.startDate = moment(startDateValid).format("MM/DD/YYYY")
    }
    if (startDateValid === '') {
      validity.startDate = ''
    }
    if (endDateValid !== '') {
      validity.endDate = moment(endDateValid).format("MM/DD/YYYY")
    }
    if (endDateValid === '') {
      validity.endDate = ''
    }

    if (startDateAdded !== '') {
      addedDate.startDate = moment(startDateAdded).format("MM/DD/YYYY")
    }
    if (startDateAdded === '') {
      addedDate.startDate = ''
    }
    if (endDateAdded !== '') {
      addedDate.endDate = moment(endDateAdded).format("MM/DD/YYYY")
    }
    if (endDateAdded === '') {
      addedDate.endDate = ''
    }
    if (!isEmpty(validity)) {
      data.validity = validity
    }
    if (!isEmpty(addedDate)) {
      data.addedDate = addedDate
    }
    var body = data
    var token = localStorage.getItem('token')
    var url = '/discount/discountListing'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      let { data } = response
      let data1 = data.data
      if (data.message === "Invalid token." || data.message === "Token is expired.") {
        swal('Sesión expirada', '', 'error')
          .then((willDelete) => {
            if (willDelete) {
              this.props.history.push('/')
            }
          })
      }
      if (data.status === 0) {
        this.setState({ discountListing1: [] })
      } else {
        this.setState({ discountListing1: data.data.discountListing, loading: false })
        if (data1.manageDiscountListing) {
          this.setState({
            minAmount: data1.manageDiscountListing.minAmount, maxAmount: data1.manageDiscountListing.maxAmount, addedDate: data1.manageDiscountListing.addedDate, discountCol: data1.manageDiscountListing.discount,
            validity: data1.manageDiscountListing.validity, status: data1.manageDiscountListing.status
          })
        }
      }

    }
    )
  }


  // ############################## Reset Listing ######################
  resetListing = (e) => {
    var url = '/discount/discountListing'
    var method = 'post'
    var { page, pagesize } = this.state
    var body = { page, pagesize}
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      let data1 = data.data

      this.setState({ discountListing1: data.data.discountListing, discountFrom: '', discountTo: '', minOrder: '', maxOrder: '', statusValue: "Seleccionar", startDateValid: '', endDateValid: '', startDateAdded: '', endDateAdded: '' })
      if (data1.manageDiscountListing) {
        this.setState({
          minAmount: data1.manageDiscountListing.minAmount, maxAmount: data1.manageDiscountListing.maxAmount, addedDate: data1.manageDiscountListing.addedDate, discountCol: data1.manageDiscountListing.discount,
          validity: data1.manageDiscountListing.validity, status: data1.manageDiscountListing.status
        })
      }
    }
    )
  }

  // ########################### DeleteDiscount #########################
  deleteDiscount = (uid) => {
    var delArr = this.state.multipleDelete
    swal({ title: '¿Estás seguro?', icon: 'warning', buttons: true, dangerMode: true }).then(willDelete => {
      if (willDelete) {
        if (delArr.length > 0) {
          var body = { discountId: delArr }
        } else {
          var body = { discountId: [uid] }
        }
        var url = '/discount/deleteDiscount'
        var method = 'post'
        var token = localStorage.getItem('token')
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          let { data } = response
          if (data.status === 1) {
            this.setState({ selectedOption: 'Select' })
            this.discountListing()
          }
        })
        swal('Borrado exitosamente', '', 'success')
      }
    })
  }
  // ########################### Add Discount #########################
  addDiscount = () => {
    if (this.validateForm()) {
      var { minPrice, maxPrice, discount, startDate, endDate } = this.state
      var data = { validity: { startDate: moment(startDate).format("MM/DD/YYYY"), endDate: moment(endDate).format("MM/DD/YYYY") } }
      if (JSON.parse(discount) > 100) {
        toast.error("¡El descuento no debe ser superior al 100%!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
      }
      if (JSON.parse(discount) <= 100) {
        data.discount = JSON.parse(discount)
      }
      if (JSON.parse(minPrice) > JSON.parse(maxPrice)) {
        toast.error("¡La cantidad mínima no debe ser mayor que la cantidad máxima!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        data.minAmount = JSON.parse(minPrice)
        data.maxAmount = JSON.parse(maxPrice)
      }
      if (JSON.parse(minPrice) < JSON.parse(maxPrice)) {
        data.minAmount = JSON.parse(minPrice)
        data.maxAmount = JSON.parse(maxPrice)
      }
      if (JSON.parse(minPrice) == JSON.parse(maxPrice)) {
        toast.error("¡La cantidad mínima y la cantidad máxima no deberían ser iguales!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        data.minAmount = JSON.parse(minPrice)
        data.maxAmount = JSON.parse(maxPrice)
      }
      var url = '/discount/addDiscount'
      var method = 'post'
      var token = localStorage.getItem('token')
      var body = data
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          this.setState({ minPrice: '', maxPrice: '', discount: '' })
          swal('Agregado exitosamente', '', 'success')
          this.discountListing()
        }
      })
    } else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  }

  // ########################### UpdateDiscount ###################

  updateDiscount = (each) => {
    var _ = require('lodash');
    var updateDiscountdata = _.map(this.state.discountListing1, (obj) => obj.selected = false)
    var updateDiscountdata = _.map(this.state.discountListing1, (obj) => obj.specified = false)
    each.selected = true;
    each.specified = true;
    var testdate = new Date(each.validity.startDate);
    testdate.setDate(testdate.getDate()-1)

    var testdateend = new Date(each.validity.endDate);
    testdateend.setDate(testdateend.getDate() - 1)
    this.setState({
      discountListing1: this.state.discountListing1,
      updatedDiscount: each.discount,
      updatedMaxPrice: each.maxAmount,
      updatedMinPrice: each.minAmount,
      startDateEdit: testdate,
      endDateEdit: testdateend
    })
  }

  // ###########################  Save UpdateDiscountDetails #########################

  savedata = (each) => {
    var _ = require('lodash');
    if (this.validateForm1()) {
      var { updatedMinPrice, updatedMaxPrice, updatedDiscount, startDateEdit, endDateEdit } = this.state
      var data = { validity: { startDate: moment(startDateEdit).format("MM/DD/YYYY"), endDate: moment(endDateEdit).format("MM/DD/YYYY") }, discountId: each }
      if (JSON.parse(updatedDiscount) > 100) {
        toast.error("¡El descuento no debe ser superior al 100%!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
      }
      if (JSON.parse(updatedDiscount) <= 100) {
        data.discount = JSON.parse(updatedDiscount)
      }
      if (JSON.parse(updatedMinPrice) > JSON.parse(updatedMaxPrice)) {
        toast.error("¡La cantidad mínima no debe ser mayor que la cantidad máxima!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        data.minAmount = JSON.parse(updatedMinPrice)
        data.maxAmount = JSON.parse(updatedMaxPrice)
      }
      if (JSON.parse(updatedMinPrice) < JSON.parse(updatedMaxPrice)) {
        data.minAmount = JSON.parse(updatedMinPrice)
        data.maxAmount = JSON.parse(updatedMaxPrice)
      }
      if (JSON.parse(updatedMinPrice) == JSON.parse(updatedMaxPrice)) {
        toast.error("¡La cantidad mínima y la cantidad máxima no deberían ser iguales!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        data.minAmount = JSON.parse(updatedMinPrice)
        data.maxAmount = JSON.parse(updatedMaxPrice)
      }
      var url = '/discount/addDiscount'
      var method = 'post'
      var token = localStorage.getItem('token')
      var body = data
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          swal({
            title: "Actualizado con éxito",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
                this.discountListing()
              // }
            })
        }
      })
    }
  }

  applyfilter = () => {
    var delArr = this.state.multipleDelete
    if(delArr.length >0){
    if (this.state.selectedOption == 'Delete') {
      if (delArr.length > 0) {
        this.deleteDiscount(...delArr)
      }
    }
    if (this.state.selectedOption == 'Active') {
      var body = { discountId: delArr, isActive: true }
    }
    if (this.state.selectedOption == 'Inactive') {
      var body = { discountId: delArr, publish: false }
    }
    var url = '/discount/changeDiscountStatus'
    var method = 'post'
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if(data.status===1){
      this.discountListing()
      this.setState({ multipleDelete: [], selectedOption: 'Select here' })
      swal('Actualizado con éxito', '', 'success')
      }
    })
  }else{
    this.setState({ selectedOption: 'Select here' })
    swal('Seleccione al menos un registro', '', 'info')
  }
  }


  // ########################### Set Table Rows #########################

  setTableRows = () => {
    var token = localStorage.getItem('token')
    let { minAmount, maxAmount, addedDate, discountCol, validity, status } = this.state
    var body = { minAmount, maxAmount, addedDate, discount: discountCol, validity, status }
    var url = '/discount/setDiscountFilter'
    var method = 'post'
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      let { data } = response
      if (data.status === 1) {
        this.discountListing()
      }
    })
  }
  // ########################### Validation #########################

  validateForm() {
    let { minPrice, maxPrice, discount, errors } = this.state;
    let formIsValid = true
    if (!minPrice) {
      formIsValid = false
      errors['minPrice'] = '* Se requiere una cantidad mínima'
    }
    if (!maxPrice) {
      formIsValid = false
      errors['maxPrice'] = '* Cantidad máxima requerida'
    }
    if (!discount) {
      formIsValid = false
      errors['discount'] = '* Se requiere % de descuento'
    }
    if (Number(discount)>100) {
      formIsValid = false
      errors['discount'] = '* Descuento % no debe ser más de 100'
    }
    this.setState({ errors })
    return formIsValid
  }
  validateForm1() {
    let { updatedDiscount, updatedMaxPrice, updatedMinPrice, errors } = this.state;
    let formIsValid = true

    if (!updatedDiscount) {
      formIsValid = false
      errors['updatedDiscount'] = '* Se requiere % de descuento'
    }
    if (!updatedMaxPrice) {
      formIsValid = false
      errors['updatedMaxPrice'] = '* Cantidad máxima requerida'
    }
    if (!updatedMinPrice) {
      formIsValid = false
      errors['updatedMinPrice'] = '* Se requiere una cantidad mínima'
    }
    // else formIsValid = true
    this.setState({ errors })
    return formIsValid
  }


  handleChange1 = ({ startDate, endDate }) => {
    startDate = startDate || this.state.startDate;
    endDate = endDate || this.state.endDate;

    if (isAfter(startDate, endDate)) {
      endDate = startDate;
    }

    this.setState({ startDate, endDate });

  };
  handleChangeStart = startDate => this.handleChange1({ startDate });
  handleChangeEnd = endDate => this.handleChange1({ endDate });



  handleChangeValid = ({ startDate, endDate }) => {
    if (startDate === null) {
      startDate = ''
    } else { startDate = startDate || this.state.startDateValid; }
    if (endDate === null) {
      endDate = ''
    } else { endDate = endDate || this.state.endDateValid; }
    if (isAfter(startDate, endDate)) {
      endDate = startDate;
    }
    this.setState({ startDateValid: startDate, endDateValid: endDate });
  };
  handleChangeStart1 = startDate => this.handleChangeValid({ startDate });
  handleChangeEnd1 = endDate => this.handleChangeValid({ endDate });


  handleChangeAdded = ({ startDate, endDate }) => {
    if (startDate === null) {
      startDate = ''
    } else { startDate = startDate || this.state.startDateAdded; }
    if (endDate === null) {
      endDate = ''
    } else { endDate = endDate || this.state.endDateAdded; }
    if (isAfter(startDate, endDate)) {
      endDate = startDate;
    }
    this.setState({ startDateAdded: startDate, endDateAdded: endDate });
  };
  handleChangeStart2 = startDate => this.handleChangeAdded({ startDate });
  handleChangeEnd2 = endDate => this.handleChangeAdded({ endDate });


  handleChangeEdit = ({ startDate, endDate }) => {
    startDate = startDate || this.state.startDateEdit;
    endDate = endDate || this.state.endDateEdit;

    if (isAfter(startDate, endDate)) {
      endDate = startDate;
    }
    this.setState({ startDateEdit: startDate, endDateEdit: endDate });
  };
  handleChangeStart3 = startDate => this.handleChangeEdit({ startDate });
  handleChangeEnd3 = endDate => this.handleChangeEdit({ endDate });

  checkArray(_id) {
    let { multipleDelete } = this.state
    if (multipleDelete.includes(_id)) {
      return true
    } else {
      return false
    }
  }

  onCheckbox(_id, val) {
    var delarray = this.state.multipleDelete
    if (!delarray.includes(_id)) {
      delarray.push(_id)
    } else {
      delarray.splice(delarray.indexOf(_id), 1)
    }
    if (delarray.length != this.state.discountListing1.length) {
      this.setState({ checked: false })
    }
    if (this) this.setState({ multipleDelete: delarray })
  }

  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { discountListing1 } = this.state
    if (this.state.selectAll) {
      discountListing1.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      discountListing1.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }
  // ########################### Catagory Status #########################
  changeStatus(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { discountId: [Id], isActive: status }
    var urlkey = '/discount/changeDiscountStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        if (status == false) {
          swal('¡Descuento desactivado con éxito!', '', 'success')
        }
        else {
          swal('¡Descuento activado con éxito!', '', 'success')
        }
        this.discountListing()
      } else if (data.message === 'Invalid token') {
        this.props.history.push('/')
      } else {
        swal(data.message, '', 'error')
      }
    })
  }

  handleChange3 = (e) => {
    this.setState({ statusValue: e.target.value })
  }




  // ########################### download CSV #########################
  downloadCSV(type,array) {
    let { minAmount, maxAmount, addedDate, discountCol, validity, status, discountListing1,discountFrom,discountTo,
      minOrder,maxOrder,statusValue,startDateValid,endDateValid,startDateAdded,endDateAdded,multipleDelete } = this.state
    let token = localStorage.getItem('token')
    if (array === "totalList") {
      var data = { filteredFields: ["createdAt","minAmount","maxAmount","discount","isActive","validity"] }
    }
    if (array === "filteredList") {
    var data = { filteredFields: []}
    var discount = {}
    var validity1 = {}
    var addedDate1 = {}
    data.validity=validity1;
    data.discount=discount;
    data.addedDate=addedDate1

    if (minAmount) {
      data.filteredFields.push('minAmount')
    };
    if (maxAmount) {
      data.filteredFields.push('maxAmount')
    };
    if (addedDate) {
      data.filteredFields.push('createdAt')
    };
    if (discountCol) {
      data.filteredFields.push('discount')
    };
    if (validity) {
      data.filteredFields.push('validity')
    };
    if (status) {
      data.filteredFields.push('isActive')
    };


    if (discountFrom !== '') {
      discount.minDiscount = JSON.parse(discountFrom)
    }
    if (discountTo !== '') {
      discount.maxDiscount = JSON.parse(discountTo)
    }
    if (minOrder !== '') {
      data.minAmount = JSON.parse(minOrder)
    }
    if (maxOrder !== '') {
      data.maxAmount = JSON.parse(maxOrder)
    }
    if (statusValue !== '') {
      data.status = statusValue
    }
    if (startDateValid !== '') {
      validity1.startDate = moment(startDateValid).format("MM/DD/YYYY")
    }
    if (endDateValid !== '') {
      validity1.endDate = moment(endDateValid).format("MM/DD/YYYY")
    }
    if (startDateAdded !== '') {
      addedDate.startDate = moment(startDateAdded).format("MM/DD/YYYY")
    }
    if (endDateAdded !== '') {
      addedDate.endDate = moment(endDateAdded).format("MM/DD/YYYY")
    }
    if(multipleDelete.length>0){
      data.discountsArray=multipleDelete
    }
  }
    var body = data
    var url = '/discount/' + type
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var data = response.data.data
      if (data) {
        if (type === 'downloadDiscountCsvFile') {
          window.open(CSV_URL + data.filePathAndName, '_blank');
          // FileDownload(CSV_URL  +  data.filePathAndName, data.filePathAndName);

        } else {
          window.open(EXCEL_URL + data.filePathAndName, '_blank');
          // FileDownload(EXCEL_URL  +  data.filePathAndName, data.filePathAndName);
        }
      }
    })
  }


  //* ********** SORTING ************************//
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState(
          {
            sort: { [element]: value }
          },
          () => {
            this.discountListing()
          }
        )
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }



  render() {

    const Option = Select.Option;
    var { discountListing1, minPrice, maxPrice, discount, updatedMaxPrice, updatedMinPrice, updatedDiscount, errors, selectedOption, sortData,
      minAmount, maxAmount, addedDate, discountCol, validity, status, errors,
      discountFrom, discountTo, minOrder, maxOrder, statusValue } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Configuraciones de descuento </h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal  </li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/defaultMeta')}>Configuraciones </li>
                <li className='breadcrumb-item active'>Configuraciones de descuento </li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />

        <div className='card animated fadeIn'>
          <div className="card-header">
            <div className="row data-filter">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  <Select
                    showSearch
                    placeholder={<b>Seleccionar</b>}
                    optionFilterProp="children"
                    className="applyselect"
                    value={selectedOption}
                    onSelect={(value) => this.setState({ selectedOption: value })}
                  >
                    <Option value="Active">Activo</Option>
                    <Option value="Inactive">Inactivo</Option>
                    <Option value="Delete">Eliminar</Option>
                  </Select>
                  <button type="button" className="btn btn-primary ml-0 apply_btn" onClick={this.applyfilter}>Aplicar</button>
                  <div className="dropdown">
                      <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <span>Tools</span>
                      </button>
                      <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadDiscountExcelFile", "totalList")}>Exportar a Excel</a>
                        <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadDiscountCsvFile", "totalList")}>Exportar a CSV</a>
                      </div>
                    </div>
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ addedDate: !this.state.addedDate })} checked={addedDate} /><span></span>Fecha Agregada</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ minAmount: !this.state.minAmount })} checked={minAmount} /><span></span>Cantidad mínima de pedido</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ maxAmount: !this.state.maxAmount })} checked={maxAmount} /><span></span>Cantidad máxima de pedido</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ discountCol: !this.state.discountCol })} checked={discountCol} /><span></span>Descuento %</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ validity: !this.state.validity })} checked={validity} /><span></span>Valido hasta</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ status: !this.state.status })} checked={status} /><span></span>Estado</label></li>
            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.discountListing} >
            Reiniciar
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({ minAmount: true, maxAmount: true, addedDate: true, discountCol: true, validity: true, status: true })}>
            Seleccionar todo
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={this.setTableRows}> Guardar </button>
          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Fecha agregada desde</label>
                  <div className="row">
                    <div className='col-md-6'>
                      <DatePicker
                        placeholderText="De"
                        selected={this.state.startDateAdded}
                        selectsStart
                        startDate={this.state.startDateAdded}
                        endDate={this.state.endDateAdded}
                        onChange={this.handleChangeStart2}
                        className="form-control"
                      />
                    </div>
                    <div className='col-md-6'>
                      <DatePicker
                        placeholderText="A"
                        selected={this.state.endDateAdded}
                        selectsEnd
                        startDate={this.state.startDateAdded}
                        endDate={this.state.endDateAdded}
                        onChange={this.handleChangeEnd2}
                        className="form-control"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Cantidad mínima de pedido</label>
                  <input className="form-control" type="text" name="minOrder" placeholder="Cantidad mínima de pedido" id="min_order_amount" value={minOrder} onChange={this.handleChange} />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Cantidad máxima de pedido</label>
                  <input className="form-control" type="text" name="maxOrder" placeholder="Cantidad máxima de pedido" id="max_order_amount" value={maxOrder} onChange={this.handleChange} />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Descuento %</label>
                  <div className="row">
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="discountFrom" placeholder="De" id="d_value" value={discountFrom} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="discountTo" placeholder="A" id="d_value" value={discountTo} onChange={this.handleChange} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Valido hasta</label>
                  <div className="row">
                    <div className='col-md-6'>
                      <DatePicker
                        placeholderText="De"
                        selected={this.state.startDateValid}
                        selectsStart
                        startDate={this.state.startDateValid}
                        endDate={this.state.endDateValid}
                        onChange={this.handleChangeStart1}
                        className="form-control"
                      />
                    </div>
                    <div className='col-md-6'>
                      <DatePicker
                        placeholderText="A"
                        selected={this.state.endDateValid}
                        selectsEnd
                        startDate={this.state.startDateValid}
                        endDate={this.state.endDateValid}
                        onChange={this.handleChangeEnd1}
                        className="form-control"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Estado</label>
                  <select className="form-control" onChange={this.handleChange3} value={statusValue}>
                    <option value="">Seleccionar</option>
                    <option value="active">Activo</option>
                    <option value="inActive">Inactivo</option>
                  </select>
                </div>
              </div>
            </div>
            <hr />
            <div className="pull-right filter-button">
              <div className="dropdown ml-2">
                <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                  <span>Herramientas</span>
                </button>
                <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                  <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadDiscountExcelFile",'filteredList')}>Exportar a Excel</a>
                  <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadDiscountCsvFile",'filteredList')}>Exportar a CSV</a>
                </div>
              </div>
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={()=>this.discountListing('filter')} >
              Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={() => this.resetListing()}>
              Reiniciar
            </button>
            </div>
          </div>
          <div className='card-body'>

            <div className='table-responsive'>
              <table
                className='table dataTable with-image row-border hover  input-table custom-table table-striped'
                style={{ marginTop: 10 }}
              >
                <thead>
                  <tr>
                    <th><div className="checkbox">
                      <label>
                        <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} /><span></span>
                        <i className="input-helper" />
                      </label>
                    </div>
                    </th>
                    {addedDate ? <th sortable-column="createdAt" onClick={this.onSort.bind(this, 'createdAt')}>Fecha Agregada
                 <i aria-hidden='true' className={(sortData['createdAt']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}

                    {minAmount ? <th sortable-column="minAmount" onClick={this.onSort.bind(this, 'minAmount')}>Cantidad mínima de pedido
                  <i aria-hidden='true' className={(sortData['minAmount']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}

                    {maxAmount ? <th sortable-column="maxAmount" onClick={this.onSort.bind(this, 'maxAmount')}>Cantidad máxima de pedido
                  <i aria-hidden='true' className={(sortData['maxAmount']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}

                    {discountCol ? <th sortable-column="discount" onClick={this.onSort.bind(this, 'discount')}>Descuento %
                 <i aria-hidden='true' className={(sortData['discount']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}

                    {validity ? <th sortable-column="validity" onClick={this.onSort.bind(this, 'validity')}>Valido hasta
                 <i aria-hidden='true' className={(sortData['validity']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /> </th> : null}

                    {status ? <th>Estado</th> : null}
                    <th>Comportamiento</th>
                  </tr>
                </thead>
                {discountListing1.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                  <tbody>
                    {discountListing1 && discountListing1.map((each, id) => {
                      return (
                        <tr key={id}>

                          <td>
                            <div className="checkbox">
                              <label>
                                <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray(each._id)} onChange={() => this.onCheckbox(each._id)} /><span></span>
                                <i className="input-helper" />
                              </label>
                            </div>
                          </td>
                          {addedDate ? <td> {moment(each.createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")} </td> : null}
                          {minAmount ?
                            <td>{each.selected ? <>
                              <input className="form-control" type="number" name="updatedMinPrice" value={updatedMinPrice} onChange={this.handleChange} /><span className="error-msg">{errors.updatedMinPrice}</span>
                            </> :
                              each.minAmount}
                            </td> : null}
                          {maxAmount ?
                            <td>{each.selected ? <>
                              <input className="form-control" type="number" name="updatedMaxPrice" value={updatedMaxPrice} onChange={this.handleChange} /><span className="error-msg">{errors.updatedMaxPrice}</span> </> :
                              each.maxAmount}
                            </td> : null}
                          {discountCol ?
                            <td>{each.selected ? <>
                              <input className="form-control" type="number" name="updatedDiscount" value={updatedDiscount} onChange={this.handleChange} /><span className="error-msg">{errors.updatedDiscount}</span></> :
                              each.discount}
                            </td> : null}
                          {validity ?
                            <td className="table-datepicker">
                              {each.selected ? <>
                                <DatePicker
                                  className="form-control"
                                  selected={this.state.startDateEdit}
                                  selectsStart
                                  startDate={this.state.startDateEdit}
                                  endDate={this.state.endDate}
                                  onChange={this.handleChangeStart3}
                                />
                                <DatePicker
                                  className="form-control"
                                  selected={this.state.endDateEdit}
                                  selectsEnd
                                  startDate={this.state.startDateEdit}
                                  endDate={this.state.endDateEdit}
                                  onChange={this.handleChangeEnd3}
                                />
                              </> :
                                <>  {moment(each.validity.startDate.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}-{moment(each.validity.endDate.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</>
                              }
                            </td> : null}
                          {status ?
                            <td>
                              <Toggle
                                checked={each.isActive}
                                className='custom-classname'
                                onChange={() => this.changeStatus(each.isActive, each._id)}
                              />
                            </td> : null}
                          <td>
                            {
                              each.specified ?
                                <button onClick={() => this.savedata(each._id)}>< i className="fa fa-save text-primary" aria-hidden="true"></i> </button> :
                                <button onClick={() => this.updateDiscount(each)}>< i className="fa fa-pencil-square-o text-primary" aria-hidden="true"></i> </button >

                            }
                            <button onClick={() => this.deleteDiscount(each._id)}>
                              <i className='fa fa-trash text-danger' aria-hidden='true' />
                            </button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>}

              </table>
            </div>


            <hr />

            {/* Data table start */}
            <div className='row inline-form' >
              <div className='col-lg-2 col-md-3'>
                <label className='mr-1'>Cantidad mínima de pedido  <span className="text-danger">*</span></label>
                <input
                  className='form-control'
                  type='number'
                  name='minPrice'
                  placeholder='Cantidad mínima de pedido'
                  id='regularprice'
                  required
                  value={minPrice}
                  onChange={this.handleChange}
                />
                <span className="error-msg">{errors.minPrice}</span>
              </div>
              <div className='col-lg-2 col-md-3'>
                <label className='mr-1'>Cantidad máxima de pedido  <span className="text-danger">*</span></label>
                <input
                  className='form-control'
                  type='number'
                  name='maxPrice'
                  placeholder='Cantidad máxima de pedido'
                  id='salesprice'
                  required
                  value={maxPrice}
                  onChange={this.handleChange}
                />
                <span className="error-msg">{errors.maxPrice}</span>
              </div>
              <div className='col-lg-2 col-md-3'>
                <label className='mr-1'>Descuento %  <span className="text-danger">*</span></label>
                <input
                  className='form-control'
                  type='number'
                  name='discount'
                  placeholder='% value'
                  id='salesprice'
                  required
                  value={discount}
                  onChange={this.handleChange}
                />
                <span className="error-msg">{errors.discount}</span>
              </div>
              <div className='col-lg-4 col-md-6 mt-md-2 mt-lg-0'>
                <label className='mr-1'>Valido hasta<span className="text-danger">*</span></label>
                <div className="row">
                  <div className="col-md-6">
                    <DatePicker
                      selected={this.state.startDate}
                      selectsStart
                      startDate={this.state.startDate}
                      endDate={this.state.endDate}
                      onChange={this.handleChangeStart}
                      className="form-control"
                    />
                  </div>
                  <div className="col-md-6">
                    <DatePicker
                      selected={this.state.endDate}
                      selectsEnd
                      startDate={this.state.startDate}
                      endDate={this.state.endDate}
                      onChange={this.handleChangeEnd}
                      className="form-control"
                    />
                  </div>
                </div>
              </div>
              <div className='col-auto'>
                <button className='btn btn-primary cu-btn' onClick={this.addDiscount}>Añadir nuevo</button>
              </div>
            </div>
          </div>

        </div>
      </Home>
    )
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(DiscountManagement)