import React, { Component } from 'react';
import Home from '../Home'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import swal from 'sweetalert'
import Toggle from 'react-toggle'

class Payment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            bankDetails: '',
            wallet: '',
            mCommision: 0,
            mercadoClientId: '',
            mercadoAccessToken: '',
            mercadoClientSecret: '',
            bankActive: false,
            mPagoActive: false,
            rapigoActive: false,
            stripeActive: false,
            sCommision: 0,
            sKey: '',
            sToken: '',
            paymentsId: '',
            errors: {},
            mPagoBtn:true,
            stripeBtn:true,
            rPagoBtn:true,
            bankBtn:true
        }
    }
    componentDidMount() {
        var permissions = this.props.permissionsList;
        if (permissions && permissions.role != 'Super Admin') {
            this.props.history.push('/dashboard')
        }
        this.getDetails()
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
        if (e.target.value) {
            this.setState({
                errors: Object.assign(this.state.errors, { [e.target.name]: "" })
            });
        }
    }
    validateForm = () => {
        let { bankDetails, wallet, mercadoClientId, mercadoAccessToken, mercadoClientSecret, sKey, sToken, errors } = this.state
        let formIsValid = true
        if (!bankDetails || bankDetails.trim() === '') {
            formIsValid = false
            errors['bankDetails'] = '* Se requiere Detalles del banco'
        }
        if (!wallet || wallet.trim() === '') {
            formIsValid = false
            errors['wallet'] = '* Se requiere wallet'
        }
        if (!mercadoClientId || mercadoClientId.trim() === '') {
            formIsValid = false
            errors['mercadoClientId'] = '* Se requiere mercadoClientId'
        }
        if (!mercadoAccessToken || mercadoAccessToken.trim() === '') {
            formIsValid = false
            errors['mercadoAccessToken'] = '* Se requiere mercadoAccessToken'
        }
        if (!mercadoClientSecret || mercadoClientSecret.trim() === '') {
            formIsValid = false
            errors['mercadoClientSecret'] = '* Se requiere mercadoClientSecret'
        }
        if (!sKey || sKey.trim() === '') {
            formIsValid = false
            errors['sKey'] = '* Se requiere sKey'
        }
        if (!sToken || sToken.trim() === '') {
            formIsValid = false
            errors['sToken'] = '* Se requiere sToken'
        }
        this.setState({ errors })
        return formIsValid
    }
    getDetails = () => {
        let token = localStorage.getItem('token');
        let url = "/payments/getDefaultPayments"
        let method = 'get'
        this.props.commonApiCall(url, method, null, token, null, this.props, response => {
            let { data } = response
            if (data && data.data) {
                var data1 = data.data
                this.setState({
                    bankDetails: data1.bankDetails,
                    wallet: data1.repigoDetails,
                    mCommision: data1.mercadoCommission,
                    mercadoClientId: data1.mercadoClientId,
                    mercadoAccessToken: data1.mercadoAccessToken,
                    mercadoClientSecret: data1.mercadoClientSecret,
                    sCommision: data1.stripeCommission,
                    sKey: data1.stripeSk,
                    sToken: data1.stripePk,
                    paymentsId: data1._id,
                    bankActive: data1.bank,
                    mPagoActive: data1.mercado,
                    rapigoActive: data1.rapigo,
                    stripeActive: data1.stripe,
                })
            }

        })
    }
    updateDetails = () => {
        let { bankDetails, wallet, errors, paymentsId, mCommision, mercadoClientId, mercadoAccessToken, mercadoClientSecret, sCommision, sKey, sToken } = this.state

        if (this.validateForm()) {
            let token = localStorage.getItem('token');
            var data = {
                mercadoClientId, mercadoClientSecret, mercadoAccessToken,
                stripeSk: sKey  , stripePk:sToken, bankDetails, repigoDetails: wallet, mercadoCommission: mCommision, stripeCommission: sCommision
            }
            if (paymentsId != '') {
                data.paymentsId = paymentsId
            }
            var body = data
            let url = "/payments/addDefaultPayments"
            let method = 'post'
            this.props.commonApiCall(url, method, body, token, null, this.props, response => {
                let { data } = response
                if (data.status === 1) {
                    swal('Actualizado con éxito', '', 'success')
                }

            })
        }
    }
    // ########################### Change Status #########################
    changeStatus_(status, type) {
        var status = !status
        var token = localStorage.getItem('token')
        var body = { paymentId: this.state.paymentsId, [type]: status }
        var urlkey = '/payments/changePaymentStatus'
        this.props.changeStatus(body, token, urlkey, response => {
            let { data } = response
            if (data.status == 1) {
                if (status == false) {
                    swal('Desactivado con éxito!', '', 'success')
                }
                else {
                    swal('Publicado con éxito!', '', 'success')
                }
                this.getDetails()
            } else if (data.message === 'Invalid token') {
                this.props.history.push('/')
            } else {
                swal(data.message, '', 'error')
            }
        })
    }


    render() {
        let { bankDetails, wallet, errors, mCommision, mercadoClientId, mercadoAccessToken, bankActive, mPagoActive,mPagoBtn,stripeBtn,
            rPagoBtn,bankBtn,stripeActive, rapigoActive, mercadoClientSecret, sCommision, sKey, sToken } = this.state
        return (
            <Home>
                <div className="row">
                    <div className="col-md-12">
                        <div className="page-header">
                            <h3>Configuración de pago</h3>
                            <ul className="breadcrumb ">
                                <li className="breadcrumb-item" onClick={() => this.props.history.push("/dashboard")} >Página principal</li>
                                <li className="breadcrumb-item">Set Configuracionestings</li>
                                <li className="breadcrumb-item active">Configuración de pago </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="card animated fadeIn">
                    <div className="card-body">
                        <div className="fadeInUp">
                            <form className="form-sample">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="row">
                                            <div className="col-md-6 status-title">
                                                <h3 className="col-form-label"> Detalles del banco : <Toggle checked={bankActive} className='custom-classname' onChange={() => this.changeStatus_(bankActive, 'bank')} /></h3>
                                            </div>
                                            <button type='button' onClick={()=>this.setState({bankBtn:!this.state.bankBtn})} style={{fontSize:25}}> <i className='fa fa-edit text-primary'  aria-hidden='true' data-toggle="tooltip" title="Edit" /> </button>

                                            <div className='col-md-8 co-sm-7'>
                                                <CKEditor
                                                    className='ck-editor__editable '
                                                    columns={40}
                                                    editor={ClassicEditor}
                                                    data={bankDetails}
                                                    disabled={bankBtn?true:false}
                                                    onInit={editor => {
                                                    }}
                                                    onChange={(event, editor) => {
                                                        const data = editor.getData()
                                                        this.setState({ bankDetails: data, errors: Object.assign(this.state.errors, { bankDetails: "" }) })
                                                    }}
                                                />
                                                <span className="error-block"> {errors.bankDetails} </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                                <div className="row mt-3">
                                    <div className="col-md-6 status-title">
                                        <h3 className="col-form-label"> Pago Fácil / Rapipago : <Toggle checked={rapigoActive} className='custom-classname' onChange={() => this.changeStatus_(rapigoActive, 'rapigo')} /></h3>
                                    </div>
                                    <button type='button' onClick={()=>this.setState({rPagoBtn:!this.state.rPagoBtn})} style={{fontSize:25}}> <i className='fa fa-edit text-primary'  aria-hidden='true' data-toggle="tooltip" title="Edit" /> </button>
                                </div>
                                <div >
                                    <div className="row" >
                                        <div className="mb-3 col-md-6" >
                                            <div className='col-md-8 co-sm-7'>
                                                <CKEditor
                                                    className='ck-editor__editable '
                                                    columns={40}
                                                    editor={ClassicEditor}
                                                    data={wallet}
                                                    disabled={rPagoBtn?true:false}
                                                    onInit={editor => {
                                                    }}
                                                    onChange={(event, editor) => {
                                                        const data = editor.getData()
                                                        this.setState({ wallet: data, errors: Object.assign(this.state.errors, { wallet: "" }) })
                                                    }}
                                                />
                                                <span className="error-block"> {errors.wallet} </span>
                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>
                                <div className="row mt-3">
                                    <div className="col-md-6 status-title">
                                        <h3 className="col-form-label"> Mercado pago : <Toggle checked={mPagoActive} className='custom-classname' onChange={() => this.changeStatus_(mPagoActive, 'mercado')} /></h3>
                                    </div>
                                    <button type='button' onClick={()=>this.setState({mPagoBtn:!this.state.mPagoBtn})} style={{fontSize:25}}> <i className='fa fa-edit text-primary'  aria-hidden='true' data-toggle="tooltip" title="Edit" /> </button>
                                </div>
                                <div >
                                    <div className="form-group row" >
                                    <label className="col-lg-2 col-sm-3  col-form-label">Client Id:</label>
                                        <div className="col-md-8 co-sm-7">
                                            <input
                                                className='form-control'
                                                type='text'
                                                name='mercadoClientId'
                                                placeholder='mercadoClientId'
                                                value={mercadoClientId}
                                                onChange={this.handleChange}
                                                disabled={mPagoBtn?true:false}
                                            />
                                            <span className="error-block"> {errors.mercadoClientId} </span>
                                        </div>
                                    </div>
                                    
                                    <div className="form-group row" >
                                    <label className="col-lg-2 col-sm-2 col-form-label">Client Secret Key:</label>
                                        <div className="col-md-8 co-sm-7">
                                            <input
                                                className='form-control'
                                                type='text'
                                                name='mercadoClientSecret'
                                                placeholder='mercadoClientSecret'
                                                value={mercadoClientSecret}
                                                onChange={this.handleChange}
                                                disabled={mPagoBtn?true:false}
                                            />
                                            <span className="error-block"> {errors.mercadoClientSecret} </span>
                                        </div>
                                    </div>
                                    
                                    <div className="form-group row" >
                                    <label className="col-lg-2 col-sm-2 col-form-label"> Access Token:</label>
                                        <div className="col-md-8 co-sm-7">
                                            <input
                                                className='form-control'
                                                type='text'
                                                name='mercadoAccessToken'
                                                placeholder='mercadoAccessToken'
                                                value={mercadoAccessToken}
                                                onChange={this.handleChange}
                                                disabled={mPagoBtn?true:false}
                                            />
                                            <span className="error-block"> {errors.mercadoAccessToken} </span>
                                        </div>
                                    </div>
                                    
                                    <div className="form-group row" >
                                    <label className="col-lg-2 col-sm-2 col-form-label">Comisión Medio de pago:</label>
                                        <div className="col-md-8 co-sm-7">
                                            <input
                                                className='form-control'
                                                type='number'
                                                name='mCommision'
                                                placeholder='commision'
                                                value={mCommision}
                                                onChange={this.handleChange}
                                                disabled={mPagoBtn?true:false}
                                            />
                                        </div>
                                    </div>

                                </div>
                                <div className="row mt-3">
                                    <div className="col-md-6 status-title">
                                        <h3 className="col-form-label"> Stripe : <Toggle checked={stripeActive} className='custom-classname' onChange={() => this.changeStatus_(stripeActive, 'stripe')} /></h3>
                                    </div>
                                    <button type='button' onClick={()=>this.setState({stripeBtn:!this.state.stripeBtn})} style={{fontSize:25}}> <i className='fa fa-edit text-primary'  aria-hidden='true' data-toggle="tooltip" title="Edit" /> </button>
                                </div>
                                <div className="">
                                    
                                    <div className="form-group row" >
                                        <label className="col-lg-2 col-sm-2 col-form-label">Token:</label>
                                        <div className="col-md-8 co-sm-7">
                                            <input
                                                className='form-control'
                                                type='text'
                                                name='sToken'
                                                placeholder='token'
                                                value={sToken}
                                                onChange={this.handleChange}
                                                disabled={stripeBtn?true:false}
                                            />
                                            <span className="error-block"> {errors.sToken} </span>
                                        </div>
                                        
                                    </div>
                                    
                                    <div className="form-group row" >
                                    <label className="col-lg-2 col-sm-2 col-form-label">Secrete Key:</label>
                                        <div className="col-md-8 co-sm-7 ">
                                            <input
                                                className='form-control'
                                                type='text'
                                                name='sKey'
                                                placeholder='key'
                                                value={sKey}
                                                onChange={this.handleChange}
                                                disabled={stripeBtn?true:false}

                                            />
                                            <span className="error-block"> {errors.sKey} </span>
                                        </div>
                                    </div>
                                    
                                    <div className="form-group row" >
                                    <label className="col-lg-2 col-sm-2 col-form-label"> Comisión Medio de pago: </label>
                                        <div className="col-md-8 co-sm-7">
                                            <input
                                                className='form-control'
                                                type='number'
                                                name='sCommision'
                                                placeholder='commision'
                                                value={sCommision}
                                                onChange={this.handleChange}
                                                disabled={stripeBtn?true:false}

                                            />
                                        </div>
                                    </div>
                                    
                                    <div className="form-group">
                                        <div className="button-group-container float-right">
                                            <button type="button" className="btn btn-primary" onClick={() => this.updateDetails()}>
                                                <span>Actualizar</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Home>
        );
    }
}

const mapStateToProps = state => ({
    permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(Payment)