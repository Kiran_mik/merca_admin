import React, { Component } from 'react';
import * as actions from '../../actions'
import { connect } from 'react-redux'
import swal from 'sweetalert'
import axios from 'axios'
import { API_URL } from '../../config/configs'

import Home from '../Home';
class NotificationSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signUp: false,
      outOfStock: false,
      orderReceived: false,
      delivered: false,
      cancelled: false
    };
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.role!='Super Admin') {
      this.props.history.push('/dashboard')
    }
    this.notificationsGet()
  }

  // ############################## Getting Listing ######################
  notificationsGet = () => {
    var token = localStorage.getItem('token')
    axios({
      method: 'get',
      url: API_URL + '/admins/getNotificationSettings',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      },
    }).then(response => {
      var { data } = response
      if (data.message === "Invalid token." || data.message === "Token is expired.") {
        swal('Sesión expirada', '', 'error')
          .then((willDelete) => {
            // if (willDelete) {
              localStorage.removeItem("token"); this.props.history.push('/');
            // }
          })
      }
      if (data.status === 1) {
        let { notificationSettings } = data.data
        if (data.data.length == 0) {
          this.setState({ signUp: false, outOfStock: false, orderReceived: false, delivered: false, cancelled: false })
        }
        else {
          this.setState({
            signUp: notificationSettings.signUp,
            outOfStock: notificationSettings.productOutOfStock,
            orderReceived: notificationSettings.orderReceived,
            cancelled: notificationSettings.orderCancelled,
            delivered: notificationSettings.orderDelievered
          })
        }
      }
    })
  }


  // ############################## Setting Listing ######################
  notificationSet = () => {
    let { signUp, outOfStock, orderReceived, delivered, cancelled } = this.state
    var url = '/admins/setNotificationSettings'
    var method = 'post'
    var token = localStorage.getItem('token')
    var body = { signUp, productOutOfStock: outOfStock, orderReceived, orderDelievered: delivered, orderCancelled: cancelled }
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      let { data } = response
      if (data.status === 1) {
        swal({
          title: "Actualizado con éxito!",
          icon: "success",
        })
          .then((willDelete) => {
            // if (willDelete) {
              this.notificationsGet()
            // }
          })
      }else{
        swal(data.message,'','warning')
        this.notificationsGet()
      }
    }
    )
  }

  render() {
    let { signUp, outOfStock, orderReceived, delivered, cancelled } = this.state
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Configuración de las notificaciones</h3>
              <ul className="breadcrumb ">
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/defaultMeta')}>SetConfiguracionestings </li>
                <li className="breadcrumb-item active">Configuración de las notificaciones</li>
              </ul>
            </div>
          </div>
        </div>
        <div className='card animated fadeIn'>
          <div className='card-body'>
            <div className="checkbox">
              <label>
                <input type="checkbox" className="form-check-input" name="Signup" checked={signUp} onChange={() => this.setState({ signUp: !this.state.signUp })} /><span></span>
                <i className="input-helper" /><h6>Regístrate</h6>
              </label>
            </div><br/>
            <div className="checkbox">
              <label>
                <input type="checkbox" className="form-check-input" name="Stock" checked={outOfStock} onChange={() => this.setState({ outOfStock: !this.state.outOfStock })} /><span></span>
                <i className="input-helper" /><h6>Producto agotado</h6>
              </label>
            </div><br/>
            <div className="checkbox">
              <label>
                <input type="checkbox" className="form-check-input" name="received" checked={orderReceived} onChange={() => this.setState({ orderReceived: !this.state.orderReceived })} /><span></span>
                <i className="input-helper" /><h6>Orden recibida</h6>
              </label>
            </div><br/>
            <div className="checkbox">
              <label>
                <input type="checkbox" className="form-check-input" name="delivered" checked={delivered} onChange={() => this.setState({ delivered: !this.state.delivered })} /><span></span>
                <i className="input-helper" /><h6>Estado del pedido: Entregado</h6>
              </label>
            </div><br/>
            <div className="checkbox">
              <label>
                <input type="checkbox" className="form-check-input" name="cancelled" checked={cancelled} onChange={() => this.setState({ cancelled: !this.state.cancelled })} /><span></span>
                <i className="input-helper" /><h6>Estado del pedido: Cancelado</h6>
              </label>
            </div>
            <hr />
            <button
              type='button'
              className='btn btn-primary pull-right'
              onClick={this.notificationSet}
            >
              <span>Guardar</span>
            </button>
          </div>
        </div>
      </Home>
    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(NotificationSettings)