import React, { Component } from 'react'
import * as actions from '../../actions'
import { connect } from 'react-redux'
import SimpleReactValidator from 'simple-react-validator'
import Home from '../Home'
import axios from 'axios'
import swal from 'sweetalert'
import { API_URL, IMAGE_URL } from '../../config/configs'
import $ from 'jquery';
class DefaultMeta extends Component {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      metaTitle: '',
      metaKeyword: '',
      metaDescription: '',
      minOrderVal: '',
      orderDelivery:1,
      metaId: '',
      currency: '',
      dateFormat: '',
      imagePreviewUrl: '',
      fileVisible: false,
      fileVisible1: false,
      fileVisible2: false,
      fileVisible3: false,
      filename: '',
      file: '',
      errors: {
        metaTitle: '',
        metaKeyword: '',
        metaDescription: ''
      }

    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.role != 'Super Admin') {
      this.props.history.push('/dashboard')
    }
    this.getMetaDetails()
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }
  }

  validateForm = () => {
    let { metaDescription, minOrderVal,orderDelivery, metaKeyword, metaTitle, errors } = this.state
    let formIsValid = true
    if (!metaDescription || metaDescription.trim() === '') {
      formIsValid = false
      errors['metaDescription'] = '* Se requiere meta descripción'
    }
    if (!minOrderVal) {
      formIsValid = false
      errors['minOrderVal'] = '* Se requiere un valor mínimo de pedido'
    }
    if(Number(orderDelivery&&orderDelivery>100)){
      formIsValid = false
      errors['orderDelivery'] = '* No más de 100 días'
    }
    if (!metaKeyword || metaKeyword.trim() === '') {
      formIsValid = false
      errors['metaKeyword'] = '* Se requiere meta palabra clave'
    }
    if (!metaTitle || metaTitle.trim() === '') {
      formIsValid = false
      errors['metaTitle'] = '* Se requiere meta título'
    }
    // else formIsValid = true
    this.setState({ errors })
    return formIsValid
  }


  // ########################### Add MetaDetails #########################
  addMetaDetails = e => {
    let { metaDescription, minOrderVal,orderDelivery, metaKeyword, metaTitle, metaId, currency, dateFormat } = this.state
    e.preventDefault()
    if (this.validateForm()) {
      var url = '/meta/addDefaultMetaData'
      var method = 'post'
      var data = { metaDescription, minOrderAmount: Number(minOrderVal), metaTitle, metaKeyword, type: "metaData", currency, dateFormat ,orderDelivery}
      if (metaId != '') {
        data.metaDataId = metaId
      }
      var body = data
      var token = localStorage.getItem('token')
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data) {
          swal('Actualizado con éxito', '', 'success')
        }
      }
      )
    }
  }




  // ########################### Get MetaDetails #########################
  getMetaDetails = e => {
    var token = localStorage.getItem('token')
    axios({
      method: 'get',
      url: API_URL + '/meta/getDefaultMetaData',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      },
    }).then(response => {
      let { data } = response
      if (data.message === "Invalid token." || data.message === "Token is expired.") {
        swal('Sesión expirada', '', 'error')
          .then((willDelete) => {
            if (willDelete) {
              localStorage.removeItem("token"); this.props.history.push('/');
            }
          })
      }
      if (data.data) {
        this.setState({
          metaTitle: data.data.metaTitle,
          metaKeyword: data.data.metaKeyword,
          metaDescription: data.data.metaDescription,
          orderDelivery:data.data.orderDelivery,
          minOrderVal:data.data.minOrderAmount,
          metaId: data.data._id,
          currency: data.data.currency,
          dateFormat: data.data.dateFormat
        })
      }
    })
  }


  // #################################  Image Preview #################################

  fileChangedHandler(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file: file,
        filename: filename,
        imagePreviewUrl: reader.result,
        fileVisible: true
      })
    }
    reader.readAsDataURL(file)
  }


  fileChangedHandlerProd(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file1: file,
        filename1: filename,
        prodPreviewUrl: reader.result,
        fileVisible1: true
      })
    }
    reader.readAsDataURL(file)
  }

  fileChangedHandlerCat(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file2: file,
        filename2: filename,
        catPreviewUrl: reader.result,
        fileVisible2: true
      })
    }
    reader.readAsDataURL(file)
  }

  fileChangedHandlerIcon(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file3: file,
        filename3: filename,
        iconPreviewUrl: reader.result,
        fileVisible3: true
      })
    }
    reader.readAsDataURL(file)
  }
  // ################################# Avatar Image Upload #################################

  metaFileUpload = (name) => {
    if (name) {
      var { file, file1, file2, file3 } = this.state
      var token = localStorage.getItem('token')
      var formData = new FormData()
      formData.append('imageType', [name])
      if (name == "avatar") {
        formData.append('file', file)
      }
      else if (name == "product") {
        formData.append('file', file1)
      }
      else if (name == "category") {
        formData.append('file', file2)
      }
      else if (name == "icon") {
        formData.append('file', file3)
      }
      else {
        formData.append('file', file)
      }
      axios
        .post(API_URL + '/auth/fileUploadForDefaultImages', formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: token
          }
        })
        .then(response => {
          let { data } = response.data
          if (data) {
            if (name == "avatar") {
              this.setState({ fileVisible: false, file: '' })
              swal('Imagen subida correctamente', '', 'success')
            }
            else if (name == "product") {
              this.setState({ fileVisible1: false, file1: '' })
              swal('Imagen subida correctamente', '', 'success')
            }
            else if (name == "category") {
              this.setState({ fileVisible2: false, file2: '' })
              swal('Imagen subida correctamente', '', 'success')
            } else if (name == "icon") {
              this.setState({ fileVisible3: false, file3: '' })
              swal('Icono cargado correctamente', '', 'success')
            }
            else {
              console.log('image not found');

            }
          }
        })
        .catch(err => console.log(err))
    }
  }




  render() {
    let { metaTitle, minOrderVal, metaDescription, orderDelivery,metaKeyword, currency, dateFormat, errors } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Configuración global</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item'>Configuraciones</li>
                <li className='breadcrumb-item active'>Configuración global</li>
              </ul>
            </div>
          </div>
        </div>
        <div className='card animated fadeIn'>
          <div className='card-body'>
            <form className='form-sample'>
              <ul className='nav nav-tabs' id='myTab' role='tablist'>
                <li className='nav-item active'>
                  <a className='nav-link active' data-toggle='tab' href='#meta_data' role='tab' aria-controls='meta_data' aria-selected='false' >Configuración global</a>
                </li>
                <li className='nav-item'>
                  <a className='nav-link' data-toggle='tab' href='#upload_image' role='tab' aria-controls='upload_image' aria-selected='false' >Imagen </a>
                </li>
              </ul>
              <div className='tab-content' id='myTabContent'>
                <div
                  className='fade show active tab-pane'
                  id='meta_data'
                  role='tabpanel'
                  aria-labelledby='meta_data'
                >
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                    Moneda :
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <select className='form-control' onChange={(e) => this.setState({ currency: e.target.value })} value={currency}>
                        <option value="USD">USD</option>
                        <option value="AUD">AUD</option>
                        <option value="ARS">ARS</option>

                      </select>
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                    Formato de fecha:
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <select className='form-control' onChange={(e) => this.setState({ dateFormat: e.target.value })} value={dateFormat}>
                        <option value="YYYY/MM/DD">YYYY/MM/DD</option>
                        <option value="DD/MM/YYYY">DD/MM/YYYY</option>
                        <option value="MM/DD/YYYY">MM/DD/YYYY</option>
                      </select>
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                    Valor mínimo de pedido:
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <input
                        className='form-control'
                        type='text'
                        name='minOrderVal'
                        placeholder='Ingrese el valor mínimo de pedido'
                        value={minOrderVal}
                        onChange={this.handleChange}
                      />
                      <span className="error-block"> {errors.minOrderVal} </span>

                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                    Entrega de pedidos:
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <input
                        className='form-control'
                        type='number'
                        name='orderDelivery'
                        placeholder='Entrega de pedidos'
                        value={orderDelivery}
                        onChange={this.handleChange}
                      />
                      <span className="error-block"> {errors.orderDelivery} </span>

                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                    Meta título:
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <input
                        className='form-control'
                        type='text'
                        name='metaTitle'
                        placeholder='Ingrese título'
                        value={metaTitle}
                        onChange={this.handleChange}
                      />
                      <span className='notes'>
                      Máximo 70 caracteres es adecuado
                      </span>
                      <span className="error-block"> {errors.metaTitle} </span>

                    </div>
                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                    Meta palabra clave:
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <textarea
                        className='form-control'
                        type='text'
                        name='metaKeyword'
                        placeholder='Introduzca palabra clave meta'
                        value={metaKeyword}
                        onChange={this.handleChange}
                      />
                      <span className='notes'>
                      Máximo 10 palabras es adecuado
                      </span>
                      <span className="error-block"> {errors.metaKeyword} </span>

                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                    Metadescripción :
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <textarea
                        className='form-control'
                        type='text'
                        name='metaDescription'
                        placeholder='Ingrese la descripción'
                        value={metaDescription}
                        onChange={this.handleChange}
                      />
                      <span className='notes'>
                      Máximo 156 caracteres es adecuado
                      </span>
                      <span className="error-block"> {errors.metaDescription} </span>

                    </div>
                  </div>
                  <hr />
                  <div className='button-continer text-right'>
                    <button type='button' className='btn btn-primary' onClick={this.addMetaDetails} > Guardar </button>
                  </div>
                </div>

                <div
                  className='tab-pane fade'
                  id='upload_image'
                  role='tabpanel'
                  aria-labelledby='Image Upload'
                >
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                    Imagen de Avatar:
                    </label>
                    <div className='drop-section mb-3 col-md-4'>
                      <div className='form-group inputDnD mb-0'>
                        <div className='upload-overlay d-flex justify-content-center align-items-center'>
                          <div className='upload-wrapper'>
                            <i className='fa fa-upload' />
                            <span> <button type='button' > Elige un archivo</button> o &nbsp;  arrástralo aquí </span>
                          </div>
                        </div>
                        <label className='sr-only' htmlFor="for">
                        Subir archivo
                        </label>
                        <input
                          type='file'
                          className='form-control-file text-primary font-weight-bold hidden'
                          name='image'
                          id='file'
                          accept='image/*'
                          onChange={this.fileChangedHandler.bind(this)}
                          data-title='Drag and drop a file'
                        />
                      </div>
                      {this.state.fileVisible ?
                        <span>
                          <div className='product-img'> <img src={this.state.imagePreviewUrl} /><span> <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ fileVisible: !this.state.fileVisible, file: '' })} /></span> </div>
                          <span>{this.state.filename}</span>

                        </span> : null}

                      <div className='button-continer pull-right my-2'>
                        <button className='btn btn-primary' type='button' onClick={this.metaFileUpload.bind(this, "avatar")}  > Cargar imagen </button>
                      </div>
                    </div>
                    <div className='col-md-4'>
                      <div className='row justify-content-between'>
                        <div className='col-md-12 select-image'>
                          <div className='product-img' >
                            <img src={IMAGE_URL + "avatar.jpg" + "?" + Math.random()} alt="avatarImage" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                    Imagen del producto :
                    </label>
                    <div className='drop-section mb-3 col-md-4'>
                      <div className='form-group inputDnD mb-0'>
                        <div className='upload-overlay d-flex justify-content-center align-items-center'>
                          <div className='upload-wrapper'>
                            <i className='fa fa-upload' />
                            <span> <button type='button' >Elige un archivo, </button> o &nbsp;  arrástralo aquí </span>
                          </div>
                        </div>
                        <label className='sr-only' >
                          Subir archivo
                        </label>
                        <input
                          type='file'
                          className='form-control-file text-primary font-weight-bold'
                          name='image'
                          id='file'
                          accept='image/*'
                          onChange={this.fileChangedHandlerProd.bind(this)}
                          data-title='Drag and drop a file'
                        />
                      </div>
                      {this.state.fileVisible1 ?
                        <span>
                          <div className='product-img'> <img src={this.state.prodPreviewUrl} /><span> <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ fileVisible1: !this.state.fileVisible1, file1: '' })} /></span> </div>
                          <span>{this.state.filename1}</span>

                        </span> : null}

                      <div className='button-continer pull-right my-2'>
                        <button className='btn btn-primary' type='button' onClick={this.metaFileUpload.bind(this, "product")} > Cargar imagen </button>
                      </div>
                    </div>
                    <div className='col-md-4'>
                      <div className='row justify-content-between'>
                        <div className='col-md-12 select-image'>
                          <div className='product-img' >
                            <img src={IMAGE_URL + "product.jpg" + "?" + Math.random()} alt="productImage" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>



                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                    Imagen de categoría:
                    </label>
                    <div className='drop-section mb-3 col-md-4'>
                      <div className='form-group inputDnD mb-0'>
                        <div className='upload-overlay d-flex justify-content-center align-items-center'>
                          <div className='upload-wrapper'>
                            <i className='fa fa-upload' />
                            <span> <button type='button' > Elige un archivo </button> o &nbsp; arrástralo aquí </span>
                          </div>
                        </div>
                        <label className='sr-only' >
                          Subir archivo
                        </label>
                        <input
                          type='file'
                          className='form-control-file text-primary font-weight-bold'
                          name='image'
                          id='file'
                          accept='image/*'
                          onChange={this.fileChangedHandlerCat.bind(this)}
                          data-title='Drag and drop a file'

                        />
                      </div>
                      {this.state.fileVisible2 ?
                        <span>
                          <div className='product-img'> <img src={this.state.catPreviewUrl} /><span> <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ fileVisible2: !this.state.fileVisible2, file2: '' })} /></span> </div>
                          <span>{this.state.filename2}</span>

                        </span> : null}
                      <div className='button-continer pull-right my-2'>
                        <button className='btn btn-primary' type='button' onClick={this.metaFileUpload.bind(this, "category")} > Cargar imagen </button>
                      </div>
                    </div>
                    <div className='col-md-4'>
                      <div className='row justify-content-between'>
                        <div className='col-md-12 select-image'>
                          <div className='product-img' >
                            <img src={IMAGE_URL + "category.jpg" + "?" + Math.random()} alt="categoryImage" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                    Imagen de icono:
                    </label>
                    <div className='drop-section mb-3 col-md-4'>
                      <div className='form-group inputDnD mb-0'>
                        <div className='upload-overlay d-flex justify-content-center align-items-center'>
                          <div className='upload-wrapper'>
                            <i className='fa fa-upload' />
                            <span>
                              <button type='button'  > Elige un archivo </button>   o &nbsp; arrástralo aquí
                            </span>
                          </div>
                        </div>
                        <label className='sr-only' >
                          Subir archivo
                        </label>
                        <input
                          type='file'
                          className='form-control-file text-primary font-weight-bold'
                          name='image'
                          id='file'
                          accept='image/*'
                          onChange={this.fileChangedHandlerIcon.bind(this)}
                          data-title='Drag and drop a file'
                        />
                      </div>
                      {this.state.fileVisible3 ?
                        <span>
                          <div className='product-img'> <img src={this.state.iconPreviewUrl} /><span> <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ fileVisible3: !this.state.fileVisible3, file3: '' })} /></span> </div>
                          <span>{this.state.filename3}</span>

                        </span> : null}
                      <div className='button-continer pull-right my-2'>
                        <button className='btn btn-primary' type='button' onClick={this.metaFileUpload.bind(this, "icon")} > Cargar imagen </button>
                      </div>
                    </div>
                    <div className='col-md-4'>
                      <div className='row justify-content-between my-2'>
                        <div className='col-md-12 select-image'>
                          <div className='product-img' >
                            <img src={IMAGE_URL + "icon.png" + "?" + Math.random()} alt="iconImage" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </form>
          </div>
        </div>
      </Home>
    )
  }
}
const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(DefaultMeta)

