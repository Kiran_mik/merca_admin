import React, { Component } from 'react'
import swal from 'sweetalert'
import { connect } from 'react-redux'
import * as actions from '../actions'
import { toast } from 'react-toastify';


class AdminLogin extends Component {
  constructor(props) {
    super(props)
    this.state = {
      emailId: '',
      password: '',
      rememberme: false,
      loggedin: false,
      errors: {}
    }

    this.adminLogin = this.adminLogin.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  async componentDidMount() {
    let loggedin = await localStorage.getItem('rememberme')
    if (loggedin === true) {
      this.props.history('/dashboard`')
    }
    let emailId = await localStorage.getItem('emailId')
    let password = await localStorage.getItem('password')
    let rememberme = await localStorage.getItem('rememberme')

    if (emailId && password) {
      this.setState({ emailId, password: atob(password), rememberme })
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }

  }


  // ################################ Admin Login ###########################
  adminLogin = (e) => {
    e.preventDefault();
    var self = this;
    let { emailId, password, rememberme } = this.state;
    if (this.validateForm()) {
      let data = { emailId, password };
      this.props.adminLogin(data, (response) => {
        let { data } = response;
        if (data.status === 1) {
          if (rememberme) {
            localStorage.setItem('emailId', data.data.email)
            localStorage.setItem('password', btoa(password))
            localStorage.setItem('rememberme', !rememberme)
          } else {
            localStorage.removeItem('email')
            localStorage.removeItem('password')
            localStorage.removeItem('rememberme')
            this.setState({ rememberme: !rememberme, email: emailId, password: password })
          }
          self.props.history.push('/dashboard')
        } else if (data.statusCode === 433) {
          toast.error("¡El correo electrónico y la contraseña no coinciden!", { position: "top-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        } else if (data.statusCode === 427) {
          toast.error("Ingrese el correo electrónico registrado!", { position: "top-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        } else if (data.statusCode === 410) {
          toast.error("Credencial inválida !", { position: "top-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }else if (data.statusCode === 415) {
          toast.error("¡El usuario está bloqueado por el administrador!", { position: "top-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }
        
      })
    } else {
      return false
    }
  }

  validateForm() {
    let { emailId, password } = this.state
    let errors = {}
    var pattern = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
    var pas_pattern = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+/*-.])[A-Za-z\d@!@#$%^&*()_+/*-.]{6,}$/);
    let formIsValid = true
    if (!pattern.test(emailId)) {
      formIsValid = false
      if(!emailId||emailId.trim() === '' ){
        errors['emailId'] = 'Introduce tu correo electrónico'
      }else
      errors['emailId'] = 'Introduzca un correo electrónico válido'
    }
   
    if(!pas_pattern.test(password)){
      formIsValid = false
      if (!password||password.trim() === '') {
        errors['password'] = 'Ingresa tu contraseña'
      }else
      errors['password'] = 'La contraseña debe tener al menos 6 letras con mayúsculas, números y caracteres especiales'
    }
    this.setState({ errors: errors })
    return formIsValid
  }

  // ################################ Remember Me ###########################
  rememberMe() {
    var { emailId, password, rememberme } = this.state
    if (rememberme == false) {
      localStorage.setItem('emailId', emailId)
      localStorage.setItem('password', btoa(password))
      localStorage.setItem('rememberme', !rememberme)
      this.setState({ rememberme: !rememberme })
    } else {
      localStorage.removeItem('email')
      localStorage.removeItem('password')
      localStorage.removeItem('rememberme')
      this.setState({
        rememberme: !rememberme,
        email: emailId,
        password: password
      })
    }
  }

  render() {
    let { emailId, password, errors } = this.state
    return (
      <div className='container-scroller'>
        <div className='container-fluid page-body-wrapper full-page-wrapper'>
          <div className='content-wrapper d-flex align-items-center auth login-full-bg'>
         
            <div className='row w-100 margin-l-0'>
              <div className='col-lg-6 col-sm-8 mx-auto authentication-form'>
                <div className='login-header'>
                  <img src='assets/images/logo.svg' alt='logo' />
                  <h2>Bienvenido a MercadoMayorista.com</h2>
                </div>
                <div className='auth-form-light text-left p-5'>
                  <form autoComplete='off' onSubmit={this.adminLogin}>
                    <h3 className='pb-2 text-center'>
                    Iniciar sesión en su cuenta
                    </h3>
                    <div className='form-group'>
                      <input
                        type='text'
                        className={errors.emailId ? 'form-control input-error' : 'form-control'}
                        name='emailId'
                        value={emailId}
                        id='email'
                        placeholder='Ingresar ID de correo electrónico'
                        onChange={this.handleChange}
                      />
                      <i className='fa fa-user' aria-hidden='true' />
                      <span className='error-block'>{errors.emailId}</span>
                    </div>
                    <div className='form-group'>
                      <input
                        className={errors.password ? 'form-control input-error' : 'form-control'}
                        type='password'
                        name='password'
                        value={password}
                        id='password'
                        placeholder='Contraseña'
                        onChange={this.handleChange}
                      />
                      <i className='fa fa-lock' aria-hidden='true' />
                      <span className='error-block'>{errors.password}</span>
                    </div>
                    <div className='mt-5'>
                      <button className='btn btn-block btn-primary btn-lg font-weight-medium' type='submit' > Iniciar sesión </button>
                    </div>
                    <div className='row'>
                      <div className='col-md-6 col-sm-6'>
                        <div className='checkbox mt-3'>
                          <label>
                            <input
                              type='checkbox'
                              name='rememberMe'
                              checked={this.state.rememberme ? true : false}
                              onChange={() => this.setState({ rememberme: !this.state.rememberme })}
                            />
                            <span />
                            Recuérdame
                            <i className='input-helper' />
                          </label>
                        </div>
                      </div>
                      <div className='col-md-6 col-sm-6'>
                        <div className='mt-3 text-right'>
                          <a className='auth-link text-gray' onClick={() => this.props.history.push('/forgotpassword')} >
                          ¿Se te olvidó tu contraseña?
                          </a>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(null, actions)(AdminLogin)
