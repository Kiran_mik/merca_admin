import React, { Component } from "react";
import Home from "../Home";
import * as actions from "../../actions";
import { connect } from "react-redux";
import _ from "lodash";
import swal from 'sweetalert';
class AdminEmails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: ['signUp', 'productOutOfStock', 'orderReceived', 'orderDelievered', 'orderCancelled'],
      defaultEmails: [
        {
          event: '',
          emailId: ''
        }
      ],
      admin_email: '',
      errors: {
        admin_email: '',
        event: '',
        emailId: ''
      }
    };
  }


  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (
      permissions && permissions.rolePermission && permissions.rolePermission.emailSettingsAccess &&
      permissions.rolePermission.emailSettingsAccess.viewEmailTemplates === false
    ) {
      this.props.history.push("/dashboard");
    }
    this.getDetails()
  }

  validateForm = () => {
    let { defaultEmails, errors, admin_email } = this.state
    let formIsValid = true
    if (admin_email == '' || admin_email.trim() === '') {
      formIsValid = false
      errors['admin_email'] = '* Admin Email is required'
    } else if (typeof admin_email !== '') {
      var pattern = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
      if (!pattern.test(admin_email)) {
        formIsValid = false
        errors['admin_email'] = '* Ingrese un correo electrónico válido'
      }
    }

    defaultEmails.map(each => {
      var pattern = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      )
      if (!pattern.test(each.emailId)) {
        formIsValid = false
        if (!each.emailId || each.emailId.trim() === '') {
          errors['emailId'] = '* correo electronico es requerido'
        } else
          errors['emailId'] = '* Ingrese un correo electrónico válido'
      }
      if (each.event == '') {
        formIsValid = false
        errors['event'] = '* Event is required'
      }
    })
    this.setState({ errors })
    return formIsValid
  }

  // ####################### Get Details ######################
  getDetails = () => {
    let token = localStorage.getItem('token')
    var url = '/emailSettings/getAdminEmails'
    var method = 'get'
    this.props.commonApiCall(url, method, null, token, null, this.props, response => {
      let { data } = response
      if (data && data.data) {
        this.setState({ admin_email: data.data.adminEmail, defaultEmails: data.data.defaultEmails })
      }
    })
  }
  // ####################### Add Details ######################

  addEmail = () => {
    if (this.validateForm()) {
      var url = '/emailSettings/addAdminEmails'
      var method = 'post'
      var body = {
        adminEmail: this.state.admin_email,
        defaultEmails: this.state.defaultEmails
      }
      var token = localStorage.getItem('token')
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          swal("Email(s) Updated Successfully !", '', 'success')
        }
      })
    }
  }

  plus = () => {
    this.setState(prevState => ({
      defaultEmails: [
        ...prevState.defaultEmails,
        {
          event: '',
          emailId: ''
        }
      ]
    }))
  }


  render() {
    let { options, admin_email, defaultEmails, errors } = this.state
    var pattern = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Correos electrónicos de administrador</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push("/dashboard")} >Página principal</li>
                <li className="breadcrumb-item"> Ajustes del correo electrónico </li>
                <li className="breadcrumb-item active">Correos electrónicos de administrador </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card animated fadeIn">
          <div className="card-body">
            <div className="fadeInUp">
              <form className="form-sample">
                <div className="row">
                  <div className="col-md-6">
                    <div className="row">
                      <div className="col-md-12">
                        <p className="col-form-label d-block"> Correo electrónico de administrador predeterminado </p>
                      </div>
                      <div className="col-md-12">
                        <input
                          className="form-control"
                          type="email"
                          name="admin_email"
                          value={admin_email}
                          placeholder="Default Email"
                          onChange={(e) => this.setState({ admin_email: e.target.value }, () => this.setState({
                            errors: Object.assign(this.state.errors, { admin_email: "" })
                          }))}
                        />
                        <span className="error-block"> {errors.admin_email} </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row mt-3">
                  <div className="col-md-6">
                    <p className="col-form-label d-block"> Seleccionar evento </p>
                  </div>
                  <div className="col-md-6">
                    <p className="col-form-label d-block"> Correo electrónico de administrador</p>
                  </div>
                </div>
                <div >

                  {defaultEmails.map((each, key) => {
                    return (
                      <div className="row" key={key}>
                        <div className="col-md-6" >
                          <select
                            value={each.event}
                            className='form-control'
                            title='event'
                            placeholder='Enter Email'
                            onChange={(event) => { each.event = event.target.value; this.setState({ defaultEmails }) }}
                          >
                            <option>Select</option>
                            {
                              options.map((obj, index) => {
                                return (
                                  <option value={obj} key={index}>{obj}</option>
                                )
                              })}
                          </select>
                          {each.event == '' ? <span className="error-block"> {errors.event} </span> : null}
                        </div>
                        <div className="mb-3 col-md-6" >
                          <div className="input-group ">
                            <input
                              value={each.emailId}
                              className='form-control'
                              title='emailId'
                              placeholder='Enter Email'
                              onChange={(event) => { each.emailId = event.target.value; this.setState({ defaultEmails }) }}
                            />
                            {each.emailId == '' || !pattern.test(each.emailId) ? <span className="error-block"> {errors.emailId} </span> : null}
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
                <div className="row">
                  <div className="col-md-12 text-right button-continer">
                    {defaultEmails.length === 5 ? null :
                      <button type="button" className="btn btn-primary" onClick={this.plus} > <span>Añadir</span></button>}
                    <button type="button" className="btn btn-outline-primary" onClick={this.addEmail}> <span>Guardar</span></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Home>
    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(AdminEmails);
