import React, { Component } from 'react';
import Home from '../Home'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import swal from 'sweetalert'


class SMTPsetting extends Component {
  constructor(props) {
    super(props)
    this.state = {
      smtpHost: '',
      smtpPort: '',
      smtpUser: '',
      smtpPwd: '',
      smtp_id: '',
      fromMail:'',
      errors: {}
    }
  }
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
    if (e.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [e.target.name]: "" })
      });
    }
  }
  componentDidMount() {
    this.getDetails()
  }

  // #################################### Get SMTP ################################
  getDetails = () => {
    let token = localStorage.getItem('token')
    var url = '/emailSettings/getSmtpDetails'
    var method = 'get'
    this.props.commonApiCall(url, method, null, token, null, this.props, response => {
      let { data } = response.data
      if (data) {
        this.setState({
          smtpHost: data.smtpHost,
          smtpPort: data.smtpPort,
          smtpUser: data.smtpUsername,
          smtpPwd: data.smtpPassword,
          fromMail:data.fromMail
        })
      }
    })
  }

  myFunction = () => {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }

  // #################################### Update SMTP ################################
  updateDetails = () => {
    if (this.validateForm()) {
      let { smtpHost, smtpPort, smtpPwd, smtpUser,fromMail, smtp_id } = this.state
      let token = localStorage.getItem('token')
      var url = '/emailSettings/addSmtpSettings'
      var data = { smtpHost, smtpPort, smtpUsername: smtpUser, smtpPassword: smtpPwd,fromMail }
      var body = data
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if(data.status===1){
          swal('Added Successfully', '', 'success')
          this.setState({smtphost:false})
        }
        
      })
    }
  }

  // #################################### Validation ################################
  validateForm = () => {
    let { smtpHost, smtpPort, smtpPwd, smtpUser,fromMail, errors } = this.state
    let formIsValid = true
    if (!smtpUser||smtpUser.trim()==='') {
      formIsValid = false
      errors['smtpUser'] = '* Se requiere nombre de usuario'
    }
    if (!smtpHost||smtpHost.trim()==='') {
      formIsValid = false
      errors['smtpHost'] = '* Se requiere host'
    }
    if (!smtpPort) {
      formIsValid = false
      errors['smtpPort'] = '* Se requiere puerto'
    }
    if (!smtpPwd||smtpPwd.trim()==='') {
      formIsValid = false
      errors['smtpPwd'] = '* se requiere contraseña'
    }
    if (!fromMail||fromMail.trim()==='') {
      formIsValid = false
      errors['fromMail'] = '* se requiere mail'
    }
    this.setState({ errors })
    return formIsValid
  }

  render() {
    let { smtpHost, smtpPort, smtpPwd, smtpUser, smtp_id,fromMail, errors } = this.state
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Configuraciones SMTP</h3>
              <ul className="breadcrumb ">
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')} > Página principal </li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/adminEmails')} >Ajustes del correo electrónico </li>
                <li className="breadcrumb-item active">Configuraciones SMTP</li>
              </ul>
            </div>
          </div>
        </div>
        <div className='card animated fadeIn'>
          <div className='card-body'>
            <div className='form-group row'>
              <label className='col-lg-2 col-sm-3 col-form-label'> SMTP Host <span className='text-danger'>*</span> : </label> 
              <div className='col-md-8 co-sm-7'>
                <input 
                  className='form-control'
                  type='text'
                  name='smtpHost'
                  value={smtpHost}
                  readOnly={this.state.smtphost?false:true}
                  onChange={this.handleChange}
                />
                <span className="error-block"> {errors.smtpHost} </span>
              </div>
              <button type='button' onClick={()=>this.setState({smtphost:!this.state.smtphost})} style={{border:0 ,fontSize:25}}> <i className='fa fa-edit text-primary'  aria-hidden='true' data-toggle="tooltip" title="Edit" /> </button>
            </div>
            <div className='form-group row'>
              <label className='col-lg-2 col-sm-3 col-form-label'> SMTP Port <span className='text-danger'>*</span> : </label>
              <div className='col-md-8 co-sm-7'>
                <input
                  className='form-control'
                  type='number'
                  name='smtpPort'
                  value={smtpPort}
                  readOnly={this.state.smtphost?false:true}
                  onChange={this.handleChange}
                />
                <span className="error-block"> {errors.smtpPort} </span>
              </div>
            </div>
            <div className='form-group row'>
              <label className='col-lg-2 col-sm-3 col-form-label'> Nombre de usuario SMTP <span className='text-danger'>*</span> : </label>
              <div className='col-md-8 co-sm-7'>
                <input
                  className='form-control'
                  type='text'
                  name='smtpUser'
                  value={smtpUser}
                  readOnly={this.state.smtphost?false:true}
                  onChange={this.handleChange}
                />
                <span className="error-block"> {errors.smtpUser} </span>
              </div>
            </div>
            <div className='form-group row '>
              <label className='col-lg-2 col-sm-3 col-form-label'> Contraseña SMTP <span className='text-danger'>*</span> : </label>
              <div className='col-md-8 co-sm-7 input-group'>
                <input
                  className='form-control'
                  type='password'
                  name='smtpPwd'
                  value={smtpPwd}
                  readOnly={this.state.smtphost?false:true}
                  id="myInput"
                  onChange={this.handleChange}
                />
                <span className="input-group-addon"><i className="fa fa-eye col view-icon" onClick={this.myFunction}></i></span>
                <span className="error-block"> {errors.smtpPwd} </span>
              </div>
            </div>
            <div className='form-group row '>
              <label className='col-lg-2 col-sm-3 col-form-label'> From  <span className='text-danger'>*</span> : </label>
              <div className='col-md-8 co-sm-7'>
                <input
                  className='form-control'
                  type='text'
                  name='fromMail'
                  value={fromMail}
                  readOnly={this.state.smtphost?false:true}
                  id="myInput"
                  onChange={this.handleChange}
                />
                <span className="error-block"> {errors.fromMail} </span>
              </div>
            </div>
            <hr />
            <div className='button-continer text-right'>
              <button type='button' className='btn btn-primary' onClick={this.updateDetails} >{smtp_id != '' ? "Guardar" : "Agregar"}</button>
            </div>
          </div>
        </div>
      </Home>
    );
  }
}

export default connect(null, actions)(SMTPsetting);






