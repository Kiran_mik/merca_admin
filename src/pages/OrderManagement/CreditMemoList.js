import React, { Component } from 'react'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css';
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import Pagination from 'rc-pagination';
import { Select } from 'antd'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'antd/dist/antd.css'
import isAfter from "date-fns/isAfter";
import { isEmpty } from "lodash"
import swal from 'sweetalert'
import { API_URL, PDF_URL } from '../../config/configs'
import _ from 'lodash'
import * as actions from '../../actions'
import Home from '../Home'
import moment from 'moment'
import { BeatLoader } from 'react-spinners';
import Loader from '../Loader'
import $ from 'jquery';
import axios from 'axios'
class CreditMemo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      download:false,
      page: 1,
      total: 0,
      pagesize: 10,
      creditMemoList: [],
      creditMemoId: "",
      createdAt: "",
      billToName: "",
      orderId: "",
      orderedDate: "",
      totalAmount: "",
      status: "",
      totalmoney: "",
      sort: {},
      sortData: { creditMemoId: false,transportType:false, createdAt: false, billToName: false, orderedDate: false, orderId: false, totalAmount: false, status: false },
      transport_:true,
      creditMemoId1: true,
      createdAt1: true,
      billToName1: true,
      orderedDate1: true,
      orderId1: true,
      totalAmount1: true,
      status1: true,
      transportType_:'',
      selectedcreditMemoId: [],
      maincreditMemoId: [],
      selectedOrderid: [],
      mainorderId: [],
      selectedBuyername: [],
      mainBuyername: [],
      statusvalue: "",
      startDate: new Date(),
      endDate: new Date(),
      startDateAdded: '',
      endDateAdded: '',
      minTotal: "",
      maxTotal: "",
      //ordered date
      orderstartDate: new Date(),
      orderendDate: new Date(),
      orderstartDateAdded: '',
      orderendDateAdded: '',
      loading: true,
      ordersAccess: {},
      multipleDelete: []
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess && permissions.rolePermission.ordersAccess.viewMemo === false) {
      this.props.history.push('/dashboard')
    }
    if (permissions.rolePermission) {
      let { ordersAccess } = permissions.rolePermission
      this.setState({ ordersAccess: ordersAccess })
    }


    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    this.creditMemosListing('pagesize')
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  // ############################## Memos Listing ######################
  creditMemosListing = (e) => {
    var url = '/creditMemos/creditMemosListing'
    var method = 'post'
    var { page, pagesize, sort, minTotal, maxTotal, selectedcreditMemoId, selectedOrderid, selectedBuyername,selectedBuyername,
      statusvalue, endDateAdded,transportType_, startDateAdded, orderendDateAdded, orderendDateAdded, orderstartDateAdded, selectedcreditMemoId } = this.state
    var { page, pagesize } = this.state
    var data = { }
    if (e==='filter') {
      data.page = 1
    } else if(e==='pagesize') {
      data=data
    }else{
      data.page = page
    }
    if(e==='pagesize'){
      data=data
    }else{
      data.pagesize=pagesize
    }
    var body = data
    var refund = {}
    var creditMemoDate = {}
    var orderedDate = {}
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (selectedcreditMemoId != '') {
      data.creditMemoId = selectedcreditMemoId
    }
    if(transportType_!=''){
      data.transportType=transportType_
    }
    if (selectedOrderid != '') {
      data.orderId = selectedOrderid
    }
    if (selectedBuyername != '') {
      data.billToName = selectedBuyername
    }
    if (minTotal != '') {
      refund.minTotal = JSON.parse(minTotal)
    }
    if (minTotal == '') {
      refund.minTotal = null
    }
    if (maxTotal != '') {
      refund.maxTotal = JSON.parse(maxTotal)
    }
    if (maxTotal == '') {
      refund.maxTotal = null
    }



    //orderdate
    if (orderstartDateAdded != '') {
      orderedDate.startDate = moment(orderstartDateAdded).format("MM/DD/YYYY")
    }
    if (orderstartDateAdded == '') {
      orderedDate.startDate = ''
    }
    if (orderendDateAdded != '') {
      orderedDate.endDate = moment(orderendDateAdded).format("MM/DD/YYYY")
    }
    if (orderendDateAdded == '') {
      orderedDate.endDate = ''
    }
    if (!isEmpty(orderedDate)) {
      data.orderedDate = orderedDate
    }

    if (statusvalue != '') {
      data.status = statusvalue
    }

    if (startDateAdded != '') {
      creditMemoDate.startDate = moment(startDateAdded).format("MM/DD/YYYY")
    }
    if (startDateAdded == '') {
      creditMemoDate.startDate = ''
    }
    if (endDateAdded != '') {
      creditMemoDate.endDate = moment(endDateAdded).format("MM/DD/YYYY")
    }
    if (endDateAdded == '') {
      creditMemoDate.endDate = ''
    }
    if (!isEmpty(creditMemoDate)) {
      data.creditMemoDate = creditMemoDate
    }
    data.orderedDate = orderedDate
    data.creditMemoDate = creditMemoDate
    data.refund = refund
    data.status = statusvalue
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (e == 'filter') {
        this.setState({ page: 1 })
      }
      if (data1.status === 0) {
        this.setState({ creditMemoList: [] })
      }
      this.setState({ creditMemoList: data.creditMemoList, total: data.total, length: data.creditMemoList.length, loading: false })
      if (data.manageCreditMemosListing) {
        this.setState({
          creditMemoId1: data.manageCreditMemosListing.creditMemoId,
          createdAt1: data.manageCreditMemosListing.creditMemoDate,
          billToName1: data.manageCreditMemosListing.billToName,
          transport_:data.manageCreditMemosListing.transportType,
          orderId1: data.manageCreditMemosListing.orderId,
          orderedDate1: data.manageCreditMemosListing.orderedDate,
          totalAmount1: data.manageCreditMemosListing.refund,
          status1: data.manageCreditMemosListing.status,
          pagesize:data.manageCreditMemosListing.pageSize
        })
      }
    }
    )
  }


  //* ***************** Reset Listing ************************//
  resetListing = () => {
    var url = '/creditMemos/creditMemosListing'
    var method = 'post'
    var { page, pagesize, sort } = this.state
    var data = { page: page, pagesize: pagesize }
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (data1.status === 0) {
        this.setState({ creditMemoList: [] })
      }
      this.setState({
        creditMemoList: data.creditMemoList, total: data.total, length: data.creditMemoList.length,
        selectedcreditMemoId: [],
        selectedOrderid: [],
        selectedBuyername: [],
        minTotal: "",
        maxTotal: "",
        transportType_:'',
        startDateAdded: "",
        endDateAdded: "",
        orderstartDateAdded: "",
        orderendDateAdded: "",
        statusvalue: ""
      })
      if (data.manageCreditMemosListing) {
        this.setState({
          creditMemoId1: data.manageCreditMemosListing.creditMemoId,
          createdAt1: data.manageCreditMemosListing.creditMemoDate,
          billToName1: data.manageCreditMemosListing.billToName,
          transport_:data.manageCreditMemosListing.transportType,
          orderId1: data.manageCreditMemosListing.orderId,
          orderedDate1: data.manageCreditMemosListing.orderedDate,
          totalAmount1: data.manageCreditMemosListing.refund,
          status1: data.manageCreditMemosListing.status,

        })
      }
    }
    )
  }

  filtermethod = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: name, searchText: e }
    let url = "/creditMemos/creditMemoFieldsList"
    let method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (response && response.data && response.data.data) {
        if (name === 'creditMemoId') {
          var idArr = _.uniqBy(data, (e) => {
            return e.creditMemoId;
          });
          this.setState({ maincreditMemoId: idArr })
        }
        if (name === 'orderId') {
          var oIdArr = _.uniqBy(data, (e) => {
            return e.orderId;
          });
          this.setState({ mainorderId: oIdArr })
        }
        if (name === 'billToName') {
          var namesArr = _.uniqBy(data, (e) => {
            return e.billToName.toString();
          });
          this.setState({ mainBuyername: namesArr, main_BuyersName: data })
        }
      }
    })
  }




  handleChangeAdded1 = ({ orderstartDate, orderendDate }) => {
    if (orderstartDate === null) {
      orderstartDate = ''
    } else { orderstartDate = orderstartDate || this.state.orderstartDateAdded; }
    if (orderendDate === null) {
      orderendDate = ''
    } else { orderendDate = orderendDate || this.state.orderendDateAdded; }
    if (isAfter(orderstartDate, orderendDate)) {
      orderendDate = orderstartDate;
    }
    this.setState({ orderstartDateAdded: orderstartDate, orderendDateAdded: orderendDate });
  };
  handleChangeStart3 = orderstartDate => this.handleChangeAdded1({ orderstartDate });
  handleChangeEnd3 = orderendDate => this.handleChangeAdded1({ orderendDate });

  gettingUserDetails = (Id) => {
    var body = { Id }
    this.props.history.push(`/creditmemoview/${'oId'}/${Id}`)
  }


  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { creditMemoId1, createdAt1, billToName1, orderId1, orderedDate1,transport_, status1, totalAmount1 ,pagesize} = this.state
    var body = { creditMemoId: creditMemoId1, creditMemoDate: createdAt1,transportType:transport_, billToName: billToName1, orderId: orderId1, orderedDate: orderedDate1, status: status1, refund: totalAmount1,pageSize:pagesize }
    let url = "/creditMemos/setFilterForCreditMemos"
    let method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data.status === 1) {
        this.creditMemosListing()
        if(e){
          swal('Actualizado con éxito!', '', 'success')
        }
      }
    })
  }



  //* ********** SORTING ************************//
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState(
          {
            sort: { [element]: value }
          },
          () => {
            this.creditMemosListing()
          }
        )
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }
  handleChangeAdded = ({ startDate, endDate }) => {
    if (startDate === null) {
      startDate = ''
    } else { startDate = startDate || this.state.startDateAdded; }
    if (endDate === null) {
      endDate = ''
    } else { endDate = endDate || this.state.endDateAdded; }
    if (isAfter(startDate, endDate)) {
      endDate = startDate;
    }
    this.setState({ startDateAdded: startDate, endDateAdded: endDate });
  };
  handleChangeStart2 = startDate => this.handleChangeAdded({ startDate });
  handleChangeEnd2 = endDate => this.handleChangeAdded({ endDate });


  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize
    }, () => this.creditMemosListing());
    this.setState({ selectAll: false }, () => this.selectAllcheck())
  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 },()=>this.setTableRows(), () => this.creditMemosListing());
  }
  handleChange_= (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1 ,page_:true},()=>this.setTableRows(), () => this.creditMemosListing());
    } 
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.creditMemosListing());
    // }
  }

  checkArray(_id) {
    let { multipleDelete } = this.state;
    if (multipleDelete.includes(_id)) {
      return true;
    }
    else {
      return false;
    }
  }

  onCheckbox(_id, val) {
    var delarray = this.state.multipleDelete;
    if (!delarray.includes(_id)) {
      delarray.push(_id);
    }
    else {
      delarray.splice(delarray.indexOf(_id), 1);
    }
    if (delarray.length != this.state.creditMemoList.length) {
      this.setState({ checked: false });
    }
    // if (this)
    this.setState({ multipleDelete: delarray })
  }
  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { creditMemoList } = this.state
    if (this.state.selectAll) {
      creditMemoList.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      creditMemoList.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }

  // ########################### download PDF #########################
  downloadPDF(type) {
    let { creditMemoId1, createdAt1, billToName1, orderedDate1, orderId1, totalAmount1, status1, multipleDelete,
      selectedcreditMemoId, selectedOrderid, selectedBuyername, minTotal, maxTotal, orderstartDateAdded, orderendDateAdded, startDateAdded, endDateAdded } = this.state
    let token = localStorage.getItem('token')
    this.setState({download:!this.state.download})
    var ids = []
    if (type === "totalList") {
      var data = { filteredFields: ["creditMemoId", "creditMemoDate", "orderId", "orderedDate", "billToName", "status", "refund"] }
    }
    if (type === "filteredList") {
      var data = { filteredFields: [] }
      if (creditMemoId1) {
        data.filteredFields.push('creditMemoId')
      }
      if (createdAt1) {
        data.filteredFields.push('creditMemoDate')
      }
      if (orderId1) {
        data.filteredFields.push('orderId')
      }
      if (orderedDate1) {
        data.filteredFields.push('orderedDate')
      }
      if (billToName1) {
        data.filteredFields.push('billToName')
      }
      if (status1) {
        data.filteredFields.push('status')
      }
      if (totalAmount1) {
        data.filteredFields.push('refund')
      }
      if (multipleDelete.length > 0) {
        data.ids = multipleDelete
      }
      if (selectedcreditMemoId != '', selectedOrderid != '', selectedBuyername != '', minTotal != '', maxTotal != '', orderstartDateAdded != '', orderendDateAdded != '', startDateAdded != '', endDateAdded != '') {
        this.state.creditMemoList.map(each => {
          ids.push(each._id)
        })
        data.ids = ids
      }
    }
    var body = data
    var url = '/pdf/downloadCreditMemosPdfFile'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var { data } = response.data
      if (data) {
        this.setState({ multipleDelete: [],download:false })
        window.open(PDF_URL + data.filePathAndName, '_blank');
      }
    })
  }


  render() {
    const Option = Select.Option;
    let { createdAt1, ordersAccess, billToName1,transport_, orderId1, orderedDate1,transportType_, status1, totalAmount1, sortData, sort, creditMemoList, total, length, page, pagesize, maincreditMemoId, selectedOrderid, mainBuyername, selectedBuyername, totalmoney, statusvalue, mainorderId, minTotal, maxTotal, selectedcreditMemoId, creditMemoId1, createdAt } = this.state
    const maindata1 = maincreditMemoId.map((each) => { return each.creditMemoId });
    const maindata = maindata1.filter(o => !selectedcreditMemoId.includes(o));

    const mainorderIddata1 = mainorderId.map((each) => { return each.orderId });
    const mainorderIddata = mainorderIddata1.filter(o => !selectedOrderid.includes(o));

    const mainBuyernamedata1 = mainBuyername.map((each) => { return each.billToName });
    const mainBuyernamedata = mainBuyernamedata1.filter(o => !selectedBuyername.includes(o));
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Nota de crédito</h3>
              <ul className="breadcrumb ">
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Home</li>
                <li className='breadcrumb-item'>Sales</li>
                <li className="breadcrumb-item active">Nota de crédito</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
        {this.state.download ?  <Loader/> :null }
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  {ordersAccess.download == true ?
                    <div className="dropdown">
                      <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <span>Herramientas</span>
                      </button>
                      <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a className="dropdown-item" href="#" onClick={this.downloadPDF.bind(this, "totalList")}>Exportar a PDF</a>
                      </div>
                    </div> : null}
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ creditMemoId1: !this.state.creditMemoId1 })} checked={creditMemoId1} /><span></span>ID de CreditMemo</label></li>

              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ createdAt1: !this.state.createdAt1 })} checked={createdAt1} /><span></span>Fecha de nota de crédito</label></li>

              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ orderId1: !this.state.orderId1 })} checked={orderId1} /><span></span>Orden</label></li>

              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ orderedDate1: !this.state.orderedDate1 })} checked={orderedDate1} /><span></span>Fecha de orden</label></li>

              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ billToName1: !this.state.billToName1 })} checked={billToName1} /><span></span>Bill a nombre</label></li>

              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ status1: !this.state.status1 })} checked={status1} /><span></span>Estado</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ transport_: !this.state.transport_ })} checked={transport_} /><span></span>Tipo de transporte</label></li>

              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ totalAmount1: !this.state.totalAmount1 })} checked={totalAmount1} /><span></span>Reembolso</label></li>
            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.creditMemosListing} >
            Reiniciar
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({
              creditMemoId1: true, createdAt1: true, billToName1: true, orderId1: true, orderedDate1: true, page: true, totalAmount1: true,transport_:true,

              status1: true
            })}>
              Seleccionar todo
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={()=>this.setTableRows('rows')}>
            Guardar
            </button>

          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>ID de la nota de crédito</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese la identificación de la nota de crédito"
                    value={selectedcreditMemoId}
                    onChange={(selectedcreditMemoId) => this.setState({ selectedcreditMemoId })}
                    onSearch={(e) => this.filtermethod(e, 'creditMemoId')}
                    onFocus={(e) => this.filtermethod(e, 'creditMemoId')}
                    style={{ width: '100%' }}
                  >
                    {maindata.map(order => (
                      <Select.Option key={order} value={order}>
                        {order}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Notas de crédito de</label>
                  <DatePicker
                    placeholderText="Notas de crédito de"
                    className="form-control"
                    selected={this.state.startDateAdded}
                    startDate={this.state.startDateAdded}
                    endDate={this.state.endDateAdded}
                    onChange={this.handleChangeStart2}
                  />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Notas de crédito a</label>
                  <DatePicker
                    placeholderText="Notas de crédito a"
                    className="form-control"
                    selected={this.state.endDateAdded}
                    selectsEnd
                    startDate={this.state.startDateAdded}
                    endDate={this.state.endDateAdded}
                    onChange={this.handleChangeEnd2}
                  />
                </div>
              </div>


              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Órdenes de</label>
                  <DatePicker
                    placeholderText="Órdenes de"
                    className="form-control"
                    selected={this.state.orderstartDateAdded}
                    startDate={this.state.orderstartDateAdded}
                    endDate={this.state.orderendDateAdded}
                    onChange={this.handleChangeStart3}
                  />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Órdenes a</label>
                  <DatePicker
                    placeholderText="Órdenes a"
                    className="form-control"
                    selected={this.state.orderendDateAdded}
                    selectsEnd
                    startDate={this.state.orderstartDateAdded}
                    endDate={this.state.orderendDateAdded}
                    onChange={this.handleChangeEnd3}
                  />
                </div>
              </div>


              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Solicitar ID</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingresar ID de pedido"
                    value={selectedOrderid}
                    onChange={(selectedOrderid) => this.setState({ selectedOrderid })}
                    onSearch={(e) => this.filtermethod(e, 'orderId')}
                    onFocus={(e) => this.filtermethod(e, 'orderId')}
                    style={{ width: '100%' }}
                  >
                    {mainorderIddata.map(order => (
                      <Select.Option key={order} value={order}>
                        {order}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre del comprador</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre del comprador"
                    value={selectedBuyername}
                    onChange={(val) => this.setState({ selectedBuyername: val })}
                    onSearch={(e) => this.filtermethod(e, 'billToName')}
                    onFocus={(e) => this.filtermethod(e, 'billToName')}
                    style={{ width: '100%' }}
                  >
                    {mainBuyernamedata.map(each => (
                      <Select.Option key={each} value={each}>
                        {each}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>

              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>cantidad devuelta</label>
                  <div className="row">
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="minTotal" placeholder="Reembolso mínimo" id="min_Total" required="" value={minTotal} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="maxTotal" placeholder="Reembolso máximo" id="max_Total" required="" value={maxTotal} onChange={this.handleChange} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group" >
                  <label> Satus</label>
                  <select className="form-control" onChange={(e) => this.setState({ statusvalue: e.target.value })} value={statusvalue}>
                    <option value=''>Seleccionar</option>
                    <option value='pending'>pendiente</option>
                    <option value='refunded'>reembolsados</option>
                    <option value='cancelled'>Cancelado</option>
                  </select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group" >
                  <label>Tipo de transporte</label>
                  <select className="form-control"  name={transportType_} onChange={(e) => this.setState({ transportType_: e.target.value })} value={transportType_}>
                    <option value=''>Seleccionar</option>
                    <option value='Ship To Warehouse'>Enviar al almacén</option>
                    <option value='Ship To Home'>Enviar a Página principal</option>
                  </select>
                </div>
              </div>

            </div>
            <hr />
            <div className="pull-right filter-button">
              <div className="dropdown ml-2">
                <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                  <span>Herramientas</span>
                </button>
                <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                  <a className="dropdown-item" href="#" onClick={this.downloadPDF.bind(this, "filteredList")}>Exportar a PDF</a>
                </div>
              </div>
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.creditMemosListing('filter')} >
              Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetListing}  >
              Reiniciar
            </button>
            </div>
          </div>
          <div className="card-body">
            <div className="table-responsive">
              <table className="table dataTable with-image row-border hover custom-table table-striped" >
                <thead>
                  <tr>
                    <th>
                      <div className='checkbox'>
                        <label>
                          <input type="checkbox" className="form-check-input" checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} />
                          <span />
                          <i className='input-helper' />
                        </label>
                      </div>
                    </th>
                    {creditMemoId1 ? <th>ID de la nota de crédito</th> : null}
                    {createdAt1 ? <th sortable-column="createdAt" onClick={this.onSort.bind(this, 'createdAt')} >
                    Fecha de nota de crédito
                      <i aria-hidden='true' className={(sortData['createdAt']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {orderId1 ? <th sortable-column="orderId" onClick={this.onSort.bind(this, 'orderId')} >
                    Solicitar ID
                      <i aria-hidden='true' className={(sortData['orderId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {orderedDate1 ? <th sortable-column="orderedDate" onClick={this.onSort.bind(this, 'orderedDate')}>
                    Fecha de orden
                      <i aria-hidden='true' className={(sortData['orderedDate']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {billToName1 ? <th sortable-column="billToName" onClick={this.onSort.bind(this, 'billToName')}>
                    Nombre del comprador
                      <i aria-hidden='true' className={(sortData['billToName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {transport_ ? <th sortable-column="transportType" onClick={this.onSort.bind(this, 'transportType')}>
                    Tipo de transporte
                      <i aria-hidden='true' className={(sortData['transportType']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {status1 ? <th sortable-column="status" onClick={this.onSort.bind(this, 'status')}>
                    Estado
                    <i aria-hidden='true' className={(sortData['status']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {totalAmount1 ? <th sortable-column="totalAmount" onClick={this.onSort.bind(this, 'totalAmount')}>
                    Reembolso
                    <i aria-hidden='true' className={(sortData['totalAmount']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    <th> Comportamiento</th>
                  </tr>
                </thead>
                {
                  creditMemoList.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                    <tbody>
                      {creditMemoList.map((order, Key) => {
                        return (
                          <tr key={order._id} key={Key} >
                            <td>
                              <div className='checkbox'>
                                <label>
                                  <input type="checkbox" className="form-check-input" checked={(this.checkArray(order._id)) ? true : false} onChange={() => this.onCheckbox(order._id)} />
                                  <span />
                                  <i className='input-helper' />
                                </label>
                              </div>
                            </td>
                            {creditMemoId1 ? <td>{order.creditMemoId}</td> : null}
                            {createdAt1 ? <td>{moment(order.createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td> : null}
                            {orderId1 ? <td>{order.orderId}</td> : null}
                            {orderedDate1 ? <td>{moment(order.orderedDate.slice(0, 10)).format("DD/MM/YYYY")}</td> : null}
                            {billToName1 ? <td>{order.billToName}</td> : null}
                            {transport_ ?<td>{order.transportType}</td> : null}
                            {status1 ? <td>{order.status}</td> : null}
                            {totalAmount1 ? <td>{order.totalAmount ? order.totalAmount.toFixed(2) : "-"}</td> : null}
                            <td>
                              <button onClick={() => this.gettingUserDetails(order._id)}><i className="fa fa-eye text-primary" aria-hidden="true"></i></button>
                            </td>
                          </tr>
                        )
                      })}
                    </tbody>}
              </table>
            </div>
            {
              this.state.creditMemoList.length > 0 ?
                <div className="table-footer text-right">
                  <label>Demostración</label>
                  <Select showSearch placeholder={<b> {total <= 10 ? total : length}</b>} optionFilterProp="children"
                    onSelect={this.handleChangePageSize.bind(this)}
                    value={this.state.page_ ? pagesize : (length <=pagesize ?length:pagesize)}
                    onSearch={this.handleChange_}>
                    <Option value={10}>10</Option>
                    <Option value={25}>25</Option>
                    <Option value={50}>50</Option>
                    <Option value={75}>75</Option>
                  </Select>
                  <label>Fuera de {total} Notas de crédito</label>
                  <div className="pagination-list">
                    <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                  </div>
                </div> : null}
          </div>
        </div>
      </Home >
    );
  }
}



const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(CreditMemo)

