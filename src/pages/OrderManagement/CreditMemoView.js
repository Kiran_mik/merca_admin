import React, { Component } from 'react'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css'
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import 'antd/dist/antd.css'
import swal from 'sweetalert'
import * as actions from '../../actions'
import moment from 'moment'
import Home from '../Home'
import $ from 'jquery';
class CreditMemoView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 5,
      billingAddress: {},
      shippingAddress: {},
      orderedProducts: [],
      adjustmentFee: '',
      adjustmentRefund: '',
      refundShipping: '',
      totalAmount: '',
      buyerName: "",
      createdAt: "",
      customerGroup: "",
      emailId: "",
      ipAddress: "",
      orderId: "",
      orderStatus: "",
      paymentType: "",
      shippingMethod: '',
      shippingTrackingUrl: '',
      comments: "",
      status: '',
      emailToCustomer: true,
      creditmemoid: "",
      order_id: "",
      ord_id: "",
      transportType_: '',
      options: '',
      paymentDetails: ''
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess && permissions.rolePermission.ordersAccess.viewMemo === false) {
      this.props.history.push('/dashboard')
    }

    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    var creditmemoid = this.props.match.params.cId;
    var order_id = this.props.match.params.oId;
    this.setState({ creditmemoid: creditmemoid, order_id: order_id }, () => this.getDetails());
  }

  handleComment = (event) => {
    this.setState({
      comments: event.target.value,
      error_comm: false
    });
  }
  submitComment = (e) => {
    var { comments, emailToCustomer, orderId } = this.state
    if (comments != '') {
      var url = '/comments/addComments'
      var method = 'post'
      var data = { type: "orderPlaced", comments: comments, emailToCustomer, id: orderId, orderId: this.state.ord_id }
      var body = data
      var token = localStorage.getItem('token')
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let data1 = response.data
        let data = response.data.data
        if (data) {
          swal('Comentario agregado con éxito !', '', 'success')
        }
      })
    } else {
      this.setState({ error_comm: true })
    }
  }
  sendEmail = (e) => {
    var { creditmemoid } = this.state
    var data = { creditMemoId: creditmemoid }
    var body = data
    var token = localStorage.getItem('token')
    let url = "/creditMemos/sendMailCreditMemos"
    let method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data.statusCode === 1) {
        swal('Correo electrónico enviado con éxito', '', 'success')
      } else {
        swal(data.statusCode, 'warning')
      }
    })
      .catch(err => console.log(err))
  }
  getDetails = () => {
    if (this.state.creditmemoid) {
      var body = { creditMemoId: this.state.creditmemoid }
      let token = localStorage.getItem('token')
      var url = '/creditMemos/getCreditMemosDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let data = response.data.data
        if (data.orderDetails.length > 0) {
          let firstName = data.orderDetails[0].firstName;
          let lastName = data.orderDetails[0].lastName;
          var buyerName = firstName + lastName
          if (buyerName) {
            this.setState({ buyerName: buyerName })
          }
        }

        if (data.orderDetails.length > 0) {
          this.setState({
            createdAt: data.orderDetails[0].orderedDate,
            customerGroup: data.orderDetails[0].customerGroup,
            emailId: data.orderDetails[0].emailId,
            ipAddress: data.orderDetails[0].ipAddress,
            orderId: data.orderDetails[0].orderId,
            orderStatus: data.orderDetails[0].orderStatus,
            shippingTrackingUrl: data.orderDetails[0].shippingTrackingUrl,
            shippingMethod: data.orderDetails[0].shippingMethod,
            billingAddress: data.orderDetails[0].billingAddress,
            shippingAddress: data.orderDetails[0].shippingAddress,
            paymentType: data.orderDetails[0].paymentType,
            ord_id: data.orderDetails[0].ord_id,
            transportType_: data.orderDetails[0].transportType
          });
        }
        if (data.orderDetails.length > 0) {
          this.setState({
            options: data.orderDetails[0].options
          })
        }
        if (data.paymentDetails) {
          this.setState({ paymentDetails: data.paymentDetails })
        }
        if (data.orderTotals) {
          this.setState({
            adjustmentFee: data.orderTotals[0].adjustmentFee.toFixed(2),
            adjustmentRefund: data.orderTotals[0].adjustmentRefund.toFixed(2),
            refundShipping: data.orderTotals[0].refundShipping.toFixed(2),
            totalAmount: data.orderTotals[0].totalAmount.toFixed(2)
          })
        }
        if (data.creditMemoProducts) {
          this.setState({ orderedProducts: data.creditMemoProducts });
        }
      })
    }
  }

  render() {
    let { buyerName, order_id, billingAddress, shippingAddress, createdAt, customerGroup, emailId, ipAddress, orderId, orderStatus, transportType_,
      paymentType, paymentDetails, options, shippingMethod, orderedProducts, comments, adjustmentFee, adjustmentRefund, refundShipping, totalAmount, } = this.state
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3> Ver CreditMemo</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Órdenes</li>
                <li className="breadcrumb-item active">CreditMemoView</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 col-lg-12 text-md-right">
                <ul className="d-inline-block p-0 mb-0 ul-list">
                  {order_id != "oId" ?
                    <li onClick={() => this.props.history.push(`/orderview/${order_id}`)}><i className="fa fa-angle-left"></i>atrás</li> :
                    <li onClick={() => this.props.history.push('/creditmemo')}><i className="fa fa-angle-left"></i>atrás</li>}
                  <li onClick={this.sendEmail}>Enviar correo electrónico</li>
                  {/* <li><a className="" href="javascript:void()">Print</a></li> */}
                </ul>
              </div>
            </div>
          </div>
          <div className="card-body">
            <div className="tab-content" id="myTabContent">
              <div className="fade show active tab-pane" id="information" role="tabpanel" aria-labelledby="information">
                <div className="Address-info">
                  <div className="card-header p-0 mb-3">
                    <h5>Información de pedido y crédito</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Orden# {orderId}</h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Fecha de orden</b></td>
                              <td className="text-right">{moment(createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td>
                            </tr>
                            <tr>
                              <td><b>Estado del pedido</b></td>
                              <td className="text-right">{orderStatus}</td>
                            </tr>
                            <tr>
                              <td><b>Colocado desde IP</b></td>
                              <td className="text-right"> {ipAddress}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <h5>Información de la cuenta </h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Nombre del cliente</b></td>
                              <td className="text-right">{buyerName}</td>
                            </tr>
                            <tr>
                              <td><b>Email</b></td>
                              <td className="text-right">{emailId}</td>
                            </tr>
                            <tr>
                              <td><b>grupo de clientes	</b></td>
                              <td className="text-right">{customerGroup}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="Address-info mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Datos del Domicilio</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Dirección de Envio</h5>
                      {billingAddress.name}<br />
                      {billingAddress.addressLine1}<br />
                      {billingAddress.addressLine2}<br />
                      {billingAddress.state}<br />
                      {billingAddress.zipCode}<br />
                      {billingAddress.city}<br />
                      T: {billingAddress.phone}
                    </div>
                    <div className="col-md-6">
                      <h5>Dirección de Envío</h5>
                      {shippingAddress.name}<br />
                      {shippingAddress.addressLine1}<br />
                      {shippingAddress.addressLine2}<br />
                      {shippingAddress.state}<br />
                      {shippingAddress.zipCode}<br />
                      {shippingAddress.city}<br />
                      T:{shippingAddress.phone}
                    </div>
                  </div>

                  <div className="payment_shipping mt-5">
                    <div className="card-header p-0 mb-3">
                      <h5>Pago y método de envío</h5>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <h5>Información del pago</h5>
                        <ul className="comman-list">
                          <li><b>{paymentType}</b></li>
                          {paymentDetails ? <p dangerouslySetInnerHTML={{ __html: `${paymentDetails}` }} /> : null}
                        </ul>
                        {options != '' ? <>
                          <h5>Opción seleccionada</h5>
                          <p>{options}</p></> : null}
                      </div>
                      <div className="col-md-6">
                        <h5>Información de envío y manejo</h5>
                        <p>{shippingMethod ? shippingMethod : '-'}</p>
                        <h5>Lugar de envío</h5>
                        <p>{transportType_ ? transportType_ : '-'}</p>
                      </div>
                    </div>
                  </div>
                  <div className="item-order mt-5">
                    <div className="card-header p-0 mb-3">
                      <h5>Artículos reembolsados</h5>
                    </div>
                    <div className="table-responsive">
                      <table className="table dataTable with-image row-border hover custom-table table-striped" >
                        <thead>
                          <tr>
                            <th>Producto</th>
                            <th>SKU</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Total parcial</th>
                            <th>Importe del impuesto</th>
                            {/* <th>Discount Amount</th> */}
                            <th>Fila total</th>
                          </tr>
                        </thead>
                        <tbody>
                          {orderedProducts.map((order, id) => {
                            return (
                              <tr key={id}>
                                <td>{order.productName}</td>
                                <td>{order.SKU}</td>
                                <td>${order.price.toFixed(2)}</td>
                                <td>{order.quantity}</td>
                                <td>${order.subTotal.toFixed(2)}</td>
                                <td>${order.taxAmount ? order.taxAmount.toFixed(2) : '0.00'}</td>
                                {/* <td>${order.discountAmount.toFixed(2)}</td> */}
                                <td>${order.rawTotal.toFixed(2)}</td>
                              </tr>
                            )
                          })}
                        </tbody>
                      </table>
                      {
                        orderedProducts.lenght == 0 ? <p className="text-danger text-center"> No se encontraron productos</p> : null
                      }
                    </div>
                  </div>
                  <div className="order_total mt-5">
                    <label className="d-block"> <h5>Historia de la nota</h5></label>
                    <div className="card-header p-0 mb-3">
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className='form-group'>
                          <label>Comentario</label>
                          <textarea className='form-control' onChange={this.handleComment} value={comments} name="comments" ></textarea>
                          {this.state.error_comm ? <span className="error-block">* Ingrese comentario</span> : null}

                        </div>
                        <div className='checkbox mt-2'>
                          <label>
                            <input type="checkbox" className="form-check-input" onChange={() => this.setState({ emailToCustomer: !this.state.emailToCustomer })} checked={this.state.emailToCustomer} />
                            <span />
                            <i className='input-helper' />
                            Notificar al cliente por correo electrónico
                        </label>
                        </div>
                        <button className="btn btn-primary mt-2" onClick={this.submitComment}>enviar comentario</button>
                      </div>
                      <div className="col-md-6">
                        <label> <h5>Totales de notas de crédito</h5></label>
                        <table className='table dataTable with-image row-border hover custom-table'>
                          <tbody>
                            <tr className='animated fadeIn'>
                              <td>Gastos de envío y manipulación </td>
                              <td className="text-right">${refundShipping}</td>
                            </tr>
                            <tr className='animated fadeIn'>
                              <td>Reembolso de ajuste	</td>
                              <td className="text-right">${adjustmentRefund}</td>
                            </tr>
                            <tr className='animated fadeIn'>
                              <td>Tarifa de ajuste</td>
                              <td className="text-right">${adjustmentFee}</td>
                            </tr>
                            <tr className='animated fadeIn'>
                              <td><b>Gran total</b></td>
                              <td className="text-right"><b>${totalAmount}</b></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home >
    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(CreditMemoView)


