import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css';
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import Pagination from 'rc-pagination';
import { Select } from 'antd'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'antd/dist/antd.css'
import isAfter from "date-fns/isAfter";
import { isEmpty } from "lodash"
import swal from 'sweetalert'
import { API_URL, PDF_URL } from '../../config/configs'
import * as actions from '../../actions'
import Home from '../Home'
import moment from 'moment'
import Loader from '../Loader'
import _ from "lodash"
import $ from 'jquery';
import { BeatLoader } from 'react-spinners';
class Shipments extends Component {
  constructor(props) {
    super(props)
    this.state = {
      download:false,
      page: 1,
      total: 0,
      length: 10,
      pagesize: 10,
      shipmentsList: [],
      shipmentId: "",
      orderId: "",
      orderedDate: "",
      totalQuantity: "",
      sort: {},
      sortData: { shipmentId: false, createdAt: false, billToName: false, orderedDate: false, orderId: false, shipmentDate: false, totalQuantity: false ,transportType:false},
      shipmentId1: true,
      shipmentDate: true,
      billToName1: true,
      orderId1: true,
      orderedDate1: true,
      totalQuantity1: true,
      whereToship: true,
      ShipMethodvalue:'',

      selectedshpid: [],
      mainshipeId: [],
      startDate: new Date(),
      endDate: new Date(),
      startDateAdded: '',
      endDateAdded: '',
      selectedOrderid: [],
      mainorderId: [],
      minQuantity: "",
      maxQuantity: "",
      selectedBuyername: [],
      mainBuyername: [],
      loading: true,
      orderstartDate: new Date(),
      orderendDate: new Date(),
      orderstartDateAdded: '',
      orderendDateAdded: '',
      ordersAccess: {},
      multipleDelete: [],
      page_:false,
      transportType_:''
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess && permissions.rolePermission.ordersAccess.viewShipment === false) {
      this.props.history.push('/dashboard')
    }
    if (permissions.rolePermission) {
      let { ordersAccess } = permissions.rolePermission
      this.setState({ ordersAccess: ordersAccess })
    }
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    this.shipmentListing('pagesize')
  }
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }


  handleChangeAdded1 = ({ orderstartDate, orderendDate }) => {
    if (orderstartDate === null) {
      orderstartDate = ''
    } else { orderstartDate = orderstartDate || this.state.orderstartDateAdded; }
    if (orderendDate === null) {
      orderendDate = ''
    } else { orderendDate = orderendDate || this.state.orderendDateAdded; }
    if (isAfter(orderstartDate, orderendDate)) {
      orderendDate = orderstartDate;
    }
    this.setState({ orderstartDateAdded: orderstartDate, orderendDateAdded: orderendDate });
  };
  handleChangeStart3 = orderstartDate => this.handleChangeAdded1({ orderstartDate });
  handleChangeEnd3 = orderendDate => this.handleChangeAdded1({ orderendDate });



  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }


  handleChangeAdded = ({ startDate, endDate }) => {
    if (startDate === null) {
      startDate = ''
    } else { startDate = startDate || this.state.startDateAdded; }
    if (endDate === null) {
      endDate = ''
    } else { endDate = endDate || this.state.endDateAdded; }

    if (isAfter(startDate, endDate)) {
      endDate = startDate;
    }
    this.setState({ startDateAdded: startDate, endDateAdded: endDate });
  };
  handleChangeStart2 = startDate => this.handleChangeAdded({ startDate });
  handleChangeEnd2 = endDate => this.handleChangeAdded({ endDate });

  gettingUserDetails = (Id) => {
    var body = { Id }
    this.props.history.push(`/shipmentview/${'oId'}/${Id}`)
  }
  // //* ********** SORTING ************************//
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState(
          {
            sort: { [element]: value }
          },
          () => {
            this.shipmentListing()
          }
        )
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }




  filtermethod = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: name, searchText: e }
    var url = '/shipments/shipmentsFieldsList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var data = response.data.data
      if (response && response.data && response.data.data) {
        if (name === 'shipmentId') {
          var idArr = _.uniqBy(data, (e) => {
            return e.shipmentId;
          });
          this.setState({ mainshipeId: idArr })
        }
        if (name === 'orderId') {
          var oIdArr = _.uniqBy(data, (e) => {
            return e.orderId;
          });
          this.setState({ mainorderId: oIdArr })
        }
        if (name === 'shipToName') {
          var sIdArr = _.uniqBy(data, (e) => {
            return e.shipToName.toString();
          });
          this.setState({ mainBuyername: sIdArr, main_Buyers: data })

        }
      }
    })
  }


  // ############################## Order Listing ######################


  shipmentListing = (e) => {
    var url = '/shipments/shipmentsListing'
    var method = 'post'
    var { page, pagesize, sort, minQuantity, maxQuantity, ShipMethodvalue,transportType_, selectedshpid, selectedOrderid, selectedBuyername, sort, selectedBuyername, buyerName, startDateValid, startDateValid, endDateValid, endDateAdded, startDateAdded, orderstartDate, orderendDate, orderendDateAdded, orderendDateAdded, orderstartDateAdded } = this.state
    var { page, pagesize } = this.state
    var data = {  }
    if (e==='filter') {
      data.page = 1
    } else if(e==='pagesize') {
      data=data
    }else{
      data.page = page
    }
    if(e==='pagesize'){
      data=data
    }else{
      data.pagesize=pagesize
    }
    var body = data
    var totalQuantity = {}
    var shipmentDate = {}
    var orderedDate = {}
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (selectedshpid != '') {
      data.shipmentId = selectedshpid
    }
    if (selectedOrderid != '') {
      data.orderId = selectedOrderid
    }
    if (selectedBuyername != '') {
      data.shipToName = selectedBuyername
    }
    if(transportType_!=''){
      data.transportType=transportType_
    }
   
    if (ShipMethodvalue != '') {
      var paymentMethod = []
      paymentMethod.push(ShipMethodvalue);
      data.transportType = paymentMethod
    }
    if (minQuantity != '') {
      totalQuantity.minQuantity = JSON.parse(minQuantity)
    }
    if (minQuantity == '') {
      totalQuantity.minQuantity = null
    }
    if (maxQuantity != '') {
      totalQuantity.maxQuantity = JSON.parse(maxQuantity)
    }
    if (maxQuantity == '') {
      totalQuantity.maxQuantity = null
    }
    //orderdate
    if (orderstartDateAdded != '') {
      orderedDate.startDate = moment(orderstartDateAdded).format("MM/DD/YYYY")
    }
    if (orderstartDateAdded == '') {
      orderedDate.startDate = ''
    }
    if (orderendDateAdded != '') {
      orderedDate.endDate = moment(orderendDateAdded).format("MM/DD/YYYY")
    }
    if (orderendDateAdded == '') {
      orderedDate.endDate = ''
    }
    if (!isEmpty(orderedDate)) {
      data.orderedDate = orderedDate
    }

    if (startDateAdded != '') {
      shipmentDate.startDate = moment(startDateAdded).format("MM/DD/YYYY")
    }
    if (startDateAdded == '') {
      shipmentDate.startDate = ''
    }
    if (endDateAdded != '') {
      shipmentDate.endDate = moment(endDateAdded).format("MM/DD/YYYY")
    }
    if (endDateAdded == '') {
      shipmentDate.endDate = ''
    }
    if (!isEmpty(shipmentDate)) {
      data.shipmentDate = shipmentDate
    }
    data.orderedDate = orderedDate
    data.shipmentDate = shipmentDate
    data.totalQuantity = totalQuantity
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data = response.data.data
      if (data.status === 0) {
        this.setState({ shipmentsList: [] })
      }
      this.setState({ shipmentsList: data.shipmentsList, total: data.total, length: data.shipmentsList.length, loading: false })
      if (data&&data.shipmentsList.length <= pagesize) {
        this.setState({ page_: false }
        )
      }
      if (data.manageShipmentsListing) {
        this.setState({
          shipmentId1: data.manageShipmentsListing.shipmentId,
          shipmentDate: data.manageShipmentsListing.shipmentDate,
          billToName1: data.manageShipmentsListing.shipToName,
          orderId1: data.manageShipmentsListing.orderId,
          orderedDate: data.manageShipmentsListing.orderedDate,
          totalQuantity1: data.manageShipmentsListing.totalQuantity,
          whereToship:data.manageShipmentsListing.transportType,
          pagesize:data.manageShipmentsListing.pageSize
        })
      }
      if (e == 'filter') {
        this.setState({ page: 1 })
      }
    }
    )
  }
  checkArray(_id) {
    let { multipleDelete } = this.state;
    if (multipleDelete.includes(_id)) {
      return true;
    }
    else {
      return false;
    }
  }

  onCheckbox(_id, val) {
    var delarray = this.state.multipleDelete;
    if (!delarray.includes(_id)) {
      delarray.push(_id);
    }
    else {
      delarray.splice(delarray.indexOf(_id), 1);
    }
    if (delarray.length != this.state.shipmentsList.length) {
      this.setState({ checked: false });
    }
    this.setState({ multipleDelete: delarray })
  }

  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { shipmentsList } = this.state
    if (this.state.selectAll) {
      shipmentsList.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      shipmentsList.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }


  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { shipmentId1, shipmentDate, billToName1, orderId1, orderedDate1, totalQuantity1, whereToship, sortData, sort, orderStatus,pagesize } = this.state
    var body = { shipmentId: shipmentId1, shipmentDate: shipmentDate, shipToName: billToName1, orderId: orderId1, orderedDate: orderedDate1, orderStatus: orderStatus, totalQuantity: totalQuantity1, transportType:whereToship, sortData, sort, orderStatus, sortData, sort,pageSize:pagesize }
    var url = '/shipments/setFilterForShipments'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      if (response) {
        this.shipmentListing()
        if(e){
          swal('Actualizado exitosamente !', '', 'success')
        }
      }
    })
  }



  resetShipmentListing = (e) => {
    var url = '/shipments/shipmentsListing'
    var method = 'post'
    var { page, pagesize, sort, sort } = this.state
    var { page, pagesize } = this.state
    var data = { page: page, pagesize: pagesize }
    var body = data
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data = response.data.data
      if (data.status === 0) {
        this.setState({ shipmentsList: [] })
      }
      this.setState({
        shipmentsList: data.shipmentsList, total: data.total, length: data.shipmentsList.length,
        selectedshpid: [],
        selectedOrderid: [],
        selectedBuyername: [],
        minQuantity: "",
        maxQuantity: "",
        startDateAdded: "",
        endDateAdded: "",
        transportType_:'',
        orderstartDateAdded: "",
        orderendDateAdded: "",
        ShipMethodvalue:''
      })
      if (data.manageShipmentsListing) {
        this.setState({
          shipmentId1: data.manageShipmentsListing.shipmentId,
          shipmentDate: data.manageShipmentsListing.shipmentDate,
          billToName1: data.manageShipmentsListing.shipToName,
          orderId1: data.manageShipmentsListing.orderId,
          orderedDate: data.manageShipmentsListing.orderedDate,
          totalQuantity1: data.manageShipmentsListing.totalQuantity,
          whereToship:data.manageShipmentsListing.whereToShip
        })
      }
    }
    )
  }

  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize,
    }, () => this.shipmentListing());
    this.setState({ selectAll: false }, () => this.selectAllcheck())
  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 },()=>this.setTableRows(), () => this.shipmentListing());
  }
  handleChange_= (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1 ,page_:true},()=>this.setTableRows(), () => this.shipmentListing());
    } 
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.shipmentListing());
    // }
  }
  // ########################### download PDF #########################
  downloadPDF(type) {
    let { shipmentId1, shipmentDate, billToName1, orderId1, orderedDate1, totalQuantity1, whereToship, multipleDelete,
      selectedshpid, selectedOrderid, selectedBuyername, minQuantity, maxQuantity, orderstartDateAdded, orderendDateAdded, startDateAdded, endDateAdded
    } = this.state
    this.setState({download:!this.state.download})
    let token = localStorage.getItem('token')
    var ids = []
    if (type === "totalList") {
      var data = { filteredFields: ["shipmentId", "shipmentDate","orderId","orderedDate","transportType", "shipToName","totalQuantity"] }
    }
    if (type === "filteredList") {
      var data = { filteredFields: [] }
      if (shipmentId1) {
        data.filteredFields.push('shipmentId')
      }
      if (shipmentDate) {
        data.filteredFields.push('shipmentDate')
      }
      if (orderId1) {
        data.filteredFields.push('orderId')
      }
      if (orderedDate1) {
        data.filteredFields.push('orderedDate')
      }
      
      if (whereToship) {
        data.filteredFields.push('transportType')
      }
      if (billToName1) {
        data.filteredFields.push('shipToName')
      }
      if (totalQuantity1) {
        data.filteredFields.push('totalQuantity')
      }
      if (multipleDelete.length > 0) {
        data.ids = multipleDelete
      }

      if (selectedshpid != '' || selectedOrderid != '' || selectedBuyername != '' || minQuantity != '' || maxQuantity != '' || orderstartDateAdded != '' || orderendDateAdded != '' || startDateAdded != '' || endDateAdded != '') {
        this.state.shipmentsList.map(each => {
          ids.push(each._id)
        })
        data.ids = ids
      }
    }
    var body = data
    var url = '/pdf/downloadShipmentsPdfFile'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var { data } = response.data
      if (data) {
        this.setState({download:false,multipleDelete:[]})
        window.open(PDF_URL + data.filePathAndName, '_blank');
      }
    })
  }



  render() {
    const Option = Select.Option;
    let { sortData, sort, ordersAccess, total, length, page, pagesize, selectedInvoiceid,transportType_, whereToship, mainoinvoiceId, selectedOrderid, mainBuyername, selectedBuyername, mainorderId, shipmentsList, minQuantity, maxQuantity, selectedshpid, mainshipeId, shipmentId1, shipmentDate, billToName1, orderId1, orderedDate1, totalQuantity1
    } = this.state
    const mainshipIddata1 = mainshipeId.map((each) => { return each.shipmentId });
    const mainshipIddata = mainshipIddata1.filter(o => !selectedshpid.includes(o));
    const mainorderIddata1 = mainorderId.map((each) => { return each.orderId });
    const mainorderIddata = mainorderIddata1.filter(o => !selectedOrderid.includes(o));
    const mainBuyernamedata1 = mainBuyername.map((each) => { return each.shipToName });
    const mainBuyernamedata = mainBuyernamedata1.filter(o => !selectedBuyername.includes(o));
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Envíos</h3>
              <ul className="breadcrumb ">
                <li className='breadcrumb-item' >Página principal</li>
                <li className='breadcrumb-item'>ventas</li>
                <li className="breadcrumb-item active">Envíos</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
        {this.state.download ?  <Loader/> :null }
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  {ordersAccess.download == true ?
                    <div className="dropdown">
                      <button type="button" className="btn btn-primary" onClick={() => this.props.history.push('/placeOrderByAdmin')}><i className="fa fa-plus" /> <span>Crear nuevo pedido</span></button>
                      <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <span>Herramientas</span>
                      </button>
                      <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a className="dropdown-item" href="#" onClick={this.downloadPDF.bind(this, "totalList")}>Export to PDF</a>
                      </div>
                    </div> : null}
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ shipmentId1: !this.state.shipmentId1 })} checked={shipmentId1} /><span></span>Identificación del envío</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ shipmentDate: !this.state.shipmentDate })} checked={shipmentDate} /><span></span>Fecha de envío</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ orderId1: !this.state.orderId1 })} checked={orderId1} /><span></span>Orden</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ orderedDate1: !this.state.orderedDate1 })} checked={orderedDate1} /><span></span>Fecha de orden</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ billToName1: !this.state.billToName1 })} checked={billToName1} /><span></span>Nombre del comprador</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ whereToship: !this.state.whereToship })} checked={whereToship} /><span></span>Tipo de transporte</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ totalQuantity1: !this.state.totalQuantity1 })} checked={totalQuantity1} /><span></span>Cantidad total</label></li>
            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.shipmentListing} >
            Reiniciar
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({
              shipmentId1: true, shipmentDate: true, createdAt: true, billToName1: true, orderId1: true, orderedDate1: true, page: true, totalQuantity1: true, page: true, whereToship: true
            })}>
             Seleccionar todo
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={()=>this.setTableRows('rows')}>
            Salvar
            </button>
          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Identificación del envío</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingresar ID de envío"
                    value={selectedshpid}
                    onChange={(selectedshpid) => this.setState({ selectedshpid })}
                    onSearch={(e) => this.filtermethod(e, 'shipmentId')}
                    onFocus={(e) => this.filtermethod(e, 'shipmentId')}
                    style={{ width: '100%' }}
                  >
                    {mainshipIddata.map(order => (
                      <Select.Option key={order} value={order}>
                        {order}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>


              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Envíos desde</label>
                  <DatePicker
                    placeholderText="Seleccionar fecha de inicio"
                    className="form-control"
                    selected={this.state.startDateAdded}
                    selectsStart
                    startDate={this.state.startDateAdded}
                    endDate={this.state.endDateAdded}
                    onChange={this.handleChangeStart2}
                  />

                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Envíos a</label>
                  <DatePicker
                    placeholderText="Seleccionar fecha de finalización"
                    className="form-control"
                    selected={this.state.endDateAdded}
                    selectsEnd
                    startDate={this.state.startDateAdded}
                    endDate={this.state.endDateAdded}
                    onChange={this.handleChangeEnd2}
                  />
                </div>
              </div>


              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Órdenes de</label>
                  <DatePicker
                    placeholderText="Seleccionar fecha de inicio"
                    className="form-control"
                    selected={this.state.orderstartDateAdded}
                    startDate={this.state.orderstartDateAdded}
                    endDate={this.state.orderendDateAdded}
                    onChange={this.handleChangeStart3}
                  />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Órdenes a</label>
                  <DatePicker
                    placeholderText="Seleccionar fecha de finalización"
                    className="form-control"
                    selected={this.state.orderendDateAdded}
                    selectsEnd
                    startDate={this.state.orderstartDateAdded}
                    endDate={this.state.orderendDateAdded}
                    onChange={this.handleChangeEnd3}
                  />
                </div>
              </div>

              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Solicitar ID</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingresar ID de pedido"
                    value={selectedOrderid}
                    onChange={(selectedOrderid) => this.setState({ selectedOrderid })}
                    onSearch={(e) => this.filtermethod(e, 'orderId')}
                    onFocus={(e) => this.filtermethod(e, 'orderId')}
                    style={{ width: '100%' }}
                  >
                    {mainorderIddata.map(order => (
                      <Select.Option key={order} value={order}>
                        {order}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>

              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre del comprador</label>
                  <Select
                    mode="multiple"
                    placeholder="Nombre del comprador"
                    value={selectedBuyername}
                    onChange={(val) => this.setState({ selectedBuyername: val })}
                    onSearch={(e) => this.filtermethod(e, 'shipToName')}
                    onFocus={(e) => this.filtermethod(e, 'shipToName')}
                    style={{ width: '100%' }}
                  >
                    {mainBuyernamedata.map(each => (
                      <Select.Option key={each} value={each}>
                        {each}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Dónde enviar</label>
                  <select className="form-control" onChange={(e) => this.setState({ ShipMethodvalue: e.target.value })} value={this.state.ShipMethodvalue}>
                    <option value='' disabled>Seleccionar</option>
                    <option value='Enviar a sucursal'>Enviar a sucursal</option>
                    <option value='Enviar a domicilio'>Enviar a domicilio</option>
                  </select>
                </div>
              </div>

              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Cantidad</label>
                  <div className="row">
                    <div className="col-md-6">
                      <input className="form-control" type="text" name="minQuantity" placeholder="Cantidad mínima" id="min_quantity" required="" value={minQuantity} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-6">
                      <input className="form-control" type="text" name="maxQuantity" placeholder="Cantidad máxima" id="max_quantity" required="" value={maxQuantity} onChange={this.handleChange} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group" >
                  <label>Tipo de transporte</label>
                  <select className="form-control"  name={transportType_} onChange={(e) => this.setState({ transportType_: e.target.value })} value={transportType_}>
                    <option value=''>Seleccionar</option>
                    <option value='Enviar a sucursal'>Enviar a sucursal</option>
                    <option value='Enviar a domicilio'>Enviar a domicilio</option>
                  </select>
                </div>
              </div>

            </div>
            <hr />
            <div className="pull-right filter-button">
              <div className="dropdown ml-2">
                <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                  <span>Herramientas</span>
                </button>
                <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                  <a className="dropdown-item" href="#" onClick={this.downloadPDF.bind(this, "filteredList")}>Exportar a PDF</a>
                </div>
              </div>
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.shipmentListing('filter')} >
              Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetShipmentListing}   >
              Reiniciar
            </button>
            </div>
          </div>

          <div className="card-body">
            <div className="table-responsive">
              <table className="table dataTable with-image row-border hover custom-table table-striped" >
                <thead>
                  <tr>
                    <th>
                      <div className='checkbox'>
                        <label>
                          <input type="checkbox" className="form-check-input" checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} />
                          <span />
                          <i className='input-helper' />
                        </label>
                      </div>
                    </th>
                    {shipmentId1 ? <th sortable-column="shipmentId" onClick={this.onSort.bind(this, 'shipmentId')} >
                    Identificación del envío
                      <i aria-hidden='true' className={(sortData['shipmentId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {shipmentDate ? <th sortable-column="createdAt" onClick={this.onSort.bind(this, 'createdAt')} >
                    Fecha de envío
                      <i aria-hidden='true' className={(sortData['createdAt']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {orderId1 ? <th sortable-column="orderId" onClick={this.onSort.bind(this, 'orderId')} >
                    Solicitar ID
                      <i aria-hidden='true' className={(sortData['orderId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {orderedDate1 ? <th sortable-column="orderedDate" onClick={this.onSort.bind(this, 'orderedDate')}>
                    Fecha de pedido
                      <i aria-hidden='true' className={(sortData['orderedDate']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {billToName1 ? <th sortable-column="billToName" onClick={this.onSort.bind(this, 'billToName')}>
                    Nombre del comprador
                      <i aria-hidden='true' className={(sortData['billToName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {whereToship ? <th sortable-column="transportType" onClick={this.onSort.bind(this, 'transportType')}>
                    Tipo de transporte
                      <i aria-hidden='true' className={(sortData['transportType']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {totalQuantity1 ? <th sortable-column="totalQuantity" onClick={this.onSort.bind(this, 'totalQuantity')}>
                    Calidad Total
                    <i aria-hidden='true' className={(sortData['totalQuantity']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    <th> Comportamiento</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                {shipmentsList.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                  <tbody>
                    {shipmentsList.map((order, Key) => {
                      return (
                        <tr key={order._id} key={Key} >
                          <td>
                            <div className='checkbox'>
                              <label>
                                <input type="checkbox" className="form-check-input" checked={(this.checkArray(order._id)) ? true : false} onChange={() => this.onCheckbox(order._id)} />
                                <span />
                                <i className='input-helper' />
                              </label>
                            </div>
                          </td>
                          {shipmentId1 ? <td>{order.shipmentId}</td> : null}
                          {shipmentDate ? <td>{moment(order.createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td> : null}
                          {orderId1 ? <td>{order.orderId}</td> : null}
                          {orderedDate1 ? <td>{moment(order.orderedDate.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td> : null}
                          {billToName1 ? <td>{order.billToName}</td> : null}
                          {whereToship ?<td>{order.transportType?order.transportType:"-"}</td> : null}
                          {totalQuantity1 ? <td>{order.totalQuantity}</td> : null}
                          <td>
                            <button onClick={() => this.gettingUserDetails(order._id)}><i className="fa fa-eye text-primary" aria-hidden="true"></i></button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>}
              </table>

            </div>
            {this.state.shipmentsList.length > 0 ?
              <div className="table-footer text-right">
                <label>Demostración</label>
                <Select showSearch placeholder={<b> {total <= 10 ? total : length}</b>} optionFilterProp="children"
                  onSelect={this.handleChangePageSize.bind(this)} 
                  value={this.state.page_ ? pagesize : (length <=pagesize ?length:pagesize)}
                  onSearch={this.handleChange_}>
                  <Option value={10}>10</Option>
                  <Option value={25}>25</Option>
                  <Option value={50}>50</Option>
                  <Option value={75}>75</Option>
                </Select>
                <label>Fuera de {total} Envíos</label>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                </div>
              </div> : null}
          </div>
        </div>
      </Home >
    );
  }
}


const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(Shipments)

