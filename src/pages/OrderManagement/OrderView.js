import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css'
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import { Select } from 'antd'
import 'antd/dist/antd.css'
import axios from 'axios'
import swal from 'sweetalert'
import { API_URL, PDF_URL } from '../../config/configs'
import * as actions from '../../actions'
import Home from '../Home'
import moment from 'moment'
import $ from 'jquery';
import { BeatLoader } from 'react-spinners';

// window.jQuery = $;
// window.$ = $;
// global.jQuery = $;
const FileDownload = require('js-file-download')

class OrderView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 5,
      searchText: "",
      OrderInformation: [],
      billingAddress: {},
      shippingAddress: {},
      orderedProducts: [],
      order_totals: {},
      buyerName: "",
      createdAt: "",
      customerGroup: "",
      emailId: "",
      ipAddress: "",
      orderId: "",
      orderStatus: "",
      paymentType: "",
      shippingMethod: '',
      comments: "",
      status: '',
      paymentDetails: '',
      emailToCustomer: true,
      forStoreFront: true,
      id: "",
      invoice_list: [],
      memos_list: [],
      shipment_list: [],
      transaction_list: [],
      gettingComments: [],
      iscomment: false,
      iscommentStatus: false,
      status: "",
      emailsent: false,
      hold: true,
      loading: false,
      options: '',
      transportType: '',
      ordersAccess: {
      },
      shipmentButton: true,
      creditMemoButton: true,
      invoiceButton: true,
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess && permissions.rolePermission.ordersAccess.viewDetails === false) {
      this.props.history.push('/dashboard')
    }

    if (permissions.rolePermission) {
      let { ordersAccess } = permissions.rolePermission
      this.setState({ ordersAccess: ordersAccess })
    }
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    var orde_id = this.props.match.params.oId;
    this.setState({ id: orde_id }
      , () => {
        return (
          this.invoiceListing(this.state.id),
          this.shipmentListing(this.state.id),
          this.getDetails(this.state.id),
          this.transactionListing(this.state.id),
          this.creditMemoListing(this.state.id),
          this.gettingOrderComents(this.state.id)
        )
      })
    this.allMethods()
  }

  allMethods = () => {
    this.creditMemoListing();
    this.getDetails();
    this.invoiceListing();
    this.shipmentListing();
    this.transactionListing();
    this.gettingOrderComents();
  }

  // #################################### Get Complete Details ################################
  getDetails = () => {
    var id = this.state.id;
    if (id) {
      var body = { orderId: id, type: "order" }
      let token = localStorage.getItem('token')
      var url = '/order/getOrderDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response.data
        let { orderDetails } = data
        let datadetails = orderDetails.details
        this.setState({
          createdAt: datadetails.createdAt,
          orderStatus: datadetails.orderStatus,
          ipAddress: datadetails.ipAddress,
          customerGroup: datadetails.customerGroup,
          orderId: datadetails.orderId,
          billingAddress: datadetails.billingAddress,
          shippingAddress: datadetails.shippingAddress,
          paymentType: datadetails.paymentType,
          transportType: datadetails.transportType,
          shippingMethod: datadetails.shippingName,
          emailsent: datadetails.emailOrderConfirmation,
          buyerName: datadetails.firstName + " " + datadetails.lastName,
          emailId: datadetails.emailId,
          shipmentButton: data.shipmentButton,
          creditMemoButton: data.creditMemoButton,
          invoiceButton: data.invoiceButton
        })
        if (datadetails.options) {
          this.setState({ options: datadetails.options })
        }
        if (orderDetails.paymentDetails) {
          this.setState({ paymentDetails: orderDetails.paymentDetails })
        }
        if (data.orderedProducts) {
          this.setState({ orderedProducts: data.orderedProducts })
        }
        if (data.orderTotals) {
          this.setState({
            order_totals: data.orderTotals
          })
        }
      })
    }
  }
  submitComment = () => {
    let { status, comments, emailToCustomer, id } = this.state
    if (comments != "" || undefined) {
      // var data = { type: "orderPlaced", comments, orderId: id, emailToCustomer, subStatus: status, }
      var data = { type: "orderPlaced", comments, orderId: id, emailToCustomer }
      if (status != '') {
        data.subStatus = status
      }
      var body = data
      var token = localStorage.getItem('token')
      var url = '/comments/addComments'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data.status == 1) {
          this.getDetails()
          swal('Comentario enviado con éxito!', '', 'success')
        }
      })
        .catch(err => console.log(err))
    }
    else {
      if (status == "" || undefined) {
        this.setState({ iscommentStatus: true });
      }
      if (comments == "" || undefined) {
        this.setState({ iscomment: true });
      }
    }
  }



  handleComment = (event) => {
    this.setState({
      comments: event.target.value, iscomment: false
    });
  }

  // #################################### Cancel Order ################################
  cancelOrder = () => {
    var url = '/order/changeOrderStatus'
    var method = 'post'
    var body = { type: "cancelled", orderId: this.state.id }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data = response.data
      if (data.status === 1) {
        swal('Pedido cancelado con éxito!', '', 'success')
        this.allMethods()
      }
    }
    )
  }

  // #################################### Hold Order ################################
  holdOrder = () => {
    let { hold } = this.state
    var url = '/order/changeOrderStatus'
    var method = 'post'
    var data = { orderId: this.state.id }
    if (hold) {
      data.type = "hold"
    } else {
      data.type = "unHold"
    }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data = response.data
      if (data.status === 1) {
        if (hold) {
          swal('La orden está en espera!', '', 'success')
        } else {
          swal('¡La orden está en Unhold!', '', 'success')
        }
        this.setState({ hold: !this.state.hold })
        this.allMethods();
      }
    }
    )
  }

  // #################################### Listing Invoices ################################
  invoiceListing = () => {
    var url = '/invoices/invoicesListingOfOrder'
    var method = 'post'
    var sort = { invoiceId: -1 }
    var data = { orderId: this.props.match.params.oId, sort: sort }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data && data.data) {
        this.setState({ invoice_list: data.data });
      }
    }
    )
  }

  // #################################### Listing Shipments ################################
  shipmentListing = () => {
    var url = '/shipments/shipmentsListingOfOrder'
    var method = 'post'
    var sort = { invoiceId: 1 }
    var data = { orderId: this.props.match.params.oId, sort: sort }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data && data.data) {
        this.setState({ shipment_list: data.data });
      }
    }
    )
  }

  // #################################### Listing Transaction ################################
  transactionListing = () => {
    var url = '/order/transactionsListingOfOrder'
    var method = 'post'
    var sort = { invoiceId: 1 }
    var data = { orderId: this.props.match.params.oId, sort: sort }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data && data.data) {
        this.setState({ transaction_list: data.data });
      }
    }
    )
  }

  // #################################### Listing Comments ################################
  gettingOrderComents = () => {
    var url = '/comments/gettingCommentsOfOrder'
    var method = 'post'
    var sort = { invoiceId: 1 }
    var data = { orderId: this.state.id }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      if (response && data) {
        this.setState({ gettingComments: data });
      }
    }
    )
  }


   // #################################### Resend Email ################################
   resendEmail = () => {
    var url = '/order/resendEmailToCustomer'
    var method = 'post'
    var sort = { invoiceId: 1 }
    var data = { orderId: this.state.id }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data.status===1) {
        swal('Correo electrónico reenviar correctamente','','success')
      }
    }
    )
  }
  // #################################### Listing Memos ################################
  creditMemoListing = (e) => {
    var url = '/creditMemos/creditMemosListingOfOrder'
    var method = 'post'
    var sort = { invoiceId: 1 }
    var data = { orderId: this.props.match.params.oId }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data && data.data) {
        this.setState({ memos_list: data.data });
      }
    }
    )
  }

  // #################################### Invoice Print ################################
  printInvoice = () => {
    var id = this.props.match.params.oId;
    if (id) {
      this.setState({ loading: true })
      var body = { orderId: id }
      let token = localStorage.getItem('token')
      var url = '/pdf/ordersInfoPdf'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        this.setState({ loading: false })
        var data = response.data.data
        // FileDownload(PDF_URL  +  data.filePathAndName, data.filePathAndName);
        window.open(PDF_URL + data.filePathAndName, '_blank');
      })
    }
  }

  render() {
    let { buyerName, ordersAccess, billingAddress, shippingAddress, createdAt, customerGroup, emailId, options, paymentDetails,
      ipAddress, orderId, orderStatus, paymentType, shippingMethod, orderedProducts, transportType, shipmentButton,
      creditMemoButton, invoiceButton, comments, status, emailToCustomer, hold, id, invoice_list, memos_list,
      shipment_list, transaction_list, gettingComments, emailsent, order_totals } = this.state
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3> Vista de orden </h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Órdenes</li>
                <li className="breadcrumb-item active">Vista de orden</li>
              </ul>
            </div>
          </div>
        </div>
        {this.state.loading ?
          <div className="loader-wrapper">
            <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
          </div> : null}

        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 col-lg-12 text-md-right">
                <ul className="d-inline-block p-0 mb-0 ul-list">
                  <li >{orderId}</li>
                  <li className="btn btn-secondary" onClick={() => this.props.history.push("/orderlisting")} ><i className="fa fa-angle-left" ></i><span>Atrás</span></li>
                  {paymentType != 'Stripe' && invoiceButton && ordersAccess.createInvoice == true ?
                    <li className="btn btn-secondary" onClick={() => this.props.history.push(`/newinvoice/${id}`)}>Factura</li> : null}
                  {shipmentButton && ordersAccess.createShipment == true ?
                    <li className="btn btn-secondary" onClick={() => this.props.history.push(`/newshipment/${id}`)}>Envío</li> : null}
                  {orderStatus === "shipped" && ordersAccess.createMemo == true ?
                    <li className="btn btn-secondary" onClick={() => this.props.history.push(`/newcreditmemo/${id}`)}>Nota de crédito</li> : null}
                  {orderStatus === "pending" && ordersAccess.cancel == true ?
                    <li className="btn btn-secondary" onClick={() => this.cancelOrder()}>Cancelar</li> : null}
                  {orderStatus === "processing" || orderStatus === "hold" || orderStatus === "pending" ?
                    <li className="btn btn-secondary" onClick={() => this.holdOrder()}>{hold ? "Hold" : "Unhold"}</li> : null}
                  {/* {orderStatus === "cancelled" || orderStatus === "closed" ? */}
                  <li className="btn btn-secondary" onClick={() => this.props.history.push(`/reorder/${id}`)}>Reordenar</li>
                  {/* : null} */}
                  {orderStatus === "pending" && ordersAccess.editOrder == true ?
                    <li className="btn btn-secondary" onClick={() => this.props.history.push(`/editOrder/${id}`)}>Editar</li> : null}
                  <li className="btn btn-secondary" onClick={this.printInvoice}>Print</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="card-body">
            <ul className="nav nav-tabs" id="myTab" role="tablist">
              <li className="nav-item active">
                <a className="nav-link active" data-toggle="tab" href="#information" role="tab" aria-controls="item" aria-selected="false">información</a>
              </li>
              {ordersAccess.viewInvoice == true ?
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#invoice" role="tab" aria-controls="invoice" aria-selected="false">facturas</a>
                </li> : null}
              {ordersAccess.viewMemo == true ?
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#credit_memo" role="tab" aria-controls="invoice" aria-selected="false">Notas de crédito</a>
                </li> : null}
              {ordersAccess.viewShipment == true ?
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#shipment" role="tab" aria-controls="invoice" aria-selected="false">Envíos</a>
                </li> : null}
              {ordersAccess.viewTransaction == true ?
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#transactions" role="tab" aria-controls="comment" aria-selected="false">Actas</a>
                </li> : null}
              <li className="nav-item" onClick={() => this.gettingOrderComents()}>
                <a className="nav-link" data-toggle="tab" href="#comment" role="tab" aria-controls="comment" aria-selected="false"> Historial de comentarios</a>
              </li>
              <li className="nav-item ml-auto" onClick={() => this.resendEmail()}>
                <a className="nav-link btn-link" href="javascript:;" >Enviar Correo Electrónico </a>
              </li>
              

            </ul> 
            <div className="tab-content" id="myTabContent">
              <div className="fade show active tab-pane" id="information" role="tabpanel" aria-labelledby="information">
                <div className="Address-info">
                  <div className="card-header p-0 mb-3">
                    <h5>Información de pedido y cuenta</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Orden {orderId} </h5> {emailsent ? <h6> (The order confirmation email was sent) </h6> : null}
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Fecha de orden</b></td>
                              <td className="text-right">{moment(createdAt.slice(0, 10)).format("DD/MM/YYYY")}</td>
                            </tr>
                            <tr>
                              <td><b>Estado del pedido</b></td>
                              <td className="text-right">{orderStatus}</td>
                            </tr>
                            <tr>
                              <td><b>Colocado desde IP</b></td>
                              <td className="text-right"> {ipAddress}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <h5>Información de la cuenta </h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Nombre del cliente</b></td>
                              <td className="text-right">{buyerName}</td>
                            </tr>
                            <tr>
                              <td><b>Email</b></td>
                              <td className="text-right">{emailId}</td>
                            </tr>
                            <tr>
                              <td><b>grupo de clientes</b></td>
                              <td className="text-right">{customerGroup}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="Address-info mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Datos del Domicilio</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Dirección de Envio</h5>
                      {billingAddress.name}<br />
                      {billingAddress.addressLine1}<br />
                      {billingAddress.addressLine2}<br />
                      {billingAddress.state}<br />
                      {billingAddress.zipCode}<br />
                      {billingAddress.city}<br />
                      T:{billingAddress.phone}
                    </div>
                    <div className="col-md-6">
                      <h5>Dirección de Envío</h5>
                      {shippingAddress.name}<br />
                      {shippingAddress.addressLine1}<br />
                      {shippingAddress.addressLine2}<br />
                      {shippingAddress.state}<br />
                      {shippingAddress.zipCode}<br />
                      {shippingAddress.city}<br />
                      T:{shippingAddress.phone}
                    </div>
                  </div>
                </div>

                <div className="payment_shipping mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Pago y método de envío</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Información del pago</h5>
                      <ul className="comman-list">
                        <li><b>{paymentType}</b></li>
                        {paymentDetails ? <p dangerouslySetInnerHTML={{ __html: `${paymentDetails}` }} /> : null}
                      </ul>
                      {options != '' ? <>
                        <h5>Opción seleccionada</h5>
                        <p>{options}</p></> : null}
                    </div>
                    <div className="col-md-6">
                      <h5>Información de envío y manejo</h5>
                      <p>{shippingMethod ? shippingMethod : '-'} </p> <br />
                      <h5>Lugar de envío</h5>
                      <p>{transportType ? transportType : '-'}</p>
                    </div>
                  </div>
                </div>
                <div className="item-order mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>artículos ordenados</h5>
                  </div>
                  <div className="table-responsive">
                    <table className="table dataTable with-image row-border hover custom-table table-striped" >
                      <thead>
                        <tr>
                          <th>nombre del producto</th>
                          <th>SKU</th>
                          <th>Precio</th>
                          <th>Precio de venta</th>
                          <th>Cantidad</th>
                          <th>Total parcial</th>
                          <th>Importe del impuesto</th>
                          <th>Fila total</th>
                        </tr>
                      </thead>
                      <tbody>
                        {orderedProducts.map((order, id) => {
                          return (
                            <tr key={id}>
                              <td>{order.productName}</td>
                              <td>{order.SKU}</td>
                              <td>${order.price}</td>
                              <td>${order.salePrice}</td>
                              <td>ordenados:{order.ordered}<br />
                                Facturada:{order.invoiced}<br />
                                Enviada:{order.shipped}<br />
                                Reintegrada:{order.refunded}</td>
                              <td>${order.subTotal}</td>
                              <td>${order.taxAmount}</td>
                              <td>${order.rawTotal.toFixed(2)}</td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </table>
                    {
                      orderedProducts.length == 0 ? <p className="text-center"> No se encontraron productos</p> : null
                    }
                  </div>
                </div>

                <div className="order_total mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Total del pedido</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label className="d-block"> <h5>Notas para esta orden </h5></label>
                      {orderStatus === 'shipped' || orderStatus === 'refunded' ?
                        <div className='form-group'>
                          <label>Status</label>
                          <select className="form-control" onChange={(e) => this.setState({ status: e.target.value, iscommentStatus: false })} value={status} >
                            <option value='' disabled>Seleccionar</option>
                            {orderStatus === 'shipped' || orderStatus === 'refunded' ? <option value="closed">Cerrada</option> : null}
                          </select>
                          {this.state.iscommentStatus ? <span className="error-block">* Seleccionar estado </span> : null}
                        </div> : null}
                      <div className='form-group'>
                        <label>comentario</label>
                        <textarea className='form-control' onChange={this.handleComment} value={comments} name="comments" ></textarea>
                        {this.state.iscomment ? <span className="error-block">* Ingrese comentario</span> : null}
                      </div>
                      <div className='checkbox mt-2'>
                        <label>
                          <input type="checkbox" className="form-check-input" checked={emailToCustomer ? true : false} onChange={() => this.setState({ emailToCustomer: !this.state.emailToCustomer })} />
                          <span />
                          <i className='input-helper' />
                          Notificar al cliente por correo electrónico
                        </label>
                      </div><br />
                      <button className="btn btn-primary mt-2" onClick={this.submitComment}>enviar comentario</button>
                    </div>
                    <div className="col-md-6">
                      <label> <h5>Totales de pedidos</h5></label>
                      <table className='table dataTable with-image row-border hover custom-table'>
                        <tbody>
                          <tr className='animated fadeIn'>
                            <td>Total parcial</td>
                            <td className="text-right">${order_totals.subTotal ? order_totals.subTotal.toFixed(2) : 0.00}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Gastos de envío y manipulación</td>
                            <td className="text-right">${order_totals.shippingCost ? order_totals.shippingCost.toFixed(2) : 0.00}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Descuento	</td>
                            <td className="text-right">${order_totals.discount ? order_totals.discount.toFixed(2) : 0.00}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Impuesto</td>
                            <td className="text-right">${order_totals.taxAmount ? order_totals.taxAmount.toFixed(2) : 0.00}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td><b>Gran total</b></td>
                            <td className="text-right"><b>${order_totals.grandTotal ? order_totals.grandTotal.toFixed(2) : 0.00}</b></td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Comisión Medio de pago</td>
                            <td className="text-right">${order_totals.commission ? order_totals.commission.toFixed(2) : 0.00}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td><b>Total pagado </b></td>
                            <td className="text-right"><b>${order_totals.totalPaid ? order_totals.totalPaid.toFixed(2) : 0.00}</b></td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td><b>Total reembolsado </b></td>
                            <td className="text-right"><b>${order_totals.refunded ? order_totals.refunded.toFixed(2) : 0.00}</b></td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td><b>Total adeudado </b></td>
                            <td className="text-right"><b>${order_totals.TotalDue ? order_totals.TotalDue.toFixed(2) : 0.00}</b></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              {/* {tab2} */}
              <div id="invoice" className="fade tab-pane" role="tabpanel" aria-labelledby="invoice" >
                <div className="table-responsive">
                  <table className="table dataTable with-image row-border hover custom-table table-striped" >
                    <thead>
                      <tr>
                        <th>Factura</th>
                        <th>Fecha de la factura</th>
                        <th>Solicitar</th>
                        <th>Fecha de orden</th>
                        <th>Bill a nombre</th>
                        <th>Estado</th>
                        <th>Cantidad total </th>
                        <th>Comportamiento</th>
                      </tr>
                    </thead>
                    <tbody>
                      {invoice_list.length > 0 && invoice_list.map((order, id) => {
                        return (
                          <tr key={id}>
                            <td>{order.invoiceId}</td>
                            <td>{moment(order.createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td>
                            <td>{order.orderId}</td>
                            <td>{moment(order.orderedDate.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td>
                            <td>{order.billToName}</td>
                            <td>{order.orderStatus}</td>
                            <td>{order.totalQuantity}</td>
                            <td><i className="fa fa-eye text-primary" onClick={() => this.props.history.push(`/invoiceView/${this.state.id}/${order._id}`)}></i></td>
                          </tr>
                        )
                      })}
                    </tbody>
                  </table>
                  {
                    invoice_list.length == 0 ? <p className="text-center">No se encontraron registros</p> : null
                  }
                </div>
              </div>
              {/* {tab3} */}
              <div className="fade tab-pane" id="credit_memo" role="tabpanel" aria-labelledby="credit memo">
                <div className="table-responsive">
                  <table className="table dataTable with-image row-border hover custom-table table-striped" >
                    <thead>
                      <tr>
                        <th>Nota de crédito</th>
                        <th>creado con</th>
                        <th>Orden</th>
                        <th>Fecha de orden</th>
                        <th>Bill a nombre</th>
                        <th>Estado</th>
                        <th>Reembolso</th>
                        <th>Comportamiento</th>
                      </tr>
                    </thead>
                    <tbody>
                      {memos_list.length > 0 && memos_list.map((order, id) => {
                        return (
                          <tr key={id}>
                            <td>{order.creditMemoId}</td>
                            <td>{moment(order.orderedDate.slice(0, 10)).format("DD/MM/YYYY")}</td>
                            <td>{order.orderId}</td>
                            <td>{moment(order.orderedDate.slice(0, 10)).format("DD/MM/YYYY")}</td>
                            <td>{order.billToName}</td>
                            <td>{order.status}</td>
                            <td>{order.totalAmount}</td>
                            <td><i className="fa fa-eye text-primary" onClick={() => this.props.history.push(`/creditmemoview/${this.state.id}/${order._id}`)}></i></td>
                          </tr>
                        )
                      })}
                    </tbody>
                  </table>
                  {memos_list.length == 0 ? <p className="text-center">Datos no encontrados </p> : null}
                </div>
              </div>
              {/* {tab4} */}
              <div className="fade tab-pane" id="shipment" role="tabpanel" aria-labelledby="shipment">
                <div className="table-responsive">
                  <table className="table dataTable with-image row-border hover custom-table table-striped" >
                    <thead>
                      <tr>
                        <th>Envío</th>
                        <th>Fecha de envío </th>
                        <th>Pedido</th>
                        <th>Fecha de orden</th>
                        <th>Enviar a nombre de </th>
                        <th>Cantidad total </th>
                        <th>Comportamiento</th>
                      </tr>
                    </thead>
                    <tbody>
                      {shipment_list.length > 0 && shipment_list.map((order, id) => {
                        return (
                          <tr key={id}>
                            <td>{order.shipmentId}</td>
                            <td>{moment(order.orderedDate.slice(0, 10)).format("DD/MM/YYYY")}</td>
                            <td>{order.orderId}</td>
                            <td>{moment(order.orderedDate.slice(0, 10)).format("DD/MM/YYYY")}</td>
                            <td>{order.shipToName}</td>
                            <td>{order.totalQuantity}</td>
                            <td><i className="fa fa-eye text-primary" onClick={() => this.props.history.push(`/shipmentview/${this.state.id}/${order._id}`)}></i></td>
                          </tr>
                        )
                      })}
                    </tbody>
                  </table>
                  {shipment_list.length == 0 ? <p className="text-center"> Datos no encontrados </p> : null}
                </div>
              </div>
              {/* {tab5} */}
              <div className="fade tab-pane" id="transactions" role="tabpanel" aria-labelledby="transactions">
                <div className="table-responsive">
                  <table className="table dataTable with-image row-border hover custom-table table-striped" >
                    <thead>
                      <tr>
                        <th >pedido </th>
                        <th>ID de transacción </th>
                        <th>Creada </th>
                        <th>Método de pago </th>
                        <th>tipo de transacción </th>
                        <th >Estado </th>
                        <th>Comportamiento</th>
                      </tr>
                    </thead>
                    <tbody>
                      {transaction_list.length > 0 && transaction_list.map((order, id) => {
                        return (
                          <tr key={id}>
                            <td>{order.orderId}</td>
                            <td>{order.transactionId}</td>
                            <td>{moment(order.createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td>
                            <td>{order.paymentType}</td>
                            <td>{order.status}</td>
                            <td> -- </td>
                            <td><i className="fa fa-eye text-primary" onClick={() => this.props.history.push(`/transactionview/${this.state.id}/${order._id}`)}></i></td>
                          </tr>
                        )
                      })}
                    </tbody>
                  </table>
                  {transaction_list.length == 0 ? <p className="text-center"> Datos no encontrados </p> : null}
                </div>
              </div>
              {/* {tab6} */}
              <div className="fade tab-pane" id="comment" role="tabpanel" aria-labelledby="Comment history">
                {gettingComments.length > 0 ? <>
                  {gettingComments.map(each => {
                    return (
                      <>
                        <h5>{each.comments}</h5>
                        <p className="mb-1">{each.commentCreatedBy} || {moment(each.createdAt).format('MMMM Do YYYY, h:mm:ss a')}</p>
                      </>
                    )
                  })} </> : <div className="text-center p-2"><b> No se encontraron comentarios</b></div>}
              </div>
            </div>
          </div>
        </div>
      </Home >
    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(OrderView)


