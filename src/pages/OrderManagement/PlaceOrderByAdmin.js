import React, { Component } from 'react'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css';
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import swal from 'sweetalert'
import * as actions from '../../actions'
import 'antd/dist/antd.css'
import { Select } from 'antd'
import Home from '../Home'
import axios from 'axios'
import { API_URL } from '../../config/configs'
import $ from 'jquery';
import _ from "lodash"

class UsersList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedCategory: [],
      relatedCategoryNames: [],
      relatedProducts: [],
      selectedProduct: [],
      selectedExisting: [],
      selectedExistingName: '',
      selectedExistingId: '',
      productId: "",
      discount: 5,
      orderItemdetails: [],
      qty: 1,
      subtotal: 1,
      paymentMethod: true,
      group: 'general',
      firstName: "",
      lastName: "",
      emailId: "",
      addressLine1Inshipp: "",
      addressLine2Inshipp: "",
      nameInshipp: "",
      cityInshipp: "",
      stateInshipp: "",
      zipCodeInshipp: "",
      phoneInshipp: "",
      shippingId: '',
      trackingUrl: '',
      discount_Id: '',
      discount_Val: "0.00",
      addressLine1Inbilling: "",
      addressLine2Inbilling: "",
      nameInbilling: "",
      cityInbilling: "",
      stateInbilling: "",
      zipCodeInbilling: "",
      phoneInbilling: "",
      subtotalData: [],
      checked: false,
      productsArray: [],
      mainorderId: {},
      _id: '',
      shippingName: '',
      errors: {
      },
      billing_errors: {},
      emailConfirmation: true,
      formIsValid: false,
      selectedCategoryS: [],
      selectedProductS: [],
      shippingMethod: [],
      shipping_method: [],
      cust_err: false,
      wheretoShip: '',
      option_toprint: ''
    }
  }

  componentDidMount() {
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').stop().slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').stop().slideToggle();
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('.product-link').click(function () {
      $('.add_item').slideToggle();
    })
    $('.add-customer-link').click(function () {
      $('.add-customer').slideToggle();
      $('.select-customer').hide();
      $('.default-settings').show();
    });
    $('.select-customer-link').click(function () {
      $('.default-settings').hide();
      $('.select-customer').slideToggle();
      $('.add-customer').hide();
    });
  }

  handleChangeQuantity(event, orderobject) {
    let { orderItemdetails } = this.state;
    orderobject["qty"] = event.target.value
    if (event.target.value > orderobject.stockQuantity) {
      orderobject["err_or"] = `${orderobject.stockQuantity} available`
    } else {
      orderobject["err_or"] = ''
    }
    this.setState({ orderItemdetails }, () => this.discountVal())
  }



  validateForm = () => {
    let {
      addressLine1Inshipp, option_toprint, addressLine2Inshipp, nameInshipp, cityInshipp, stateInshipp, zipCodeInshipp, phoneInshipp, addressLine1Inbilling,
      addressLine2Inbilling, nameInbilling, cityInbilling, stateInbilling, zipCodeInbilling, phoneInbilling, firstName, emailId, lastName, errors, billing_errors } = this.state
    let formIsValid = true
    var mblpattern = new RegExp(/^\d{8,20}$/);
    if (!firstName || firstName.trim() === '') {
      formIsValid = false
      errors['firstName'] = '* Se requiere el primer nombre'
    }
    if (!lastName || lastName.trim() === '') {
      formIsValid = false
      errors['lastName'] = '* Apellido es obligatorio'
    }
    if (!emailId || emailId === "") {
      formIsValid = false
      errors['emailId'] = '* correo electronico es requerido'
    }
    if (typeof emailId !== "undefined") {
      var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      if (!pattern.test(emailId)) {
        formIsValid = false;
        errors["emailId"] = "Dirección de correo electrónico no válida.";
      }
    }
    if (!nameInshipp || nameInshipp.trim() === '') {
      formIsValid = false
      errors['nameInshipp'] = '* Se requiere el nombre'
    }
    if (!addressLine1Inshipp || addressLine1Inshipp.trim() === '') {
      formIsValid = false
      errors['addressLine1Inshipp'] = '* AddressLine 1 es obligatorio'
    }
    // if (!addressLine2Inshipp || addressLine2Inshipp.trim() === '') {
    //   formIsValid = false
    //   errors['addressLine2Inshipp'] = '* AddressLine 2 es obligatorio'
    // }
    if (!cityInshipp || cityInshipp.trim() === '') {
      formIsValid = false
      errors['cityInshipp'] = '* Se requiere ciudad'
    }
    if (stateInshipp === '') {
      formIsValid = false
      errors['stateInshipp'] = '* Se requiere estado'
    }
    if (!zipCodeInshipp) {
      formIsValid = false
      errors['zipCodeInshipp'] = '* Se requiere código postal'
    }
    if (!mblpattern.test(phoneInshipp)) {
      formIsValid = false
      if (!phoneInshipp) {
        errors['phoneInshipp'] = 'Se requiere número de celular'
      } else
        errors['phoneInshipp'] = 'Ingrese un número de teléfono celular válido 8 to 20'
    }
    //billing form
    if (!nameInbilling || nameInbilling.trim() === '') {
      formIsValid = false
      billing_errors['nameInbilling'] = '* Se requiere el nombre'
    }
    if (!addressLine1Inbilling.length) {
      formIsValid = false
      billing_errors['addressLine1Inbilling'] = '* AddressLine 1 es obligatorio'
    }

    // if (!addressLine2Inbilling.length) {
    //   formIsValid = false
    //   billing_errors['addressLine2Inbilling'] = '* AddressLine 2 es obligatorio'
    // }

    if (!cityInbilling || cityInbilling.trim() === '') {
      formIsValid = false
      billing_errors['cityInbilling'] = '* Se requiere ciudad'
    }

    if (!stateInbilling || stateInbilling.trim() === '') {
      formIsValid = false
      billing_errors['stateInbilling'] = '* Se requiere estado'
    }

    if (!zipCodeInbilling) {
      formIsValid = false
      billing_errors['zipCodeInbilling'] = '* Se requiere código postal'
    }
    if (!mblpattern.test(phoneInbilling)) {
      formIsValid = false
      if (!phoneInbilling) {
        billing_errors['phoneInbilling'] = 'Se requiere número de celular'
      } else
        billing_errors['phoneInbilling'] = 'Ingrese un número de teléfono celular válido 8 to 20'
    }
    this.setState({ errors, billing_errors })
    return formIsValid
  }

  // ########################### Shipping Address===Billing Address #########################
  handleAddressChange = (e) => {
    const { checked } = e.target
    if (checked === true) {
      this.setState({
        checked: checked,
        nameInbilling: this.state.nameInshipp,
        addressLine1Inbilling: this.state.addressLine1Inshipp,
        addressLine2Inbilling: this.state.addressLine2Inshipp,
        cityInbilling: this.state.cityInshipp,
        stateInbilling: this.state.stateInshipp,
        zipCodeInbilling: this.state.zipCodeInshipp,
        phoneInbilling: this.state.phoneInshipp,
        billing_errors: {},
        cust_err: false
      })
    }
    else {
      this.setState({
        checked: checked,
        nameInbilling: "",
        addressLine1Inbilling: "",
        addressLine2Inbilling: "",
        cityInbilling: "",
        stateInbilling: "",
        zipCodeInbilling: "",
        phoneInbilling: ""
      })
    }
  }


  // ########################### handleChange  #########################

  handleCategory(val) {
    let idsArray = [];
    this.state.related_Catnames.map((each) => {
      val.map((each1) => {
        if (each.categoryName === each1) {
          return idsArray.push(each._id);
        }
      })
      this.setState({ selectedCategory: val, selectedCategoryS: idsArray })
    })
  };

  handleShipping(val) {
    var idsArray = ''
    var track_url = ''
    this.state.shipping_method.map((each) => {
      if (each.shippingName === val) {
        idsArray = each.shippingName;
        track_url = each.trackingUrl
      }
      this.setState({ shippingName: val, shippingId: idsArray, trackingUrl: track_url })
    })
  }

  handleExestingCustomer(val) {
    var idsArray = ''
    this.state.selected_existing.map((each) => {
      if (each.emailId === val) {
        return idsArray = each._id;
      }
      this.setState({ selectedExistingName: val, selectedExistingId: idsArray, cust_err: false })
    })
  }

  handleChangeShipping = (e) => {
    this.setState({ [e.target.name]: e.target.value })
    // if (e.target.name === 'phoneInshipp') {
    //   var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    //   e.target.value = x[1] + x[2] + x[3];
    //   this.setState({ [e.target.name]: e.target.value ,cust_err:false})
    // }
    if (e.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [e.target.name]: "" })
      });
    }
  }

  handleChangeForm = (e) => {
    this.setState({ [e.target.name]: e.target.value })
    if (e.target.value) {
      this.setState({
        billing_errors: Object.assign(this.state.billing_errors, { [e.target.name]: "" })
      });
    }
  }


  // ########################### Listing Related Category #########################
  relatedCatagory = e => {
    let token = localStorage.getItem('token')
    var body = { searchText: e }
    var url = '/categories/getAllCategories'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var { data } = response.data
      var catArr = _.uniqBy(data, (e) => {
        return e.categoryName.toString();
      });
      this.setState({ relatedCategoryNames: catArr, related_Catnames: data })
    })
  }


  // ########################### Getting discount value #########################
  discountVal = e => {
    let value = this.state.orderItemdetails.map((order, key) => {
      return (order.salePrice * order.qty)
    });
    let orderSubtotalData = _.reduce(value, function (acc, cur) {
      return acc + cur;
    });
    let token = localStorage.getItem('token')
    var body = { amount: orderSubtotalData }
    var url = '/discount/getDiscount'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var { data } = response.data
      if (response && data) {
        this.setState({ discount_Id: data._id, discount_Val: data.discount })
      } else {
        this.setState({ discount_Val: this.state.discount_Val })
      }
    })
  }
  // ########################### Listing Related Product #########################
  relatedProductsArr = e => {
    let token = localStorage.getItem('token')
    var { selectedCategoryS } = this.state
    var data = { type: "productName", searchText: e }
    if (selectedCategoryS.length > 0) {
      data.categoryName = selectedCategoryS
    }
    var body = data
    var url = '/products/productFieldsList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var { data } = response.data
      if (data) {
        var prArr = _.uniqBy(data, (e) => {
          return e.productName.toString();
        });
        this.setState({ relatedProducts: prArr, related_Products: data })
      }
    })
  }

  // ########################### Add Related Product #########################
  addingRelatedProducts = () => {
    var { selectedProductS } = this.state
    let token = localStorage.getItem('token')
    var body = { productsArray: _.uniq(selectedProductS) }
    var url = '/order/productsListing'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      var subtotalData = []
      data.map(each => { subtotalData.push(each.salePrice) })
      this.setState({ subtotalData })
      if (data) {
        var orderItemdetail = this.state.orderItemdetails;
        if (orderItemdetail && Array.isArray(orderItemdetail) && orderItemdetail.length) {
          var arr = [];
          orderItemdetail = _.map(data, (objects, index) => {
            var isobject = _.filter(orderItemdetail, (items) => { if (items._id === objects._id) return items })
            if (isobject && Array.isArray(isobject) && isobject.length > 0) {
              arr.push(...isobject)
            } else {
              objects.qty = 1;
              arr.push(objects)
            }
          })
          orderItemdetail = arr
        }
        else {
          data.map((each, key) => {
            each.qty = each.qty ? each.qty : 1;
            orderItemdetail.push(each)
          })
        }
        this.setState({ orderItemdetails: orderItemdetail, isProduct: true, selectedProduct: [], selectedCategory: [] })
        this.discountVal()
      }
    }
    )
  }

  // ########################### Listing Existing Customers ######################### 
  selectExistingcustomer = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: name, searchText: e }
    var url = '/user/userFieldsList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var { data } = response.data
      if (data) {
        var custArr = _.uniqBy(data, (e) => {
          return e.emailId.toString();
        });
        this.setState({ selectedExisting: custArr, selected_existing: data })
      }
    })
  }

  // ########################### Listing Shipping Methods ######################### 
  selectShipmentMethod = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: name, searchText: e }
    var url = '/order/getTransport'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var { data } = response.data
      if (data) {
        var shArr = _.uniqBy(data, (e) => {
          return e.shippingName.toString();
        });
        this.setState({ shippingMethod: shArr, shipping_method: data })
      }
    })
  }

  // ########################### Get Existing Customer Details ######################### 
  getExCustdetails = (e) => {
    var url = '/order/getCustomerDetails'
    var method = 'post'
    var { selectedExistingId } = this.state
    var data = { customerId: selectedExistingId }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data = response.data.data
      this.setState({
        emailId: data.emailId, firstName: data.firstName, lastName: data.lastName, group: data.group,
        addressLine1Inshipp: data.shippingAddress.addressLine1, addressLine2Inshipp: data.shippingAddress.addressLine2,
        nameInshipp: data.shippingAddress.name, cityInshipp: data.shippingAddress.city,
        stateInshipp: data.shippingAddress.state, zipCodeInshipp: data.shippingAddress.zipCode,
        phoneInshipp: data.shippingAddress.phone,
        errors: {}, billing_errors: {}
      })
    }
    )
  }






  addNewCustmer = () => {
    this.setState({
      selectedExistingName: "", emailId: "", firstName: "", lastName: "", group: "general",
      addressLine1Inshipp: "", addressLine2Inshipp: "", nameInshipp: "", cityInshipp: "", stateInshipp: "",
      checked: false,
      zipCodeInshipp: "", phoneInshipp: "", addressLine1Inbilling: '',
      addressLine2Inbilling: '', nameInbilling: '', cityInbilling: '', stateInbilling: '', zipCodeInbilling: '', phoneInbilling: '', cust_err: false
    })
  }

  delete = (key) => {
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          var orderItemdetails = this.state.orderItemdetails;
          orderItemdetails.splice(key, 1);
          this.setState({ orderItemdetails }, () => {
          })
          var selectedProductS = this.state.selectedProductS;
          selectedProductS.splice(key, 1);
          this.setState({ selectedProductS })
        }
      })
  }

  // ########################## Submit Order ######################### 
  submitOrder = () => {
    if (this.state.orderItemdetails.length === 0) {
      this.setState({ isProduct: true });
    }
    if (this.state.wheretoShip === '') {
      this.state.errors['wheretoShip'] = '* Donde se requiere enviar'
      this.setState({ errors: this.state.errors })
    } else {
      this.state.errors['wheretoShip'] = ''
      this.setState({ errors: this.state.errors })
    }
    if (this.state.option_toprint === '') {
      this.state.errors['option_toprint'] = '* Opción requerida'
      this.setState({ errors: this.state.errors })
    } else {
      this.state.errors['option_toprint'] = ''
      this.setState({ errors: this.state.errors })
    }
    if (this.validateForm() && this.state.wheretoShip != '') {
      let token = localStorage.getItem('token')
      let { selectedExistingId, wheretoShip, option_toprint, addressLine1Inshipp,paymentMethod, emailConfirmation, addressLine2Inshipp,discount_Val, nameInshipp,discount, cityInshipp, stateInshipp, zipCodeInshipp, phoneInshipp, addressLine1Inbilling, shippingId, trackingUrl,
        addressLine2Inbilling, nameInbilling, cityInbilling, stateInbilling, zipCodeInbilling, phoneInbilling, firstName, emailId, lastName, group, productsArray, selectedExistingName } = this.state
      if (paymentMethod) {
        var paymentType = 'Depósito/Transferencia Bancaria'
      } else {
        var paymentType = 'Pago Fácil/Rapipago/CobroExpress'
      }
      var shippingAddress = {};
      shippingAddress.name = nameInshipp
      shippingAddress.addressLine1 = addressLine1Inshipp
      shippingAddress.addressLine2 = addressLine2Inshipp
      shippingAddress.city = cityInshipp
      shippingAddress.state = stateInshipp
      shippingAddress.zipCode = zipCodeInshipp
      shippingAddress.phone = parseInt(phoneInshipp)
      this.setState({ productsArray: [] })
      this.state.orderItemdetails.map((each) => {
        if (each.qty > 0) {
          this.setState({ productsArray: [] })
          var mainorderId = {}
          return (
            mainorderId.productId = each.productId,
            mainorderId.productCount = JSON.parse(each.qty),
            mainorderId.price = each.price,
            mainorderId.salePrice = each.salePrice,
            mainorderId.discount = each.discount,
            mainorderId.supplierName = each.supplierName,
            mainorderId.brandName = each.brandName,
            mainorderId.productThumbnailImage = each.productThumbnailImage,
            mainorderId.productName = each.productName,
            mainorderId.SKU = each.SKU,
            productsArray.push(mainorderId)
          )
        }
      }
      )
      var billingAddress = {};
      billingAddress.name = nameInbilling
      billingAddress.addressLine1 = addressLine1Inbilling
      billingAddress.addressLine2 = addressLine2Inbilling
      billingAddress.city = cityInbilling
      billingAddress.state = stateInbilling
      billingAddress.zipCode = zipCodeInbilling
      billingAddress.phone = parseInt(phoneInbilling)
      if (selectedExistingName) {
        var body = {
          customerId: selectedExistingId, shippingAddress, billingAddress, group: group, paymentType, productsArray: productsArray, transportType: wheretoShip, shippingName: shippingId, trackingUrl, firstName, emailId, lastName, emailOrderConfirmation: emailConfirmation, options: option_toprint,discount: discount_Val,
        }
      } else {
        var body = {
          shippingAddress, billingAddress, group: group, paymentType, productsArray: productsArray, transportType: wheretoShip, shippingName: shippingId, trackingUrl, firstName, emailId, lastName, emailOrderConfirmation: emailConfirmation, options: option_toprint,discount: discount_Val,
        }
      }
      var url = '/order/placeOrderByAdmin'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          swal({
            title: 'Pedido realizado con éxito',
            icon: "success",
            dangerMode: true,
          })
            .then(willDelete => {
              this.props.history.push('/orderlisting')
            });

        } else if (data.statusCode === 437) {
          var respo = data.data + data.message
          swal(respo, '', 'warning')
        }
        else if (data.statusCode === 402) {
          swal('¡El usuario ya existe con el ID de correo electrónico anterior!!', '', 'warning')
        }
        else if (data.statusCode === 458) {
          swal(` Para realizar un pedido, el valor mínimo del carrito debe se ${data.data}`, '', 'warning')
        }
      })
        .catch(err => console.log(err))
    }
    else if (!this.validateForm()) {
      this.setState({ cust_err: true })
    }
  }


  prodChange = (val) => {
    let idsArray = this.state.selectedProductS;
    this.state.related_Products.map((each) => {
      val.map((each1) => {
        if (each.productName === each1) {
          idsArray.push(each._id);
        }
      })
      this.setState({ selectedProductS: _.uniq(idsArray), selectedProduct: val })
    })
  }
  handleChange = (e) => {
    this.setState({ discount_Val: e.target.value })
  }
  render() {
    let value = this.state.orderItemdetails.map((order, key) => {
      return (order.salePrice * order.qty)
    });
    let orderSubtotalData = _.reduce(value, function (acc, cur) {
      return acc + cur;
    });
    let { billing_errors, option_toprint, wheretoShip, relatedProducts, selectedCategory, relatedCategoryNames, selectedProduct, selectedExisting, orderItemdetails, errors, shippingMethod, cust_err,
      addressLine1Inshipp, addressLine2Inshipp, nameInshipp, cityInshipp, stateInshipp, zipCodeInshipp, phoneInshipp, addressLine1Inbilling,
      addressLine2Inbilling, nameInbilling, cityInbilling, stateInbilling, zipCodeInbilling, phoneInbilling, firstName, emailId, lastName, selectedExistingName, paymentMethod, shippingName } = this.state

    const filteredOptions1 = relatedProducts.map((each) => { return each.productName });
    const filteredOptions = filteredOptions1.filter(o => !selectedProduct.includes(o));

    const filteredCatnames1 = relatedCategoryNames.map((each) => { return each.categoryName });
    const filteredCatnames = filteredCatnames1.filter(o => !selectedCategory.includes(o));

    const filteredShipping1 = shippingMethod.map((each) => { return each.shippingName });
    const filteredShipping = filteredShipping1.filter(o => !shippingName.includes(o));

    const filteredCustomers1 = selectedExisting.map((each) => { return each.emailId });
    const filteredCustomers = filteredCustomers1.filter(o => !selectedExistingName.includes(o));
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Crear nuevo pedido</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item'>Precios</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/orderlisting')}>Pedidos</li>
                <li className='breadcrumb-item active'>Crear nuevo pedido</li>
              </ul>
            </div>
          </div>
        </div>
        <div className='card animated fadeIn'>
          <div className="card-header">
            <div className="row data-filter justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  <button type="button" className="btn btn-outline-primary" onClick={() => this.props.history.push('/orderlisting')}> <span>Cancelar</span></button>
                  <button type="button" className="btn btn-success" onClick={this.submitOrder}> <span>Orden de envio</span></button>
                </div>
              </div>
            </div>
          </div>
          <div className='card-body'>
            <div className="item-order float-left w-100 mb-3">
              <div className="card-header p-0 mb-3 d-flex justify-content-between align-items-center">
                <h5>artículos ordenados</h5>
                <button type="button" className="btn btn-primary product-link mb-2" ><i className="fa fa-plus" /> <span>Agregar producto</span></button>
              </div>
              <div className="add_item">
                <div className="form-group row">
                  <div className='col-md-4'>
                    <Select
                      mode="multiple"
                      placeholder="Ingrese el nombre de la categoría"
                      value={selectedCategory}
                      onChange={(value) => this.handleCategory(value)}
                      onSearch={(e) => this.relatedCatagory(e)}
                      onFocus={(e) => this.relatedCatagory(e)}
                      style={{ width: '100%' }}
                    >
                      {filteredCatnames.map(item => (
                        <Select.Option key={item} value={item}>
                          {item}
                        </Select.Option>
                      ))}
                    </Select>
                  </div>
                  <div className='col-md-4'>
                    <Select
                      mode="multiple"
                      placeholder="Ingrese el nombre del producto"
                      value={selectedProduct}
                      onChange={(val) => this.prodChange(val)}
                      onSearch={(e) => this.relatedProductsArr(e)}
                      onFocus={(e) => this.relatedProductsArr(e)}
                      style={{ width: '100%' }}
                    >
                      {filteredOptions.map(each => (
                        <Select.Option key={each} value={each}>
                          {each}
                        </Select.Option>
                      ))}
                    </Select>
                  </div>
                  <div className="col-md-3">
                    <button className="btn btn-primary" onClick={this.addingRelatedProducts}><i className="fa fa-plus"></i><span>Añadir</span></button>
                  </div>
                </div>
              </div>
              <div className='table-responsive mb-3'>
                <table className='table dataTable with-image row-border hover custom-table table-striped'>
                  <thead>
                    <tr>
                      <th>Producto</th>
                      <th>Precio</th>
                      <th>Cantidad</th>
                      <th>Total parcial</th>
                      {/* <th>Discount</th> */}
                      <th>Subtotal de fila</th>
                      <th>Comportamiento</th>
                    </tr>
                  </thead>
                  <tbody>
                    {orderItemdetails.map((order, Key) => {
                      return (
                        <tr className='animated fadeIn' key={Key}>
                          <td>{order.productName} <br />
                            <span style={{ color: 'red' }}>  {order.stockQuantity === 0 ? 'Out of Stock' : (order.err_or ? order.err_or : null)} </span></td>
                          <td>{order.salePrice ? order.salePrice : 0} </td>
                          <td width="7%"><input className="form-control" name="qty" value={order.qty ? order.qty : ''} onChange={(e) => this.handleChangeQuantity(e, order)} type="number" /></td>
                          <td>{order.salePrice && order.qty ? order.salePrice * parseInt(order.qty) : 0}</td>
                          {/* <td>{order.discount ? order.discount * order.qty : 0}</td> */}
                          <td>{order.discount && order.salePrice && order.qty ? (order.salePrice * order.qty - ((0 * order.salePrice * order.qty) / 100)) : order.salePrice && order.qty ? order.salePrice * order.qty : 0}</td>
                          <td><button ><i className="fa fa-trash text-danger" onClick={() => this.delete(Key)}></i></button></td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
              {
                !orderItemdetails.length > 0 && this.state.isProduct ? <p className="text-danger text-center">No hay productos agregados</p> : null
              }
            </div>
            <hr className="dashed-hr clearfix mb-5" />
            <div className="card-header p-0 my-3 d-flex justify-content-between align-items-center">
              <h5>Información del cliente</h5>

              <div className="text-center text-danger">
                {
                  this.state.formIsValid ? <p > Por favor agregue la información del cliente</p> : null
                }

              </div>
              <div className="mb-2">
                <button className="btn btn-primary add-customer-link mr-2" onClick={this.addNewCustmer}>Agregar nuevo cliente </button>
                <button className="btn btn-primary select-customer-link">Seleccionar cliente existente</button>
              </div>
            </div>
            <div className="add-customer">
              <div className="row">
                <div className="col-md-6">
                  <label>Grupo<span className="text-danger">*</span></label>
                  <select className="form-control col-md-12 mr-2"  >
                    <option value="General">General</option>
                  </select>
                </div>

                <div className="col-md-6">
                  <label>Email<span className="text-danger">*</span></label>
                  <input type="email" className="form-control" value={emailId}
                    name='emailId' placeholder="Email" onChange={this.handleChangeShipping} />
                  <span className="error-block"> {errors.emailId} </span>
                </div>

              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className='form-group row'>
                    <label className='col-md-12 text-left'> Primer nombre <span className="text-danger">*</span></label>
                    <div className='col-md-12'>
                      <input
                        className='form-control'
                        type='text'
                        placeholder='Primer nombre'
                        value={firstName}
                        name='firstName'
                        onChange={this.handleChangeShipping}
                      />
                      <span className="error-block"> {errors.firstName} </span>
                    </div>
                  </div>

                </div>
                <div className="col-md-6">
                  <div className='form-group row'>
                    <label className='col-md-12 text-left'>Apellido <span className="text-danger">*</span></label>
                    <div className='col-md-12'>
                      <input
                        className='form-control'
                        type='text'
                        placeholder='Apellido'
                        value={lastName}
                        name='lastName'
                        onChange={this.handleChangeShipping}
                      />
                      <span className="error-block"> {errors.lastName} </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="address-info mt-5">
                <div className="card-header p-0 mb-3">
                  <h5>Datos del Domicilio</h5>
                </div>
                <form className='form-sample row'>
                  <div className="col-md-6">
                    <div className="top-form">
                      <h5>Dirección de Envío</h5>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Nombre<span className="text-danger">*</span></label>
                      <div className='col-md-12'>
                        <input
                          className='form-control'
                          type='text'
                          placeholder='Nombre'
                          value={nameInshipp}
                          onChange={this.handleChangeShipping}
                          name='nameInshipp'
                        />
                        <span className="error-block"> {errors.nameInshipp} </span>
                      </div>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Dirección Línea 1<span className="text-danger">*</span></label>
                      <div className='col-md-12'>
                        <textarea className="form-control" onChange={this.handleChangeShipping} value={addressLine1Inshipp}
                          name='addressLine1Inshipp'></textarea>
                        <span className="error-block"> {errors.addressLine1Inshipp} </span>
                      </div>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Dirección Línea 2</label>
                      <div className='col-md-12'>
                        <textarea className="form-control" onChange={this.handleChangeShipping} value={addressLine2Inshipp}
                          name='addressLine2Inshipp'></textarea>
                        {/* <span className="error-block"> {errors.addressLine2Inshipp} </span> */}
                      </div>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Ciudad<span className="text-danger">*</span></label>
                      <div className='col-md-12'>
                        <input
                          className='form-control'
                          type='text'
                          onChange={this.handleChangeShipping}
                          placeholder='Nombre de la ciudad'
                          value={cityInshipp}
                          name='cityInshipp'
                        />
                        <span className="error-block"> {errors.cityInshipp} </span>
                      </div>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Estado<span className="text-danger">*</span></label>
                      <div className='col-md-12'>
                        <input
                          className='form-control'
                          type='text'
                          placeholder='Nombre Estado / provice'
                          value={stateInshipp}
                          name='stateInshipp'
                          onChange={this.handleChangeShipping}
                        />
                        <span className="error-block"> {errors.stateInshipp} </span>
                      </div>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Código postal</label>
                      <div className='col-md-12'>
                        <input
                          className='form-control'
                          type='number'
                          placeholder='Código postal'
                          value={zipCodeInshipp}
                          name='zipCodeInshipp'
                          onChange={this.handleChangeShipping}
                        />
                        <span className="error-block"> {errors.zipCodeInshipp} </span>
                      </div>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Telefono no<span className="text-danger">*</span></label>
                      <div className='col-md-12'>
                        <input
                          className='form-control'
                          type='number'
                          onChange={this.handleChangeShipping}
                          placeholder='Telefono no'
                          value={phoneInshipp}
                          name='phoneInshipp'
                        />
                      </div>
                      <span className="error-block"> {errors.phoneInshipp} </span>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <h5>Dirección de Envio</h5>
                    <div className='checkbox mt-2'>
                      <label>
                        <input type="checkbox" onChange={e => this.handleAddressChange(e)}
                          checked={this.state.checked ? true : false} className="form-check-input" />
                        <span />
                        <i className='input-helper' />
                        Facturación como dirección de envío
                      </label>
                    </div>
                    <div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Nombre<span className="text-danger">*</span></label>
                        <div className='col-md-12'>
                          <input
                            className='form-control'
                            type='text'
                            placeholder='Nombre'
                            onChange={this.handleChangeForm}
                            value={nameInbilling}
                            name='nameInbilling'
                          />
                          <span className="error-block"> {billing_errors.nameInbilling} </span>
                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'> Dirección Línea 1<span className="text-danger">*</span></label>
                        <div className='col-md-12'>
                          <textarea className="form-control" onChange={this.handleChangeForm} value={addressLine1Inbilling}
                            name='addressLine1Inbilling'></textarea>
                          <span className="error-block"> {billing_errors.addressLine1Inbilling} </span>
                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'> Dirección Línea 2</label>
                        <div className='col-md-12'>
                          <textarea className="form-control" onChange={this.handleChangeForm} value={addressLine2Inbilling}
                            name='addressLine2Inbilling'></textarea>
                          {/* <span className="error-block"> {billing_errors.addressLine2Inbilling} </span> */}
                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Ciudad<span className="text-danger">*</span></label>
                        <div className='col-md-12'>
                          <input
                            className='form-control'
                            type='text'
                            onChange={this.handleChangeForm}
                            placeholder='Nombre de la ciudad'
                            value={cityInbilling}
                            name='cityInbilling'
                          />
                          <span className="error-block"> {billing_errors.cityInbilling} </span>
                        </div>
                      </div>

                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Estado<span className="text-danger">*</span></label>
                        <div className='col-md-12'>
                          <input
                            className='form-control'
                            type='text'
                            onChange={this.handleChangeForm}
                            placeholder='Nombre Estado / provice'
                            value={stateInbilling}
                            name='stateInbilling'
                          />
                          <span className="error-block"> {billing_errors.stateInbilling} </span>
                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Código postal</label>
                        <div className='col-md-12'>
                          <input
                            className='form-control'
                            type='number'
                            onChange={this.handleChangeForm}
                            placeholder='Código postal'
                            value={zipCodeInbilling}
                            name='zipCodeInbilling'
                          />
                          <span className="error-block"> {billing_errors.zipCodeInbilling} </span>
                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Telefono no<span className="text-danger">*</span></label>
                        <div className='col-md-12'>
                          <input
                            className='form-control'
                            type='number'
                            onChange={this.handleChangeForm}
                            placeholder='Telefono no'
                            value={phoneInbilling}
                            name='phoneInbilling'
                          />
                          <span className="error-block"> {billing_errors.phoneInbilling} </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            {cust_err ? <span className="error-block"> Detalles del cliente requeridos </span> : null}
            <div className="default-settings  mt-5 clearfix">
              <div className="payment_ship  ping clearfix">
                <div className="card-header p-0 mb-3">
                  <h5>Información de pago y envío</h5>
                </div>


                <div className="row">
                  <div className="col-md-6">
                    <label>Método de pago</label>
                    <div className="radio vertical">
                      <label >
                        <input type="radio" checked={paymentMethod ? true : false} name="transfer" value={this.state.paymentMethod}
                          onChange={() => this.setState({ paymentMethod: !this.state.paymentMethod })} id="transfer" /><span>
                        </span>Depósito/Transferencia Bancaria</label>
                      <label >
                        <input type="radio" checked={paymentMethod ? false : true} name="transfer" value={this.state.paymentMethod}
                          onChange={() => this.setState({ paymentMethod: !this.state.paymentMethod })} id="transfer" />
                        <span></span>Pago Fácil/Rapipago/CobroExpress</label>
                    </div>
                    <label>En caso de faltante:</label>
                    <div className='col-md-8 col-sm-7'>
                      <input
                        type='text'
                        name={option_toprint}
                        value={option_toprint}
                        className='form-control'
                        placeholder='Ingrese opción'
                        onChange={(e) => this.setState({ option_toprint: e.target.value }, this.setState({
                          errors: Object.assign(this.state.errors, { option_toprint: "" }),
                        }))}
                      />
                      <span className="error-block"> {errors.option_toprint} </span>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <label>Método de envío</label>
                    <div className="form-group">
                      <Select
                        showSearch
                        placeholder="Ingrese el método de envío"
                        value={shippingName}
                        onChange={(val) => this.handleShipping(val)}
                        onSearch={(e) => this.selectShipmentMethod(e, 'shippingName')}
                        onFocus={(e) => this.selectShipmentMethod(e, 'shippingName')}
                        style={{ width: '100%' }}
                        filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      >
                        {filteredShipping.map((each, id) => {
                          return <Select.Option key={each} value={each}>
                            {each}
                          </Select.Option>
                        })}
                      </Select>
                    </div>
                  </div>
                  <div className="col-md-6"></div>
                  <div className="col-md-6">

                    <label>Dónde enviar</label>
                    <select className="form-control" name="wheretoShip" onChange={(e) => this.setState({ wheretoShip: e.target.value }, this.setState({
                      errors: Object.assign(this.state.errors, { [e.target.name]: "" }),
                    }))} value={wheretoShip}>
                      <option value=''>Seleccionar</option>
                      <option value='Enviar a sucursal'>Enviar a sucursal</option>
                      <option value='Enviar a domicilio'>Enviar a domicilio</option>
                    </select>
                    <span className="error-block"> {errors.wheretoShip} </span>

                  </div>
                </div>
              </div>
              <div className="order_total mt-5 clearfix">
                <div className="card-header p-0 mb-3">
                  <h5>Totales de pedidos</h5>
                </div>
                <div className="row">
                  <div className="col-md-6">

                  </div>
                  <div className="col-md-6">
                    <table className='table dataTable with-image row-border hover custom-table table-striped'>
                      <tbody>
                        <tr className='animated fadeIn'>
                          <td>Total parcial</td>
                          <td>$ {orderSubtotalData !== undefined ? orderSubtotalData : "0.00"}</td>
                        </tr>
                        <tr className='animated fadeIn'>
                          <td>Gastos de envío y manipulación</td>
                          <td>$ 0.00</td>
                        </tr>
                        {/* <tr className='animated fadeIn'>
                          <td>Descuento Mercado Pago	</td>
                          <td>$ {this.state.discount_Val && orderSubtotalData ? -((orderSubtotalData * this.state.discount_Val) / 100).toFixed(2) : "0.00"} </td>
                        </tr> */}
                        <tr className='animated fadeIn'>
                          <td>Descuento	</td>
                          {this.state.dis_edit ?
                            <td> <input value={this.state.discount_Val} name='discount_Val' onChange={(e) => this.handleChange(e)} type='number' /> %</td> : <td>   {this.state.discount_Val} %</td>}
                          <button onClick={() => this.setState({ dis_edit: !this.state.dis_edit })} style={{ border: 0, fontSize: 25 }}> <i className='fa fa-edit text-primary' aria-hidden='true' data-toggle="tooltip" title="Edit" /> </button>
                        </tr>
                        <tr className='animated fadeIn'>
                          <td>Costo de financiamiento</td>
                          <td>$ 0.00</td>
                        </tr>
                        <tr className='animated fadeIn'>
                          <td>Impuesto</td>
                          <td>$ 0.00</td>
                        </tr>
                        <tr className='animated fadeIn'>
                          <td><b>Gran total</b></td>
                          <td><b>$ {this.state.discount_Val && orderSubtotalData ? Math.round(orderSubtotalData - (orderSubtotalData * this.state.discount_Val) / 100).toFixed(2) : orderSubtotalData} </b></td>
                        </tr>
                      </tbody>
                    </table>
                    <div className='checkbox mt-2'>
                      <label>
                        <input type="checkbox" className="form-check-input" onChange={() => this.setState({ emailConfirmation: !this.state.emailConfirmation })} checked={this.state.emailConfirmation} />
                        <span />
                        <i className='input-helper' />
                        Confirmación de pedido por correo electrónico
                      </label>
                    </div>
                    <button className="btn btn-primary float-right" onClick={this.submitOrder}>Orden de envio</button>
                  </div>
                </div>
              </div>
            </div>

            <div className="select-customer">
              <div className="form-group row">
                <div className="col-md-3">
                  <Select
                    showSearch
                    placeholder="Enter Customer Name"
                    value={selectedExistingName}
                    onChange={(val) => this.handleExestingCustomer(val)}
                    onSearch={(e) => this.selectExistingcustomer(e, 'emailId')}
                    onFocus={(e) => this.selectExistingcustomer(e, 'emailId')}
                    style={{ width: '100%' }}
                    filterOption={(input, option) =>
                      option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {filteredCustomers.map((each, id) => {
                      return <Select.Option key={each} value={each}>
                        {each}
                      </Select.Option>
                    })}
                  </Select>
                </div >
                <div className="col-md-3">
                  <button className="btn btn-primary add-customer-link " onClick={this.getExCustdetails}><span>Seleccionar</span></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home>
    )
  }
}

const mapStateToProps = state => ({
  getadmindata: state.user.userdatails,
});
export default connect(mapStateToProps, actions)(UsersList)
