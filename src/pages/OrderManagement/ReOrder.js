import React, { Component } from 'react'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css';
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import swal from 'sweetalert'
import * as actions from '../../actions'
import 'antd/dist/antd.css'
import { Select } from 'antd'
import Home from '../Home'
import axios from 'axios'
import { API_URL } from '../../config/configs'
import $ from 'jquery';
var _ = require('lodash')
window.jQuery = $;
window.$ = $;
global.jQuery = $;

class ReOrder extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 5,
      selectedCategory: [],
      relatedCategoryNames: [],
      relatedProducts: [],
      selectedProduct: [],
      selectedCategoryS: [],
      selectedExisting: [],
      selectedExistingName: '',

      selectedExistingShipment: '',
      selectedExistingMethod: [],
      productId: "",
      discount: 5,
      orderItemdetails: [],
      quantity: 1,
      subtotal: 1,
      orderSubtotal: "",
      paymentMethod: true,
      group: '',
      firstName: "",
      lastName: "",
      emailId: "",
      addressLine1Inshipp: "",
      addressLine2Inshipp: "",
      nameInshipp: "",
      cityInshipp: "",
      stateInshipp: "",
      zipCodeInshipp: "",
      phoneInshipp: "",
      billing_errors: {},
      addressLine1Inbilling: "",
      addressLine2Inbilling: "",
      discount_Val: "0.00",
      nameInbilling: "",
      cityInbilling: "",
      stateInbilling: "",
      zipCodeInbilling: "",
      phoneInbilling: "",
      subtotalData: [],
      comments: "",
      checked: true,
      productsArray: [],
      mainorderId: {},
      _id: '',
      shippingName: '',
      errors: {
      },
      isGroupValid: false,
      isComment: false,
      emailComment: true,
      storefrontComment: true,
      emailConfirmation: false,
      orderid: "",
      billingAddress: {},
      ipAddress: "",
      paymentType: "",
      shippingMethod: [],
      selectedProductS: [],
      cust_id: '',
      checkboxVal: true,
      wheretoShip: '',
      option_toprint: ''
    }
  }

  componentDidMount() {
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').stop().slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').stop().slideToggle();
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('.product-link').click(function () {
      $('.add_item').slideToggle();
    })
    $('.add-customer-link').click(function () {
      $('.add-customer').slideToggle();
      $('.select-customer').hide();
      $('.default-settings').show();
    });
    $('.select-customer-link').click(function () {
      $('.default-settings').hide();
      $('.select-customer').slideToggle();
      $('.add-customer').hide();
    });
    var id = this.props.match.params.oId;
    this.setState({ orderid: id });
    this.getDetails();
  }

  // ########################### Shipping Address===Billing Address #########################
  getDetails = () => {
    var id = this.props.match.params.oId;
    var selectedProductS = this.state.selectedProductS
    if (id) {
      var body = { orderId: id, type: "order" }
      let token = localStorage.getItem('token')
      var url = '/order/getOrderDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let data = response.data.data
        let datadetails = data.orderDetails.details
        let dataUserdetails = data.orderDetails
        this.setState({
          firstName: datadetails.firstName,
          lastName: datadetails.lastName,
          createdAt: datadetails.createdAt,
          cust_id: datadetails._id,
          group: datadetails.customerGroup,
          emailId: datadetails.emailId,
          ipAddress: datadetails.ipAddress,
          orderId: datadetails.orderId,
          orderStatus: datadetails.orderStatus,
          shippingTrackingUrl: datadetails.trackingUrl,
          shippingName: datadetails.shippingName,
          paymentMethod: datadetails.paymentType,
          wheretoShip: datadetails.transportType,
          _id: datadetails.transportId,
        })

        if (dataUserdetails) {
          let billingAddress = datadetails.billingAddress;
          let shippingAddress = datadetails.shippingAddress;

          this.setState({
            addressLine1Inshipp: shippingAddress.addressLine1, addressLine2Inshipp: shippingAddress.addressLine2, nameInshipp: shippingAddress.name, cityInshipp: shippingAddress.city, stateInshipp: shippingAddress.state, zipCodeInshipp: shippingAddress.zipCode, phoneInshipp: shippingAddress.phone,
            addressLine1Inbilling: billingAddress.addressLine1, addressLine2Inbilling: billingAddress.addressLine2, nameInbilling: billingAddress.name, cityInbilling: billingAddress.city, stateInbilling: billingAddress.state, zipCodeInbilling: billingAddress.zipCode, phoneInbilling: billingAddress.phone
          })
        }

        if (data.orderTotals) {
          this.setState({ orderItemdetails: data.orderedProducts, discount_Val: data.orderTotals.discountPercentage, })
        }
      })
    }
  }
  handleChangeQuantity(event, orderobject) {
    let { orderItemdetails } = this.state;
    orderobject["quantity"] = event.target.value
    this.setState({ orderItemdetails }, () => this.discountVal())
  }


  delete = (key) => {
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          let { orderItemdetails, selectedProductS } = this.state
          orderItemdetails.splice(key, 1);
          this.setState({ orderItemdetails }, () => {
            orderItemdetails.map(each => {
              selectedProductS.push(each._id)
            })
          })
        }
      })
  }




  handleChangeShipping = (e) => {
    this.setState({ [e.target.name]: e.target.value })
    if (e.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [e.target.name]: "" })
      });
    }
  }
  handleChangeForm = (e) => {
    this.setState({ [e.target.name]: e.target.value })
    if (e.target.value) {
      this.setState({
        billing_errors: Object.assign(this.state.billing_errors, { [e.target.name]: "" })
      });
    }
  }




  validateForm = () => {
    let {
      addressLine1Inshipp,option_toprint, addressLine2Inshipp, nameInshipp, cityInshipp, wheretoShip, stateInshipp, zipCodeInshipp, phoneInshipp, addressLine1Inbilling,
      addressLine2Inbilling, nameInbilling, cityInbilling, stateInbilling, zipCodeInbilling, phoneInbilling, firstName, emailId, lastName, errors, billing_errors } = this.state
    var mblpattern = new RegExp(/^\d{8,20}$/);

    let formIsValid = true
    if (!firstName || firstName.trim() === '') {
      formIsValid = false
      errors['firstName'] = '* Se requiere el nombre'
    }
    if (this.state.orderItemdetails.length === 0) {
      this.setState({ isProduct: true });
      formIsValid = false
    }
    if (!lastName || lastName.trim() === '') {
      formIsValid = false
      errors['lastName'] = '* El apellido es obligatorio'
    }
    if (!emailId || emailId === "") {
      formIsValid = false
      errors['emailId'] = '* Correo electronico es requerido'
    }
    if (!wheretoShip) {
      formIsValid = false
      errors['wheretoShip'] = '* Donde se requiere enviar'
    }
    if (typeof emailId !== "undefined") {
      var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      if (!pattern.test(emailId)) {
        formIsValid = false;
        errors["emailId"] = "Dirección de correo electrónico no válida.";
      }
    }
    if (!nameInshipp || nameInshipp.trim() === '') {
      formIsValid = false
      errors['nameInshipp'] = '* Se requiere el nombre'
    }
    if (!addressLine1Inshipp || addressLine1Inshipp.trim() === '') {
      formIsValid = false
      errors['addressLine1Inshipp'] = '* Se requiere Dirección 1'
    }
    // if (!addressLine2Inshipp||addressLine2Inshipp.trim()==='') {
    //   formIsValid = false
    //   errors['addressLine2Inshipp'] = '* AddressLine 2 is required'
    // }
    if (!cityInshipp || cityInshipp.trim() === '') {
      formIsValid = false
      errors['cityInshipp'] = '* Se requiere ciudad '
    }
    if (stateInshipp === '') {
      formIsValid = false
      errors['stateInshipp'] = '* Se requiere estado'
    }
    if (!zipCodeInshipp) {
      formIsValid = false
      errors['zipCodeInshipp'] = '* Se requiere código postal'
    }
    // if (!phoneInshipp) {
    //   formIsValid = false
    //   errors['phoneInshipp'] = '* Se requiere número de teléfono'
    // }
    if (!mblpattern.test(phoneInshipp)) {
      formIsValid = false
      if (!phoneInshipp) {
        errors['phoneInshipp'] = 'Se requiere número de celular'
      } else
        errors['phoneInshipp'] = 'Ingrese un número de teléfono celular válido 8 to 20'
    }


    //billing form
    if (!nameInbilling || nameInbilling.trim() === '') {
      formIsValid = false
      billing_errors['nameInbilling'] = '* Se requiere el nombre'
    }
    if (!addressLine1Inbilling.length) {
      formIsValid = false
      billing_errors['addressLine1Inbilling'] = '* Se requiere Dirección 1'
    }

    // if (!addressLine2Inbilling.length) {
    //   formIsValid = false
    //   billing_errors['addressLine2Inbilling'] = '* AddressLine 2 is required'
    // }

    if (!cityInbilling || cityInbilling.trim() === '') {
      formIsValid = false
      billing_errors['cityInbilling'] = '* Se requiere ciudad'
    }

    if (!stateInbilling || stateInbilling.trim() === '') {
      formIsValid = false
      billing_errors['stateInbilling'] = '* Se requiere estado'
    }

    if (!zipCodeInbilling) {
      formIsValid = false
      billing_errors['zipCodeInbilling'] = '* Se requiere código postal'
    }


    if (!mblpattern.test(phoneInbilling)) {
      formIsValid = false
      if (!phoneInbilling) {
        billing_errors['phoneInbilling'] = 'Se requiere número de celular'
      } else
        billing_errors['phoneInbilling'] = 'Ingrese un número de teléfono celular válido 8 to 20'
    }


    if (!option_toprint || option_toprint.trim() === '') {
      formIsValid = false
      errors['option_toprint'] = '* Opción requerida'
    }
    this.setState({ errors, billing_errors })
    return formIsValid
  }
  handleChangeGroup(value) {
    let { group } = this.state
    this.setState({ group: value, isGroupValid: false })
  }


  // ########################### Shipping Address===Billing Address #########################
  handleAddressChange = (e) => {
    const { checked } = e.target
    if (checked === true) {
      this.setState({
        checked: checked,
        nameInbilling: this.state.nameInshipp,
        addressLine1Inbilling: this.state.addressLine1Inshipp,
        addressLine2Inbilling: this.state.addressLine2Inshipp,
        cityInbilling: this.state.cityInshipp,
        stateInbilling: this.state.stateInshipp,
        zipCodeInbilling: this.state.zipCodeInshipp,
        phoneInbilling: this.state.phoneInshipp,
        billing_errors: {}
      })
    }
    else {
      this.setState({
        checked: checked,
        nameInbilling: "",
        addressLine1Inbilling: "",
        addressLine2Inbilling: "",
        cityInbilling: "",
        stateInbilling: "",
        zipCodeInbilling: "",
        phoneInbilling: ""
      })
    }
  }


  handleComment = (event) => {
    this.setState({
      comments: event.target.value, isComment: false
    });
  }

  // ########################### Listing Related Category #########################
  relatedCatagory = e => {
    let token = localStorage.getItem('token')
    var body = { categoryName: e }
    var url = '/categories/getAllCategories'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var { data } = response.data
      var catArr = _.uniqBy(data, (e) => {
        return e.categoryName.toString();
      });
      this.setState({ relatedCategoryNames: catArr, related_Catnames: data })
    })
  }

  handleShipping(val) {
    var idsArray = ''
    this.state.shipping_method.map((each) => {
      if (each.shippingName === val) {
        return idsArray = each._id;
      }
      this.setState({ shippingName: val, _id: idsArray })
    })
  }

  // ########################### Listing Related Product #########################
  relatedProductsArr = e => {
    let token = localStorage.getItem('token')
    var { selectedCategoryS } = this.state
    var data = { type: "productName", searchText: e }
    if (selectedCategoryS.length > 0) {
      data.categoryName = selectedCategoryS
    }
    var body = data
    var url = '/products/productFieldsList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var { data } = response.data
      if (data) {
        var prArr = _.uniqBy(data, (e) => {
          return e.productName.toString();
        });
        this.setState({ relatedProducts: prArr, related_Products: data })
      }
    })
  }
  // ########################### Listing Shipping Methods ######################### 
  selectShipmentMethod = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: name, searchText: e }
    var url = '/order/getTransport'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var { data } = response.data
      if (data) {
        var shArr = _.uniqBy(data, (e) => {
          return e.shippingName.toString();
        });
        this.setState({ shippingMethod: shArr, shipping_method: data })
      }
    })
  }


  submitComment = (e) => {
    var { comments, checkboxVal, orderid, orderId } = this.state
    if (comments != "") {
      var data = { type: "orderPlaced", comments: comments, emailToCustomer: checkboxVal, id: orderId, orderId: orderid }
      var body = data
      var token = localStorage.getItem('token')
      var url = '/comments/addComments'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data.status === 1) {
          swal('Comentario enviado con éxito!', '', 'success')
        }
      })
        .catch(err => console.log(err))
    } else {
      this.setState({ isComment: true });
    }
  }



  addingRelatedProducts = () => {
    var { selectedProduct, selectedProductS, related_Products } = this.state
    let idsArray = this.state.selectedProductS;
    related_Products.map((each) => {
      selectedProduct.map((each1) => {
        if (each.productName === each1) {
          idsArray.push(each._id);
        }
      })
      this.setState({ selectedProductS: idsArray })
    })
    let token = localStorage.getItem('token')

    var body = { productsArray: _.uniq(selectedProductS) }
    var url = '/order/productsListing'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      var subtotalData = []
      data.map(each => { subtotalData.push(each.salePrice) })
      this.setState({ subtotalData })

      if (data) {
        var orderItemdetail = this.state.orderItemdetails;
        if (orderItemdetail && Array.isArray(orderItemdetail) && orderItemdetail.length) {
          var arr = orderItemdetail;
          orderItemdetail = _.map(data, (objects, index) => {
            var isobject = _.filter(orderItemdetail, (items) => { return items._id === objects._id });
            if (isobject && Array.isArray(isobject) && isobject.length == 0) {
              objects.quantity = 1;
              arr.push(objects)
            }
          })
          orderItemdetail = arr;
        }
        else {
          data.map((each, key) => {
            each.quantity = each.quantity ? each.quantity : 1;
            orderItemdetail.push(each)
          })
        }
        this.setState({ orderItemdetails: orderItemdetail, isProduct: true, selectedProduct: [], selectedCategory: [], selectedProductS: [] })
      }
    }
    )
  }

  // ########################### Getting discount value #########################
  discountVal = e => {
    let value = this.state.orderItemdetails.map((order, key) => {
      return ((order.salePrice * order.quantity) - (order.discount ? ((order.discount * (order.salePrice * order.qty)) / 100) : 0))
    });
    let orderSubtotalData = _.reduce(value, function (acc, cur) {
      return acc + cur;
    });
    let token = localStorage.getItem('token')
    var body = { amount: orderSubtotalData }
    var url = '/discount/getDiscount'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var { data } = response.data
      if (response && data) {
        this.setState({ discount_Id: data._id, discount_Val: data.discount })
      } else {
        this.setState({ discount_Val:this.state.discount_Val })
      }
    })
  }

  handleChange = (e) => {
    this.setState({ discount_Val: e.target.value })
  }


  placeorderByadmin = () => {

    if (this.validateForm()) {
      let token = localStorage.getItem('token')
      let { cust_id, addressLine1Inshipp, addressLine2Inshipp, nameInshipp,discount_Val, cityInshipp, stateInshipp, zipCodeInshipp, phoneInshipp, addressLine1Inbilling, option_toprint,
        addressLine2Inbilling, nameInbilling, cityInbilling, stateInbilling, wheretoShip, zipCodeInbilling, phoneInbilling, firstName, emailId, lastName, group, paymentMethod, _id, shippingName, productsArray, selectedExistingName, selectedProduct, orderid, ipAddress, isGroupValid } = this.state
      if (paymentMethod) {
        var paymentType = 'Depósito/Transferencia Bancaria'
      } else {
        var paymentType = 'Pago Fácil/Rapipago/CobroExpress'
      }
      var shippingAddress = {};
      shippingAddress.name = nameInshipp
      shippingAddress.addressLine1 = addressLine1Inshipp
      shippingAddress.addressLine2 = addressLine2Inshipp
      shippingAddress.city = cityInshipp
      shippingAddress.state = stateInshipp
      shippingAddress.zipCode = zipCodeInshipp
      shippingAddress.phone = phoneInshipp
      this.setState({ productsArray: [] })

      this.state.orderItemdetails.map((each, key) => {
        this.setState({ productsArray: [] })
        var mainorderId = {}
        mainorderId.productId = each.productId ? each.productId : each._id;
        mainorderId.productCount = JSON.parse(each.quantity);
        mainorderId.price = each.price;
        mainorderId.salePrice = each.salePrice;
        mainorderId.productName = each.productName;
        mainorderId.discount = each.discount;
        mainorderId.SKU = each.SKU;
        productsArray.push(mainorderId);


      })
      var billingAddress = {};
      billingAddress.name = nameInbilling
      billingAddress.addressLine1 = addressLine1Inbilling
      billingAddress.addressLine2 = addressLine2Inbilling
      billingAddress.city = cityInbilling
      billingAddress.state = stateInbilling
      billingAddress.zipCode = zipCodeInbilling
      billingAddress.phone = parseInt(phoneInbilling)
      if (orderid) {
        var body = {
          customerId: cust_id, id: orderid, shippingAddress, billingAddress, group, paymentType, shippingName, options: option_toprint,discount: discount_Val,
          productsArray: productsArray, transportType: wheretoShip, firstName, emailId, lastName, ipAddress: ipAddress
        }
      }
      var url = '/order/reOrder'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {

        let { data } = response
        if (data.status === 1) {
          swal({
            title: 'Pedido realizado con éxito',
            icon: "success",
            dangerMode: true,
          })
            .then(willDelete => {
              // if (willDelete) {
              this.props.history.push('/orderlisting')
              // }
            });

        } else if (data.statusCode === 437) {
          var resp = data.data + data.message
          swal(resp, '', 'warning')
        }
        else if (data.statusCode == 402) {
          swal('¡El usuario ya existe con el ID de correo electrónico anterior!', '', 'warning')
        }
        else if (data.statusCode == 432) {
          swal('Por favor ingrese correo electrónico válido!', '', 'warning')
        }
        else {
          swal(data.message, '', 'warning')
        }
      })
        .catch(err => console.log(err))
    }
  }

  handleCategory(val) {
    let idsArray = this.state.selectedCategoryS;
    this.state.related_Catnames.map((each) => {
      val.map((each1) => {
        if (each.categoryName === each1) {
          return idsArray.push(each._id);
        }
      })
      this.setState({ selectedCategory: val, selectedCategoryS: idsArray })
    })
  };

  render() {
    let value = this.state.orderItemdetails.map((order, key) => {
      return (order.salePrice * order.quantity)
    });

    let orderSubtotalData = _.reduce(value, function (acc, cur) {
      return acc + cur;
    });

    const Option = Select.Option
    let { billing_errors, wheretoShip, checkboxVal, option_toprint, relatedProducts, selectedCategory, relatedCategoryNames, selectedProduct, selectedExisting, orderItemdetails, name, errors,
      addressLine1Inshipp, addressLine2Inshipp, nameInshipp, cityInshipp, stateInshipp, zipCodeInshipp, phoneInshipp, addressLine1Inbilling, shippingMethod,
      addressLine2Inbilling, nameInbilling, cityInbilling, stateInbilling, zipCodeInbilling, phoneInbilling, firstName, emailId, lastName, group, selectedExistingName, discount, comments, selectedExistingShipment, selectedExistingMethod, paymentMethod, shippingName, billingAddress } = this.state
    const filteredCatnames1 = relatedCategoryNames.map((each) => { return each.categoryName });
    const filteredCatnames = filteredCatnames1.filter(o => !selectedCategory.includes(o));


    const filteredOptions1 = relatedProducts.map((each) => { return each.productName });
    const filteredOptions = filteredOptions1.filter(o => !selectedProduct.includes(o));

    const filteredShipping1 = shippingMethod.map((each) => { return each.shippingName });
    const filteredShipping = filteredShipping1.filter(o => !shippingName.includes(o));

    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Reordenar</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item'>Ventas</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/orderlisting')}>Pedidos</li>
                <li className='breadcrumb-item active'>Reordenar</li>
              </ul>
            </div>
          </div>
        </div>
        <div className='card animated fadeIn'>
          <div className="card-header">
            <div className="row data-filter justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  <button type="button" className="btn btn-outline-primary" onClick={() => this.props.history.push('/orderlisting')}> <span>Cancelar</span></button>
                  <button type="button" className="btn btn-success" onClick={this.placeorderByadmin}> <span>Orden de envio</span></button>
                </div>
              </div>
            </div>
          </div>
          <div className='card-body'>
            <div className="item-order float-left w-100 mb-3">
              <div className="card-header p-0 mb-3 d-flex justify-content-between align-items-center">
                <h5>artículos ordenados</h5>
                <button type="button" className="btn btn-primary product-link mb-2" ><i className="fa fa-plus" /> <span>Agregar producto</span></button>
              </div>
              <div className="add_item">
                <div className="form-group row">
                  <div className='col-md-4'>
                    <Select
                      mode="multiple"
                      placeholder="Ingrese el nombre de la categoría"
                      value={selectedCategory}
                      onChange={(value) => this.handleCategory(value)}
                      onSearch={(e) => this.relatedCatagory(e)}
                      onFocus={(e) => this.relatedCatagory(e)}
                      style={{ width: '100%' }}
                    >
                      {filteredCatnames.map((item, key) => (
                        <Select.Option key={key} value={item}>
                          {item}
                        </Select.Option>
                      ))}
                    </Select>
                  </div>
                  <div className='col-md-4'>
                    <Select
                      mode="multiple"
                      placeholder="Ingrese el nombre del producto"
                      value={selectedProduct}
                      onChange={(value) => this.setState({ selectedProduct: value })}
                      onSearch={(e) => this.relatedProductsArr(e)}
                      onFocus={(e) => this.relatedProductsArr(e)}
                      style={{ width: '100%' }}
                    >
                      {filteredOptions.map((each, id) => (
                        <Select.Option key={each} value={each}>
                          {each}
                        </Select.Option>
                      ))}
                    </Select>
                  </div>
                  <div className="col-md-3">
                    <button className="btn btn-primary" onClick={this.addingRelatedProducts}><i className="fa fa-plus"></i><span>Add</span></button>
                  </div>
                </div>
              </div>
              <div className='table-responsive mb-3'>
                <table className='table dataTable with-image row-border hover custom-table table-striped'>
                  <thead>
                    <tr>
                      <th>Producto</th>
                      <th>Precio</th>
                      <th>Cantidad</th>
                      <th>Total parcial</th>
                      {/* <th>Discount</th> */}
                      {/* <th>Fila total</th> */}
                      <th>Comportamiento</th>
                    </tr>
                  </thead>
                  <tbody>
                    {orderItemdetails.map((order, Key) => {
                      return (
                        <tr className='animated fadeIn' key={Key}>
                          <td>{order.productName}<br />
                            <span style={{ color: 'red' }}>  {order.stockQuantity === 0 ? 'Out of Stock' : (order.stockQuantity < order.quantity ? `Only ${order.stockQuantity} units available` : null)} </span>
                          </td>
                          <td>{order.salePrice ? order.salePrice : ''}
                          </td>
                          <td width="7%"><input className="form-control" name="quantity" value={order.quantity ? order.quantity : ''} onChange={(e) => this.handleChangeQuantity(e, order)} type="number" /></td>
                          <td>{order.salePrice && order.quantity ? order.salePrice * parseInt(order.quantity) : 0}</td>
                          {/* <td>{order.discount ? order.discount : 0}</td> */}
                          {/* <td>{order.salePrice * order.quantity}</td> */}

                          <td><button ><i className="fa fa-trash text-danger" onClick={() => this.delete(Key)}></i></button></td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
              {
                orderItemdetails.length === 0 && this.state.isProduct ? <p className="text-danger text-center"> No hay productos agregados</p> : null
              }
            </div>
            <hr className="dashed-hr clearfix mb-5" />
            <div className="card-header p-0 my-3 d-flex justify-content-between align-items-center">
              <h5>Información del cliente</h5>
            </div>
            <div>
              <div className="card-header p-0 mb-3">
                <h5>Información de la cuenta</h5>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <label>Grupo<span className="text-danger">*</span></label>

                  <Select
                    value={this.state.group}
                    onChange={(value) => this.handleChangeGroup(value)}
                    style={{ width: '100%' }}
                  >
                    <Option value="">Seleccionar</Option>
                    <Option value="general">General</Option>
                    <Option value="wholeSale">Venta al por mayor</Option>
                    <Option value="retailer">Detallista</Option>
                  </Select>
                  {this.state.isGroupValid ? <p className="text-danger">Se requiere grupo</p> : null}
                </div>

                <div className="col-md-6">
                  <label>Email</label>
                  <input type="email" className="form-control" value={emailId}
                    name='emailId' placeholder="Email" onChange={this.handleChangeShipping} />
                  <p className="text-danger">{this.state.errors.emailId}</p>
                </div>

              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className='form-group row'>
                    <label className='col-md-12 text-left'> Primer nombre <span className="text-danger">*</span></label>
                    <div className='col-md-12'>
                      <input
                        className='form-control'
                        type='text'
                        placeholder='Primer nombre'
                        value={firstName}
                        name='firstName'
                        onChange={this.handleChangeShipping}
                      />
                      <span className="error-block"> {errors.firstName} </span>
                    </div>
                  </div>

                </div>
                <div className="col-md-6">
                  <div className='form-group row'>
                    <label className='col-md-12 text-left'>Apellido <span className="text-danger">*</span></label>
                    <div className='col-md-12'>
                      <input
                        className='form-control'
                        type='text'
                        placeholder='Apellido'
                        value={lastName}
                        name='lastName'
                        onChange={this.handleChangeShipping}
                      />
                      <span className="error-block"> {errors.lastName} </span>

                    </div>
                  </div>
                </div>
              </div>
              <div className="address-info mt-5">
                <div className="card-header p-0 mb-3">
                  <h5>Datos del Domicilio</h5>
                </div>
                <form className='form-sample row'>
                  <div className="col-md-6">
                    <div className="top-form">
                      <h5>Dirección de Envío</h5>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Nombre<span className="text-danger">*</span></label>
                      <div className='col-md-12'>
                        <input
                          className='form-control'
                          type='text'
                          placeholder='Nombre'
                          value={nameInshipp}
                          onChange={this.handleChangeShipping}
                          name='nameInshipp'
                        />
                        <span className="error-block"> {errors.nameInshipp} </span>

                      </div>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Dirección Línea 1<span className="text-danger">*</span></label>
                      <div className='col-md-12'>
                        <textarea className="form-control" onChange={this.handleChangeShipping} value={addressLine1Inshipp}
                          name='addressLine1Inshipp'></textarea>
                      </div>
                      <span className="error-block"> {errors.addressLine1Inshipp} </span>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Dirección línea 2</label>
                      <div className='col-md-12'>
                        <textarea className="form-control" onChange={this.handleChangeShipping} value={addressLine2Inshipp}
                          name='addressLine2Inshipp'></textarea>
                      </div>
                      <span className="error-block"> {errors.addressLine2Inshipp} </span>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Ciudad<span className="text-danger">*</span></label>
                      <div className='col-md-12'>
                        <input
                          className='form-control'
                          type='text'
                          onChange={this.handleChangeShipping}
                          placeholder='Nombre de la ciudad'
                          value={cityInshipp}
                          name='cityInshipp'
                        />
                        <span className="error-block"> {errors.cityInshipp} </span>
                      </div>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Estado<span className="text-danger">*</span></label>
                      <div className='col-md-12'>
                        <input
                          className='form-control'
                          type='text'
                          placeholder='Nombre Estado / provice'
                          value={stateInshipp}
                          name='stateInshipp'
                          onChange={this.handleChangeShipping}
                        />
                        <span className="error-block"> {errors.stateInshipp} </span>

                      </div>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Código postal</label>
                      <div className='col-md-12'>
                        <input
                          className='form-control'
                          type='number'
                          placeholder='Código postal'
                          value={zipCodeInshipp}
                          name='zipCodeInshipp'
                          onChange={this.handleChangeShipping}
                        />
                        <span className="error-block"> {errors.zipCodeInshipp} </span>

                      </div>
                    </div>
                    <div className='form-group row'>
                      <label className='col-md-12 text-left'>Telefono no<span className="text-danger">*</span></label>
                      <div className='col-md-12'>
                        <input
                          className='form-control'
                          type='number'
                          onChange={this.handleChangeShipping}
                          placeholder='Telefono no'
                          value={phoneInshipp}
                          name='phoneInshipp'
                        />
                        <span className="error-block"> {errors.phoneInshipp} </span>

                      </div>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <h5>Dirección de Envio</h5>
                    <div className='checkbox mt-2'>
                      <label>
                        <input type="checkbox" onChange={e => this.handleAddressChange(e)}
                          checked={this.state.checked ? true : false} className="form-check-input" />
                        <span />
                        <i className='input-helper' />
                        Facturación como dirección de envío
                      </label>
                    </div>
                    <div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Nombre<span className="text-danger">*</span></label>
                        <div className='col-md-12'>
                          <input
                            className='form-control'
                            type='text'
                            placeholder='Nombre'
                            onChange={this.handleChangeForm}
                            value={nameInbilling}
                            name='nameInbilling'
                          />
                          <span className="error-block"> {billing_errors.nameInbilling} </span>

                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Dirección Línea 1<span className="text-danger">*</span></label>
                        <div className='col-md-12'>
                          <textarea className="form-control" onChange={this.handleChangeForm} value={addressLine1Inbilling}
                            name='addressLine1Inbilling'></textarea>
                          <span className="error-block"> {billing_errors.addressLine1Inbilling} </span>

                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Dirección línea 2</label>
                        <div className='col-md-12'>
                          <textarea className="form-control" onChange={this.handleChangeForm} value={addressLine2Inbilling}
                            name='addressLine2Inbilling'></textarea>
                          <span className="error-block"> {billing_errors.addressLine2Inbilling} </span>

                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Ciudad<span className="text-danger">*</span></label>
                        <div className='col-md-12'>
                          <input
                            className='form-control'
                            type='text'
                            onChange={this.handleChangeForm}
                            placeholder='Nombre de la ciudad'
                            value={cityInbilling}
                            name='cityInbilling'
                          />
                          <span className="error-block"> {billing_errors.cityInbilling} </span>

                        </div>
                      </div>

                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Estado<span className="text-danger">*</span></label>
                        <div className='col-md-12'>
                          <input
                            className='form-control'
                            type='text'
                            onChange={this.handleChangeForm}
                            placeholder='Nombre Estado / provice'
                            value={stateInbilling}
                            name='stateInbilling'
                          />
                          <span className="error-block"> {billing_errors.stateInbilling} </span>

                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Código postal</label>
                        <div className='col-md-12'>
                          <input
                            className='form-control'
                            type='number'
                            onChange={this.handleChangeForm}
                            placeholder='Código postal'
                            value={zipCodeInbilling}
                            name='zipCodeInbilling'
                          />
                          <span className="error-block"> {billing_errors.zipCodeInbilling} </span>

                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-md-12 text-left'>Telefono no<span className="text-danger">*</span></label>
                        <div className='col-md-12'>
                          <input
                            className='form-control'
                            type='number'
                            onChange={this.handleChangeForm}
                            placeholder='Telefono no'
                            value={phoneInbilling}
                            name='phoneInbilling'
                          />
                          <span className="error-block"> {billing_errors.phoneInbilling} </span>

                        </div>
                      </div>
                      <div className='form-group row'>
                        <div className='col-md-12'>
                        </div>
                      </div>
                    </div>

                  </div>
                </form>
              </div>
            </div>
            <div className="default-settings  mt-5 clearfix">
              <div className="payment_ship  ping clearfix">
                <div className="card-header p-0 mb-3">
                  <h5>Información de pago y envío</h5>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <label>Método de pago</label>
                    <div className="radio vertical">
                      <label >
                        <input type="radio" checked={paymentMethod ? true : false} name="transfer" value={this.state.paymentMethod}
                          onChange={() => this.setState({ paymentMethod: !this.state.paymentMethod })} id="transfer" /><span>
                        </span>Depósito/Transferencia Bancaria</label>
                      <label >
                        <input type="radio" checked={paymentMethod ? false : true} name="transfer" value={this.state.paymentMethod}
                          onChange={() => this.setState({ paymentMethod: !this.state.paymentMethod })} id="transfer" />
                        <span></span>Pago Fácil/Rapipago/CobroExpress</label>
                    </div>
                    <label>En caso de faltante:</label>
                    <div className='col-md-8 col-sm-7'>
                      <input
                        type='text'
                        name={option_toprint}
                        value={option_toprint}
                        className='form-control'
                        placeholder='Ingrese opción'
                        onChange={(e) => this.setState({ option_toprint: e.target.value }, this.setState({
                          errors: Object.assign(this.state.errors, { option_toprint: "" }),
                        }))}
                      />
                      <span className="error-block"> {errors.option_toprint} </span>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <label>Método de envío</label>
                    <div className="form-group">
                      <Select
                        showSearch
                        placeholder="Ingrese el método de envío"
                        value={shippingName}
                        onChange={(val) => this.handleShipping(val)}
                        onSearch={(e) => this.selectShipmentMethod(e, 'shippingName')}
                        onFocus={(e) => this.selectShipmentMethod(e, 'shippingName')}
                        style={{ width: '100%' }}
                        filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      >
                        {filteredShipping.map((each, id) => {
                          return <Select.Option key={each} value={each}>
                            {each}
                          </Select.Option>
                        })}
                      </Select>
                    </div>
                  </div>
                  <div className="col-md-6"></div>
                  <div className="col-md-6">
                    <label>Dónde enviar</label>
                    <select className="form-control" name="wheretoShip" onChange={(e) => this.setState({ wheretoShip: e.target.value }, this.setState({
                      errors: Object.assign(this.state.errors, { [e.target.name]: "" }),
                    }))} value={wheretoShip}>
                      <option value=''>Select</option>
                      <option value='Enviar a sucursal'>Enviar a sucursal</option>
                      <option value='Enviar a domicilio'>Enviar a domicilio</option>
                    </select>
                    <span className="error-block"> {errors.wheretoShip} </span>

                  </div>
                </div>
              </div>
              <div className="order_total mt-5 clearfix">
                <div className="card-header p-0 mb-3">
                  <h5>Total del pedido</h5>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <label className="d-block"> <h5>Historial de pedidos</h5></label>
                    Comentario de pedido
              <div className='form-group'>
                      <textarea className='form-control' onChange={this.handleComment} value={comments} name="comments" ></textarea>
                      {this.state.isComment ? <span className="error-block">* Ingrese comentario</span> : null}
                      <div className='checkbox mt-2'>
                        <label>
                          <input type="checkbox" className="form-check-input" checked={checkboxVal} onChange={() => this.setState({ checkboxVal: !this.state.checkboxVal })} />
                          <span />
                          <i className='input-helper' />
                          Añadir comentarios
                        </label>
                      </div>
                      <button className="btn btn-primary mt-2 float-right" onClick={this.submitComment}>enviar comentario</button>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <label> <h5>Totales de pedidos</h5></label>
                    <table className='table dataTable with-image row-border hover custom-table table-striped'>
                      <tbody>
                        <tr className='animated fadeIn'>
                          <td>Total parcial</td>
                          <td>${orderSubtotalData ? orderSubtotalData.toFixed(2) : 0}</td>
                        </tr>
                        <tr className='animated fadeIn'>
                          <td>Gastos de envío y manipulación</td>
                          <td>$0.00</td>
                        </tr>
                        <tr className='animated fadeIn'>
                          <td>Descuento	</td>
                          {this.state.dis_edit ?
                            <td>   {this.state.discount_Val} %</td> :
                            <td> <input value={this.state.discount_Val} name='discount_Val' onChange={(e) => this.handleChange(e)} type='number' /> %</td>}
                          <button onClick={() => this.setState({ dis_edit: !this.state.dis_edit })} style={{ border: 0, fontSize: 25 }}> <i className='fa fa-edit text-primary' aria-hidden='true' data-toggle="tooltip" title="Edit" /> </button>

                        </tr>
                        <tr className='animated fadeIn'>
                          <td>Costo de financiamiento</td>
                          <td>$0.00</td>
                        </tr>
                        <tr className='animated fadeIn'>
                          <td>Impuesto</td>
                          <td>$0.00</td>
                        </tr>
                        <tr className='animated fadeIn'>
                          <td><b>Gran total</b></td>
                          <td>{this.state.discount_Val && orderSubtotalData ? Math.round(orderSubtotalData - (orderSubtotalData * this.state.discount_Val) / 100).toFixed(2) : orderSubtotalData} </td>

                        </tr>
                      </tbody>
                    </table>
                    <div className='checkbox mt-2'>
                      <label>
                        <input type="checkbox" className="form-check-input" onChange={() => this.setState({ emailConfirmation: !this.state.emailConfirmation })} checked={this.state.emailConfirmation} />
                        <span />
                        <i className='input-helper' />
                        Confirmación de pedido por correo electrónico
                </label>
                    </div>
                    <button className="btn btn-primary float-right" onClick={this.placeorderByadmin}>Orden de envio</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home>
    )
  }
}

const mapStateToProps = state => ({
  getadmindata: state.user.userdatails,
});
export default connect(mapStateToProps, actions)(ReOrder)
