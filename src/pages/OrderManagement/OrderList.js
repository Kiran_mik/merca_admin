import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css';
import Pagination from 'rc-pagination';
import { Select } from 'antd'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'antd/dist/antd.css'
import isAfter from "date-fns/isAfter";
import { isEmpty } from "lodash"
import swal from 'sweetalert'
import { API_URL, PDF_URL } from '../../config/configs'
import * as actions from '../../actions'
import Home from '../Home'
import moment from 'moment'
import _ from 'lodash'
import { BeatLoader } from 'react-spinners';
import Loader from '../Loader'
import $ from 'jquery';
const FileDownload = require('js-file-download')
class OrderManagement extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      total: 0,
      pagesize: 10,
      orderListing: [],
      sort: {},
      sortData: { createdAt: false, buyerName: false, emailId: false, count: false, discount: false, totalAmount: false, orderStatus: false, paymentType: false, transportType: false,shippingName:false },
      length: 10,
      orderId: true,
      orderedDate: true,
      transportType: true,
      shippingName:true,
      buyerName: true,
      emailId: true,
      quantity: true,
      discount: true,
      totalAmount: true,
      paymentMethod: true,
      orderStatus: true,
      selectAll: false,
      loading: true,
      loadingPDF: false,
      transportType_: '',

      selectedOrderid: [],
      mainorderId: [],
      selectedBuyername: [],
      mainBuyername: [],
      mainBuyernameS: [],
      selectedEmailid: [],
      mainEmailid: [],
      minQuantity: "",
      maxQuantity: "",
      minDiscount: '',
      maxDiscount: '',
      startDate: "",
      endDate: "",
      orderStatusvalue: '',
      PaymentMethodvalue: '',
      startDateValid: '',
      startDate: new Date(),
      endDate: new Date(),
      startDateAdded: '',
      endDateAdded: '',
      multipleDelete: [],
      loading: true,
      ordersAccess: {},
      page_: false,
      download:false
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess && permissions.rolePermission.ordersAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }
    if (permissions.rolePermission) {
      let { ordersAccess } = permissions.rolePermission
      this.setState({ ordersAccess: ordersAccess })
    }

    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    this.orderListing('pagesize')
  }


  filtermethod(e, name) {
    let token = localStorage.getItem('token')
    var body = { type: name, searchText: e }
    var url = '/order/orderFieldsList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var data = response.data.data
      if (response && response.data && response.data.data) {
        if (name === 'orderId') {
          var mainorderId = []
          data.map((each, key) => {
            mainorderId.push(each.orderId)
            this.setState({ mainorderId })
          })
        }
        if (name === 'buyerName') {
          var byArr = _.uniqBy(data, (e) => {
            return e.buyerName.toString();
          });
          this.setState({ mainBuyername: byArr, listOfBuyers: data })
        }
        if (name === 'emailId') {
          var emArr = _.uniqBy(data, (e) => {
            return e.emailId.toString();
          });
          this.setState({ mainEmailid: emArr, listOfEmails: data })

        }
      }
    })
  }

  // ############################## Order Listing ######################
  orderListing = (e) => {
    var url = '/order/orderListing'
    var method = 'post'
    var { page, pagesize, sort, selectedOrderid, selectedBuyername, selectedBuyername, selectedEmailid, transportType_,
      minQuantity, quantity, maxQuantity, minDiscount, maxDiscount, orderStatusvalue, endDateAdded, startDateAdded, PaymentMethodvalue } = this.state
    var data = {}
    if (e === 'filter') {
      data.page = 1
    } else if (e === 'pagesize') {
      data = data
    } else {
      data.page = page
    }
    if (e === 'pagesize') {
      data = data
    } else {
      data.pagesize = pagesize
    }
    var quantity = {}
    var discount = {}
    var orderedDate = {}
    var paymentMethod = []
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (selectedOrderid != '') {
      data.orderId = selectedOrderid
    }
    if (selectedBuyername != '') {
      data.buyerName = selectedBuyername
    }
    if (selectedEmailid != '') {
      data.emailId = selectedEmailid
    }
    if (minQuantity != '') {
      quantity.minQuantity = JSON.parse(minQuantity)
    }
    if (minQuantity == '') {
      quantity.minQuantity = null
    }
    if (maxQuantity != '') {
      quantity.maxQuantity = JSON.parse(maxQuantity)
    }
    if (maxQuantity == '') {
      quantity.maxQuantity = null
    }
    if (minDiscount != '') {
      discount.minDiscount = JSON.parse(minDiscount)
    }
    if (minDiscount == '') {
      discount.minDiscount = null
    }
    if (maxDiscount != '') {
      discount.maxDiscount = JSON.parse(maxDiscount)
    }
    if (maxDiscount == '') {
      discount.maxDiscount = null
    }
    if (orderStatusvalue != '') {
      data.orderStatus = orderStatusvalue
    }
    if (transportType_ != '') {
      data.transportType = transportType_
    }
    if (PaymentMethodvalue != '') {
      var paymentMethod = []
      paymentMethod.push(PaymentMethodvalue);
      data.paymentType = paymentMethod
    }
    if (startDateAdded != '') {
      orderedDate.startDate = moment(startDateAdded).format("MM/DD/YYYY")
    }
    if (startDateAdded == '') {
      orderedDate.startDate = ''
    }
    if (endDateAdded != '') {
      orderedDate.endDate = moment(endDateAdded).format("MM/DD/YYYY")
    }
    if (endDateAdded == '') {
      orderedDate.endDate = ''
    }
    if (!isEmpty(orderedDate)) {
      data.orderedDate = orderedDate
    }
    data.orderedDate = orderedDate
    data.quantity = quantity
    data.discount = discount
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (data1.status === 0) {
        this.setState({ orderListing: [] })
      }
      this.setState({ orderListing: data.orderListing, total: data.total, length: data.orderListing.length, loading: false })
      if (data && data.orderListing.length <= pagesize) {
        this.setState({ page_: false })
      }
      if (data.manageOrderListing) {
        this.setState({
          orderId: data.manageOrderListing.orderId,
          orderedDate: data.manageOrderListing.orderedDate,
          buyerName: data.manageOrderListing.buyerName,
          emailId: data.manageOrderListing.emailId,
          quantity: data.manageOrderListing.quantity,
          discount: data.manageOrderListing.discount,
          totalAmount: data.manageOrderListing.totalAmount,
          paymentMethod: data.manageOrderListing.paymentMethod,
          orderStatus: data.manageOrderListing.orderStatus,
          transportType: data.manageOrderListing.transportType,
          pagesize: data.manageOrderListing.pageSize
        })
      }

    }
    )
  }

  // ############################## Reset Listing ######################
  resetorderListing = () => {
    var url = '/order/orderListing'
    var method = 'post'
    var { page, pagesize } = this.state
    var data = { page, pagesize }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (data1.status === 0) {
        this.setState({ orderListing: [] })
      }
      this.setState({
        orderListing: data.orderListing, total: data.total, length: data.orderListing.length,
        selectedBuyername: [], selectedOrderid: [], orderStatusvalue: '', transportType_: '', selectedEmailid: [], minQuantity: "", maxQuantity: "", minDiscount: "", maxDiscount: "", PaymentMethodvalue: "", startDateAdded: "", endDateAdded: ""
      })
      if (data.manageOrderListing) {
        this.setState({
          orderId: data.manageOrderListing.orderId,
          orderedDate: data.manageOrderListing.orderedDate,
          buyerName: data.manageOrderListing.buyerName,
          emailId: data.manageOrderListing.emailId,
          quantity: data.manageOrderListing.quantity,
          discount: data.manageOrderListing.discount,
          totalAmount: data.manageOrderListing.totalAmount,
          paymentMethod: data.manageOrderListing.paymentMethod,
          orderStatus: data.manageOrderListing.orderStatus,
          transportType: data.manageOrderListing.transportType
        })
      }
    }
    )
  }

  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { orderId, orderedDate, buyerName, emailId, quantity, discount, page, totalAmount, paymentMethod, transportType, orderStatus, pagesize, sortData, total, PaymentMethodvalue } = this.state
    var body = { orderId: orderId, orderedDate: orderedDate, buyerName: buyerName, emailId: emailId, quantity: quantity, discount: discount, transportType: transportType, totalAmount: totalAmount, paymentMethod: paymentMethod, orderStatus: orderStatus, pageSize: pagesize }
    var url = '/order/setFilterForOrders'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      if (response) {
        this.orderListing()
        if (e) {
          swal('Actualizado con éxito!', '', 'success')
        }
      }
    })
  }


  //* ********** SORTING ************************//
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState({ sort: { [element]: value } }, () => { this.orderListing() })
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }



  handleChangeAdded = ({ startDate, endDate }) => {
    if (startDate === null) {
      startDate = ''
    } else { startDate = startDate || this.state.startDateAdded; }
    if (endDate === null) {
      endDate = ''
    } else { endDate = endDate || this.state.endDateAdded; }
    if (isAfter(startDate, endDate)) {
      endDate = startDate;
    }
    this.setState({ startDateAdded: startDate, endDateAdded: endDate });
  };
  handleChangeStart2 = startDate => this.handleChangeAdded({ startDate });
  handleChangeEnd2 = endDate => this.handleChangeAdded({ endDate });


  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize,
    }, () => this.orderListing());
    this.setState({ selectAll: false }, () => this.selectAllcheck())
  }


  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 }, () => this.setTableRows(), () => this.orderListing());
  }
  handleChange_ = (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1, page_: true }, () => this.setTableRows(), () => this.orderListing());
    }
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.orderListing());
    // }
  }

  checkArray(_id) {
    let { multipleDelete } = this.state
    if (multipleDelete.includes(_id)) {
      return true
    } else {
      return false
    }
  }

  onCheckbox(_id, val) {
    var delarray = this.state.multipleDelete
    if (!delarray.includes(_id)) {
      delarray.push(_id)
    } else {
      delarray.splice(delarray.indexOf(_id), 1)
    }
    if (delarray.length != this.state.orderListing.length) {
      this.setState({ checked: false })
    }
    if (this) this.setState({ multipleDelete: delarray })
  }


  selectAllcheck = () => {
    this.setState({ pagination_: true })
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { orderListing } = this.state
    if (this.state.selectAll) {
      orderListing.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      orderListing.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }

  downloadPDF = () => {
    let { multipleDelete} = this.state
    this.setState({download:!this.state.download})
    var orderIds = []
    multipleDelete.map(each => {
      if (each != undefined) {
        orderIds.push(each)
      }
    })
    var body = { orderIds }
    let token = localStorage.getItem('token')
    var url = '/pdf/ordersPdf'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var data = response.data.data
      if (data) {
        window.open(PDF_URL + data.filePathAndName, '_blank');
        this.setState({download:false, checked: false, multipleDelete: [] })
      }
    })
  }
  render() {
    const Option = Select.Option;
    let { mainorderId, ordersAccess, sortData, transportType,shippingName, length, orderListing, orderId, orderedDate, buyerName, emailId, quantity, discount, total, page, totalAmount, paymentMethod,
      orderStatus, pagesize, selectedOrderid, mainBuyername, selectedBuyername, selectedEmailid, maxQuantity, mainEmailid, transportType_,
      minQuantity, minDiscount, maxDiscount, orderStatusvalue, PaymentMethodvalue } = this.state
    const mainorderIddata = mainorderId.filter(o => !selectedOrderid.includes(o));

    const filteredBuyers1 = mainBuyername.map((each) => { return each.buyerName });
    const filteredBuyers = filteredBuyers1.filter(o => !selectedBuyername.includes(o));

    const mainEmailiddata1 = mainEmailid.map((each) => { return each.emailId });
    const mainEmailiddata = mainEmailiddata1.filter(o => !selectedEmailid.includes(o));
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Pedidos</h3>
              <ul className="breadcrumb ">
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item'>Ventas </li>
                <li className="breadcrumb-item active">Pedidos</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
      {this.state.download ?  <Loader/> :null }

        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  <div className="dropdown">
                    {ordersAccess.create == true ?
                      <button type="button" className="btn btn-primary" onClick={() => this.props.history.push('/placeOrderByAdmin')}><i className="fa fa-plus" /> <span>Crear nueva Orden</span></button> : null}
                    {ordersAccess.download == true ?
                      <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <span>Herramientas</span>
                      </button> : null}
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a className="dropdown-item" href="javascript:;" onClick={this.downloadPDF}>Exportar a PDF</a>
                    </div>
                  </div>
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ orderId: !this.state.orderId })} checked={orderId} /><span></span>Solicitar ID</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ orderedDate: !this.state.orderedDate })} checked={orderedDate} /><span></span>Fecha de orden</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ buyerName: !this.state.buyerName })} checked={buyerName} /><span></span>Nombre del comprador</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ emailId: !this.state.emailId })} checked={emailId} /><span></span>Email</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ quantity: !this.state.quantity })} checked={quantity} /><span></span>Cantidad</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ discount: !this.state.discount })} checked={discount} /><span></span>Descuento</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ totalAmount: !this.state.totalAmount })} checked={totalAmount} /><span></span>Cantidad total</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ paymentMethod: !this.state.paymentMethod })} checked={paymentMethod} /><span></span>Método de pago</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ orderStatus: !this.state.orderStatus })} checked={orderStatus} /><span></span>Estado del pedido</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ transportType: !this.state.transportType })} checked={transportType} /><span></span>Tipo de transporte</label></li>

            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.orderListing} >
              Reiniciar
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({ orderId: true, orderedDate: true, buyerName: true, emailId: true, quantity: true, discount: true, page: true, totalAmount: true, paymentMethod: true, transportType: true, orderStatus: true })}>
              Seleccionar todo
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setTableRows('rows')}>
              Guardar
            </button>

          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Solicitar ID</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingresar ID de pedido"
                    value={selectedOrderid}
                    onChange={(selectedOrderid) => this.setState({ selectedOrderid })}
                    onSearch={(e) => this.filtermethod(e, 'orderId')}
                    onFocus={(e) => this.filtermethod(e, 'orderId')}
                    style={{ width: '100%' }}
                  >
                    {mainorderIddata.map(order => (
                      <Select.Option key={order} value={order}>
                        {order}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Órdenes de</label>
                  <DatePicker
                    placeholderText="Órdenes de"
                    className="form-control"
                    selected={this.state.startDateAdded}
                    selectsStart
                    startDate={this.state.startDateAdded}
                    endDate={this.state.endDateAdded}
                    onChange={this.handleChangeStart2}
                  />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Órdenes a</label>
                  <DatePicker
                    placeholderText="Órdenes a"
                    className="form-control"
                    selected={this.state.endDateAdded}
                    selectsEnd
                    startDate={this.state.startDateAdded}
                    endDate={this.state.endDateAdded}
                    onChange={this.handleChangeEnd2}
                  />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre del comprador</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre del comprador"
                    value={selectedBuyername}
                    onChange={(val) => this.setState({ selectedBuyername: val })}
                    onSearch={(e) => this.filtermethod(e, 'buyerName')}
                    onFocus={(e) => this.filtermethod(e, 'buyerName')}
                    style={{ width: '100%' }}
                  >
                    {filteredBuyers.map((each, id) => (
                      <Select.Option key={id} value={each}>
                        {each}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Email</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingresar Email"
                    value={selectedEmailid}
                    onChange={(val) => this.setState({ selectedEmailid: val })}
                    onSearch={(e) => this.filtermethod(e, 'emailId')}
                    onFocus={(e) => this.filtermethod(e, 'emailId')}
                    style={{ width: '100%' }}
                  >
                    {mainEmailiddata.map(order => (
                      <Select.Option key={order} value={order}>
                        {order}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Cantidad</label>
                  <div className="row">
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="minQuantity" placeholder="Ingrese la cantidad mínima" id="min_quantity" required="" value={minQuantity} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="maxQuantity" placeholder="Ingrese la cantidad máxima" id="max_quantity" required="" value={maxQuantity} onChange={this.handleChange} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Descuento %</label>
                  <div className="row">
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="minDiscount" placeholder="Min Discount" id="min_discount" required="" value={minDiscount} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="maxDiscount" placeholder="Max Discount" id="max_discount" required="" value={maxDiscount} onChange={this.handleChange} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Método de pago</label>
                  <select className="form-control" onChange={(e) => this.setState({ PaymentMethodvalue: e.target.value })} value={PaymentMethodvalue}>
                    <option value='' disabled>Seleccionar</option>
                    <option value='Tarjeta de crédito (1 pago)'>Tarjeta de crédito (1 pago)</option>
                    <option value='Pago Fácil/Rapipago/CobroExpress'>Pago Fácil/Rapipago/CobroExpress</option>
                    <option value='Depósito/Transferencia Bancaria'>Depósito/Transferencia Bancaria</option>
                    <option value='Mercado Pago'>Mercadopago</option>

                  </select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group" >
                  <label>Estado del pedido</label>
                  <select className="form-control" onChange={(e) => this.setState({ orderStatusvalue: e.target.value })} value={orderStatusvalue}>
                    <option value=''>Selecciona</option>
                    <option value='processing'>tratamiento</option>
                    <option value='pending'>pendiente</option>
                    <option value='shipped'>Enviada</option>
                    <option value='closed'>Cerrada</option>
                    <option value='refunded'>Reembolsados</option>
                    <option value='cancelled'>Cancelado</option>
                    <option value='hold'>Sostener</option>
                    <option value='unHold'>Deshacer</option>
                  </select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group" >
                  <label>Tipo de transporte</label>
                  <select className="form-control" name={transportType_} onChange={(e) => this.setState({ transportType_: e.target.value })} value={transportType_}>
                    <option value=''>Seleccionar</option>
                    <option value='Enviar a sucursal'>Enviar a sucursal</option>
                    <option value='Enviar a domicilio'>Enviar a domicilio</option>
                  </select>
                </div>
              </div>
            </div>
            <hr />
            <div className="pull-right filter-button">
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.orderListing('filter')} >
                Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetorderListing}  >
                Reiniciar
            </button>
            </div>
          </div>
          <div className="card-body">
            <div className="table-responsive">

              <table className="table dataTable with-image row-border hover custom-table table-striped" >
                <thead>
                  <tr>
                    {ordersAccess.download == true ?
                      <th>
                        <div className='checkbox'>
                          <label>
                            <input type="checkbox" className="form-check-input" checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} />
                            <span />
                            <i className='input-helper' />
                          </label>
                        </div>
                      </th> : null}
                    {orderId ? <th>Solicitar ID</th> : null}
                    {orderedDate ? <th sortable-column="createdAt" onClick={this.onSort.bind(this, 'createdAt')} >
                      Fecha de orden
                      <i aria-hidden='true' className={(sortData['createdAt']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {buyerName ? <th sortable-column="buyerName" onClick={this.onSort.bind(this, 'buyerName')} >
                      Nombre del comprador
                      <i aria-hidden='true' className={(sortData['buyerName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {emailId ? <th sortable-column="emailId" onClick={this.onSort.bind(this, 'emailId')}>
                      Email
                      <i aria-hidden='true' className={(sortData['emailId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {quantity ? <th sortable-column="count" onClick={this.onSort.bind(this, 'count')}> Quantity
                    <i aria-hidden='true' className={(sortData['count']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /> </th> : null}
                    {discount ? <th sortable-column="discount" onClick={this.onSort.bind(this, 'discount')}>
                      Descuento
                      <i aria-hidden='true' className={(sortData['discount']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {totalAmount ? <th sortable-column="totalAmount" onClick={this.onSort.bind(this, 'totalAmount')}>
                      Cantidad total
                    <i aria-hidden='true' className={(sortData['totalAmount']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {paymentMethod ? <th sortable-column="paymentType" onClick={this.onSort.bind(this, 'paymentType')}> Método de pago
                    <i aria-hidden='true' className={(sortData['paymentType']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /> </th> : null}

                    {transportType ? <th sortable-column="transportType" onClick={this.onSort.bind(this, 'transportType')}> Tipo de transporte
                    <i aria-hidden='true' className={(sortData['transportType']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /> </th> : null}

                    {shippingName ? <th sortable-column="shippingName" onClick={this.onSort.bind(this, 'shippingName')}> Nombre del transporte
                    <i aria-hidden='true' className={(sortData['shippingName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /> </th> : null}

                    {orderStatus ? <th sortable-column="orderStatus" onClick={this.onSort.bind(this, 'orderStatus')}> Estado del pedido
                    <i aria-hidden='true' className={(sortData['orderStatus']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /> </th> : null}
                    {ordersAccess.viewDetails == true ? <th> Actions</th> : null}
                  </tr>
                </thead>
                <tbody>

                </tbody>
                {orderListing.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                  <tbody>
                    {orderListing.map((order, Key) => {
                      return (
                        <tr key={order._id} key={Key} >
                          {ordersAccess.download == true ?
                            <td>
                              <div className='checkbox'>
                                <label>
                                  <input type="checkbox" className="form-check-input" checked={(this.checkArray(order._id)) ? true : false} onChange={() => this.onCheckbox(order._id)} />
                                  <span />
                                  <i className='input-helper' />
                                </label>
                              </div>
                            </td> : null}
                          {orderId ? <td>{order.orderId}</td> : null}
                          {orderedDate ? <td>{moment(order.createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td> : null}
                          {buyerName ? <td>{order.buyerName}</td> : null}
                          {emailId ? <td>{order.emailId}</td> : null}
                          {quantity ? <td>{order.count}</td> : null}
                          {discount ? <td>{order.discount}</td> : null}
                          {totalAmount ? <td>{order.totalAmount.toFixed(2)}</td> : null}
                          {paymentMethod ? <td>{order.paymentType}</td> : null}
                          {transportType ? <td>{order.transportType ? order.transportType : "-"}</td> : null}
                          {shippingName ? <td>{order.shippingName ? order.shippingName : "-"}</td> : null}
                          {orderStatus ? <td>{order.orderStatus}</td> : null}
                          {ordersAccess.viewDetails == true ?
                            <td>
                              <button onClick={() => this.props.history.push(`/orderview/${order._id}`)}><i className="fa fa-eye text-primary" aria-hidden="true"></i></button>
                            </td> : null}
                        </tr>
                      )
                    })}
                  </tbody>}
              </table>
            </div>
            {this.state.orderListing.length > 0 ?
              <div className="table-footer text-right">
                <label>Demostración</label>
                <Select showSearch placeholder={<b> {total <= 10 ? total : length}</b>} optionFilterProp="children"
                  onSelect={this.handleChangePageSize.bind(this)}
                  value={this.state.page_ ? pagesize : (length <= pagesize ? length : pagesize)}
                  onSearch={this.handleChange_}>
                  <Option value={10}>10</Option>
                  <Option value={25}>25</Option>
                  <Option value={50}>50</Option>
                  <Option value={75}>75</Option>
                </Select>
                <label>Fuera de {total} Facturas</label>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                </div>
              </div> : null}
          </div>
        </div>
      </Home >
    );
  }
}

const mapStateToProps = state => ({
  getadmindata: state.user.userdatails,
  permissionsList: state.admindata.rolePermissions,
})

export default withRouter(connect(mapStateToProps, actions)(OrderManagement))


