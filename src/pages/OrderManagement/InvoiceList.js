import React, { Component } from 'react'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css';
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import Pagination from 'rc-pagination';
import { Select } from 'antd'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'antd/dist/antd.css'
import isAfter from "date-fns/isAfter";
import { isEmpty } from "lodash"
import swal from 'sweetalert'
import * as actions from '../../actions'
import _ from 'lodash'
import { BeatLoader } from 'react-spinners';
import Home from '../Home'
import moment from 'moment'
import $ from 'jquery';
import Loader from '../Loader'
import axios from 'axios'
import { API_URL, PDF_URL } from '../../config/configs'
class Invoices extends Component {
  constructor(props) {
    super(props)
    this.state = {
      download:false,
      page: 1,
      total: 0,
      pagesize: 15,
      invoicesList: [],
      invoiceId: "",
      createdAt: "",
      billToName: "",
      orderId: "",
      orderedDate: "",
      totalAmount: "",
      orderStatus: "",
      totalmoney: "",
      multipleDelete: [],
      invoiceId1: true,
      createdAt1: true,
      billToName1: true,
      transportType:true,
      orderId1: true,
      orderedDate1: true,
      totalAmount1: true,
      orderStatus1: true,
      invoiceDate: true,
      transportType_:'',
      isActive: true,
      sort: {},
      sortData: { invoiceId: false, createdAt: false, billToName: false,transportType:false, orderedDate: false, orderId: false, totalAmount: false, orderStatus: false, status: false },

      selectedInvoiceid: [],
      mainoinvoiceId: [],
      selectedOrderid: [],
      mainorderId: [],
      selectedBuyername: [],
      mainBuyername: [],
      orderStatusvalue: "Select",
      //invoice date
      startDate: new Date(),
      endDate: new Date(),
      startDateAdded: '',
      endDateAdded: '',
      //ordered date
      orderstartDate: new Date(),
      orderendDate: new Date(),
      orderstartDateAdded: '',
      orderendDateAdded: '',
      loading: true,
      minTotal: "",
      maxTotal: "",
      ordersAccess: {},
      page_:false
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess&& permissions.rolePermission.ordersAccess.viewInvoice === false) {
      this.props.history.push('/dashboard')
    }
    if (permissions.rolePermission) {
      let { ordersAccess } = permissions.rolePermission
      this.setState({ ordersAccess: ordersAccess })
    }


    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });

    this.invoiceListing('pagesize')
  }


  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { invoicesList } = this.state
    if (this.state.selectAll) {
      invoicesList.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      invoicesList.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }

  checkArray(_id) {
    let { multipleDelete } = this.state;
    if (multipleDelete.includes(_id)) {
      return true;
    }
    else {
      return false;
    }
  }

  onCheckbox(_id, val) {
    var delarray = this.state.multipleDelete;
    if (!delarray.includes(_id)) {
      delarray.push(_id);
    }
    else {
      delarray.splice(delarray.indexOf(_id), 1);
    }
    if (delarray.length != this.state.invoicesList.length) {
      this.setState({ checked: false });
    }
    // if (this)
    this.setState({ multipleDelete: delarray })
  }

  filtermethod = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: name, searchText: e }
    let url = "/invoices/invoiceFieldsList"
    let method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (response && response.data && response.data.data) {
        if (name === 'invoiceId') {
          var idArr = _.uniqBy(data, (e) => {
            return e.invoiceId;
          });
          this.setState({ mainoinvoiceId: idArr })
        }
        if (name === 'orderId') {
          var oIdArr = _.uniqBy(data, (e) => {
            return e.orderId;
          });
          this.setState({ mainorderId: oIdArr })
        }
        if (name === 'billToName') {
          var oIdArr = _.uniqBy(data, (e) => {
            return e.billToName.toString();
          });
          this.setState({ mainBuyername: oIdArr, listofBuyerName: data })
        }
      }
    })
  }
  gettingUserDetails = (Id) => {
    var body = { Id }
    this.props.history.push(`/invoiceView/${'oId'}/${Id}`)
  }
  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { invoiceId1, billToName1,transportType, orderId1, orderedDate1, isActive, invoiceDate, totalAmount1,pagesize } = this.state
    var body = { orderId: orderId1, invoiceId: invoiceId1, invoicedDate: invoiceDate,transportType, orderedDate: orderedDate1, billToName: billToName1, status: isActive, totalAmount: totalAmount1,pageSize:pagesize }
    let url = "/invoices/setFilterForInvoices"
    let method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, res => {
      if (res) {
        this.invoiceListing()
        if(e){
          swal('Actualizado con éxito! !', '', 'success')
        }
      }
    })
  }


  //* ********** SORTING ************************//
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState(
          {
            sort: { [element]: value }
          },
          () => {
            this.invoiceListing()
          }
        )
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }

  // ############################## Invoive Listing ######################
  invoiceListing = (e) => {
    var url = '/invoices/invoicesListing'
    var method = 'post'
    var { page, pagesize, sort, minTotal, maxTotal,transportType_, selectedInvoiceid, selectedBuyername, selectedOrderid, selectedBuyername, sort, selectedBuyername, buyerName, orderStatusvalue, startDateValid, startDateValid, endDateValid, endDateAdded, startDateAdded, invoiceId, orderstartDate, orderstartDateAdded, orderendDate, orderendDateAdded } = this.state
    var { page, pagesize } = this.state
    var data = { }
    if (e==='filter') {
      data.page = 1
    } else if(e==='pagesize') {
      data=data
    }else{
      data.page = page
    }
    if(e==='pagesize'){
      data=data
    }else{
      data.pagesize=pagesize
    }
    var body = data
    var totalAmount = {}
    var invoicedDate = {}
    var orderedDate = {}

    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (selectedInvoiceid != '') {
      data.invoiceId = selectedInvoiceid
    }
    if (selectedOrderid != '') {
      data.orderId = selectedOrderid
    }
    if (selectedBuyername != '') {
      data.billToName = selectedBuyername
    }
    if(transportType_!=''){
      data.transportType=transportType_
    }
    if (minTotal != '') {
      totalAmount.minTotal = JSON.parse(minTotal)
    }
    if (minTotal == '') {
      totalAmount.minTotal = null
    }
    if (maxTotal != '') {
      totalAmount.maxTotal = JSON.parse(maxTotal)
    }
    if (maxTotal == '') {
      totalAmount.maxTotal = null
    }
    if (orderStatusvalue != 'Select') {
      data.status = orderStatusvalue
    }

    //invoce date
    if (startDateAdded != '') {
      invoicedDate.startDate = moment(startDateAdded).format("MM/DD/YYYY")
    }
    if (startDateAdded == '') {
      invoicedDate.startDate = ''
    }
    if (endDateAdded != '') {
      invoicedDate.endDate = moment(endDateAdded).format("MM/DD/YYYY")
    }
    if (endDateAdded == '') {
      invoicedDate.endDate = ''
    }
    if (!isEmpty(invoicedDate)) {
      data.invoicedDate = invoicedDate
    }

    //orderdate
    if (orderstartDateAdded != '') {
      orderedDate.startDate = moment(orderstartDateAdded).format("MM/DD/YYYY")
    }
    if (orderstartDateAdded == '') {
      orderedDate.startDate = ''
    }
    if (orderendDateAdded != '') {
      orderedDate.endDate = moment(orderendDateAdded).format("MM/DD/YYYY")
    }
    if (orderendDateAdded == '') {
      orderedDate.endDate = ''
    }
    if (!isEmpty(orderedDate)) {
      data.orderedDate = orderedDate
    }
    data.invoicedDate = invoicedDate
    data.orderedDate = orderedDate
    data.totalAmount = totalAmount
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (data1.status === 0) {
        this.setState({ invoicesList: [] })
      }
      if (e == 'filter') {
        this.setState({ page: 1 })
      }
      this.setState({ invoicesList: data.invoicesList, total: data.total, length: data.invoicesList.length, loading: false })
      if (data&&data.invoicesList.length <= pagesize) {
        this.setState({ page_: false })
      }
      if (data.manageInvoiceListing) {
        this.setState({
          invoiceId1: data.manageInvoiceListing.invoiceId,
          invoiceDate: data.manageInvoiceListing.invoicedDate,
          billToName1: data.manageInvoiceListing.billToName,
          transportType:data.manageInvoiceListing.transportType,
          orderId1: data.manageInvoiceListing.orderId,
          orderedDate1: data.manageInvoiceListing.orderedDate,
          totalAmount1: data.manageInvoiceListing.totalAmount,
          isActive: data.manageInvoiceListing.status,
          pagesize:data.manageInvoiceListing.pageSize
        })
      }
      if (e == 'filter') {
        this.setState({ page: 1 })
      }
    }
    )
  }


  resetinvoiceListing = (e) => {
    var url = '/invoices/invoicesListing'
    var method = 'post'
    var { page, pagesize, sort, selectedOrderid, } = this.state
    var data = { page: page, pagesize: pagesize }
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (selectedOrderid != '') {
      data.Orderid = selectedOrderid
    }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (data1.status === 0) {
        this.setState({ invoicesList: [] })
      }
      this.setState({
        invoicesList: data.invoicesList, total: data.total, length: data.invoicesList.length,
        selectedInvoiceid: [],
        selectedOrderid: [],
        minTotal: "",
        maxTotal: "",
        startDateAdded: "",
        endDateAdded: "",
        startDateAdded: "",
        orderstartDateAdded: "",
        transportType_:'',
        orderendDateAdded: "",
        selectedBuyername: [],
        orderStatusvalue: "Select",
        selectedBuyername: []
      })
      if (data.manageInvoiceListing) {
        this.setState({
          invoiceId1: data.manageInvoiceListing.invoiceId,
          invoiceDate: data.manageInvoiceListing.invoicedDate,
          billToName1: data.manageInvoiceListing.billToName,
          transportType:data.manageInvoiceListing.transportType,
          orderId1: data.manageInvoiceListing.orderId,
          orderedDate1: data.manageInvoiceListing.orderedDate,
          totalAmount1: data.manageInvoiceListing.totalAmount,
          isActive: data.manageInvoiceListing.status,
        })
      }

    }
    )
  }





  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  handleChangeAdded = ({ startDate, endDate }) => {
    if (startDate === null) {
      startDate = ''
    } else { startDate = startDate || this.state.startDateAdded; }
    if (endDate === null) {
      endDate = ''
    } else { endDate = endDate || this.state.endDateAdded; }
    if (isAfter(startDate, endDate)) {
      endDate = startDate;
    }
    this.setState({ startDateAdded: startDate, endDateAdded: endDate });
  };
  handleChangeStart2 = startDate => this.handleChangeAdded({ startDate });
  handleChangeEnd2 = endDate => this.handleChangeAdded({ endDate });


  //ordered date
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  handleChangeAdded1 = ({ orderstartDate, orderendDate }) => {
    if (orderstartDate === null) {
      orderstartDate = ''
    } else { orderstartDate = orderstartDate || this.state.orderstartDateAdded; }
    if (orderendDate === null) {
      orderendDate = ''
    } else { orderendDate = orderendDate || this.state.orderendDateAdded; }
    if (isAfter(orderstartDate, orderendDate)) {
      orderendDate = orderstartDate;
    }
    this.setState({ orderstartDateAdded: orderstartDate, orderendDateAdded: orderendDate });
  };
  handleChangeStart3 = orderstartDate => this.handleChangeAdded1({ orderstartDate });
  handleChangeEnd3 = orderendDate => this.handleChangeAdded1({ orderendDate });



  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize,
      pagination_:false
    }, () => this.invoiceListing());
    this.setState({ selectAll: false }, () => this.selectAllcheck())

  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 },()=>this.setTableRows(), () => this.invoiceListing());
  }
  handleChange_= (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1 ,page_:true},()=>this.setTableRows(), () => this.invoiceListing());
    }
    //  else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.invoiceListing());
    // }
  }

  // ########################### download PDF #########################
  downloadPDF(type) {
    let { invoiceId1, billToName1, orderId1, orderedDate1, totalAmount1, invoiceDate, isActive, multipleDelete,
      selectedBuyername, selectedInvoiceid, selectedOrderid, minTotal, maxTotal, startDateAdded, endDateAdded } = this.state
      this.setState({download:!this.state.download})
    let token = localStorage.getItem('token')
    var ids = []
    if (type === "totalList") {
      var data = { filteredFields: ["invoiceId", "invoicedDate", "orderId", "orderedDate", "billToName", "totalAmount", "status"] }
    }
    if (type === "filteredList") {
      var data = { filteredFields: [] }
      if (invoiceId1) {
        data.filteredFields.push('invoiceId')
      }
      if (invoiceDate) {
        data.filteredFields.push('invoicedDate')
      }
      if (orderId1) {
        data.filteredFields.push('orderId')
      }
      if (orderedDate1) {
        data.filteredFields.push('orderedDate')
      }
      if (billToName1) {
        data.filteredFields.push('billToName')
      }
      if (totalAmount1) {
        data.filteredFields.push('totalAmount')
      }
      if (isActive) {
        data.filteredFields.push('status')
      }
      if (multipleDelete.length > 0) {
        data.ids = multipleDelete
      }
      if (selectedBuyername != '' || selectedInvoiceid != '' || selectedOrderid != '' || minTotal != '' || maxTotal != '' || startDateAdded != '' || endDateAdded != '') {
        this.state.invoicesList.map(each => {
          ids.push(each._id)
        })
        data.ids = ids
      }
    }
    var body = data
    var url = '/pdf/downloadInvoicePdfFile'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var { data } = response.data
      if (data) {
        this.setState({download:false,multipleDelete:[]})
        window.open(PDF_URL + data.filePathAndName, '_blank');
      }
    })
  }

  render() {
    const Option = Select.Option;
    let { invoiceDate, ordersAccess, isActive, sortData, invoicesList, total, length, page, pagesize, selectedInvoiceid, mainoinvoiceId,
      selectedOrderid, mainBuyername, selectedBuyername, orderStatusvalue, mainorderId, minTotal, maxTotal,transportType,transportType_,
      invoiceId1, billToName1, orderId1, orderedDate1, totalAmount1, } = this.state

    const mainBuyernamedata1 = mainBuyername.map((each) => { return each.billToName });
    const mainBuyernamedata = mainBuyernamedata1.filter(o => !selectedBuyername.includes(o));


    const mainorderIddata1 = mainorderId.map((each) => { return each.orderId });
    const mainorderIddata = mainorderIddata1.filter(o => !selectedOrderid.includes(o));


    const maindata1 = mainoinvoiceId.map((each) => { return each.invoiceId });
    const maindata = maindata1.filter(o => !selectedInvoiceid.includes(o));
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Facturas</h3>
              <ul className="breadcrumb ">
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item'>Sales</li>
                <li className="breadcrumb-item active">Facturas</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
        {this.state.download ?  <Loader/> :null }
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  {ordersAccess.download == true ?
                    <div className="dropdown">
                      <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <span>Herramientas</span>
                      </button>
                      <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a className="dropdown-item" href="#" onClick={this.downloadPDF.bind(this, "totalList")}>Exportar a PDF</a>
                      </div>
                    </div> : null}
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ invoiceId1: !this.state.invoiceId1 })} checked={invoiceId1} /><span></span>ID de factura</label></li>

              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ invoiceDate: !this.state.invoiceDate })} checked={invoiceDate} /><span></span>Fecha de la factura</label></li>

              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ orderId1: !this.state.orderId1 })} checked={orderId1} /><span></span>Solicitar ID</label></li>

              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ orderedDate1: !this.state.orderedDate1 })} checked={orderedDate1} /><span></span>Fecha de orden</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ transportType: !this.state.transportType })} checked={transportType} /><span></span>Tipo de transporte</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ billToName1: !this.state.billToName1 })} checked={billToName1} /><span></span>Bill a nombre</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ isActive: !this.state.isActive })} checked={isActive} /><span></span>Estado</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ totalAmount1: !this.state.totalAmount1 })} checked={totalAmount1} /><span></span>Cantidad total</label></li>
            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.invoiceListing} >
            Reiniciar
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({
              invoiceId1: true, invoiceDate: true, billToName1: true, orderId1: true, orderedDate1: true, page: true, totalAmount1: true,transportType:true,
              isActive: true })}>
              Seleccionar todo
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={()=>this.setTableRows('rows')}>
            Guardar
            </button>

          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>ID de factura</label>
                  <Select
                    mode="multiple"
                    placeholder="ID de factura"
                    value={selectedInvoiceid}
                    onChange={(selectedInvoiceid) => this.setState({ selectedInvoiceid })}
                    onSearch={(e) => this.filtermethod(e, 'invoiceId')}
                    onFocus={(e) => this.filtermethod(e, 'invoiceId')}
                    style={{ width: '100%' }}
                  >
                    {maindata.map(order => (
                      <Select.Option key={order} value={order}>
                        {order}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Facturas de</label>
                  <DatePicker
                    placeholderText="Seleccionar fecha de inicio"
                    className="form-control"
                    selected={this.state.startDateAdded}
                    startDate={this.state.startDateAdded}
                    endDate={this.state.endDateAdded}
                    onChange={this.handleChangeStart2}
                  />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label> Facturas a</label>
                  <DatePicker
                    placeholderText="Seleccionar fecha de finalización"
                    className="form-control"
                    selected={this.state.endDateAdded}
                    selectsEnd
                    startDate={this.state.startDateAdded}
                    endDate={this.state.endDateAdded}
                    onChange={this.handleChangeEnd2}
                  />
                </div>
              </div>




              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Órdenes de</label>
                  <DatePicker
                    placeholderText="Seleccionar fecha de inicio"
                    className="form-control"
                    selected={this.state.orderstartDateAdded}
                    startDate={this.state.orderstartDateAdded}
                    endDate={this.state.orderendDateAdded}
                    onChange={this.handleChangeStart3}
                  />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Órdenes a</label>
                  <DatePicker
                    placeholderText="Seleccionar fecha de finalización"
                    className="form-control"
                    selected={this.state.orderendDateAdded}
                    selectsEnd
                    startDate={this.state.orderstartDateAdded}
                    endDate={this.state.orderendDateAdded}
                    onChange={this.handleChangeEnd3}
                  />
                </div>
              </div>

              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Solicitar ID</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingresar ID de pedido"
                    value={selectedOrderid}
                    onChange={(selectedOrderid) => this.setState({ selectedOrderid })}
                    onSearch={(e) => this.filtermethod(e, 'orderId')}
                    onFocus={(e) => this.filtermethod(e, 'orderId')}
                    style={{ width: '100%' }}
                  >
                    {mainorderIddata.map(order => (
                      <Select.Option key={order} value={order}>
                        {order}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre del comprador</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre del comprador"
                    value={selectedBuyername}
                    onChange={(val) => this.setState({ selectedBuyername: val })}
                    onSearch={(e) => this.filtermethod(e, 'billToName')}
                    onFocus={(e) => this.filtermethod(e, 'billToName')}
                    style={{ width: '100%' }}
                  >
                    {mainBuyernamedata.map(each => (
                      <Select.Option key={each} value={each}>
                        {each}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>

              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Cantidad total</label>
                  <div className="row">
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="minTotal" placeholder="Min Total" id="min_Total" required="" value={minTotal} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="maxTotal" placeholder="Max Total" id="max_Total" required="" value={maxTotal} onChange={this.handleChange} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group" >
                  <label> Estado</label>
                  <select className="form-control" onChange={(e) => this.setState({ orderStatusvalue: e.target.value })} value={orderStatusvalue}>
                    <option value=''>Seleccionar</option>
                    <option value='paid'>pagado</option>
                    <option value='processing'>tratamiento</option>
                    <option value='pending'>pendiente</option>
                    <option value='Cancelled'>Cancelado</option>
                  </select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group" >
                  <label>Tipo de transporte</label>
                  <select className="form-control"  name={transportType_} onChange={(e) => this.setState({ transportType_: e.target.value })} value={transportType_}>
                    <option value=''>Seleccionar</option>
                    <option value='Ship To Warehouse'>Enviar a sucursal</option>
                    <option value='Ship To Home'>Enviar a domicilio</option>
                  </select>
                </div>
              </div>

            </div>
            <hr />
            <div className="pull-right filter-button">
              <div className="dropdown ml-2">
                <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                  <span>Herramientas</span>
                </button>
                <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                  <a className="dropdown-item" href="javascript:;" onClick={this.downloadPDF.bind(this, "filteredList")}>Exportar a PDF</a>
                </div>
              </div>
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.invoiceListing('filter')} >
              Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetinvoiceListing}  >
              Reiniciar
            </button>
            </div>
          </div>
          <div className="card-body">
            <div className="table-responsive">
              <table className="table dataTable with-image row-border hover custom-table table-striped" >
                <thead>
                  <tr>
                    {ordersAccess.download == true ?
                      <th>
                        <div className='checkbox'>
                          <label>
                            <input type="checkbox" className="form-check-input" checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} />
                            <span />
                            <i className='input-helper' />
                          </label>
                        </div>
                      </th> : null}
                    {invoiceId1 ? <th>ID de factura</th> : null}
                    {invoiceDate ? <th sortable-column="createdAt" onClick={this.onSort.bind(this, 'createdAt')} >
                    Fecha de la factura
                      <i aria-hidden='true' className={(sortData['createdAt']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {orderId1 ? <th sortable-column="orderId" onClick={this.onSort.bind(this, 'orderId')} >
                    Solicitar ID
                      <i aria-hidden='true' className={(sortData['orderId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {orderedDate1 ? <th sortable-column="orderedDate" onClick={this.onSort.bind(this, 'orderedDate')}>
                    Fecha de orden
                      <i aria-hidden='true' className={(sortData['orderedDate']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {billToName1 ? <th sortable-column="billToName" onClick={this.onSort.bind(this, 'billToName')}>
                    Nombre del cliente
                      <i aria-hidden='true' className={(sortData['billToName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {transportType ? <th sortable-column="transportType" onClick={this.onSort.bind(this, 'transportType')}>
                    Tipo de transporte
                      <i aria-hidden='true' className={(sortData['transportType']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {isActive ? <th sortable-column="orderStatus" onClick={this.onSort.bind(this, 'orderStatus')}>
                    Estado del pedido
                    <i aria-hidden='true' className={(sortData['orderStatus']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {totalAmount1 ? <th sortable-column="totalAmount" onClick={this.onSort.bind(this, 'totalAmount')}>
                    Cantidad total
                    <i aria-hidden='true' className={(sortData['totalAmount']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    <th> Comportamiento</th>
                  </tr>
                </thead>
                {invoicesList.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                  <tbody>
                    {invoicesList.map((order, Key) => {
                      return (
                        <tr key={order._id} key={Key} >
                          {ordersAccess.download == true ?
                            <td>
                              <div className='checkbox'>
                                <label>
                                  <input type="checkbox" className="form-check-input" checked={(this.checkArray(order._id)) ? true : false} onChange={() => this.onCheckbox(order._id)} />
                                  <span />
                                  <i className='input-helper' />
                                </label>
                              </div>
                            </td> : null}
                          {invoiceId1 ? <td>{order.invoiceId}</td> : null}
                          {invoiceDate ? <td>{moment(order.createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td> : null}
                          {orderId1 ? <td>{order.orderId}</td> : null}
                          {orderedDate1 ? <td>{moment(order.orderedDate.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td> : null}
                          {billToName1 ? <td>{order.billToName}</td> : null}
                          {transportType?  <td>{order.transportType?order.transportType:'-'}</td> : null}
                          {isActive ? <td>{order.orderStatus}</td> : null}
                          {totalAmount1 ? <td>{order.totalAmount.toFixed(2)}</td> : null}
                          <td>
                            <button onClick={() => this.gettingUserDetails(order._id)}><i className="fa fa-eye text-primary" aria-hidden="true"></i></button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>}
              </table>
            </div>
            {this.state.invoicesList.length > 0 ?
              <div className="table-footer text-right">
                <label>Mostrando</label>
                <Select showSearch placeholder={<b> {total <= 10 ? total : length}</b>} optionFilterProp="children"
                  onSelect={this.handleChangePageSize.bind(this)}
                  value={this.state.page_ ? pagesize : (length <=pagesize ?length:pagesize)}
                  onSearch={this.handleChange_}>
                  <Option value={10}>10</Option>
                  <Option value={25}>25</Option>
                  <Option value={50}>50</Option>
                  <Option value={75}>75</Option>
                </Select>
                <label>Fuera de {total} Facturas</label>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                </div>
              </div> : null}
          </div>
        </div>
      </Home >
    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(Invoices)

