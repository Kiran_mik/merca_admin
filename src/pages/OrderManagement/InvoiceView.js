import React, { Component } from 'react'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css'
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import 'antd/dist/antd.css'
import swal from 'sweetalert'
import * as actions from '../../actions'
import moment from 'moment'
import Home from '../Home'
import $ from 'jquery';

class InvoiceView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 5,
      searchText: "",
      billingAddress: {},
      shippingAddress: {},
      orderedProducts: [],
      orderTotals: {},
      buyerName: "",
      createdAt: "",
      customerGroup: "",
      emailId: "",
      ipAddress: "",
      orderId: "",
      orderStatus: "",
      paymentType: "",
      shippingMethod: '',
      shippingTrackingUrl: '',
      TotalDue: "",
      discount: "",
      grandTotal: "",
      refunded: "",
      shippingCost: "",
      subTotal: "",
      taxAmount: "",
      totalPaid: "",
      transportType_:'',
      comments: "",
      status: '',
      emailToCustomer: true,
      invoice_id: "",
      order_id: "",
      ord_id: '',
      paymentDetails: '',
      options: ''
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess && permissions.rolePermission.ordersAccess.viewInvoice === false) {
      this.props.history.push('/dashboard')
    }

    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    var invoiceid = this.props.match.params.iId;
    var order_id = this.props.match.params.oId;
    this.setState({ invoice_id: invoiceid, order_id: order_id }, () => this.invoiceDetails());
  }


  handleComment = (event) => {
    this.setState({
      comments: event.target.value,
      error_comm: false
    });
  }

  submitComment = (e) => {
    var { comments, orderId, ord_id, emailToCustomer } = this.state
    if (comments != '') {
      var data = { type: "orderPlaced", comments: comments, orderId: ord_id, id: orderId, emailToCustomer }
      var body = data
      var token = localStorage.getItem('token')
      let url = "/comments/addComments"
      let method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          swal('Comentario enviado con éxito!', '', 'success')
        }
      })
        .catch(err => console.log(err))
    } else {
      this.setState({ error_comm: true })
    }
  }

  sendEmail = (e) => {
    var { invoice_id } = this.state
    var data = { invoiceId: invoice_id }
    var body = data
    var token = localStorage.getItem('token')
    var url = '/invoices/sendMailInvoice'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      let { data } = response
      if (data.status === 1) {
        swal('Correo electrónico enviado con éxito!', '', 'success')
      } else {
        swal(data.statusCode, 'warning')
      }
    })
      .catch(err => console.log(err))
  }

  invoiceDetails = () => {
    if (this.state.invoice_id) {
      var body = { invoiceId: this.state.invoice_id }
      let token = localStorage.getItem('token')
      var url = '/invoices/getInvoiceDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let data = response.data.data
        if (data.orderDetails.length > 0) {
          let firstName = data.orderDetails[0].firstName;
          let lastName = data.orderDetails[0].lastName;
          var buyerName = firstName + lastName
          if (buyerName) {
            this.setState({ buyerName: buyerName })
          }
        }

        if (data.orderDetails.length > 0) {
          this.setState({
            createdAt: data.orderDetails[0].orderedDate,
            customerGroup: data.orderDetails[0].customerGroup,
            emailId: data.orderDetails[0].emailId,
            ipAddress: data.orderDetails[0].ipAddress,
            orderId: data.orderDetails[0].orderId,
            orderStatus: data.orderDetails[0].orderStatus,
            shippingTrackingUrl: data.orderDetails[0].shippingTrackingUrl,
            shippingMethod: data.orderDetails[0].shippingMethod,
            billingAddress: data.orderDetails[0].billingAddress,
            transportType_:data.orderDetails[0].transportType,
            shippingAddress: data.orderDetails[0].shippingAddress,
            paymentType: data.orderDetails[0].paymentType,
            ord_id: data.orderDetails[0].ord_id
          });
        }
        if (data.orderDetails.length > 0 &&data.orderDetails[0].options) {
          this.setState({options:data.orderDetails[0].options})
        }
        if (data.orderTotals) {
          this.setState({
            TotalDue: data.orderTotals.TotalDue,
            discount: data.orderTotals.discount,
            grandTotal: data.orderTotals.grandTotal,
            refunded: data.orderTotals.refunded,
            shippingCost: data.orderTotals.shippingCost,
            subTotal: data.orderTotals.subTotal,
            taxAmount: data.orderTotals.taxAmount,
            totalPaid: data.orderTotals.totalPaid,
          })
        }
        if (data.invoicedProducts) {
          this.setState({ orderedProducts: data.invoicedProducts });
        }
        if(data.paymentDetails){
          this.setState({paymentDetails:data.paymentDetails})
        }
      })
    }
  }

  render() {
    let { order_id, buyerName, billingAddress, shippingAddress, createdAt, customerGroup, emailId, ipAddress, orderId, orderStatus,paymentDetails, options,
      paymentType, shippingMethod, orderedProducts, comments,transportType_ } = this.state
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Factura</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Órdenes</li>
                <li className="breadcrumb-item active">Facturas</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 col-lg-12 text-md-right">
                <ul className="d-inline-block p-0 mb-0 ul-list">
                  {order_id != 'oId' ?
                    <li onClick={() => this.props.history.push(`/orderview/${order_id}`)}><i className="fa fa-angle-left"></i>atrás</li> :
                    <li onClick={() => this.props.history.push(`/invoices`)}><i className="fa fa-angle-left"></i>atrás</li>}
                  <li onClick={this.sendEmail}>Enviar correo electrónico</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="card-body">
            <div className="tab-content" id="myTabContent">
              <div className="fade show active tab-pane" id="information" role="tabpanel" aria-labelledby="information">
                <div className="Address-info">
                  <div className="card-header p-0 mb-3">
                    <h5>Información de pedido y crédito</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Orden {orderId} </h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Fecha de orden</b></td>
                              <td className="text-right">{moment(createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td>
                            </tr>
                            <tr>
                              <td><b>Estado del pedido</b></td>
                              <td className="text-right">{orderStatus}</td>
                            </tr>
                            <tr>
                              <td><b>IP del Cliente</b></td>
                              <td className="text-right"> {ipAddress}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <h5>Información de la cuenta</h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Nombre del cliente</b></td>
                              <td className="text-right">{buyerName}</td>
                            </tr>
                            <tr>
                              <td><b>Email</b></td>
                              <td className="text-right">{emailId}</td>
                            </tr>
                            <tr>
                              <td><b>Grupo de clientes	</b></td>
                              <td className="text-right">{customerGroup}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="Address-info mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Datos del Domicilio</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Dirección de Envio </h5>
                      {billingAddress.name}<br />
                      {billingAddress.addressLine1}<br />
                      {billingAddress.addressLine2}<br />
                      {billingAddress.state}<br />
                      {billingAddress.zipCode}<br />
                      {billingAddress.city}<br />
                      T:{billingAddress.phone}
                    </div>
                    <div className="col-md-6">
                      <h5>Dirección de Envío </h5>
                      {shippingAddress.name}<br />
                      {shippingAddress.addressLine1}<br />
                      {shippingAddress.addressLine2}<br />
                      {shippingAddress.state}<br />
                      {shippingAddress.zipCode}<br />
                      {shippingAddress.city}<br />
                      T:{shippingAddress.phone}
                    </div>
                  </div>
                </div>

                <div className="payment_shipping mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Pago y método de envío</h5>
                  </div>

                  <div className="row">
                    <div className="col-md-6">
                      <h5>Información del pago</h5>
                      <ul className="comman-list">
                        <li><b>{paymentType}</b></li>
                        {paymentDetails ? <p dangerouslySetInnerHTML={{ __html: `${paymentDetails}` }} /> : null}
                      </ul>
                      {options != '' ? <>
                        <h5>Opción seleccionada</h5>
                        <p>{options}</p></> : null}
                    </div>
                    <div className="col-md-6">
                      <h5>Información de envío y manejo</h5>
                      <p>{shippingMethod ? shippingMethod : '-'} </p> <br />
                      <h5>Lugar de envío</h5>
                      <p>{transportType_ ? transportType_ : '-'}</p>
                    </div>
                  </div>
                </div>
                <div className="item-order mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>artículos ordenados</h5>
                  </div>
                  <div className="table-responsive">
                    <table className="table dataTable with-image row-border hover custom-table table-striped" >
                      <thead>
                        <tr>
                          <th>nombre del producto</th>
                          <th>SKU</th>
                          <th>Precio</th>
                          <th>Cantidad</th>
                          <th>Total parcial</th>
                          <th>Importe del impuesto</th>
                          {/* <th>Discount Amount</th> */}
                          <th>Fila total</th>
                        </tr>
                      </thead>
                      <tbody>
                        {orderedProducts.map((order, id) => {
                          return (
                            <tr key={id}>
                              <td>{order.productName}</td>
                              <td>{order.SKU}</td>
                              <td>${order.price.toFixed(2)}</td>
                              <td>{order.quantity}</td>
                              <td>${order.subTotal.toFixed(2)}</td>
                              <td>${order.taxAmount != undefined ? order.taxAmount : "0.00"}</td>
                              {/* <td>${order.discountAmount.toFixed(2)}</td> */}
                              <td>${order.rawTotal.toFixed(2)}</td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </table>
                    {
                      orderedProducts.length <= 0 ? <p className="text-danger text-center">No se encontraron productos.</p> : null
                    }
                  </div>
                </div>

                <div className="order_total mt-5">
                  <label className="d-block"> <h5>Historial de facturas</h5></label>

                  <div className="card-header p-0 mb-3">
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className='form-group'>
                        <label>Comentario</label>
                        <textarea className='form-control' onChange={this.handleComment} value={comments} name="comments" ></textarea>
                        {this.state.error_comm ? <span className="error-block">* Ingrese comentario</span> : null}
                      </div>
                      <div className='checkbox mt-2'>
                        <label>
                          <input type="checkbox" className="form-check-input" checked={this.state.emailToCustomer} onChange={() => this.setState({ emailToCustomer: !this.state.emailToCustomer })} />
                          <span />
                          <i className='input-helper' />
                          Notificar al cliente por correo electrónico
                        </label>
                      </div>
                      <button className="btn btn-primary mt-2" onClick={this.submitComment}>Enviar comentario</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home >
    );
  }
}
const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(InvoiceView)


