import React, { Component } from 'react'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css'
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import 'antd/dist/antd.css'
import axios from 'axios'
import swal from 'sweetalert'
import { API_URL } from '../../config/configs'
import * as actions from '../../actions'
import Home from '../Home'
import moment from 'moment'
import $ from 'jquery';
import { isEmpty } from "lodash"
var _ = require('lodash')

class NewCreditMemo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 5,
      billingAddress: {},
      shippingAddress: {},
      orderedProducts: [],
      orderTotals: {},
      buyerName: "",
      createdAt: "",
      customerGroup: "",
      emailId: "",
      ipAddress: "",
      orderId: "",
      orderStatus: "",
      paymentType: "",
      shippingMethod: '',
      adjustmentFee: 0,
      adjustmentRefund: 0,
      grandTotal: 0,
      refundShipping: 0,
      errors: {},

      comments: "",
      status: '',
      emailToCustomer: true,
      productsArray: [],
      orderID: "",
      id: "",
      isComment: false,
      appendComment: false,
      emailConfirmation: true,
      sub_total: 0,
      transportType: '',
      paymentDetails: '',
      options: ''
    }
  }




  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess && permissions.rolePermission.ordersAccess.viewMemo === false || permissions.rolePermission.ordersAccess.createMemo === false) {
      this.props.history.push('/dashboard')
    }

    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    var id = this.props.match.params.oId;
    this.setState({ id: id })
    this.getDetails()
  }




  handleComment = (event) => {
    this.setState({
      comments: event.target.value, isComment: false
    });
  }

  // ###########################  get Details #########################
  getDetails = () => {
    var id = this.props.match.params.oId;
    this.setState({ orderID: id })
    if (id) {
      var body = { orderId: id, type: "creditMemo" }
      let token = localStorage.getItem('token')
      var url = '/order/getOrderDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response.data
        let { orderDetails, orderTotals, orderedProducts } = data
        let { details } = orderDetails
        this.setState({
          buyerName: details.firstName + details.lastName,
          emailId: details.emailId,
          ipAddress: details.ipAddress,
          orderId: details.orderId,
          orderStatus: details.orderStatus,
          createdAt: details.createdAt,
          customerGroup: details.customerGroup,
          billingAddress: details.billingAddress,
          shippingAddress: details.shippingAddress,
          paymentType: details.paymentType,
          transportType: details.transportType,
          shippingMethod: details.shippingName,
          emailsent: details.emailOrderConfirmation,
          orderedProducts: orderedProducts,
          adjustmentFee: orderTotals.adjustmentFee,
          adjustmentRefund: orderTotals.adjustmentRefund,
          grandTotal: orderTotals.grandTotal,
          refundShipping: orderTotals.refundShipping,
        })
        if (details.options) {
          this.setState({ options: details.options })
        }
        if (orderDetails && orderDetails.paymentDetails) {
          this.setState({ paymentDetails: orderDetails.paymentDetails })
        }

      })
    }
  }


  // ########################### submitComment #########################
  submitComment = (e) => {
    var { comments, id } = this.state
    if (comments != "") {
      var data = { type: "orderPlaced", comments: comments, orderId: id }
      var body = data
      var token = localStorage.getItem('token')
      var url = '/comments/addComments'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data.status === 1) {
          swal('Comentario enviado con éxito!', '', 'success')
        }
      })
        .catch(err => console.log(err))
    } else {
      this.setState({ isComment: true })
    }
  }

  handleChangeQuantity(event, orderobject) {
    let { orderedProducts } = this.state;
    orderobject.quantity = event.target.value
    this.setState({ orderedProducts })
  }

  validateForm = (val) => {
    let { refundShipping, adjustmentFee, adjustmentRefund, errors } = this.state
    let formIsValid = true
    if (refundShipping > val) {
      formIsValid = false
      errors['refundShipping'] = `* Not more than ${val}`
    }
    if (adjustmentFee > val) {
      formIsValid = false
      errors['adjustmentFee'] = `* Not more than ${val}`
    }
    if (adjustmentRefund > val) {
      formIsValid = false
      errors['adjustmentRefund'] = `* Not more than ${val}`
    }

    this.setState({ errors })
    return formIsValid
  }


  // ###########################  createCreditmemo #########################
  createCreditmemo = (val) => {
    if (this.validateForm(val)) {
      let { productsArray, orderID, orderedProducts, adjustmentFee, adjustmentRefund, id, errors,
        refundShipping, subTotal, emailConfirmation, sub_total } = this.state
      let value = this.state.orderedProducts.map((order, key) => {
        return (order.salePrice && order.quantity ? order.discountAmount ? ((order.salePrice * parseInt(order.quantity)) + order.taxAmount) - (order.discountAmount * order.quantity) : order.salePrice * order.quantity : 0)
      });
      let orderSubtotalData = _.reduce(value, function (acc, cur) {
        return acc + cur;
      });
      this.setState({ productsArray: [] })
      orderedProducts.map((each, key) => {
        var mainorderId = {}
        return (
          mainorderId.productId = each.productId,
          mainorderId.productCount = JSON.parse(each.quantity),
          mainorderId.productName = each.productName,
          mainorderId.price = each.price,
          mainorderId.SKU = each.SKU,
          mainorderId.salePrice = each.salePrice,
          mainorderId.supplierName = each.supplierName,
          mainorderId.brandName = each.brandName,

          productsArray.push(mainorderId),
          mainorderId = {}
        )
      })
      var data = {
        orderId: orderID,
        productsArray,
        adjustmentFee: JSON.parse(adjustmentFee),
        adjustmentRefund: JSON.parse(adjustmentRefund),
        refundShipping: JSON.parse(refundShipping),
        totalAmount: JSON.parse(orderSubtotalData) - JSON.parse(refundShipping) - JSON.parse(adjustmentRefund) - JSON.parse(adjustmentFee),
        emailOrderConfirmation: emailConfirmation
      }

      if (Number(data.totalAmount) < 0) {
        errors['grndTtl'] = `* Not in negitive values`
      } else {
        errors['grndTtl'] = ''
      }
      this.setState({ errors })

      var body = data
      var token = localStorage.getItem('token')
      let url = "/creditMemos/createCreditMemo"
      let method = 'post'
      if (data.totalAmount > 0) {
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          let data = response.data
          if (data.status === 1) {
            swal({
              title: 'CreditMemo creado con éxito!',
              icon: "success",
              dangerMode: true,
            })
              .then(willDelete => {
                this.props.history.push(`/orderview/${id}`)
              });
          } else if (data.statusCode == 427) {
            swal('Los detalles no se encuentran!', '', 'warning')
          }
        })
          .catch(err => console.log(err))
      }
    }
  }
  handleChange = (e, value) => {

    let { errors } = this.state
    var key = e.target.name
    if (e.target.value > value) {
      errors[key] = `* Not more than ${value}`
    } else {
      errors[key] = ''
    }
    this.setState({ errors })
    this.setState({ [e.target.name]: e.target.value })
  }


  render() {
    let { buyerName, billingAddress, transportType, shippingAddress, createdAt, customerGroup, emailId, ipAddress, orderId, paymentDetails, options, errors,
      orderStatus, shippingMethod, orderedProducts, comments, adjustmentFee, adjustmentRefund, paymentType,
      refundShipping, id } = this.state
    let value = this.state.orderedProducts.map((order, key) => {
      return (order.salePrice && order.quantity ? order.discountAmount ? ((order.salePrice * parseInt(order.quantity)) + order.taxAmount) - (order.discountAmount * order.quantity) : order.salePrice * order.quantity : 0)
    });
    let orderSubtotalData = _.reduce(value, function (acc, cur) {
      return acc + cur;
    });
    let grandTotalvalue = function () {
      return orderSubtotalData + refundShipping + adjustmentRefund + adjustmentFee;
    }
    let grandTotalData = _.reduce(grandTotalvalue(), function (acc, cur) {
      return acc + cur;
    });
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Nueva nota</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Sales</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Órdenes</li>
                <li className="breadcrumb-item active">Crear nueva nota</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 col-lg-12 text-md-right">
                <ul className="d-inline-block p-0 mb-0 ul-list">
                  <li><a className="" href="javascript:void()" onClick={() => this.props.history.push(`/orderview/${id}`)}><i className="fa fa-angle-left"></i>Atrás</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div className="card-body">
            <div className="tab-content" id="myTabContent">
              <div className="fade show active tab-pane" id="information" role="tabpanel" aria-labelledby="information">
                <div className="Address-info">
                  <div className="card-header p-0 mb-3">
                    <h5>Información de pedido y cuenta</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Orden # {orderId}</h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Fecha de orden</b></td>
                              <td className="text-right">{moment(createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td>
                            </tr>
                            <tr>
                              <td><b>Estado del pedido</b></td>
                              <td className="text-right">{orderStatus}</td>
                            </tr>
                            <tr>
                              <td><b>Colocado desde IP</b></td>
                              <td className="text-right"> {ipAddress}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <h5>Información de la cuenta</h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Nombre del cliente</b></td>
                              <td className="text-right">{buyerName}</td>
                            </tr>
                            <tr>
                              <td><b>Email</b></td>
                              <td className="text-right">{emailId}</td>
                            </tr>
                            <tr>
                              <td><b>grupo de clientes</b></td>
                              <td className="text-right">{customerGroup}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="Address-info mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Datos del Domicilio</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Dirección de Envio </h5>
                      {billingAddress.name}<br />
                      {billingAddress.addressLine1}<br />
                      {billingAddress.addressLine2}<br />
                      {billingAddress.state}<br />
                      {billingAddress.zipCode}<br />
                      {billingAddress.city}<br />
                      T: {billingAddress.phone}
                    </div>
                    <div className="col-md-6">
                      <h5>Dirección de Envío </h5>
                      {shippingAddress.name}<br />
                      {shippingAddress.addressLine1}<br />
                      {shippingAddress.addressLine2}<br />
                      {shippingAddress.state}<br />
                      {shippingAddress.zipCode}<br />
                      {shippingAddress.city}<br />
                      T:{shippingAddress.phone}
                    </div>
                  </div>
                </div>

                {/* <div className="payment_shipping mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Pago y método de envío</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Información del pago</h5>
                      <ul className="comman-list">
                        <li>{paymentType?paymentType:'-'}</li>
                      </ul>

                    </div>
                    <div className="col-md-6">
                      <h5>Información de envío y manejo</h5>
                      <p>{shippingMethod ? shippingMethod : '-'} <br/> {transportType}</p>
                    </div>
                  </div>
                </div> */}
                <div className="payment_shipping mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Pago y método de envío</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Información del pago</h5>
                      <ul className="comman-list">
                        <li><b>{paymentType}</b></li>
                        {paymentDetails ? <p dangerouslySetInnerHTML={{ __html: `${paymentDetails}` }} /> : null}
                      </ul>
                      {options != '' ? <>
                        <h5>Opción seleccionada</h5>
                        <p>{options}</p></> : null}
                    </div>
                    <div className="col-md-6">
                      <h5>Información de envío y manejo</h5>
                      <p>{shippingMethod ? shippingMethod : '-'} </p> <br />
                      <h5>Lugar de envío</h5>
                      <p>{transportType ? transportType : '-'}</p>
                    </div>
                  </div>
                </div>
                <div className="item-order mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>artículos ordenados</h5>
                  </div>
                  <div className="table-responsive">
                    <table className="table dataTable with-image row-border hover custom-table table-striped" >
                      <thead>
                        <tr>
                          <th>nombre del producto</th>
                          <th>SKU</th>
                          <th>Precio</th>
                          <th>Cantidad</th>
                          <th>Subtotal</th>
                          <th>Total parcial</th>
                          {/* <th>Discount Amount</th> */}
                          <th>Fila total</th>
                        </tr>
                      </thead>
                      <tbody>
                        {orderedProducts.map((order, id) => {
                          return (
                            <tr key={id}>
                              <td>{order.productName}</td>
                              <td>{order.SKU}</td>
                              <td>{order.salePrice}</td>
                              <td><input className='form-control' onChange={(e) => this.handleChangeQuantity(e, order)} type='number' value={order.quantity} /></td>
                              <td>{order.salePrice && order.quantity ? order.salePrice * parseInt(order.quantity) : 0}</td>
                              <td>{order.taxAmount}</td>
                              {/* <td>{(parseInt(order.quantity) * order.discountAmount).toFixed(2)}</td> */}
                              <td>{(((order.salePrice * order.quantity) - (parseInt(order.quantity) * order.discountAmount)) + order.taxAmount).toFixed(2)}</td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="order_total mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Totales de pedidos</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label className="d-block"> <h5>Historial de facturas</h5></label>
                      <div className='form-group'>
                        <label>Comentario</label>
                        <textarea className='form-control' onChange={this.handleComment} value={comments} name="comments" ></textarea>
                        {this.state.isComment ? <span className="error-block">* Ingrese comentario</span> : null}
                      </div>
                      <button className="btn btn-primary mt-2 pull-right" onClick={this.submitComment}>Enviar Commet</button>
                    </div>
                    <div className="col-md-6">
                      <label> <h5>Totales de notas de crédito</h5></label>
                      <table className='table dataTable with-image row-border hover custom-table'>
                        <tbody>
                          <tr className='animated fadeIn'>
                            <td>Total parcial</td>
                            <td className="text-right">$ {orderSubtotalData ? orderSubtotalData.toFixed(2) : "0.00"}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Envío de reembolso (IVA incluido)</td>
                            <td className="text-right"><input type="number" name='refundShipping' onChange={(e) => this.handleChange(e, orderSubtotalData)} className="form-control" value={refundShipping} />{errors.refundShipping ? <p style={{ color: 'red' }}>{errors.refundShipping}</p> : null}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Reembolso de ajuste</td>
                            <td className="text-right"><input type="number" name='adjustmentRefund' onChange={(e) => this.handleChange(e, orderSubtotalData)} className="form-control" value={adjustmentRefund} />{errors.adjustmentRefund ? <p style={{ color: 'red' }}>{errors.adjustmentRefund}</p> : null}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Tarifa de ajuste</td>
                            <td className="text-right"><input type="number" name='adjustmentFee' onChange={(e) => this.handleChange(e, orderSubtotalData)} className="form-control" value={adjustmentFee} />{errors.adjustmentFee ? <p style={{ color: 'red' }}>{errors.adjustmentFee}</p> : null}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td><b>Gran total</b></td>{errors.grndTtl ? <p style={{ color: 'red' }}>{errors.grndTtl}</p> : null}
                            <td className="text-right">
                              <b>${(Number(orderSubtotalData) - Number(adjustmentFee) - Number(adjustmentRefund) - Number(refundShipping)).toFixed(2)}</b>

                            </td>
                          </tr>
                        </tbody>
                      </table>

                      <div className='checkbox mt-2'>
                        <label>
                          <input type="checkbox" className="form-check-input" onChange={() => this.setState({ emailConfirmation: !this.state.emailConfirmation })} checked={this.state.emailConfirmation} />
                          <span />
                          <i className='input-helper' />
                          Copia de la nota de crédito por correo electrónico
                      </label>
                      </div>
                      <button className="btn btn-primary mt-2 float-right" onClick={() => this.createCreditmemo(orderSubtotalData)}>Reembolso sin conexión</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home >

    );
  }
}
const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(NewCreditMemo)


