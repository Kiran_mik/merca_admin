import React, { Component } from 'react'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'antd/dist/antd.css'
import swal from 'sweetalert'
import { API_URL } from '../../config/configs'
import * as actions from '../../actions'
import Home from '../Home'
import moment from 'moment'
import { toast } from 'react-toastify';
import _ from 'lodash'
import $ from 'jquery';
class NewShipment extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 5,
      billingAddress: {},
      shippingAddress: {},
      orderedProducts: [],
      buyerName: "",
      createdAt: "",
      customerGroup: "",
      emailId: "",
      ipAddress: "",
      orderId: "",
      orderStatus: "",
      paymentType: "",
      shippingTrackingUrl: '',
      comments: "",
      status: '',
      emailToCustomer: true,
      shippingName: "",
      shippingId: '',
      errors: {},
      ord_id: "",
      transportType: '',
      paymentDetails: '',
      options: ''
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess && permissions.rolePermission.ordersAccess.viewShipment === false) {
      this.props.history.push('/dashboard')
    }

    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    this.setState({ order_id: this.props.match.params.oId })
    this.getDetails()
  }





  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        error_comm: false
        // errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }
  }





  submitComment = (e) => {
    var { comments, emailToCustomer, orderId } = this.state
    if (comments != '') {
      var data = { type: "orderPlaced", comments: comments, emailToCustomer, id: orderId, orderId: this.state.ord_id }
      var body = data
      var token = localStorage.getItem('token')
      var url = '/comments/addComments'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data.status === 1) {
          swal('Comentario enviado con éxito!', '', 'success')
        }
      })
        .catch(err => console.log(err))
    } else {
      this.setState({ error_comm: true })
    }
  }







  getDetails = () => {
    var id = this.props.match.params.sId;
    if (id) {
      var body = { shipmentId: id, }
      let token = localStorage.getItem('token')
      var url = '/shipments/getShipmentDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response.data
        let datadetails = data.orderDetails
        let dataShippingdetails = data.shippingDetails
        if (datadetails.length > 0) {
          this.setState({
            buyerName: datadetails[0].firstName + " " + datadetails[0].lastName,
            createdAt: datadetails[0].orderedDate,
            customerGroup: datadetails[0].customerGroup,
            emailId: datadetails[0].emailId,
            ipAddress: datadetails[0].ipAddress,
            orderId: datadetails[0].orderId,
            orderStatus: datadetails[0].orderStatus,
            billingAddress: datadetails[0].billingAddress,
            shippingAddress: datadetails[0].shippingAddress,
            paymentType: datadetails[0].paymentType,
            ord_id: datadetails[0].ord_id,
            transportType: datadetails[0].transportType
          })
        }
        if (datadetails.length > 0) {
          this.setState({
            options: datadetails[0].options
          })
        }
        if (data.paymentDetails) {
          this.setState({ paymentDetails: data.paymentDetails })
        }

        if (dataShippingdetails) {
          this.setState({
            shippingTrackingUrl: dataShippingdetails[0].trackingNumber,
            shippingName: dataShippingdetails[0].shippingName,
          })
        }
        if (data.shippingProducts) {
          this.setState({ orderedProducts: data.shippingProducts })
        }
      })
    }
  }
  sendEmail = (e) => {
    var id = this.props.match.params.sId;
    var data = { shipmentId: id }
    var body = data
    var token = localStorage.getItem('token')
    var url = '/shipments/sendMailShipments'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      let { data } = response
      if (data.status === 1) {
        swal('Correo electrónico enviado con éxito!', '', 'success')
      } else {
        swal(data.statusCode, 'warning')
      }
    })
      .catch(err => console.log(err))
  }

  render() {
    let { order_id, orderId, errors, buyerName, paymentType, billingAddress, transportType, shippingTrackingUrl, shippingName,
      shippingAddress, createdAt, customerGroup, emailId, paymentDetails, options,
      ipAddress, orderStatus, orderedProducts, comments, id } = this.state
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Información de pedido y envío</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Ventas</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Pedidos</li>
                <li className="breadcrumb-item active">Información de envío</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 col-lg-12 text-md-right">
                <ul className="d-inline-block p-0 mb-0 ul-list">
                  {order_id != 'oId' ?
                    <li onClick={() => this.props.history.push(`/orderview/${order_id}`)}><i className="fa fa-angle-left"></i>atrás</li> :
                    <li onClick={() => this.props.history.push(`/shipments`)}><i className="fa fa-angle-left"></i>atrás</li>}
                  <li onClick={this.sendEmail} >Send Tracking Details</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="card-body">
            <div className="tab-content" id="myTabContent">
              <div className="fade show active tab-pane" id="information" role="tabpanel" aria-labelledby="information">
                <div className="Address-info">

                  <div className="row">
                    <div className="col-md-6">
                      <h5>Órdene {orderId}</h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Fecha de orden</b></td>
                              <td className="text-right">{moment(createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td>
                            </tr>
                            <tr>
                              <td><b>Estado del pedido</b></td>
                              <td className="text-right">{orderStatus}</td>
                            </tr>
                            <tr>
                              <td><b>Colocado desde IP</b></td>
                              <td className="text-right"> {ipAddress}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <h5>Información de la cuenta</h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Nombre del cliente</b></td>
                              <td className="text-right">{buyerName}</td>
                            </tr>
                            <tr>
                              <td><b>Email</b></td>
                              <td className="text-right">{emailId}</td>
                            </tr>
                            <tr>
                              <td><b>grupo de clientes</b></td>
                              <td className="text-right">{customerGroup}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="Address-info mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Datos del Domicilio</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Dirección de Envio
                      </h5>
                      {billingAddress.name}<br />
                      {billingAddress.addressLine1}<br />
                      {billingAddress.addressLine2}<br />
                      {billingAddress.state}<br />
                      {billingAddress.zipCode}<br />
                      {billingAddress.city}<br />
                      T:{billingAddress.phone}
                    </div>
                    <div className="col-md-6">
                      <h5>Dirección de Envío
                      </h5>
                      {shippingAddress.name}<br />
                      {shippingAddress.addressLine1}<br />
                      {shippingAddress.addressLine2}<br />
                      {shippingAddress.state}<br />
                      {shippingAddress.zipCode}<br />
                      {shippingAddress.city}<br />
                      T:{shippingAddress.phone}
                    </div>
                  </div>
                </div>

                <div className="payment_shipping mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Pago y método de envío</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      {/* <h5>Información del pago</h5>
                      <ul className="comman-list">
                        <li>{paymentType}</li>
                      </ul> */}
                      <h5>Información del pago</h5>
                      <ul className="comman-list">
                        <li><b>{paymentType}</b></li>
                        {paymentDetails ? <p dangerouslySetInnerHTML={{ __html: `${paymentDetails}` }} /> : null}
                      </ul>
                      {options != '' ? <>
                        <h5>Opción seleccionada</h5>
                        <p>{options}</p></> : null}
                    </div>
                    <div className="col-md-6">
                      <h5>Información de envío y manejo</h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <thead>
                            <tr>
                              <th>Mensajera</th>
                              <th>El número de rastreo</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>{shippingName} </td>
                              <td>{shippingTrackingUrl} </td>
                            </tr>
                          </tbody>
                        </table>
                        <br />
                        <h5>Lugar de envío</h5>
                        <p>{transportType ? transportType : '-'}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item-order mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>artículos ordenados</h5>
                  </div>
                  <div className="table-responsive">
                    <table className="table dataTable with-image row-border hover custom-table table-striped" >
                      <thead>
                        <tr>
                          <th>nombre del producto</th>
                          <th>SKU</th>
                          <th>Cantidad enviada</th>
                        </tr>
                      </thead>

                      <tbody>
                        {orderedProducts.map((order, id) => {
                          return (
                            <tr key={id}>
                              <td>{order.productName}</td>
                              <td>{order.SKU}</td>
                              <td>{order.quantity}</td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="order_total mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Historial de envío</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className='form-group'>
                        <label>Comentario</label>
                        <textarea className='form-control' onChange={this.handleChange} value={comments} name="comments" ></textarea>
                        <span className="error-block"> {this.state.error_comm ? "* Ingrese comentario" : null} </span>
                      </div>
                      <div className='checkbox mt-2'>
                        <label>
                          <input type="checkbox" className="form-check-input" checked={this.state.emailToCustomer} onChange={() => this.setState({ emailToCustomer: !this.state.emailToCustomer })} />
                          <span />
                          <i className='input-helper' />
                          Notificar al cliente por correo electrónico
                        </label>
                      </div>

                      <button className="btn btn-primary mt-2 pull-right" onClick={this.submitComment}>enviar comentario</button>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home >

    );
  }
}



const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(NewShipment)


