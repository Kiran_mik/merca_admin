import React, { Component } from 'react'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css'
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import 'antd/dist/antd.css'
import swal from 'sweetalert'
import * as actions from '../../actions'
import Home from '../Home'
import moment from 'moment'
import $ from 'jquery';

class TransactionView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 5,
      searchText: "",
      OrderInformation: [],
      billingAddress: {},
      shippingAddress: {},
      orderedProducts: [],
      orderTotals: {},
      buyerName: "",
      createdAt: "",
      customerGroup: "",
      emailId: "",
      ipAddress: "",
      orderId: "",
      orderStatus: "",
      paymentType: "",
      shippingMethod: '',
      shippingTrackingUrl: '',
      TotalDue: "",
      discount: "",
      grandTotal: "",
      refunded: "",
      shippingCost: "",
      subTotal: "",
      taxAmount: "",
      totalPaid: "",
      comments: "",
      status: '',
      emailToCustomer: true,
      forStoreFront: true,
      id: "",
      order_id: "",
      transactionid: "",
      orderviewpage: false,
      transport_Type:''
    } 
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess && permissions.rolePermission.ordersAccess.viewTransaction === false) {
      this.props.history.push('/dashboard')
    }

    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    var transactionid = this.props.match.params.tId;
    var order_id = this.props.match.params.oId;
    this.setState({ transactionid: transactionid, order_id: order_id }, () => this.getDetails());
  }

  handleComment = (event) => {
    this.setState({
      comments: event.target.value,
      error_comm: false
    });
  }

  submitComment = (e) => {
    var { comments, emailToCustomer, orderId } = this.state
    if (comments != '') {
      var data = { type: "orderPlaced", comments: comments, emailToCustomer, id: orderId, orderId: this.props.match.params.tId }
      var body = data
      var token = localStorage.getItem('token')
      var url = '/comments/addComments'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data.status === 1) {
          swal('Comentario enviado con éxito!', '', 'success')
        }
      })
        .catch(err => console.log(err))
    } else {
      this.setState({ error_comm: true })
    }
  }


  getDetails = () => {
    if (this.state.transactionid) {
      var body = { orderId: this.state.transactionid, type: "order" }
      let token = localStorage.getItem('token')
      var url = '/order/getOrderDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response.data
        let { orderDetails } = data
        let datadetails = orderDetails.details
        this.setState({
          createdAt: datadetails.createdAt,
          orderStatus: datadetails.orderStatus,
          ipAddress: datadetails.ipAddress,
          customerGroup: datadetails.customerGroup,
          orderId: datadetails.orderId,
          billingAddress: datadetails.billingAddress,
          shippingAddress: datadetails.shippingAddress,
          paymentType: datadetails.paymentType,
          shippingMethod: datadetails.shippingName,
          emailsent: datadetails.emailOrderConfirmation,
          buyerName: datadetails.firstName + " " + datadetails.lastName,
          emailId: datadetails.emailId,
          transport_Type:datadetails.transportType
        })
        if (data.orderTotals) {
          this.setState({
            TotalDue: data.orderTotals.TotalDue.toFixed(2),
            discount: data.orderTotals.discount.toFixed(2),
            grandTotal: data.orderTotals.grandTotal.toFixed(2),
            refunded: data.orderTotals.refunded.toFixed(2),
            shippingCost: data.orderTotals.shippingCost.toFixed(2),
            subTotal: data.orderTotals.subTotal.toFixed(2),
            commission:data.orderTotals.commission.toFixed(2),
            taxAmount: data.orderTotals.taxAmount.toFixed(2),
            totalPaid: data.orderTotals.totalPaid.toFixed(2),
          })
        }
        if (data.orderedProducts.length > 0) {
          this.setState({
            orderedProducts: data.orderedProducts
          })
        }
      })
    }
  }

  render() {
    let { order_id, buyerName, billingAddress, shippingAddress, createdAt, customerGroup, emailId, ipAddress,transport_Type,
      orderId, orderStatus, paymentType, shippingMethod, TotalDue, discount, grandTotal, refunded, shippingCost,
      subTotal,commission ,taxAmount, totalPaid, orderedProducts, comments } = this.state
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Transacción</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Pedidos</li>
                <li className="breadcrumb-item active">Transacción</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 col-lg-12 text-md-right">
                <ul className="d-inline-block p-0 mb-0 ul-list">
                  {order_id != "oId" ?
                    <li onClick={() => this.props.history.push(`/orderview/${order_id}`)}><i className="fa fa-angle-left"></i>atrás</li> :
                    <li onClick={() => this.props.history.push('/transactions')}><i className="fa fa-angle-left"></i>atrás</li>}
                </ul>
              </div>
            </div>
          </div>
          <div className="card-body">
            <div className="tab-content" id="myTabContent">
              <div className="fade show active tab-pane" id="information" role="tabpanel" aria-labelledby="information">
                <div className="Address-info">
                  <div className="card-header p-0 mb-3">
                    <h5>Información de pedido y cuenta</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Orden {orderId}</h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Fecha de orden</b></td>
                              <td className="text-right">{moment(createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td>
                            </tr>
                            <tr>
                              <td><b>Estado del pedido</b></td>
                              <td className="text-right">{orderStatus}</td>
                            </tr>
                            <tr>
                              <td><b>Colocado desde IP</b></td>
                              <td className="text-right"> {ipAddress}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <h5>Información de la cuenta </h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Nombre del cliente</b></td>
                              <td className="text-right">{buyerName}</td>
                            </tr>
                            <tr>
                              <td><b>Email</b></td>
                              <td className="text-right">{emailId}</td>
                            </tr>
                            <tr>
                              <td><b>grupo de clientes	</b></td>
                              <td className="text-right">{customerGroup}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="Address-info mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Datos del Domicilio</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Dirección de Envio</h5>
                      {billingAddress.name}<br />
                      {billingAddress.addressLine1}<br />
                      {billingAddress.addressLine2}<br />
                      {billingAddress.state}<br />
                      {billingAddress.zipCode}<br />
                      {billingAddress.city}<br />
                      T:{billingAddress.phone}
                    </div>
                    <div className="col-md-6">
                      <h5>Dirección de Envío</h5>
                      {shippingAddress.name}<br />
                      {shippingAddress.addressLine1}<br />
                      {shippingAddress.addressLine2}<br />
                      {shippingAddress.state}<br />
                      {shippingAddress.zipCode}<br />
                      {shippingAddress.city}<br />
                      T:{shippingAddress.phone}
                    </div>
                  </div>
                </div>
                <div className="payment_shipping mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Pago y método de envío</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Información del pago</h5>
                      <ul className="comman-list">
                        <li>{paymentType}</li>
                      </ul>
                    </div>
                    {/* <div className="col-md-6">
                      <h5>Shipping & Handling Information</h5>
                      <p>{shippingMethod}</p>
                    </div> */}
                    <div className="col-md-6">
                        <h5>Información de envío y manejo</h5>
                        <p>{shippingMethod ? shippingMethod : '-'}</p>
                        <h5>Lugar de envío</h5>
                      <p>{transport_Type?transport_Type:'-'}</p>
                      </div>
                  </div>
                </div>
                <div className="item-order mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>artículos ordenados</h5>
                  </div>
                  <div className="table-responsive">
                    <table className="table dataTable with-image row-border hover custom-table table-striped" >
                      <thead>
                        <tr>
                          <th>nombre del producto</th>
                          <th>Precio</th>
                          <th>Cantidad</th>
                          <th>Total parcial</th>
                          <th>Importe del impuesto</th>
                          {/* <th>Discount Amount</th> */}
                          <th>Fila total</th>
                        </tr>
                      </thead>
                      <tbody>
                        {orderedProducts.map((order, id) => {
                          return (
                            <tr key={id}>
                              <td>{order.productName}</td>
                              <td>${order.salePrice.toFixed(2)}</td>
                              <td>{order.quantity}</td>
                              <td>${order.subTotal.toFixed(2)}</td>
                              <td>${order.taxAmount.toFixed(2)}</td>
                              {/* <td>${order.discountAmount.toFixed(2)}</td> */}
                              <td>${order.rawTotal.toFixed(2)}</td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </table>
                    {
                      orderedProducts.length <= 0 ? <p className="text-center text-danger">No se encontraron productos</p> : null
                    }
                  </div>
                </div>
                <div className="order_total mt-5">
                  <label className="d-block"> <h5>Transaction History</h5></label>
                  <div className="card-header p-0 mb-3">
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className='form-group'>
                        <label>Comentario</label>
                        <textarea className='form-control' onChange={this.handleComment} value={comments} name="comments" ></textarea>
                        {this.state.error_comm ? <span className="error-block">* Ingrese comentario</span> : null}

                      </div>
                      <div className='checkbox mt-2'>
                        <label>
                          <input type="checkbox" className="form-check-input" onChange={() => this.setState({ emailToCustomer: !this.state.emailToCustomer })} checked={this.state.emailToCustomer} />
                          <span />
                          <i className='input-helper' />
                          Notificar al cliente por correo electrónico
                        </label>
                      </div>

                      <button className="btn btn-primary mt-2" onClick={this.submitComment}>enviar comentario</button>
                    </div>
                    <div className="col-md-6">
                      <label> <h5>Transacción total</h5></label>
                      <table className='table dataTable with-image row-border hover custom-table'>
                        <tbody>
                          <tr className='animated fadeIn'>
                            <td>Total parcial</td>
                            <td className="text-right">{subTotal}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Gastos de envío y manipulación</td>
                            <td className="text-right">{shippingCost}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Descuento	</td>
                            <td className="text-right">${discount}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Impuesto</td>
                            <td className="text-right">${taxAmount}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td><b>Gran total</b></td>
                            <td className="text-right"><b>${grandTotal}</b></td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Comisión Medio de pago</td>
                            <td className="text-right">${commission}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td><b>Total pagado</b></td>
                            <td className="text-right"><b>${totalPaid}</b></td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td><b>Total reembolsado	</b></td>
                            <td className="text-right"><b>${refunded}</b></td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td><b>Total adeudado</b></td>
                            <td className="text-right"><b>${TotalDue}</b></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home >

    );
  }
}



const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(TransactionView)


