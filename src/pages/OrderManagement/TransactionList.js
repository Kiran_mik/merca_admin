import React, { Component } from 'react'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css';
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import Pagination from 'rc-pagination';
import { Select } from 'antd'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'antd/dist/antd.css'
import axios from 'axios'
import isAfter from "date-fns/isAfter";
import { isEmpty } from "lodash"
import swal from 'sweetalert'
import { API_URL, PDF_URL } from '../../config/configs'
import * as actions from '../../actions'
import Home from '../Home'
import moment from 'moment'
import { BeatLoader } from 'react-spinners';
import Loader from '../Loader'
import _ from 'lodash'
import $ from 'jquery';

class Transactions extends Component {
  constructor(props) {
    super(props)
    this.state = {
      download: false,
      page: 1,
      total: 0,
      pagesize: 15,
      transactionList: [],
      transactionId: "",
      createdAt: "",
      paymentType: "",
      orderId: "",
      orderedDate: "",
      status: "",
      status: "",
      totalmoney: "",
      transportType_: true,
      transactionId1: true,
      paymentType1: true,
      orderId1: true,
      orderedDate1: true,
      status1: true,
      transactionDate1: true,
      sort: {},
      sortData: { transactionId: false, createdAt: false, paymentType: false, status: false, transportType: false },

      selectedtransactionId: [],
      mainotransactionId: [],
      selectedOrderid: [],
      mainorderId: [],
      selectedBuyername: [],
      mainBuyername: [],
      statusvalue: "",
      startDate: new Date(),
      endDate: new Date(),
      startDateAdded: '',
      endDateAdded: '',
      minTotal: "",
      maxTotal: "",
      TransactionType: "",
      Closedtype: "",
      loading: true,
      ordersAccess: {},
      multipleDelete: [],
      page_: false,
      transporttype_: ''
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess && permissions.rolePermission.ordersAccess.viewTransaction === false) {
      this.props.history.push('/dashboard')
    }
    if (permissions.rolePermission) {
      let { ordersAccess } = permissions.rolePermission
      this.setState({ ordersAccess: ordersAccess })
    }

    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    this.transactionsListing('pagesize')
  }

  // ############################# Transaction Listing ######################
  transactionsListing = (e) => {
    var url = '/order/transactionsListing'
    var method = 'post'
    var { page, pagesize, selectedtransactionId, sort, transporttype_, endDateAdded, startDateAdded, statusvalue } = this.state
    var { page, pagesize } = this.state
    var data = {}
    if (e === 'filter') {
      data.page = 1
    } else if (e === 'pagesize') {
      data = data
    } else {
      data.page = page
    }
    if (e === 'pagesize') {
      data = data
    } else {
      data.pagesize = pagesize
    }
    var body = data
    var transactionDate = {}
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (selectedtransactionId != '') {
      data.transactionId = selectedtransactionId
    }
    if (transporttype_ != '') {
      data.transportType = transporttype_
    }
    if (statusvalue != '') {
      data.paymentType = statusvalue
    }
    if (startDateAdded != '') {
      transactionDate.startDate = moment(startDateAdded).format("MM/DD/YYYY")
    }
    if (startDateAdded == '') {
      transactionDate.startDate = ''
    }
    if (endDateAdded != '') {
      transactionDate.endDate = moment(endDateAdded).format("MM/DD/YYYY")
    }
    if (endDateAdded == '') {
      transactionDate.endDate = ''
    }
    if (!isEmpty(transactionDate)) {
      data.transactionDate = transactionDate
    }
    data.transactionDate = transactionDate
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (data1.status === 0) {
        this.setState({ transactionList: [] })
      }
      this.setState({ transactionList: data.transactionList, total: data.total, length: data.transactionList.length, loading: false })
      if (data && data.transactionList.length <= pagesize) {
        this.setState({ page_: false })
      }
      if (data.manageTransactionListing) {
        this.setState({
          transactionId1: data.manageTransactionListing.transactionId,
          transactionDate1: data.manageTransactionListing.transactionDate,
          paymentType1: data.manageTransactionListing.paymentType,
          orderId1: data.manageTransactionListing.orderId,
          orderedDate1: data.manageTransactionListing.orderedDate,
          status1: data.manageTransactionListing.status,
          transportType_: data.manageTransactionListing.transportType,
          pagesize: data.manageTransactionListing.pageSize,
        })
      }
      if (e == 'filter') {
        this.setState({ page: 1 })
      }
    }
    )
  }

  // ############################# Reset Listing ######################
  resetListing = () => {
    var url = '/order/transactionsListing'
    var method = 'post'
    var { page, pagesize } = this.state
    var data = { page: 1, pagesize }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (data1.status === 0) {
        this.setState({ transactionList: [] })
      }
      this.setState({
        transactionList: data.transactionList, total: data.total, length: data.transactionList.length,
        selectedtransactionId: [],
        startDateAdded: "",
        endDateAdded: "",
        orderstartDateAdded: "",
        orderendDateAdded: "",
        statusvalue: "",
        transporttype_: ''
      })
      if (data.manageTransactionListing) {
        this.setState({
          transactionId1: data.manageTransactionListing.transactionId,
          transactionDate1: data.manageTransactionListing.transactionDate,
          paymentType1: data.manageTransactionListing.paymentType,
          orderId1: data.manageTransactionListing.orderId,
          orderedDate1: data.manageTransactionListing.orderedDate,
          status1: data.manageTransactionListing.status,
          transportType_: data.manageTransactionListing.transportType

        })
      }
    }
    )
  }

  // ############################# Filter Listing ######################
  filtermethod = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: name, searchText: e }
    var url = '/order/transactionFieldsList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var data = response.data.data
      if (response && response.data && response.data.data) {
        if (name === 'transactionId') {
          var tIdArr = _.uniqBy(data, (e) => {
            return e.transactionId;
          });
          this.setState({ mainotransactionId: tIdArr })
        }
      }
    })
  }

  // ############################# Set TableRows ######################
  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { transactionId1, transactionDate1, transportType_, paymentType1, orderId1, orderedDate1, sortData1, sort1, status1, transactionList1, pagesize } = this.state
    var body = { transactionId: transactionId1, transactionDate: transactionDate1, paymentType: paymentType1, orderId: orderId1, transportType: transportType_, orderedDate: orderedDate1, status: status1, pageSize: pagesize }
    var url = '/order/setFilterFortransactions'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      if (response) {
        this.transactionsListing()
        if (e) {
          swal('Actualizado exitosamente !', '', 'success')
        }
      }
    })
  }

  //******************** SORTING ************************
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState(
          {
            sort: { [element]: value }
          },
          () => {
            this.transactionsListing()
          }
        )
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }


  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }


  handleChangeAdded = ({ startDate, endDate }) => {
    if (startDate === null) {
      startDate = ''
    } else { startDate = startDate || this.state.startDateAdded; }
    if (endDate === null) {
      endDate = ''
    } else { endDate = endDate || this.state.endDateAdded; }
    if (isAfter(startDate, endDate)) {
      endDate = startDate;
    }
    this.setState({ startDateAdded: startDate, endDateAdded: endDate });
  };
  handleChangeStart = startDate => this.handleChangeAdded({ startDate });
  handleChangeEnd = endDate => this.handleChangeAdded({ endDate });


  paginationChange(page, pagesize) {
    this.setState({ page: page, pagesize: pagesize },
      () => this.transactionsListing());
    this.setState({ selectAll: false }, () => this.selectAllcheck())
  }

  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 }, () => this.setTableRows(), () => this.transactionsListing());
  }
  handleChange_ = (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1, page_: true }, () => this.setTableRows(), () => this.transactionsListing());
    }
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.transactionsListing());
    // }
  }

  checkArray(_id) {
    let { multipleDelete } = this.state;
    if (multipleDelete.includes(_id)) {
      return true;
    }
    else {
      return false;
    }
  }

  onCheckbox(_id) {
    var delarray = this.state.multipleDelete;
    if (!delarray.includes(_id)) {
      delarray.push(_id);
    }
    else {
      delarray.splice(delarray.indexOf(_id), 1);
    }
    if (delarray.length != this.state.transactionList.length) {
      this.setState({ checked: false });
    }
    this.setState({ multipleDelete: delarray })
  }
  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { transactionList } = this.state
    if (this.state.selectAll) {
      transactionList.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      transactionList.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }

  // ########################### download PDF #########################

  downloadPDF(type) {
    let { transactionId1, paymentType1, orderId1, transactionDate1, status1, multipleDelete,
      selectedtransactionId, statusvalue, endDateAdded, startDateAdded } = this.state
    let token = localStorage.getItem('token')
    this.setState({ download: !this.state.download })
    var ids = []
    if (type === "totalList") {
      var data = { filteredFields: ["orderId", "transactionId", "createdAt", "paymentType", "orderStatus"] }
    }
    if (type === "filteredList") {
      var data = { filteredFields: [] }
      if (orderId1) {
        data.filteredFields.push('orderId')
      }
      if (transactionId1) {
        data.filteredFields.push('transactionId')
      }
      if (transactionDate1) {
        data.filteredFields.push('createdAt')
      }
      if (paymentType1) {
        data.filteredFields.push('paymentType')
      }
      if (status1) {
        data.filteredFields.push('orderStatus')
      }
      if (multipleDelete.length > 0) {
        data.ids = multipleDelete
      }
      if (selectedtransactionId != '', statusvalue != '', startDateAdded != '', endDateAdded != '') {
        this.state.transactionList.map(each => {
          ids.push(each._id)
        })
        data.ids = ids
      }
    }
    var body = data
    var url = '/pdf/downloadTransactionsPdfFile'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var { data } = response.data
      if (data) {
        this.setState({ multipleDelete: [], download: false })
        window.open(PDF_URL + data.filePathAndName, '_blank');
      }
    })
  }


  render() {
    const Option = Select.Option;
    let { sortData, ordersAccess, transportType_, transporttype_, transactionList, total, length, page, pagesize, selectedtransactionId, mainotransactionId, statusvalue, transactionDate1, transactionId1, paymentType1, orderId1, status1, } = this.state
    const maindata1 = mainotransactionId.map((each) => { return each.transactionId });
    const maindata = maindata1.filter(o => !selectedtransactionId.includes(o));
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Actas</h3>
              <ul className="breadcrumb ">
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item'>Ventas</li>
                <li className="breadcrumb-item active">Actas</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
        {this.state.download ? <Loader /> : null}
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  {ordersAccess.download == true ?
                    <div className="dropdown">
                      <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <span>Herramientas</span>
                      </button>
                      <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a className="dropdown-item" href="#" onClick={this.downloadPDF.bind(this, "totalList")}>Exportar a PDF</a>
                      </div>
                    </div> : null}
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ orderId1: !this.state.orderId1 })} checked={orderId1} /><span></span>Order Id</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ transactionId1: !this.state.transactionId1 })} checked={transactionId1} /><span></span>ID de transacción</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ transactionDate1: !this.state.transactionDate1 })} checked={transactionDate1} /><span></span>Fecha de Transacción</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ paymentType1: !this.state.paymentType1 })} checked={paymentType1} /><span></span>Tipo de pago</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ transportType_: !this.state.transportType_ })} checked={transportType_} /><span></span>Tipo de transporte</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ status1: !this.state.status1 })} checked={status1} /><span></span>Estado</label></li>
            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.transactionsListing} >
              Reiniciar
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({
              transactionId1: true, transactionDate1: true, paymentType1: true, orderId1: true, orderedDate1: true, page: true, status1: true,
              transportType_: true
            })}>
              Seleccionar todo
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setTableRows('rows')}>
              Salvar
            </button>

          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Transaction Id</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingresar ID de transacción"
                    value={selectedtransactionId}
                    onChange={(selectedtransactionId) => this.setState({ selectedtransactionId })}
                    onSearch={(e) => this.filtermethod(e, 'transactionId')}
                    onFocus={(e) => this.filtermethod(e, 'transactionId')}
                    style={{ width: '100%' }}
                  >
                    {maindata.map(order => (
                      <Select.Option key={order} value={order}>
                        {order}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Transacciones de</label>
                  <DatePicker
                    placeholderText="Transacciones de"
                    className="form-control"
                    selected={this.state.startDateAdded}
                    startDate={this.state.startDateAdded}
                    endDate={this.state.endDateAdded}
                    onChange={this.handleChangeStart}
                  />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Transacciones a</label>
                  <DatePicker
                    placeholderText="Transacciones a"
                    className="form-control"
                    selected={this.state.endDateAdded}
                    selectsEnd
                    startDate={this.state.startDateAdded}
                    endDate={this.state.endDateAdded}
                    onChange={this.handleChangeEnd}
                  />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group" >
                  <label> Método de pago</label>
                  <select className="form-control" onChange={(e) => this.setState({ statusvalue: e.target.value })} value={statusvalue}>
                    <option value='' disabled>Seleccionar</option>
                    <option value='Tarjeta de crédito (1 pago)'>Tarjeta de crédito (1 pago)</option>
                    <option value='Pago Fácil/Rapipago/CobroExpress›'>Pago Fácil/Rapipago/CobroExpress›</option>
                    <option value='Depósito/Transferencia Bancaria'>Depósito/Transferencia Bancaria</option>
                    <option value='Mercado Pago'>Mercadopago</option>
                  </select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group" >
                  <label>Transport Type</label>
                  <select className="form-control" name={transporttype_} onChange={(e) => this.setState({ transporttype_: e.target.value })} value={transporttype_}>
                    <option value=''>Select</option>
                    <option value='Enviar a sucursal'>Enviar a sucursal</option>
                    <option value='Enviar a domicilio'>Enviar a domicilio</option>
                  </select>
                </div>
              </div>
            </div>
            <hr />
            <div className="pull-right filter-button">
              <div className="dropdown ml-2">
                <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                  <span>Herramientas</span>
                </button>
                <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                  <a className="dropdown-item" href="#" onClick={this.downloadPDF.bind(this, "filteredList")}>Exportar a PDF</a>
                </div>
              </div>
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.transactionsListing('filter')} >
                Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetListing}  >
                Reiniciar
            </button>
            </div>
          </div>
          <div className="card-body">
            <div className="table-responsive">
              <table className="table dataTable with-image row-border hover custom-table table-striped" >
                <thead>
                  <tr>
                    <th>
                      <div className='checkbox'>
                        <label>
                          <input type="checkbox" className="form-check-input" checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} />
                          <span />
                          <i className='input-helper' />
                        </label>
                      </div>
                    </th>
                    {orderId1 ? <th sortable-column="orderId" onClick={this.onSort.bind(this, 'orderId')} >
                      Solicitar ID
                      <i aria-hidden='true' className={(sortData['orderId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {transactionId1 ? <th sortable-column="transactionId" onClick={this.onSort.bind(this, 'transactionId')} >
                      ID de transacción
                      <i aria-hidden='true' className={(sortData['transactionId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {transactionDate1 ? <th sortable-column="createdAt" onClick={this.onSort.bind(this, 'createdAt')} >
                      Fecha de Transacción
                      <i aria-hidden='true' className={(sortData['createdAt']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {transportType_ ? <th sortable-column="transportType" onClick={this.onSort.bind(this, 'transportType')} >
                      Tipo de transporte
                      <i aria-hidden='true' className={(sortData['transportType']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {paymentType1 ? <th sortable-column="paymentType" onClick={this.onSort.bind(this, 'paymentType')} >
                      Tipo de pago
                      <i aria-hidden='true' className={(sortData['paymentType']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {status1 ? <th sortable-column="status" onClick={this.onSort.bind(this, 'status')}>
                      Estado
                    <i aria-hidden='true' className={(sortData['status']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    <th> Comportamiento</th>
                  </tr>
                </thead>
                {transactionList.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No Records found</td></tr></tbody> :
                  <tbody>
                    {transactionList.map((order, id) => {
                      return (
                        <tr key={id} >
                          <td>
                            <div className='checkbox'>
                              <label>
                                <input type="checkbox" className="form-check-input" checked={(this.checkArray(order._id)) ? true : false} onChange={() => this.onCheckbox(order._id)} />
                                <span />
                                <i className='input-helper' />
                              </label>
                            </div>
                          </td>
                          {orderId1 ? <td>{order.orderId}</td> : null}
                          {transactionId1 ? <td>{order.transactionId ? order.transactionId : "-"}</td> : null}
                          {transactionDate1 ? <td>{moment(order.createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td> : null}
                          {transportType_ ? <td>{order.transportType ? order.transportType : "-"}</td> : null}
                          {paymentType1 ? <td>{order.paymentType}</td> : null}
                          {status1 ? <td>{order.orderStatus}</td> : null}
                          <td>
                            <button onClick={() => this.props.history.push(`/transactionview/${'oId'}/${order._id}`)}><i className="fa fa-eye text-primary" aria-hidden="true"></i></button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>}
              </table>
            </div>
            {this.state.transactionList.length > 0 ?
              <div className="table-footer text-right">
                <label>Demostración</label>
                <Select showSearch placeholder={<b> {total <= 10 ? total : length}</b>} optionFilterProp="children"
                  onSelect={this.handleChangePageSize.bind(this)}
                  value={this.state.page_ ? pagesize : (length <= pagesize ? length : pagesize)}
                  onSearch={this.handleChange_}>
                  <Option value={10}>10</Option>
                  <Option value={25}>25</Option>
                  <Option value={50}>50</Option>
                  <Option value={75}>75</Option>
                </Select>
                <label>Fuera de {total} Actas</label>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                </div>
              </div> : null}
          </div>
        </div>
      </Home >
    );
  }
}
const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(Transactions)
