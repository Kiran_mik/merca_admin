import React, { Component } from 'react'
import 'bootstrap-less'
import { connect } from 'react-redux'
import { Select } from 'antd'
import 'antd/dist/antd.css'
import axios from 'axios'
import swal from 'sweetalert'
import { toast } from 'react-toastify';
import * as actions from '../../actions'
import Home from '../Home'
import moment from 'moment'
import _ from 'lodash'
import $ from 'jquery';
class NewShipment extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: "",
      billingAddress: {},
      shippingAddress: {},
      orderedProducts: [],
      orderTotals: {},
      buyerName: "",
      createdAt: "",
      customerGroup: "",
      emailId: "",
      ipAddress: "",
      orderId: "",
      orderStatus: "",
      paymentType: "",
      transportType:'',
      shippingMethod: [],
      shipping_method: [],
      qty_error: false,
      TotalDue: "",
      discount: "",
      grandTotal: "",
      refunded: "",
      shippingCost: "",
      subTotal: "",
      taxAmount: "",
      comments: "",
      status: '',
      emailToCustomer: true,
      forStoreFront: true,
      _id: "",
      shippingName: "",
      selectedExistingMethod: [],
      tracking_id: "",
      productsArray: [],
      orderID: "",
      id: "",
      trackingUrl: '',
      errors: {},
      emailConfirmation: true,
      paymentDetails:'',
      options:''

    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.ordersAccess && permissions.rolePermission.ordersAccess.viewShipment === false || permissions.rolePermission.ordersAccess.createShipment === false) {
      this.props.history.push('/dashboard')
    }

    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    var id = this.props.match.params.oId;
    this.setState({ id: id })
    this.getDetails()
  }


  // ########################### Listing Shipping Methods ######################### 
  selectShipmentMethod = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: name, searchText: e }
    var url = '/order/getTransport'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var { data } = response.data
      if (data) {
        var shArr = _.uniqBy(data, (e) => {
          return e.shippingName.toString();
        });
        this.setState({ shippingMethod: shArr, shipping_method: data })
      }
    })
  }



  // ########################### Submit Comment ######################### 
  submitComment = (e) => {
    var { comments, id } = this.state
    if (comments != '') {
      var data = { type: "orderPlaced", comments: comments, orderId: id }
      var body = data
      var token = localStorage.getItem('token')
      var url = '/comments/addComments'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data.status === 1) {
          swal('Comentario enviado con éxito!', '', 'success')
        }
      })
        .catch(err => console.log(err))
    } else {
      this.setState({ isComment: true })
    }
  }

  // ########################### Create Shipment ###########################
  submitShipment = () => {
    if (this.validateForm()) {
      let { productsArray, orderID, trackingUrl, shippingName, orderedProducts, tracking_id, id, emailConfirmation } = this.state
      this.setState({ productsArray: [] })
      orderedProducts.map((each, key) => {
        this.setState({ productsArray: [] })
        var mainorderId = {}
        return (
          mainorderId.productId = each.productId,
          mainorderId.productCount = JSON.parse(each.quantity),
          mainorderId.productName = each.productName,
          mainorderId.price = each.price,
          mainorderId.SKU = each.SKU,
          mainorderId.salePrice = each.salePrice,
          mainorderId.supplierName = each.supplierName,
          mainorderId.brandName = each.brandName,
          mainorderId.productImage=each.productImage,
          mainorderId.customUrl=each.customUrl,
          productsArray.push(mainorderId)
        )
      })
      var data = {
        orderId: orderID,
        productsArray,
        shippingName: shippingName,
        trackingUrl,
        trackingNumber: tracking_id,
        emailOrderConfirmation: emailConfirmation

      }
      var body = data
      var token = localStorage.getItem('token')
      var url = '/shipments/createShipment'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        // axios
        //   .post(API_URL + '/shipments/createShipment', body, {
        //     headers: {
        //       contentType: "application/json",
        //       Authorization: token
        //     }
        //   })
        //   .then(response => {
        let data = response.data
        if (data.statusCode === 228) {
          swal({
            title: 'Envío creado con éxito!',
            icon: "success",
            dangerMode: true,
          })
            .then(willDelete => {
              this.props.history.push(`/orderview/${id}`)
            });
        } else if (data.statusCode === 427) {
          swal('Los detalles no se encuentran!', '', 'warning')
        }
        else {
          swal('Por favor envíe los datos adecuados!', '', 'warning')
        }
      })
        .catch(err => console.log(err))
    }
  }
  // ########################### get Details ###########################
  getDetails = () => {
    var id = this.props.match.params.oId;
    this.setState({ orderID: id })
    if (id) {
      var body = { orderId: id, type: "shipments" }
      let token = localStorage.getItem('token')
      var url = '/order/getOrderDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response.data
        let datadetails = data.orderDetails.details
        this.setState({
          buyerName: datadetails.firstName + " " + datadetails.lastName,
          createdAt: datadetails.createdAt,
          customerGroup: datadetails.customerGroup,
          emailId: datadetails.emailId,
          ipAddress: datadetails.ipAddress,
          orderId: datadetails.orderId,
          orderStatus: datadetails.orderStatus,
          billingAddress: datadetails.billingAddress,
          shippingAddress: datadetails.shippingAddress,
          paymentType: datadetails.paymentType,
          transportType:datadetails.transportType,
          emailsent: datadetails.emailOrderConfirmation,
        })
        if (datadetails.shippingName) {
          this.setState({
            trackingUrl: datadetails.trackingUrl,
            shippingName: datadetails.shippingName,
          })
        } else {
          this.setState({
            trackingUrl: '',
            shippingName: '',
          })
        }
        if (datadetails.options) {
          this.setState({ options: datadetails.options })
        }
        if (data.orderDetails&&data.orderDetails.paymentDetails) {
          this.setState({ paymentDetails: data.orderDetails.paymentDetails })
        }

        if (data.orderedProducts) {
          this.setState({ orderedProducts: data.orderedProducts })
        }
        if (data.orderTotals) {
          this.setState({
            grandTotal: data.orderTotals.grandTotal,
            shippingCost: data.orderTotals.shippingCost,
            subTotal: data.orderTotals.subTotal,
            taxAmount: data.orderTotals.taxAmount,
          })
        }
      })
    }
  }


  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" }),
      });
      if (event.target.name === 'comments') {
        this.setState({ isComment: false })
      }
    }

  }
  handleChangeQuantity(event, orderobject) {
    // let { orderedProducts } = this.state;
    // orderobject.quantity = event.target.value
    // this.setState({ orderedProducts })
    if (event.target.value > orderobject.qty) {
      this.setState({ qty_error: true })
      toast.error("Don't add more than " + orderobject.qty, { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
    }else{
      this.setState({ qty_error: false })
    }
    let { orderedProducts } = this.state;
    orderobject.quantity = event.target.value
    this.setState({ orderedProducts })
  }
  handleShipping(val) {
    let { errors } = this.state
    if (val) {
      errors['shippingName'] = ''
    }
    var idsArray = ''
    var track_url = ''
    this.state.shipping_method.map((each) => {
      if (each.shippingName === val) {
        idsArray = each.shippingName;
        track_url = each.trackingUrl;
      }
      this.setState({ shippingName: val, shippingId: idsArray, trackingUrl: track_url, errors })
    })
  }

  validateForm = () => {
    let { tracking_id, shippingName, errors } = this.state
    let formIsValid = true
    if (!tracking_id || tracking_id.trim() === '') {
      formIsValid = false
      errors['tracking_id'] = '* Se requiere identificación de seguimiento'
    }
    if (shippingName == '') {
      formIsValid = false
      errors['shippingName'] = '* CourierName es obligatorio'
    }
    this.setState({ errors })
    return formIsValid
  }




  render() {
    let { errors, buyerName, orderId, billingAddress, shippingMethod, paymentType,transportType, shippingName,options,paymentDetails,
       shippingAddress, createdAt, customerGroup, emailId, ipAddress, orderStatus, orderedProducts, comments, tracking_id, id } = this.state
    const filteredShipping1 = shippingMethod.map((each) => { return each.shippingName });
    const filteredShipping = filteredShipping1.filter(o => !shippingName.includes(o));
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Nuevo envío</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Ventas</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Pedidos</li>
                <li className="breadcrumb-item active">Nuevo envío</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 col-lg-12 text-md-right">
                <ul className="d-inline-block p-0 mb-0 ul-list">
                  <li onClick={() => this.props.history.push(`/orderview/${id}`)}><i className="fa fa-angle-left"></i>atrás</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="card-body">
            <div className="tab-content" id="myTabContent">
              <div className="fade show active tab-pane" id="information" role="tabpanel" aria-labelledby="information">
                <div className="Address-info">
                  <div className="card-header p-0 mb-3">
                    <h5>Información de pedido y cuenta</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Orden {orderId}</h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Fecha de orden</b></td>
                              <td className="text-right">{moment(createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td>
                            </tr>
                            <tr>
                              <td><b>Estado del pedido</b></td>
                              <td className="text-right">{orderStatus}</td>
                            </tr>
                            <tr>
                              <td><b>Colocado desde IP</b></td>
                              <td className="text-right"> {ipAddress}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <h5>Información de la cuenta </h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Nombre del cliente</b></td>
                              <td className="text-right">{buyerName}</td>
                            </tr>
                            <tr>
                              <td><b>Email</b></td>
                              <td className="text-right">{emailId}</td>
                            </tr>
                            <tr>
                              <td><b>grupo de clientes	</b></td>
                              <td className="text-right">{customerGroup}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="Address-info mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Datos del Domicilio</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Dirección de Envio
                      </h5>
                      {billingAddress.name}<br />
                      {billingAddress.addressLine1}<br />
                      {billingAddress.addressLine2}<br />
                      {billingAddress.state}<br />
                      {billingAddress.zipCode}<br />
                      {billingAddress.city}<br />
                      T: {billingAddress.phone}
                    </div>
                    <div className="col-md-6">
                      <h5>Dirección de Envío
                      </h5>
                      {shippingAddress.name}<br />
                      {shippingAddress.addressLine1}<br />
                      {shippingAddress.addressLine2}<br />
                      {shippingAddress.state}<br />
                      {shippingAddress.zipCode}<br />
                      {shippingAddress.city}<br />
                      T:{shippingAddress.phone}
                    </div>
                  </div>
                </div>

                <div className="payment_shipping mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Pago y método de envío</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Información del pago</h5>
                      <ul className="comman-list">
                        <li><b>{paymentType}</b></li>
                        {paymentDetails? <p dangerouslySetInnerHTML={{ __html: `${paymentDetails}` }} />:null}
                      </ul>
                      {options != '' ? <>
                        <h5>Opción seleccionada</h5>
                        <p>{options}</p></> : null}
                    </div>
                    <div className="col-md-6">
                      <h5>Información de envío y manejo</h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <thead>
                            <tr>
                              <th>Mensajera</th>
                              <th>El número de rastreo</th>
                              <th>Acción</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <Select
                                  showSearch
                                  placeholder="Select Courier Name"
                                  value={this.state.shippingName}
                                  onChange={(val) => this.handleShipping(val)}
                                  onSearch={(e) => this.selectShipmentMethod(e, 'shippingName')}
                                  onFocus={(e) => this.selectShipmentMethod(e, 'shippingName')}
                                  style={{ width: '100%' }}
                                  filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                >
                                  {filteredShipping.map((each, id) => {
                                    return <Select.Option key={each} value={each}>
                                      {each}
                                    </Select.Option>
                                  })}
                                </Select><br /><span className="error-block"> {errors.shippingName} </span>
                              </td>
                              <td>
                                <input type="text" placeholder="Enter Tracking Id" className="form-control" name="tracking_id" value={tracking_id} onChange={this.handleChange} /><br />
                                <span className="error-block"> {errors.tracking_id} </span>
                              </td>
                              <td onClick={() => this.setState({ tracking_id: '', shippingName: '' })}><i className="fa fa-trash fa-2x"></i></td>
                            </tr>
                          </tbody>
                        </table>
                        <h5>Lugar De Envío</h5>
                         <p>{transportType}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item-order mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>artículos ordenados</h5>
                  </div>
                  <div className="table-responsive">
                    <table className="table dataTable with-image row-border hover custom-table table-striped" >
                      <thead>
                        <tr>
                          <th>nombre del producto</th>
                          <th>SKU</th>
                          <th>Cantidad enviada</th>
                        </tr>
                      </thead>

                      <tbody>
                        {orderedProducts.map((order, id) => {
                          return (
                            <tr key={id}>
                              <td>{order.productName}
                              <br/>
                              <span style={{color:'red'}}>  {order.stockQuantity===0 ? 'Agotado' : null} {order.publish?'':'Unpublished product'} </span></td>
                              <td>{order.SKU}</td>
                              <td><input className='form-control' onChange={(e) => this.handleChangeQuantity(e, order)} type='number' value={order.quantity} /></td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="order_total mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Totales de pedidos</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label className="d-block"> <h5>Historial de envío</h5></label>
                      <div className='form-group'>
                        <label>Comentario</label>
                        <textarea className='form-control' onChange={this.handleChange} value={comments} name="comments" ></textarea>
                        {this.state.isComment ? <span className="error-block">* Ingrese comentario</span> : null}

                      </div>
                      <button className="btn btn-primary mt-2 pull-right" onClick={this.submitComment}>enviar comentario</button>
                    </div>
                    <div className="col-md-6">
                      <label className="d-block"> <h5>Opciones de envío</h5></label>

                      <div className='checkbox mt-2'>
                        <label>
                          <input type="checkbox" className="form-check-input" onChange={() => this.setState({ emailConfirmation: !this.state.emailConfirmation })} checked={this.state.emailConfirmation} />
                          <span />
                          <i className='input-helper' />
                          Copia de envío por correo electrónico
                        </label>
                      </div>
                      <button className="btn btn-primary mt-2 pull-right" onClick={this.submitShipment} disabled={this.state.qty_error ?true :false}>Enviar envío</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home >

    );
  }
}



const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(NewShipment)

