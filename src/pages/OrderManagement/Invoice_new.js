import React, { Component } from 'react'
import 'bootstrap-less'
import { connect } from 'react-redux'
import 'rc-pagination/assets/index.css'
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import 'antd/dist/antd.css'
import swal from 'sweetalert'
import * as actions from '../../actions'
import Home from '../Home'
import moment from 'moment'
import { toast } from 'react-toastify';
import Loader from '../Loader'
import $ from 'jquery';
var _ = require('lodash')
class NewInvoice extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 5,
      searchText: "",
      OrderInformation: [],
      billingAddress: {},
      shippingAddress: {},
      orderedProducts: [],
      orderTotals: {},
      buyerName: "",
      createdAt: "",
      customerGroup: "",
      emailId: "",
      ipAddress: "",
      orderId: "",
      orderStatus: "",
      paymentType: "",
      transportType:'',
      shippingMethod: '',
      shippingTrackingUrl: '',
      grandTotal: 0,
      shippingCost: 0,
      subTotal: 0,
      taxAmount: 0,
      comments: "",
      status: '',
      orderID: '',
      emailToCustomer: true,
      forStoreFront: true,
      productsArray: [],
      id: "",
      isComment: false,
      appendComment: false,
      qty_error: false,
      emailConfirmation:true,
      paymentDetails:'',
      options:'',
      upload: false
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission &&permissions.rolePermission.ordersAccess&& permissions.rolePermission.ordersAccess.createInvoice === false || permissions.rolePermission.ordersAccess.viewInvoice === false) {
      this.props.history.push('/dashboard')
    }

    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    var id = this.props.match.params.oId;
    this.setState({ id: id })
    this.getDetails()
  }

  handleChangeQuantity(event, orderobject) {
    if (event.target.value > orderobject.qty) {
      this.setState({ qty_error: true })
      toast.error("Don't add more than " + orderobject.qty, { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
    }else{
      this.setState({ qty_error: false })
    }
    let { orderedProducts } = this.state;
    orderobject.quantity = event.target.value
    this.setState({ orderedProducts })
  }
  handleComment = (event) => {
    this.setState({
      comments: event.target.value, isComment: false
    });
  }

  // ########################### Submit Comment ###########################
  submitComment = () => {
    var { comments, id ,emailConfirmation} = this.state 
    if (comments === "") {
      this.setState({ isComment: true });
    }
    var data = { type: "orderPlaced", comments: comments, orderId: id,emailToCustomer:emailConfirmation }
    var body = data
    var token = localStorage.getItem('token')
    let url = "/comments/addComments"
    let method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data.status === 1) {
        swal('Comentario enviado con éxito!', '', 'success')
      }
    })
      .catch(err => console.log(err))
  }
  // ########################### Create Invoice ###########################
  createInvoice = () => {
    this.setState({ upload: true })
    let { productsArray, orderID, orderedProducts, id, qty_error,emailConfirmation } = this.state
    this.setState({ productsArray: [] })
    orderedProducts.map((each, key) => {
      var mainorderId = {}
      return (
        mainorderId.productId = each.productId,
        mainorderId.productCount = JSON.parse(each.quantity),
        mainorderId.productName=each.productName,
        mainorderId.price = each.price,
        mainorderId.SKU = each.SKU,
        mainorderId.salePrice = each.salePrice,
        mainorderId.supplierName=each.supplierName,
        mainorderId.brandName=each.brandName,
        mainorderId.discount = each.discount,
        productsArray.push(mainorderId)
      )
    })
    var data = {
      orderId: orderID,
      productsArray,
      emailOrderConfirmation:emailConfirmation
    }
    var body = data
    var token = localStorage.getItem('token')
    let url = "/invoices/createInvoice"
    let method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      this.setState({ upload: false })
      let data = response.data
      var msg = data.data + " " + data.message
      if (data.statusCode === 228) {
        swal({
          title: 'Factura creada con éxito!',
          icon: "success",
          dangerMode: true,
        })
          .then(willDelete => {
            // if (willDelete) {
              this.props.history.push(`/orderview/${id}`)
            // }
          });
      } else if (data.statusCode === 427) {
        swal('Los detalles no se encuentran!', '', 'warning')
      }
      else if (data.statusCode === 437) {
        swal(msg, '', 'warning')
      }
    })
      .catch(err => console.log(err))
    // }
  }
  // ########################### get Details ###########################
  getDetails = () => {
    this.setState({ orderID: this.props.match.params.oId })
    var id = this.props.match.params.oId;
    if (id) {
      var body = { orderId: id, type: "invoice" }
      let token = localStorage.getItem('token')
      var url = '/order/getOrderDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response.data
        let datadetails = data.orderDetails.details

        this.setState({
          buyerName: datadetails.firstName + " " + datadetails.lastName,
          createdAt: datadetails.createdAt,
          customerGroup: datadetails.customerGroup,
          emailId: datadetails.emailId,
          ipAddress: datadetails.ipAddress,
          orderId: datadetails.orderId,
          orderStatus: datadetails.orderStatus,
          billingAddress: datadetails.billingAddress,
          shippingAddress: datadetails.shippingAddress,
          paymentType: datadetails.paymentType,
          transportType:datadetails.transportType,
          shippingMethod:datadetails.shippingName,
          emailsent: datadetails.emailOrderConfirmation
        })
        if (data.orderedProducts) {
          if(data.orderedProducts.length===0){
            swal('Nada que facturar','','warning')
          }else
          this.setState({ orderedProducts: data.orderedProducts })
        }
        if (datadetails.options) {
          this.setState({ options: datadetails.options })
        }
        if (data.orderDetails&&data.orderDetails.paymentDetails) {
          this.setState({ paymentDetails: data.orderDetails.paymentDetails })
        }
        if (data.orderTotals) {
          this.setState({
            grandTotal: data.orderTotals.grandTotal.toFixed(2),
            shippingCost: data.orderTotals.shippingCost,
            subTotal: data.orderTotals.subTotal,
            taxAmount: data.orderTotals.taxAmount,
          })
        }
      })
    }
  }




  render() {
    let value = this.state.orderedProducts.map((order, key) => {
      return (order.taxAmount + (order.salePrice * order.quantity) - ((order.discount * order.salePrice * order.quantity) / 100))
    });

    let orderSubtotalData = _.reduce(value, function (acc, cur) {
      return acc + cur;
    });

    let { orderId, buyerName, billingAddress, shippingAddress, createdAt, customerGroup, emailId,emailConfirmation,transportType,options,paymentDetails,
      ipAddress, orderStatus, paymentType, shippingMethod, shippingCost, taxAmount, orderedProducts,
      comments, id, qty_error } = this.state
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Nueva factura</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Sales</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/orderlisting')}>Facturas</li>
                <li className="breadcrumb-item active">Nueva factura</li>
              </ul>
            </div>
          </div>
        </div>
        {this.state.upload ? <Loader /> : null}
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 col-lg-12 text-md-right">
                <ul className="d-inline-block p-0 mb-0 ul-list">
                  <li onClick={() => this.props.history.push(`/orderview/${id}`)}><i className="fa fa-angle-left"></i>atrás</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="card-body">
            <div className="tab-content" id="myTabContent">
              <div className="fade show active tab-pane" id="information" role="tabpanel" aria-labelledby="information">
                <div className="Address-info">
                  <div className="card-header p-0 mb-3">
                    <h5>Información de pedido y cuenta</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Orden {orderId}</h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Fecha de orden</b></td>
                              <td className="text-right">{moment(createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td>
                            </tr>
                            <tr>
                              <td><b>Estado del pedido</b></td>
                              <td className="text-right">{orderStatus}</td>
                            </tr>
                            <tr>
                              <td><b>Colocado desde IP</b></td>
                              <td className="text-right"> {ipAddress}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <h5>Información de la cuenta </h5>
                      <div className="table-responsive">
                        <table className="table dataTable with-image row-border hover custom-table">
                          <tbody>
                            <tr>
                              <td><b>Nombre del cliente</b></td>
                              <td className="text-right">{buyerName}</td>
                            </tr>
                            <tr>
                              <td><b>Email</b></td>
                              <td className="text-right">{emailId}</td>
                            </tr>
                            <tr>
                              <td><b>grupo de clientes	</b></td>
                              <td className="text-right">{customerGroup}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="Address-info mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Datos del Domicilio</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Dirección de Envio </h5>
                      {billingAddress.name}<br />
                      {billingAddress.addressLine1}<br />
                      {billingAddress.addressLine2}<br />
                      {billingAddress.state}<br />
                      {billingAddress.zipCode}<br />
                      {billingAddress.city}<br />
                      T:{billingAddress.phone}
                    </div>
                    <div className="col-md-6">
                      <h5>Dirección de Envío </h5>
                      {shippingAddress.name}<br />
                      {shippingAddress.addressLine1}<br />
                      {shippingAddress.addressLine2}<br />
                      {shippingAddress.state}<br />
                      {shippingAddress.zipCode}<br />
                      {shippingAddress.city}<br />
                      T:{shippingAddress.phone}
                    </div>
                  </div>
                </div>
            
                <div className="payment_shipping mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Pago y método de envío</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <h5>Información del pago</h5>
                      <ul className="comman-list">
                        <li><b>{paymentType}</b></li>
                        {paymentDetails? <p dangerouslySetInnerHTML={{ __html: `${paymentDetails}` }} />:null}
                      </ul>
                      {options != '' ? <>
                        <h5>Opción seleccionada</h5>
                        <p>{options}</p></> : null}
                    </div>
                    <div className="col-md-6">
                      <h5>Información de envío y manejo</h5>
                      <p>{shippingMethod ? shippingMethod : '-'} </p> <br />
                      <h5>Lugar de envío</h5>
                      <p>{transportType ? transportType : '-'}</p>
                    </div>
                  </div>
                </div>
                <div className="item-order mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>artículos ordenados</h5>
                  </div>
                  <div className="table-responsive">
                    <table className="table dataTable with-image row-border hover custom-table table-striped" >
                      <thead>
                        <tr>
                          <th>nombre del producto</th>
                          <th>SKU</th>
                          <th>Precio</th>
                          <th>Cantidad</th>
                          <th>Total parcial</th>
                          <th>Importe del impuesto</th>
                          {/* <th>Discount Amount</th> */}
                          <th>Fila total</th>
                        </tr>
                      </thead>
                      {orderedProducts.length>0?
                      <tbody>
                        {orderedProducts.map((order, id) => {
                          return (
                            <tr key={id}>
                              <td>{order.productName} <br/>
                              <span style={{color:'red'}}>  {order.stockQuantity===0 ? 'Agotado' : (order.stockQuantity<order.quantity ? `Only ${order.stockQuantity} units available` :null)}
                              {order.publish?'':'Deshabilitar'}
                               </span>
                              </td>
                              <td>{order.SKU}</td>
                              <td>{order.salePrice}</td>
                              <td><input className='form-control' onChange={(e) => this.handleChangeQuantity(e, order)} type='number' value={order.quantity} /></td>
                              <td>{order.salePrice && order.quantity ? order.salePrice * parseInt(order.quantity) : 0}</td>
                              <td>{order.taxAmount}</td>
                              {/* <td>{(parseInt(order.quantity) * order.discountAmount).toFixed(2)}</td> */}
                              <td>{(((order.salePrice * order.quantity) - (parseInt(order.quantity) * order.discountAmount)) + order.taxAmount).toFixed(2)}</td>
                            </tr>
                          )
                        })}
                      </tbody> :<tbody><tr><td style={{color:'red',textAlign:'center'}}>Nada que facturar</td></tr></tbody>}
                    </table>
                  </div>
                </div>
                <div className="order_total mt-5">
                  <div className="card-header p-0 mb-3">
                    <h5>Totales de pedido</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <label className="d-block"> <h5>Historial de facturas</h5></label>
                      <div className='form-group'>
                        <label>Comentario</label>
                        <textarea className='form-control' onChange={this.handleComment} value={comments} name="comments" ></textarea>
                        {this.state.isComment ? <span className="error-block">* Ingrese comentario</span> : null}
                      </div>
                      <button className="btn btn-primary mt-2 pull-right" onClick={this.submitComment}>Enviar Commet</button>
                    </div>
                    <div className="col-md-6">
                      <label> <h5>Totales de pedidos</h5></label>
                      <table className='table dataTable with-image row-border hover custom-table'>
                        <tbody>
                          <tr className='animated fadeIn'>
                            <td>Total parcial</td>
                            <td className="text-right">$ {orderSubtotalData ? orderSubtotalData.toFixed(2) : "0.00"}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Gastos de envío y manipulación</td>
                            <td className="text-right">$ {shippingCost ? shippingCost.toFixed(2) : "0.00"}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td>Impuesto</td>
                            <td className="text-right">$ {taxAmount ? taxAmount.toFixed(2) : "0.00"}</td>
                          </tr>
                          <tr className='animated fadeIn'>
                            <td><b>Gran total</b></td>
                            {orderSubtotalData && shippingCost && taxAmount ?
                              <td className="text-right"> <b>$ {(Number(orderSubtotalData) + Number(taxAmount) + Number(shippingCost)).toFixed(2)}</b></td>
                              : <td className="text-right">${orderSubtotalData ? orderSubtotalData.toFixed(2) : 0.00}</td>}
                          </tr>
                        </tbody>
                      </table>
                      <div className='checkbox mt-2'>
                        <label>
                          <input type="checkbox" className="form-check-input" onChange={() => this.setState({ emailConfirmation: !this.state.emailConfirmation })} checked={this.state.emailConfirmation} />
                          <span />
                          <i className='input-helper' />
                          Copia de la factura por correo electrónico
                    </label>
                      </div>
                      <button className="btn btn-primary mt-3 pull-right" onClick={this.createInvoice} disabled={qty_error ?true :false}>Enviar factura</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home >
    );
  }
}




const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
})
export default connect(mapStateToProps, actions)(NewInvoice)


