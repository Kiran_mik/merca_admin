import React, { Component } from 'react';
import { connect } from "react-redux";
import { EDITADMIN_PROFILE } from '../actions/types';
import swal from 'sweetalert';
import Home from './Home';
import axios from 'axios'
import * as actions from '../actions';
import { API_URL, IMAGE_URL } from '../config/configs';


class AdminProfilePage extends Component {

	constructor(props) {
		super(props);
		this.state = {
			firstName: "",
			lastName: "",
			emailId: "",
			mobile: "",
			photo: null,
			filepath: null,
			userName: "",
			errors: {
			}
		};
	}


	componentDidMount() {
		this.getAdminProfile();
	}
	validateForm = () => {
		let { firstName, lastName, mobile, errors } = this.state
		var pattern = new RegExp(/^\d{8,20}$/);
		let formIsValid = true
		if (!firstName || firstName.trim() === '') {
			formIsValid = false
			errors['firstName'] = '* Se requiere el nombre'
		}
		if (!lastName || lastName.trim() === '') {
			formIsValid = false
			errors['lastName'] = '* Se requiere el apellido'
		}
		if (!pattern.test(mobile)) {
		  formIsValid = false
		  if(!mobile){
			errors['mobile'] = 'Se requiere número de celular'
		  }else
		  errors['mobile'] = 'Ingrese un número de teléfono celular válido 8 to 20'
		}
		this.setState({ errors })
		return formIsValid
	}
	// ################## Get AdminData ###############
	getAdminProfile = () => {
		var token = localStorage.getItem('token')
		var url = '/auth/getAdminProfile'
		var method = 'get'
		this.props.commonApiCall(url, method, null, token, null, this.props, response => {
			let { data } = response;
			let { firstName, lastName, emailId, mobileNo, photo, userName } = data.data;
			this.setState({ firstName, lastName, emailId, mobile: mobileNo, photo, userName });
		})
	}

	// ################## Edit AdminData ###############
	editAdminProfile = (e) => {
		e.preventDefault();
		if (this.validateForm()) {
			var url = '/auth/editAdminProfile',
				method = 'post',
				{ firstName, lastName, emailId, mobile, photo } = this.state,
				token = localStorage.getItem('token'),
				body = { firstName, lastName, mobileNo: mobile, emailId, photo },
				type = EDITADMIN_PROFILE;
			this.props.commonApiCall(url, method, body, token, type, this.props, (response) => {
				let { data } = response;
				if (data.statusCode === 434) {
					swal('El número de celular debe tener al menos 8 dígitos.', '', 'error')
				}
				if (data.statusCode === 432) {
					swal('Por favor, introduzca un correo electrónico válido', '', 'error')
				}
				if (data.status === 1) {
					swal({
						title: "Actualizado con éxito!",
						icon: "success",
					})
						.then((willDelete) => {
							// if (willDelete) {
							this.props.history.push('/dashboard')
							// }
						})
				}
			})
		}
	}


	fileUploadSubmit = () => {
		var token = localStorage.getItem('token')
		var formData = new FormData();
		var imagefile = document.querySelector('#file');
		formData.append("file", imagefile.files[0]);
		axios.post(API_URL + '/auth/fileUpload', formData, {
			headers: {
				'Content-Type': 'multipart/form-data',
				'Authorization': token
			}
		})
			.then((response) => {
				let { data } = response;

				this.setState({ photo: data.data.filepath })
			})
			.catch(err => console.log(err));
	}


	handleChange = (e) => {
		this.setState({ [e.target.name]: e.target.value })
		// if (e.target.name === 'mobile') {
		// 	var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
		// 	e.target.value = x[1] + x[2] + x[3];
		// 	this.setState({ [e.target.name]: e.target.value })
		// }
		if (e.target.value) {
			this.setState({
				errors: Object.assign(this.state.errors, { [e.target.name]: "" })
			});
		}
	}

	render() {
		let { firstName, lastName, userName, photo, mobile, emailId, filepath, errors } = this.state
		let imagePreview = null;
		if (photo) {
			imagePreview = (<img src={IMAGE_URL + photo} alt={firstName} />);
		} else {
			imagePreview = (<img src={"/assets/images/no-image-icon-4.png"} alt={firstName} />);
		}

		return (
			<div>
				<Home >
					<div className="row">
						<div className="col-md-12">
							<div className="page-header">
								<h3>Perfil</h3>
								<ul className="breadcrumb ">
									<li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
									<li className="breadcrumb-item active">Perfil</li>
								</ul>
							</div>
						</div>
					</div>
					<div className="card">
						<div className="card-header">
							<h5 className="card-title">Información personal</h5>
						</div>
						<div className="card-body">
							<form className="form-sample row">
								<div className="form-group">
									<div className="user-profile-img mr-4">
										<div className="inner-image">
											{imagePreview}
										</div>
										<div className="upload-file">
											<label>
												<input className="file-input" type="file" name="profile_photo" accept="image/*" id="file" onChange={this.fileUploadSubmit.bind(this)} />
											</label>
											<a><i className="fa fa-camera" aria-hidden="true" /></a>
										</div>
									</div>
								</div>
								<div className="col-xl-6 col-md-12 profile-form">
									<div className="form-group row">
										<label className="col-form-label col">Nombre:</label>
										<div className="form-field">
											<input className="form-control" type="text" name="firstName" placeholder="Ingrese nombre" id="firstName" required
												value={firstName}
												onChange={this.handleChange}
											/>
											<span className="error-block"> {errors.firstName} </span>
										</div>
									</div>
									<div className="form-group row">
										<label className="col-form-label col">Apellido:</label>
										<div className="form-field">
											<input className="form-control" type="text" name="lastName" placeholder="Introduzca el apellido" id="lastName" required
												value={lastName} onChange={this.handleChange} />
											<span className="error-block"> {errors.lastName} </span>
										</div>
									</div>

									<div className="form-group row">
										<label className="col-form-label col">Email :</label>
										<div className="form-field">
											<input className="form-control" type="text" name="emailId" placeholder="Ingrese correo electrónico" id="emailId" disabled
												value={emailId} />
											<div className="error-block">

											</div>
										</div>
									</div>
									<div className="form-group row">
										<label className="col-form-label col">Número de celular:</label>
										<div className="form-field">
											<input className="form-control" type="number" name="mobile" placeholder="Ingrese el número celular:" id="mobile" required
												value={mobile} onChange={this.handleChange} />
											<span className="error-block"> {errors.mobile} </span>

										</div>
									</div>
									<hr />
									<div className="form-group">
										<div className="button-group-container pull-right">
											<button type="button" className="btn btn-primary" onClick={this.editAdminProfile}>
												<span>Enviar</span>
											</button>
											<button type="button" className="btn btn-outline-primary" onClick={() => this.props.history.push('/dashboard')}>Cancelar</button>
										</div>
									</div>
								</div>
							</form>
						</div >
					</div >
				</Home>
			</div>

		);
	}
}



export default connect(null, actions)(AdminProfilePage);