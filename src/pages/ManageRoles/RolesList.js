import React, { Component } from 'react'
import Home from '../Home'
import Toggle from 'react-toggle'
import { Select } from 'antd';
import * as actions from '../../actions'
import { connect } from 'react-redux'
import Pagination from 'rc-pagination'
import { BeatLoader } from 'react-spinners';

import swal from 'sweetalert'
import 'antd/dist/antd.css';
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;


class AccessManagement extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 10,
      listOfRoles: [],
      selectedRole: [],
      totalRoles: [],
      publishedvalue: 'Seleccionar',
      multipleDelete: [],
      selectedOption: 'Selecciona aquí',
      length: 0,
      rolesAccess: {

      },
      selectAll: false,
      loading: true,
      page_:false

    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission && permissions.rolePermission.rolesAccess && permissions.rolePermission.rolesAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }

    if (permissions.rolePermission) {
      let { rolesAccess } = permissions.rolePermission
      this.setState({ rolesAccess: rolesAccess })
    }
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    this.roleListing('pagesize')
  }

  // ##############################  Listing ######################
  roleListing = (e) => {
    let { page, pagesize, sort, selectedTransport, publishedvalue, selectedRole } = this.state
    var url = '/role/roleListing'
    var method = 'post'
    var token = localStorage.getItem('token')
    var data = {  }
    if (e==='filter') {
      data.page = 1
    } else if(e==='pagesize') {
      data=data
    }else{
      data.page = page
    }
    if(e==='pagesize'){
      data=data
    }else{
      data.pagesize=pagesize
    }
    if (selectedRole != '') {
      data.role = selectedRole
    }
    if (publishedvalue != 'Select') {
      data.status = publishedvalue
    }
    var body = data
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      let { data } = response.data
      if (response.data.status === 1) {
        this.setState({ listOfRoles: data.roleListing, total: data.total, length: data.roleListing.length, loading: false })
        if (data.roleListing.length <= pagesize) {
          this.setState({ page_: false })
        }

      } else {
        this.setState({ listOfRoles: [] })
      }
      if (e == 'filter') {
        this.setState({ page: 1 })
      }
    }
    )
  }

  setTableRows = () => {
    var token = localStorage.getItem('token')
    let { pagesize } = this.state
    var body = { pageSize:pagesize }
    var url = '/role/setRoleFilter'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      if (response) {
        this.roleListing()
      }
    })
  }
  resetListing = () => {
    let { page, pagesize } = this.state
    var url = '/role/roleListing'
    var method = 'post'
    var token = localStorage.getItem('token')
    var data = { page, pagesize }
    var body = data
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      let { data } = response.data
      if (response.data.status === 1) {
        this.setState({ listOfRoles: data.roleListing, total: data.total, length: data.roleListing.length, selectedRole: [], publishedvalue: 'Seleccionar' })
      } else {
        this.setState({ listOfRoles: [] })
      }
    }
    )
  }

  gettingDetails = (Id) => {
    this.props.history.push(`/editrole/${Id}`)
  }


  // ########################### Change Status #########################
  statusChange(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { roleId: [Id], isActive: status }
    var urlkey = '/role/changeRoleStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        if (status == false) {
          swal('Role Inactivated Successfully !', '', 'success')
        }
        else {
          swal('Role Activated Successfully !', '', 'success')
        }
        this.roleListing()
      } else if (data.message === 'Invalid token') {
        this.props.history.push('/')
      } else {
        swal(data.message, '', 'error')
      }
    })
  }


  // ########################### DeleteDiscount #########################
  deleteRole = (uid) => {
    var delArr = this.state.multipleDelete
    swal({ title: '¿Estás seguro?', icon: 'warning', buttons: true, dangerMode: true }).then(willDelete => {
      if (willDelete) {
        if (delArr.length > 0) {
          var body = { roleId: delArr }
        } else {
          var body = { roleId: [uid] }
        }
        var url = '/role/deleteRole'
        var method = 'post'
        var token = localStorage.getItem('token')
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          let { data } = response
          if (data.status === 1) {
            this.setState({ selectedOption: 'Select' })
            this.roleListing()
            swal('Deleted Successfully', '', 'success')
          } else {
            swal(data.message, '', 'error')
          }
        })

      }
    })
  }

  // ###########################  for Search #########################
  getRoleNames = e => {
    let token = localStorage.getItem('token')
    let url = "/role/roleFieldsList"
    let method = 'post'
    var body = { type: "role", searchText: e }
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data && data.data) {
        this.setState({ totalRoles: data.data })
      }
    })
  }

  checkArray(_id) {
    let { multipleDelete } = this.state
    if (multipleDelete.includes(_id)) {
      return true
    } else {
      return false
    }
  }

  onCheckbox(_id) {
    var delarray = this.state.multipleDelete
    if (!delarray.includes(_id)) {
      delarray.push(_id)
    } else {
      delarray.splice(delarray.indexOf(_id), 1)
    }
    if (delarray.length != this.state.listOfRoles.length) {
      this.setState({ checked: false })
    }
    if (this) this.setState({ multipleDelete: delarray })
  }

  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { listOfRoles } = this.state
    if (this.state.selectAll) {
      listOfRoles.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      listOfRoles.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }


  applyfilter = () => {
    var delArr = this.state.multipleDelete
    if (this.state.selectedOption == 'Delete') {
      if (delArr.length > 0) {
        this.deleteRole(...delArr)
      }
    }
    if (this.state.selectedOption == 'published') {
      if (delArr.length > 0) {
        var body = { roleId: delArr, isActive: true }
      }
    }
    if (this.state.selectedOption == 'unPublished') {
      if (delArr.length > 0) {
        var body = { roleId: delArr, isActive: false }
      }
    }
    var url = '/role/changeRoleStatus'
    var method = 'post'
    var token = localStorage.getItem('token')
    if (delArr.length > 0) {
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status == 1) {
          if (this.state.selectedOption == 'unPublished') {
            swal('Roles no publicados con éxito !', '', 'success')
          }
          else {
            swal('Roles publicados con éxito !', '', 'success')
          }
          this.roleListing()
        }
        this.setState({ multipleDelete: [], selectedOption: 'Selecciona aquí' })
      })
    } else {
      this.setState({ selectedOption: 'Selecciona aquí' })
      swal('Seleccione al menos un rol', '', 'info')
    }

  }


  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize
    }, () => this.roleListing());
    this.setState({ selectAll: false }, () => this.selectAllcheck())

  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 },()=>this.setTableRows(), () => this.roleListing());
  }
  handleChange_ = (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1, page_: true },()=>this.setTableRows(), () => this.roleListing());
    } 
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.roleListing());
    // }
  }

  render() {
    let { rolesAccess, listOfRoles, page, pagesize, total, length, selectedOption, selectedRole, totalRoles, publishedvalue } = this.state
    const Option = Select.Option;
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Gestionar rol</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item active'>Gestionar rol</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />

        <div className='card animated fadeIn'>
          <div className="card-header">
            <div className="data-filter row justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  {rolesAccess.delete ? <>
                    <Select
                      showSearch
                      placeholder={<b>Select</b>}
                      optionFilterProp="children"
                      value={selectedOption}
                      onSelect={(value) => this.setState({ selectedOption: value })}
                      className="applyselect"
                    >
                      <Option value="published">Activo</Option>
                      <Option value="unPublished">Inactivo</Option>
                      <Option value="Delete" >Eliminar</Option>
                    </Select>
                    <button type="button" className="btn btn-primary ml-0 apply_btn" onClick={this.applyfilter}>Aplicar</button></> : null}
                  {rolesAccess.create ? <button className="btn btn-primary" onClick={() => { this.props.history.push('/addrole') }}><i className="fa fa-plus"></i> Añadir nuevo</button> : null}
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre de rol</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre del rol"
                    value={selectedRole}
                    onChange={(selectedRole) => this.setState({ selectedRole })}
                    onSearch={(e) => this.getRoleNames(e)}
                    onFocus={(e) => this.getRoleNames(e)}
                    style={{ width: '100%' }}
                  >
                    {totalRoles.map((each, id) => (
                      <Select.Option key={id} value={each.role}>
                        {each.role}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>

              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Rol Satus</label>
                  <select className="form-control" onChange={(e, value) => this.setState({ publishedvalue: e.target.value })} value={publishedvalue}>
                    <option value="">Seleccionar</option>
                    <option value="published">Activo</option>
                    <option value="unPublished">Inactivo</option>
                  </select>
                </div>
              </div>
            </div>
            <hr />
            <div className="pull-right filter-button">
              {/* <div className="dropdown ml-2">
                <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                  <span>Tools</span>
                </button>
                <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton2">
                  <a className="dropdown-item" href="#">Export to Excel</a>
                  <a className="dropdown-item" href="#">Export to CSV</a>
                </div>
              </div> */}
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.roleListing('filter')} >
              Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetListing}>
              Reiniciar
            </button>
            </div>
          </div>

          <div className='card-body'>
            <div className='table-responsive'>

              <table className='table dataTable with-image row-border hover custom-table table-striped'>
                <thead>
                  <tr>
                    {rolesAccess.delete ?
                      <th><div className="checkbox">
                        <label>
                          <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} /><span></span>
                          <i className="input-helper" />
                        </label>
                      </div>
                      </th> : null}
                    <th>Nombre de rol</th>
                    <th>Estado</th>
                    {rolesAccess.edit == false && rolesAccess.delete == false ? null : <th> Comportamiento</th>}

                  </tr>
                </thead>
                {listOfRoles.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                  <tbody>
                    {listOfRoles.map((each, Key) => {
                      return (
                        <tr key={Key} className='animated fadeIn'>
                          {rolesAccess.delete ?
                            <td width="10%"><div className="checkbox">
                              <label>
                                <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray(each._id)} onChange={() => this.onCheckbox(each._id)} /><span></span>
                                <i className="input-helper" />
                              </label>
                            </div></td> : null}

                          <td>
                            {each.role}
                          </td>
                          <td>
                            <label>
                              <Toggle
                                checked={each.isActive}
                                className='custom-classname'
                                onChange={() => this.statusChange(each.isActive, each._id)}
                              />
                            </label>
                          </td>
                          {rolesAccess.edit == false && rolesAccess.delete == false ? null :
                            <td>
                              {rolesAccess.edit ?
                                <button onClick={() => this.gettingDetails(each._id)}>
                                  <i className='fa fa-pencil-square-o text-primary' aria-hidden='true' />
                                </button> : null}
                              {rolesAccess.delete ? <button onClick={() => this.deleteRole(each._id)}>
                                <i className='fa fa-trash text-danger' aria-hidden='true' />
                              </button> : null}
                            </td>
                          }
                        </tr>
                      )
                    })}
                  </tbody>}
              </table>
            </div>
            {listOfRoles.length > 0 ?
              <div className="table-footer d-flex justify-content-between">
                <div className="table-shorting">
                  <label>Demostración</label>
                  <Select showSearch placeholder={<b> {total <= 5 ? total : length}</b>} optionFilterProp="children"
                    onSelect={this.handleChangePageSize.bind(this)}
                    value={this.state.page_ ? pagesize : (length <=pagesize ?length:pagesize)}
                    onSearch={this.handleChange_}>
                    <Option value={5}>5</Option>
                    <Option value={10}>10</Option>
                    <Option value={15}>15</Option>
                    <Option value={50}>50</Option>
                  </Select>
                  <label>Fuera de {total} Roles</label>
                </div>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                </div>
              </div> : null}
          </div>
        </div>
      </Home>
    )
  }
}
const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(AccessManagement)
