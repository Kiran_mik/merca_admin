import React, { Component } from 'react';
import Home from '../Home'
import * as actions from '../../actions';
import { connect } from "react-redux";
import swal from 'sweetalert'
class EditPermissions extends Component {
  constructor(props) {
    super(props)
    this.state = {
      role: '',
      roleId: '',
      listOfRoles: [],
      adminUsersAccess: {
        create: false,
        delete: false,
        download: false,
        edit: false,
        status: false,
        view: false
      },
      usersAccess: {
        create: false,
        delete: false,
        download: false,
        edit: false,
        status: false,
        viewDetails: false,
        viewList: false
      },
      cmsPagesAccess: {
        edit: false,
        view: false
      },
      emailSettingsAccess: {
        editAdminEmails: false,
        editEmailTemplates: false,
        editEmailnotificationSetting: false,
        status: false,
        viewEmailTemplates: false
      },
      homeBannerAccess: {
        create: false,
        delete: false,
        edit: false,
        view: false
      },
      ordersAccess: {
        cancel: false,
        create: false,
        createInvoice: false,
        createMemo: false,
        createShipment: false,
        download: false,
        editOrder: false,
        viewDetails: false,
        viewInvoice: false,
        viewList: false,
        viewMemo: false,
        viewShipment: false,
        viewTransaction: false
      },
      productCategoryAccess: {
        create: false,
        delete: false,
        download: false,
        edit: false,
        status: false,
        viewList: false
      },
      productsAccess: {
        addBulkProduct: false,
        bulkimageUpload: false,
        create: false,
        delete: false,
        download: false,
        edit: false,
        rebuildbuildProduct: false,
        status: false,
        viewList: false
      },
      rolesAccess: {
        create: false,
        delete: false,
        edit: false,
        viewList: false
      },
      errors: {}

    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission &&permissions.rolePermission.rolesAccess&& permissions.rolePermission.rolesAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }

    this.getAllDetails()
    this.getAllRoles()
  }

  validateForm = () => {
    let { role, errors } = this.state
    let formIsValid = true
    if (!role||role.trim()==='') {
      formIsValid = false
      errors['role'] = '* Se requiere papel'
    }
    // else formIsValid = true
    this.setState({ errors })
    return formIsValid
  }
  getAllRoles = () => {
    let token = localStorage.getItem('token');
    var url = '/role/getAllRole'
    var method = 'get'
    this.props.commonApiCall(url, method, null, token, null, this.props, response => {
      var { data } = response
      if (data.status === 1) {
        this.setState({ listOfRoles: data.data })
      }
    })
  }

  addRole = () => {
    if (this.validateForm()) {

      var url = '/role/addRole'
      var method = 'post'
      let { role, roleId, adminUsersAccess, cmsPagesAccess, usersAccess, emailSettingsAccess, homeBannerAccess, ordersAccess, productCategoryAccess, productsAccess, rolesAccess } = this.state;

      var data = { role, adminUsersAccess, cmsPagesAccess, usersAccess, emailSettingsAccess, homeBannerAccess, ordersAccess, productCategoryAccess, productsAccess, rolesAccess }
      if (roleId != '') {
        data.roleId = roleId
      }
      var body = data
      var token = localStorage.getItem('token')
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          if (roleId != undefined) {
            swal({
              title: "Actualizado con éxito! !",
              icon: "success",
            })
              .then((willDelete) => {
                if (willDelete) {
                  this.props.history.push('/roleslist')
                }
              })
          } else {
            swal({
              title: "Agregado exitosamente !",
              icon: "success",
            })
              .then((willDelete) => {
                if (willDelete) {
                  this.props.history.push('/roleslist')
                }
              })
          }

        }

      }
      )
    }
  }

  getAllDetails = () => {
    var id = this.props.match.params.rId;
    this.setState({ roleId: id })
    if (id) {
      var body = { roleId: id }
      let token = localStorage.getItem('token')
      var url = '/role/getRoleDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data.data) {
          this.setState({
            role: data.data.role,
            adminUsersAccess: data.data.adminUsersAccess,
            usersAccess: data.data.usersAccess,
            cmsPagesAccess: data.data.cmsPagesAccess,
            emailSettingsAccess: data.data.emailSettingsAccess,
            homeBannerAccess: data.data.homeBannerAccess,
            ordersAccess: data.data.ordersAccess,
            productCategoryAccess: data.data.productCategoryAccess,
            productsAccess: data.data.productsAccess,
            rolesAccess: data.data.rolesAccess
          })
        }

      })
    }
  }

  handleChecked(event, key, nameOfObj) {
    let { adminUsersAccess, cmsPagesAccess, usersAccess, emailSettingsAccess, homeBannerAccess, ordersAccess, productCategoryAccess, productsAccess, rolesAccess } = this.state;
    if (nameOfObj === 'adminUsersAccess') {
      adminUsersAccess[key] = event.target.checked;
      this.setState({ adminUsersAccess })
    }
    if (nameOfObj === 'cmsPagesAccess') {
      cmsPagesAccess[key] = event.target.checked;
      this.setState({ cmsPagesAccess })
    }
    if (nameOfObj === 'usersAccess') {
      usersAccess[key] = event.target.checked;
      this.setState({ usersAccess })
    }
    if (nameOfObj === 'emailSettingsAccess') {
      emailSettingsAccess[key] = event.target.checked;
      this.setState({ emailSettingsAccess })
    }
    if (nameOfObj === 'homeBannerAccess') {
      homeBannerAccess[key] = event.target.checked;
      this.setState({ homeBannerAccess })
    } if (nameOfObj === 'ordersAccess') {
      ordersAccess[key] = event.target.checked;
      this.setState({ ordersAccess })
    } if (nameOfObj === 'productCategoryAccess') {
      productCategoryAccess[key] = event.target.checked;
      this.setState({ productCategoryAccess })
    } if (nameOfObj === 'productsAccess') {
      productsAccess[key] = event.target.checked;
      this.setState({ productsAccess })
    } if (nameOfObj === 'rolesAccess') {
      rolesAccess[key] = event.target.checked;
      this.setState({ rolesAccess })
    }
  }
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }
  }
  render() {
    var { role, errors, roleId, listOfRoles, adminUsersAccess, cmsPagesAccess, usersAccess, emailSettingsAccess, homeBannerAccess, ordersAccess, productCategoryAccess, productsAccess, rolesAccess } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Gestión de Acceso</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={()=>this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item' onClick={()=>this.props.history.push('/roleslist')}>Gestión de Acceso </li>
                <li className='breadcrumb-item active'>{roleId == undefined ? "Añadir " : "Editar "} permisos </li>
              </ul>
            </div>
          </div>
        </div>

        {roleId != undefined ?
          <div className='form-group row'>
            <div className="col-md-3 mt-2">
              <h5>Select User Role</h5>
            </div>
            <div className="col-md-9 text-right d-flex justify-content-end">
              <select className="form-control col-md-4 mr-2"
                placeholder={<h5><b>Seleccione uno</b></h5>} value={role} onChange={(e, value) => this.setState({ role: e.target.value })}>
                <option value="">Seleccionar</option>
                {listOfRoles.map((each, id) => {
                  return (
                    <option value={each.role} key={id}>{each.role}</option>
                  )
                })}
              </select>
            </div>
          </div> :
          <div className='card animated fadeIn mb-3'>
            <div className='card-header'>
              <h5 className='card-title'>Nombre</h5>
            </div>
            <div className='card-body'>
              <div className='m-auto'>
                <form className='form-sample'>
                  <div className='form-group row mb-0'>
                    <label className='col-sm-3 col-form-label'> Papel </label>
                    <div className='col-sm-6'>
                      <input
                        className='form-control'
                        type='text'
                        name='role'
                        value={role}
                        placeholder='Introduzca Papel'
                        onChange={this.handleChange}
                      />
                      <span className="error-block"> {errors.role} </span>

                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>}
        <div className='row user-role'>
          <div className="col-md-6  mb-3">
            <div className='card animated fadeIn'>
              <div className='card-header'>
                <h5 className='card-title'>Usuarios administradores</h5>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={adminUsersAccess.download ? true : false} onChange={(event) => this.handleChecked(event, "download", "adminUsersAccess")} /><span></span>
                      Descargar
                      </label>
                  </div>

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={adminUsersAccess.view ? true : false} onChange={(event) => this.handleChecked(event, "view", "adminUsersAccess")} /><span></span>
                      Ver lista
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={adminUsersAccess.create ? true : false} onChange={(event) => this.handleChecked(event, "create", "adminUsersAccess")} /><span></span>
                      Crear
                      </label>
                  </div>

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={adminUsersAccess.edit ? true : false} onChange={(event) => this.handleChecked(event, "edit", "adminUsersAccess")} /><span></span>
                      Editar
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={adminUsersAccess.delete ? true : false} onChange={(event) => this.handleChecked(event, "delete", "adminUsersAccess")} /><span></span>
                      Eliminar
                      </label>
                  </div>

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={adminUsersAccess.status ? true : false} onChange={(event) => this.handleChecked(event, "status", "adminUsersAccess")} /><span></span>
                      Actualización de estado
                      </label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-6 mb-3">
            <div className='card animated fadeIn'>
              <div className='card-header'>
                <h5 className='card-title'>Usuario</h5>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={usersAccess.download ? true : false} onChange={(event) => this.handleChecked(event, "download", "usersAccess")} /><span></span>
                      Descargar
                      </label>
                  </div>

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={usersAccess.viewList ? true : false} onChange={(event) => this.handleChecked(event, "viewList", "usersAccess")} /><span></span>
                      Ver lista
                      </label>
                  </div>

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={usersAccess.delete ? true : false} onChange={(event) => this.handleChecked(event, "delete", "usersAccess")} /><span></span>
                      Eliminar
                      </label>
                  </div>
                  {/* <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={usersAccess.edit ? true : false} onChange={(event) => this.handleChecked(event, "edit", "usersAccess")} /><span></span>
                      Edit
                      </label>
                  </div> */}
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={usersAccess.viewDetails ? true : false} onChange={(event) => this.handleChecked(event, "viewDetails", "usersAccess")} /><span></span>
                      Ver detalles
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={usersAccess.status ? true : false} onChange={(event) => this.handleChecked(event, "status", "usersAccess")} /><span></span>
                      Actualización de estado
                      </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 mb-3">
            <div className='card animated fadeIn'>
              <div className='card-header'>
                <h5 className='card-title'>categoria de producto</h5>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productCategoryAccess.download ? true : false} onChange={(event) => this.handleChecked(event, "download", "productCategoryAccess")} /><span></span>
                      Descargar
                      </label>
                  </div>

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productCategoryAccess.viewList ? true : false} onChange={(event) => this.handleChecked(event, "viewList", "productCategoryAccess")} /><span></span>
                      Ver lista
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productCategoryAccess.create ? true : false} onChange={(event) => this.handleChecked(event, "create", "productCategoryAccess")} /><span></span>
                      Crear
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productCategoryAccess.edit ? true : false} onChange={(event) => this.handleChecked(event, "edit", "productCategoryAccess")} /><span></span>
                      Editar
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productCategoryAccess.delete ? true : false} onChange={(event) => this.handleChecked(event, "delete", "productCategoryAccess")} /><span></span>
                      Eliminar
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productCategoryAccess.status ? true : false} onChange={(event) => this.handleChecked(event, "status", "productCategoryAccess")} /><span></span>
                      Actualización de estado
                      </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 mb-3">
            <div className='card animated fadeIn'>
              <div className='card-header'>
                <h5 className='card-title'>Producto</h5>
              </div>
              <div className="card-body">
                <div className="row">

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productsAccess.download ? true : false} onChange={(event) => this.handleChecked(event, "download", "productsAccess")} /><span></span>
                      Descargar
                      </label>
                  </div>

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productsAccess.viewList ? true : false} onChange={(event) => this.handleChecked(event, "viewList", "productsAccess")} /><span></span>
                      Ver lista
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productsAccess.create ? true : false} onChange={(event) => this.handleChecked(event, "create", "productsAccess")} /><span></span>
                      Crear
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productsAccess.edit ? true : false} onChange={(event) => this.handleChecked(event, "edit", "productsAccess")} /><span></span>
                      Editar
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productsAccess.delete ? true : false} onChange={(event) => this.handleChecked(event, "delete", "productsAccess")} /><span></span>
                      Eliminar
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productsAccess.addBulkProduct ? true : false} onChange={(event) => this.handleChecked(event, "addBulkProduct", "productsAccess")} /><span></span>
                      Agregar productos a granel
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productsAccess.bulkimageUpload ? true : false} onChange={(event) => this.handleChecked(event, "bulkimageUpload", "productsAccess")} /><span></span>
                      Agregar productos a granel
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={productsAccess.status ? true : false} onChange={(event) => this.handleChecked(event, "status", "productsAccess")} /><span></span>
                      Actualización de estado
                      </label>
                  </div>


                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 mb-3">
            <div className='card animated fadeIn'>
              <div className='card-header'>
                <h5 className='card-title'>Pedido</h5>
              </div>
              <div className="card-body">
                <div className="row">

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.download ? true : false} onChange={(event) => this.handleChecked(event, "download", "ordersAccess")} /><span></span>
                      Descargar
                      </label>
                  </div>

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.viewList ? true : false} onChange={(event) => this.handleChecked(event, "viewList", "ordersAccess")} /><span></span>
                      Ver lista
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.create ? true : false} onChange={(event) => this.handleChecked(event, "create", "ordersAccess")} /><span></span>
                      Crear
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.editOrder ? true : false} onChange={(event) => this.handleChecked(event, "editOrder", "ordersAccess")} /><span></span>
                      Edit Order
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.cancel ? true : false} onChange={(event) => this.handleChecked(event, "cancel", "ordersAccess")} /><span></span>
                      Cancel
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.viewDetails ? true : false} onChange={(event) => this.handleChecked(event, "viewDetails", "ordersAccess")} /><span></span>
                      Ver detalles
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.createInvoice ? true : false} onChange={(event) => this.handleChecked(event, "createInvoice", "ordersAccess")} /><span></span>
                      Crear Invoice
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.viewInvoice ? true : false} onChange={(event) => this.handleChecked(event, "viewInvoice", "ordersAccess")} /><span></span>
                      Mirar la factura
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.createShipment ? true : false} onChange={(event) => this.handleChecked(event, "createShipment", "ordersAccess")} /><span></span>
                      Crear envío
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.viewShipment ? true : false} onChange={(event) => this.handleChecked(event, "viewShipment", "ordersAccess")} /><span></span>
                      Ver envío
                      </label>
                  </div>

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.createMemo ? true : false} onChange={(event) => this.handleChecked(event, "createMemo", "ordersAccess")} /><span></span>
                      Crear nota
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.viewMemo ? true : false} onChange={(event) => this.handleChecked(event, "viewMemo", "ordersAccess")} /><span></span>
                      Ver nota
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={ordersAccess.viewTransaction ? true : false} onChange={(event) => this.handleChecked(event, "viewTransaction", "ordersAccess")} /><span></span>
                      Ver transacción
                      </label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-6 mb-3">
            <div className='card animated fadeIn'>
              <div className='card-header'>
                <h5 className='card-title'>Banner de inicio</h5>
              </div>
              <div className="card-body">
                <div className="row">

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={homeBannerAccess.view ? true : false} onChange={(event) => this.handleChecked(event, "view", "homeBannerAccess")} /><span></span>
                      Ver
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={homeBannerAccess.create ? true : false} onChange={(event) => this.handleChecked(event, "create", "homeBannerAccess")} /><span></span>
                      Crear
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={homeBannerAccess.edit ? true : false} onChange={(event) => this.handleChecked(event, "edit", "homeBannerAccess")} /><span></span>
                      Editar
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={homeBannerAccess.delete ? true : false} onChange={(event) => this.handleChecked(event, "delete", "homeBannerAccess")} /><span></span>
                      Eliminar
                      </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 mb-3">
            <div className='card animated fadeIn'>
              <div className='card-header'>
                <h5 className='card-title'>Cms Pages</h5>
              </div>
              <div className="card-body">
                <div className="row">

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={cmsPagesAccess.view ? true : false} onChange={(event) => this.handleChecked(event, "view", "cmsPagesAccess")} /><span></span>
                      Ver
                      </label>
                  </div>

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={cmsPagesAccess.edit ? true : false} onChange={(event) => this.handleChecked(event, "edit", "cmsPagesAccess")} /><span></span>
                      Editar
                      </label>
                  </div>


                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 mb-3">
            <div className='card animated fadeIn'>
              <div className='card-header'>
                <h5 className='card-title'>Roles</h5>
              </div>
              <div className="card-body">
                <div className="row">

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={rolesAccess.viewList ? true : false} onChange={(event) => this.handleChecked(event, "viewList", "rolesAccess")} /><span></span>
                      Ver lista
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={rolesAccess.create ? true : false} onChange={(event) => this.handleChecked(event, "create", "rolesAccess")} /><span></span>
                      Crear
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={rolesAccess.edit ? true : false} onChange={(event) => this.handleChecked(event, "edit", "rolesAccess")} /><span></span>
                      Editar
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={rolesAccess.delete ? true : false} onChange={(event) => this.handleChecked(event, "delete", "rolesAccess")} /><span></span>
                      Eliminar
                      </label>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 mb-3">
            <div className='card animated fadeIn'>
              <div className='card-header'>
                <h5 className='card-title'>Ajustes del correo electrónico</h5>
              </div>
              <div className="card-body">
                <div className="row">

                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={emailSettingsAccess.editEmailTemplates ? true : false} onChange={(event) => this.handleChecked(event, "editEmailTemplates", "emailSettingsAccess")} /><span></span>
                      Editar plantilla de correo electrónico
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={emailSettingsAccess.viewEmailTemplates ? true : false} onChange={(event) => this.handleChecked(event, "viewEmailTemplates", "emailSettingsAccess")} /><span></span>
                      Ver plantilla de correo electrónico
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={emailSettingsAccess.status ? true : false} onChange={(event) => this.handleChecked(event, "status", "emailSettingsAccess")} /><span></span>
                      Estado de actualización
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={emailSettingsAccess.editAdminEmails ? true : false} onChange={(event) => this.handleChecked(event, "editAdminEmails", "emailSettingsAccess")} /><span></span>
                      Editar correos electrónicos de administrador
                      </label>
                  </div>
                  <div className="col-lg-4 col-md-6 checkbox">
                    <label>
                      <input type='checkbox' className='form-check-input' name='foo' checked={emailSettingsAccess.editEmailnotificationSetting ? true : false} onChange={(event) => this.handleChecked(event, "editEmailnotificationSetting", "emailSettingsAccess")} /><span></span>
                      Editar configuración de notificación de correo electrónico
                      </label>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <button className="btn btn-primary mr-3" onClick={this.addRole}>Enviar</button>
        <button className="btn btn-outline-primary" onClick={() => this.props.history.push('/roleslist')}>Cancelar</button>
      </Home>

    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(EditPermissions)
