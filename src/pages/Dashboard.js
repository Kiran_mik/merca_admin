import React, { Component } from 'react';
import Home from './Home';
import { connect } from 'react-redux'
import * as actions from '../actions'
import { IMAGE_URL } from '../config/configs'
import { Line } from 'react-chartjs-2';
import $ from 'jquery';
import { FadeLoader } from 'react-spinners';
window.jQuery = $;
window.$ = $;
global.jQuery = $;
class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      getTotalDashboard: {},
      top_selling: [],
      new_users: [],
      latest_orders: [],
      pending_orders: [],
      closed_orders: [],
      most_viewed: [],
      ordersAccess: {},
      usersAccess: {},
      productsAccess: {},
      loading: true,
      revenue_: 'month',

      data: {
        labels: [],
        datasets: [
          {
            label: 'Ingresos',
            fill: true,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: '#FCC5C4',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: '#F89F9E',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [],
          }
        ]
      },
      data1: {
        labels: [],
        datasets: [
          {
            label: 'Pedidos',
            fill: true,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: '#FCC5C4',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: '#F89F9E',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [],
          }
        ]
      },
    };
  }
  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions.rolePermission) {
      let { ordersAccess, usersAccess, productsAccess } = permissions.rolePermission
      this.setState({ ordersAccess: ordersAccess, usersAccess: usersAccess, productsAccess: productsAccess })
    }
    $('[data-toggle="tooltip"]').tooltip();
    if (localStorage.getItem('token')) {
      this.getTotalDashboardData()
      this.getTopselling()
      this.getNewUsers()
      this.getLatestOrd()
      this.getMostviewed()
      this.getRevenue()
    }
  }


  // ##############################  Listing #############################
  getTotalDashboardData = () => {
    let token = localStorage.getItem('token')
    var url = '/admins/getTotaldashboardData'
    var method = 'get'
    this.props.commonApiCall(url, method, null, token, null, this.props, response => {
      let { data } = response
      if (data && data.data) {
        this.setState({ getTotalDashboard: data.data, loading: false })
      }

    })
  }


  // ##############################  Top Selling #############################
  getTopselling = () => {
    let token = localStorage.getItem('token')
    var url = '/admins/getTopselling'
    var method = 'get'
    this.props.commonApiCall(url, method, null, token, null, this.props, response => {
      let { data } = response.data
      if (data && data.orders) {
        this.setState({ top_selling: data.orders })
      }
    })
  }

  // ############################## Most Viewed Orders #############################
  getMostviewed = () => {
    let token = localStorage.getItem('token')
    var url = '/admins/getMostviewA'
    var method = 'get'
    this.props.commonApiCall(url, method, null, token, null, this.props, response => {
      let { data } = response.data
      if (data && data.length > 0) {
        this.setState({ most_viewed: data })
      }
    })
  }
  // ############################## New Users #############################
  getNewUsers = () => {
    let token = localStorage.getItem('token')
    var url = '/admins/getNewUsers'
    var method = 'get'
    this.props.commonApiCall(url, method, null, token, null, this.props, response => {
      let { data } = response.data
      if (data && data.length > 0) {
        this.setState({ new_users: data })
      }
    })
  }

  // ############################## Latest Orders #############################
  getLatestOrd = (e) => {
    let token = localStorage.getItem('token')
    var url = '/admins/getOrdersAllStatus'
    var body = {}
    if (e) {
      body.type = e
    } else {
      body = {}
    }
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      if (data && data.length > 0) {
        this.setState({ latest_orders: data })
      } else {
        this.setState({ latest_orders: [] })
      }
    })
  }


  getRevenue = (e) => {
    if (e === 'month') {
      this.setState({ revenue_: 'date' })
    } else {
      this.setState({ revenue_: 'month' })
    }
    var stateCopy = Object.assign({}, this.state.data);
    var stateCopy1 = Object.assign({}, this.state.data1);
    let token = localStorage.getItem('token')
    var url = '/admins/getRevenue'
    var method = 'post'
    if (e === 'month') {
      var body = { type: 'month' }
    } else {
      var body = { type: 'date' }
    }
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      if (data) {
        let orders1 = data
        var arr1 = []
        var val = []
        var count = []
        var arr2 = []
        orders1.map((each) => {
          var dev = each._id
          return (
            arr1.push(dev),
            val.push(each.totalAmount),
            count.push(each.count)
          )
        })
        if (e === 'month') {
          arr1.map((each2 => {
            let chat_data = each2.month + "/" + each2.year
            arr2.push(chat_data)
          }))
        } else {
          arr1.map((each2 => {
            let chat_data = each2.day + "/" + each2.month + "/" + each2.year
            arr2.push(chat_data)
          }))
        }

      }
      stateCopy.labels = arr2;
      stateCopy.datasets[0].data = val
      stateCopy1.labels = arr2;
      stateCopy1.datasets[0].data = count

      this.setState({ data: stateCopy, data1: stateCopy1, loading: false })
    })
  }

  render() {
    let { revenue_, loading, getTotalDashboard, top_selling, new_users, latest_orders, pending_orders, most_viewed, closed_orders, data, data1, ordersAccess, usersAccess, productsAccess } = this.state

    return (

      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Tablero</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item active">Tablero </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="row animated fadeIn">
          <div className="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
            <div className="card card-statistics">
              <div className="card-body">
                <div className="clearfix">
                  <p className="card-text">Ingresos Totales</p>
                  <div className="statistics-detail d-flex justify-content-between align-item-center">
                    <div className="statistics-icon bg-primary">
                      <i className="fa fa-money icon-md" />
                    </div>
                    <h3 className="card-title text-primary">${getTotalDashboard.totalRevenue}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
            <div className="card card-statistics">
              <div className="card-body">
                <div className="clearfix">
                  <p className="card-text">Pedidos</p>
                </div>
                <div className="statistics-detail d-flex justify-content-between align-item-center">
                  <div className="statistics-icon bg-danger">
                    <i className="fa fa-cart-plus icon-md" />
                  </div>
                  <h3 className="card-title text-danger">{getTotalDashboard.totalOrder}</h3>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-lg-4 col-md-4 col-sm-6 grid-margin stretch-card">
            <div className="card card-statistics">
              <div className="card-body">
                <div className="clearfix">
                  <p className="card-text">Usuarios</p>
                  <div className="statistics-detail d-flex justify-content-between align-item-center">
                    <div className="statistics-icon bg-success">
                      <i className="fa fa-user-plus icon-md" />
                    </div>
                    <div className="fluid-container">
                      <h3 className="card-title text-success">{getTotalDashboard.totalUserCount}</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          {productsAccess.viewList || usersAccess.viewList || ordersAccess.viewList ?
            <div className="col-lg-6 col-md-12 grid-margin">
              <div className="card">
                <div className="card-header">
                  <h5 className="card-title">Vista general</h5>
                </div>
                <div className="card-body">
                  <ul className="nav nav-tabs" id="myTab" role="tablist">
                    {productsAccess.viewList ? <>
                      <li className="nav-item active">
                        <a className="nav-link active" data-toggle="tab" href="#top_selling" role="tab" aria-controls="item" aria-selected="false">Más vendidos</a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" data-toggle="tab" href="#most_viewed" role="tab" aria-controls="imagetab" aria-selected="false">Mas vistos</a>
                      </li></> : null}
                    {usersAccess.viewList ?
                      <li className="nav-item">
                        <a className="nav-link" data-toggle="tab" href="#new_customer" role="tab" aria-controls="new_customer" aria-selected="false">Usuarios nuevos</a>
                      </li>
                      : null}
                    {ordersAccess.viewList ?
                      <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="">Pedidos</a>
                        <div className="dropdown-menu dropdown-menu-right">
                          <a className="dropdown-item" data-toggle="tab" href="#orders" role="tab" aria-controls="additionaltab" aria-selected="false" onClick={() => this.getLatestOrd()}><i className="fa fa-bell"></i>Últimos 10 pedidos</a>
                          <a className="dropdown-item" data-toggle="tab" href="#orders" role="tab" aria-controls="additionaltab" aria-selected="false" onClick={() => this.getLatestOrd('pending')}><i className="fa fa-bell"></i>Ordenes pendientes</a>
                          <a className="dropdown-item" data-toggle="tab" href="#orders" role="tab" aria-controls="additionaltab" aria-selected="false" onClick={() => this.getLatestOrd('closed')}><i className="fa fa-bell"></i>Órdenes completadas</a>
                          <a className="dropdown-item" data-toggle="tab" href="#orders" role="tab" aria-controls="additionaltab" aria-selected="false" onClick={() => this.getLatestOrd('cancelled')}><i className="fa fa-cog"></i>Órdenes rechazadas</a>
                        </div>
                      </li>
                      : null}
                  </ul>
                  <div className="tab-content" id="myTabContent">
                    {productsAccess.viewList ?
                      <div className="fade show active tab-pane" id="top_selling" role="tabpanel" aria-labelledby="itemtab">
                        <div className='table-responsive'>
                          <table className='table dataTable with-image row-border hover custom-table table-striped'>
                            <thead>
                              <tr>
                                <th>SKU<i aria-hidden='true' ></i></th>
                                <th>Imagen</th>
                                <th>Nombre del producto<i aria-hidden='true' ></i></th>
                                <th sortable-column='firstname'  > Precio <i aria-hidden='true' ></i> </th>
                                <th sortable-column='emailId'> Precio de venta <i aria-hidden='true' ></i> </th>
                                <th sortable-column='emailId'> Total de la orden <i aria-hidden='true' ></i> </th>
                                <th > Comportamiento</th>
                              </tr>
                            </thead>
                            <tbody>
                              {top_selling.map((each, id) => {
                                return (
                                  <tr className='animated fadeIn' key={id}>
                                    <td>{each.SKU}</td>
                                    <td>
                                      <div className="thumb-img">
                                        <img src={each.productImage ? IMAGE_URL + each.productImage : "assets/images/product_avatar.jpg"} alt={each.productName} />
                                      </div>
                                    </td>
                                    <td><a href={each.productPath ? each.productPath : "javascript:;"} target="_blank" className="mt-1 d-block">{each.productName ? each.productName : '-'}</a></td>
                                    <td>${each.price}</td>
                                    <td>${each.salePrice}</td>
                                    <td>${each.totalAmount}</td>
                                    <td>
                                      <button className="btn text-primary" onClick={() => this.props.history.push(`/editProduct/${each.product_Id}`)}>
                                        <i className="fa fa-eye" data-toggle="tooltip" title="View"></i>
                                      </button>
                                    </td>
                                  </tr>
                                )
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div> : null}
                    {productsAccess.viewList ?
                      <div className={this.state.tab1 ? "tab-pane fade show active" : "tab-pane fade"} id="most_viewed" role="tabpanel" aria-labelledby="itemtab">
                        <div className='table-responsive'>
                          <table className='table dataTable with-image row-border hover custom-table'>
                            <thead>
                              <tr>
                                <th>SKU</th>
                                <th>Imagen</th>
                                <th>Nombre del producto</th>
                                <th sortable-column='firstname'  > Precio </th>
                                <th sortable-column='emailId'> Precio de venta </th>
                                <th sortable-column='emailId'> Visto </th>
                                <th > Comportamiento</th>
                              </tr>
                            </thead>
                            <tbody>
                              {most_viewed.length > 0 && most_viewed.map((each, id) => {
                                return (
                                  <tr className='animated fadeIn' key={id}>
                                    <td>{each.SKU}</td>
                                    <td>
                                      <div className="thumb-img">
                                        <img src={each.productImage ? IMAGE_URL + each.productImage : "assets/images/no-image-user.png"} alt={each.productName} />
                                      </div>
                                    </td>
                                    <td><a href={each.productPath ? each.productPath : "javascript:;"} target="_blank" className="mt-1 d-block">{each.productName}</a></td>
                                    <td>${each.price}</td>
                                    <td>${each.salePrice}</td>
                                    <td>{each.mostViewed}</td>
                                    <td>
                                      <button className="btn text-primary" onClick={() => this.props.history.push(`/editProduct/${each._id}`)}>
                                        <i className="fa fa-eye" data-toggle="tooltip" title="View"></i>
                                      </button>
                                    </td>
                                  </tr>
                                )
                              })}

                            </tbody>
                          </table>
                        </div>
                      </div> : null}
                    <div className="tab-pane" id="new_customer" role="tabpanel" aria-labelledby="itemtab">
                      <div className='table-responsive'>
                        <table className='table dataTable with-image row-border hover custom-table'>
                          <thead>
                            <tr>
                              <th >Nombre de usuario</th>
                              <th sortable-column='firstname'  > Email </th>
                              <th sortable-column='emailId'> Verificar <i aria-hidden='true' ></i> </th>
                              <th > Comportamiento</th>
                            </tr>
                          </thead>
                          <tbody>
                            {new_users.map((each, id) => {
                              return (
                                <tr className='animated fadeIn' key={id}>
                                  <td>{each.firstName} {each.lastName}</td>
                                  <td>{each.emailId}</td>
                                  <td ><i className={each.isVerified ? "fa fa-check text-success" : "fa fa-close text-danger"}></i></td>
                                  <td>
                                    <button className="btn text-primary" onClick={() => this.props.history.push(`/edituser/${each._id}`)}>
                                      <i className="fa fa-eye" data-toggle="tooltip" title="View"></i>
                                    </button>
                                  </td>
                                </tr>
                              )
                            })}

                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="tab-pane" id="orders" role="tabpanel" aria-labelledby="itemtab">
                      <div className='table-responsive'>
                        <table className='table dataTable with-image row-border hover custom-table'>
                          <thead>
                            <tr>
                              <th sortable-column='name'>Nombre de usuario <i aria-hidden='true' ></i></th>
                              <th sortable-column='Price'>Precio total<i aria-hidden='true' ></i> </th>
                              <th sortable-column='items'>No de articulos <i aria-hidden='true' ></i> </th>
                              <th sortable-column='emStatusailId'>Estado<i aria-hidden='true' ></i> </th>
                              <th> Comportamiento</th>
                            </tr>
                          </thead>
                          {latest_orders.length > 0 ?
                            <tbody>
                              {latest_orders.map((each, id) => {
                                return (
                                  <tr className='animated fadeIn' key={id}>
                                    {each.buyerName ?
                                      <td>{each.buyerName}</td> : <td>-</td>}
                                    <td>{each.totalAmount ? each.totalAmount : "-"}</td>
                                    <td>{each.count ? each.count : "-"}</td>
                                    <td><span className="status bg-warning">{each.orderStatus}</span></td>
                                    <td>
                                      <button className="btn text-primary" onClick={() => { this.props.history.push(`/orderview/${each._id}`) }}>
                                        <i className="fa fa-eye" data-toggle="tooltip" title="View"></i>
                                      </button>
                                    </td>
                                  </tr>
                                )
                              })}
                            </tbody> :
                            <tbody><tr><td colSpan="10" style={{ padding: '30px', width: '100%' }}>No se encontraron registros </td></tr></tbody>}
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> : null}
          <div className="col-lg-6 col-md-12 grid-margin">
            <div className="card">
              <div className="card-header">
                <h5 className="card-title card-button">Ingresos <span> <button onClick={() => this.getRevenue(this.state.revenue_)} className="btn btn-teal">{revenue_ === 'month' ? "Mensual" : "Diario"}</button></span></h5>
              </div>
              <div className="card-body chart-img">
                <ul className="nav nav-tabs" id="myTab" role="tablist">
                  <li className="nav-item active">
                    <a className="nav-link active" data-toggle="tab" href="#revenue" role="tab" aria-controls="item" aria-selected="false">Ingresos</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" data-toggle="tab" href="#order" role="tab" aria-controls="imagetab" aria-selected="false">Orden</a>
                  </li>

                </ul>

                <div className="tab-content" id="myTabContent">
                  <div className="fade show active tab-pane" id="revenue" role="tabpanel" aria-labelledby="revenue">
                    {/* <img src="assets/images/chart.jpg"/> */}
                    {data.datasets[0].data != undefined ? <>
                      {loading ?
                        <FadeLoader sizeUnit={"px"} size={20} color={'#2472DC'} loading={loading} /> :

                        <Line ref="chart" data={data} />}</> : null}
                  </div>
                  <div className="fade tab-pane" id="order" role="tabpanel" aria-labelledby="order">
                    {data1.datasets[0].data != undefined ?
                      <Line ref="chart" data={data1} /> : null}
                  </div>
                </div>
              </div>
            </div>
          </div>
        
        </div>
      </Home>
    );
  }
}



const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(Dashboard);
