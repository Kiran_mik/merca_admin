import React, { Component } from 'react'
import Home from '../Home'
import Toggle from 'react-toggle'
import swal from 'sweetalert'
import { API_URL, CSV_URL, EXCEL_URL } from '../../config/configs'
import * as actions from '../../actions'
import { connect } from 'react-redux'
import SimpleReactValidator from 'simple-react-validator'
import { BeatLoader } from 'react-spinners';
import { Select } from 'antd'
import Pagination from 'rc-pagination'
import $ from 'jquery';
import { isEmpty } from "lodash"
import Loader from '../Loader'
import _ from "lodash"
const FileDownload = require('js-file-download')

class ShippingMethods extends Component {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      shippingName: true,
      trackingUrl: true,
      status: true,
      addNew: false,
      selectAll: false,
      shippingList: [],
      updatedtrackingUrl: '',
      updatedshippingName: '',
      page: 1,
      pagesize: 10,
      total: undefined,
      length: undefined,
      sort: {},
      sortData: { shippingName: false, trackingUrl: false, isActive: false },
      listOfTransport: [],
      selectedTransport: [],
      publishedvalue: '',
      multipleDelete: [],
      selectedOption: 'Selecciona aquí',
      loading: true,
      page_:false,
      download:false

    }
  }


  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.role != 'Super Admin') {
      this.props.history.push('/dashboard')
    }
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').stop().slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').stop().slideToggle();
    });
    $('[data-toggle="tooltip"]').tooltip();
    this.shippingListing('pagesize')
  }


  // ############################## Shipping Listing ######################
  shippingListing = (e) => {
    let { page, pagesize, sort, selectedTransport, publishedvalue } = this.state
    var url = '/shipping/shippingListing'
    var method = 'post'
    var token = localStorage.getItem('token')
    var data = {  }
    if (e==='filter') {
      data.page = 1
    } else if(e==='pagesize') {
      data=data
    }else{
      data.page = page
    }
    if(e==='pagesize'){
      data=data
    }else{
      data.pagesize=pagesize
    }
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (selectedTransport != '') {
      data.shippingName = selectedTransport
    }
    if (publishedvalue != '') {
      data.status = publishedvalue
    }
    var body = data
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      let { data } = response.data
      let data1 = response.data.data
      if (response.data.status === 1) {
        this.setState({ shippingList: data.shippingListing, total: data.total, length: data.shippingListing.length, loading: false })
        if (data1.manageShippingListing) {
          this.setState({
            shippingName: data1.manageShippingListing.shippingName,
            trackingUrl: data1.manageShippingListing.trackingUrl,
            status: data1.manageShippingListing.status,
            pagesize: data1.manageShippingListing.pageSize,

          })
        }
      } else {
        this.setState({ shippingList: [] })
      }
      
    }
    )
  }



  //* **********  RESET LISTING  ************************//
  resetListing = (e) => {
    var url = '/shipping/shippingListing'
    var method = 'post'
    var { page, pagesize } = this.state
    var body = { page: page, pagesize: pagesize }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      let data1 = response.data.data
      if (response.data.status === 1) {
        this.setState({ shippingList: data.shippingListing, total: data.total, length: data.shippingListing.length, selectedTransport: [], publishedvalue: 'Select Option' })
        if (data1.manageShippingListing) {
          this.setState({
            shippingName: data1.manageShippingListing.shippingName,
            trackingUrl: data1.manageShippingListing.trackingUrl,
            status: data1.manageShippingListing.status,
          })
        }
      } else {
        this.setState({ shippingList: [] })
      }
    }
    )
  }
  // ########################### DeleteShippingDetails #########################
  deleteShipping = id => {
    var delArr = this.state.multipleDelete
    swal({ title: "¿Estás seguro?", icon: "warning", buttons: true, dangerMode: true, }).then((willDelete) => {
      if (willDelete) {
        if (delArr.length > 0) {
          var body = { shippingId: delArr }
        } else {
          var body = { shippingId: [id] }
        }
        var url = '/shipping/deleteShippingDetails'
        var method = 'post'
        var token = localStorage.getItem('token')
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          let { data } = response
          if (data.status === 1) {
            this.shippingListing()
          }
        })
        swal('Borrado exitosamente', '', 'success')
      }
    })
  }

  applyfilter = () => {
    var delArr = this.state.multipleDelete
    if (this.state.selectedOption == 'delete') {
      if (delArr.length > 0) {
        this.deleteShipping(...delArr)
      }
    }
    if (this.state.selectedOption == 'active') {
      if (delArr.length > 0) {
        var body = { shippingId: delArr, isActive: true }
      }
    }
    if (this.state.selectedOption == 'inActive') {
      if (delArr.length > 0) {
        var body = { shippingId: delArr, isActive: false }
      }
    }
    var url = '/shipping/changeShippingStatus'
    var method = 'post'
    var token = localStorage.getItem('token')
    if(delArr.length>0){
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data.status == 1) {
        if (this.state.selectedOption == 'inActive') {
          swal('Desactivado con éxito! !', '', 'success')
        }
        else {
          swal('Publicado con éxito!', '', 'success')
        }
        this.shippingListing()
      }
      this.setState({ multipleDelete: [], selectedOption: 'Selecciona aquí' })
    })
  }else {
    this.setState({ selectedOption: 'Selecciona aquí' })
    swal('Select atleast one shipping name', '', 'info')
  }

  }


  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize
    }, () => this.shippingListing());
    this.setState({ selectAll: false }, () => this.selectAllcheck())

  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 },()=>this.setTableRows(), () => this.shippingListing());
  }
  handleChange_ = (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1,  },()=>this.setTableRows(), () => this.shippingListing());
    } 
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.shippingListing());
    // }
  }

  gettingDetails = (Id) => {
    this.props.history.push(`/editShipping/${Id}`)
  }
  //* ********** SORTING ************************//
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState(
          {
            sort: { [element]: value }
          },
          () => {
            this.shippingListing()
          }
        )
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }
  // ########################### Set Table Rows #########################
  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { shippingName, trackingUrl, status,pagesize } = this.state
    var body = { shippingName, trackingUrl, status,pageSize:pagesize }
    var url = '/shipping/setShippingFilter'
    var method = 'post'
    // var { page, pagesize } = this.state
    // var body = { page: page, pagesize: pagesize }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, res => {
      if (res) {
        this.shippingListing()
        if(e){
          swal('Actualizado con éxito!', '', 'success')
        }
      }
    })
  }

  // ###########################  for Search #########################
  getIds = (e) => {
    let token = localStorage.getItem('token')
    var body = { type: "shippingName", searchText: e }
    let url = "/shipping/shippingFieldsList"
    let method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (response && response.data) {
        var hasArr = _.uniqBy(data, (e) => {
          return e.shippingName.toString();
        });
        this.setState({ listOfTransport: hasArr })
      }
    })
  }

  handleChange2 = (e) => {
    this.setState({ publishedvalue: e.target.value })
  }


  downloadCSV(type, array) {
    let { shippingName, trackingUrl, status, selectedTransport, publishedvalue, multipleDelete } = this.state
    let token = localStorage.getItem('token')
    this.setState({download:true})
    if (array === "totalList") {
      var data = { filteredFields: ["shippingName", "trackingUrl", "publish", "isDeleted"] }
    }
    if (array === "filteredList") {
      var data = { filteredFields: [] }
      if (shippingName) {
        data.filteredFields.push('shippingName')
      };
      if (trackingUrl) {
        data.filteredFields.push('trackingUrl')
      };
      if (status) {
        data.filteredFields.push('publish')
      };
      if (selectedTransport != '') {
        data.shippingName = selectedTransport
      }
      if (publishedvalue != '') {
        data.status = publishedvalue
      }
      if (multipleDelete.length > 0) {
        data.shippingArray = multipleDelete
      }
    }
    var body = data
    var method = 'post'
    var url = '/shipping/' + type
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var data = response.data.data
      if (data) {
        this.setState({download:false,multipleDelete:[]})
        if (type === 'downloadShippingCsvFile') {
          window.open(CSV_URL + data.filePathAndName, '_blank');
          // FileDownload(CSV_URL  +  data.filePathAndName, data.filePathAndName);
        } else {
          window.open(EXCEL_URL + data.filePathAndName, '_blank');
          // FileDownload(EXCEL_URL  +  data.filePathAndName, data.filePathAndName);
        }
      }
    })
  }
  onCheckbox(_id, val) {
    var delarray = this.state.multipleDelete
    if (!delarray.includes(_id)) {
      delarray.push(_id)
    } else {
      delarray.splice(delarray.indexOf(_id), 1)
    }
    if (delarray.length != this.state.shippingList.length) {
      this.setState({ checked: false })
    }
    if (this) this.setState({ multipleDelete: delarray })
  }
  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { shippingList } = this.state
    if (this.state.selectAll) {
      shippingList.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      shippingList.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }
  checkArray(_id) {
    let { multipleDelete } = this.state
    if (multipleDelete.includes(_id)) {
      return true
    } else {
      return false
    }
  }

  // ########################### Catagory Status #########################
  statusChange(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { shippingId: [Id], isActive: status }
    var urlkey = 'shipping/changeShippingStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        if (status == false) {
          swal('Shipping Name Inactivated Successfully !', '', 'success')
        }
        else {
          swal('Shipping Name Activated Successfully !', '', 'success')
        }
        this.shippingListing()
      } else if (data.message === 'Invalid token') {
        this.props.history.push('/')
      } else {
        swal(data.message, '', 'error')
      }
    })
  }
  render() {
    const Option = Select.Option
    var { shippingName, trackingUrl, status, shippingList, total, length, page, pagesize, sortData,
      listOfTransport, selectedTransport, publishedvalue, selectedOption } = this.state
    const filteredTransport1 = listOfTransport.map((each) => { return each.shippingName });
    const filteredTransport = filteredTransport1.filter(o => !selectedTransport.includes(o));


    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Transporte</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item' >Master Management</li>
                <li className='breadcrumb-item active'>Transporte</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
        {this.state.download ?<Loader/>:null}
        <div className='card animated fadeIn'>
          <div className="card-header">
            <div className="row data-filter justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  <Select
                    showSearch
                    placeholder={<b>Seleccionar</b>}
                    optionFilterProp="children"
                    value={selectedOption}
                    onSelect={(value) => this.setState({ selectedOption: value })}
                    className="applyselect"
                  >
                    <Option value="active">Activo</Option>
                    <Option value="inActive">Inactivo</Option>
                    <Option value="delete" >Eliminar</Option>
                  </Select>
                  <button type="button" className="btn btn-primary ml-0 apply_btn" onClick={this.applyfilter}>Aplicar</button>
                  <button type="button" className="btn btn-primary" onClick={() => this.props.history.push('/addShipping')}><i className="fa fa-plus" /> <span> Agregar método de envío</span></button>
                  <div className="dropdown">
                    <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                      <span>Herramientas</span>
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadShippingExcelFile", "totalList")}>Exportar a Excel</a>
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadShippingCsvFile", "totalList")}>Exportar a CSV</a>
                    </div>
                  </div>
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ shippingName: !this.state.shippingName })} checked={shippingName} /><span></span>Nombre de envío</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ trackingUrl: !this.state.trackingUrl })} checked={trackingUrl} /><span></span>URL de seguimiento</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ status: !this.state.status })} checked={status} /><span></span>Estado</label></li>
            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.shippingListing} >
            Reiniciar
            </button>

            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({ shippingName: true, trackingUrl: true, status: true, })} >
            Seleccionar todo
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={()=>this.setTableRows('rows')}>
            Guardar
            </button>

          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre de envío</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre del envío"
                    value={selectedTransport}
                    onChange={(selectedTransport) => this.setState({ selectedTransport })}
                    onSearch={(e) => this.getIds(e)}
                    onFocus={(e) => this.getIds(e)}

                    style={{ width: '100%' }}
                  >
                    {filteredTransport.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>

              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Estado</label>
                  <select className="form-control" onChange={this.handleChange2} value={publishedvalue}>
                    <option value="">Seleccione uno</option>
                    <option value="published">Activo</option>
                    <option value="unPublished">Inactivo</option>
                  </select>
                </div>
              </div>
            </div>
            <hr />
            <div className="pull-right filter-button">

              <div className="dropdown ml-2">
                <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                  <span>Herramientas</span>
                </button>
                <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                  <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadShippingExcelFile", "filteredList")}>Exportar a Excel</a>
                  <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadShippingCsvFile", "filteredList")}>Exportar a CSV</a>
                </div>
              </div>
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.shippingListing('filter')}>
              Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetListing} >
              Reiniciar
            </button>
            </div>

          </div>
          <div className='card-body'>

            <div className='table-responsive'>
              <table className='table dataTable with-image row-border hover custom-table table-striped'>
                <thead>
                  <tr>
                    <th><div className="checkbox">
                      <label>
                        <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} /><span></span>
                        <i className="input-helper" />
                      </label>
                    </div>
                    </th>

                    {shippingName ? <th sortable-column="shippingName" onClick={this.onSort.bind(this, 'shippingName')}>
                    Nombre de envío
                      <i aria-hidden='true' className={(sortData['shippingName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {trackingUrl ? <th sortable-column="trackingUrl" onClick={this.onSort.bind(this, 'trackingUrl')}>
                    URL de seguimiento
                      <i aria-hidden='true' className={(sortData['trackingUrl']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {status ? <th sortable-column="isActive" onClick={this.onSort.bind(this, 'isActive')}>
                    Estado
                      <i aria-hidden='true' className={(sortData['isActive']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    <th> Comportamiento</th>
                  </tr>
                </thead>
                {shippingList.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                  <tbody>
                    {shippingList.map((each, id) => {
                      return (
                        <tr key={id}>
                          <td>
                            <div className='checkbox'>
                              <label>
                                <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray(each._id)} onChange={() => this.onCheckbox(each._id)} /><span></span>

                                <i className='input-helper' />
                              </label>
                            </div>
                          </td>
                          {shippingName ? <td>{each.shippingName} </td> : null}
                          {trackingUrl ? <td>{each.trackingUrl}</td> : null}
                          {status ? <td><Toggle checked={each.isActive} className='custom-classname' onChange={() => this.statusChange(each.isActive, each._id)} /></td> : null}
                          <td>
                            <button > <i className='fa fa-edit text-primary' data-toggle="tooltip" title="View" onClick={() => this.gettingDetails(each._id)} /> </button>
                            <button > <i className='fa fa-trash text-danger' data-toggle="tooltip" title="Delete" onClick={() => this.deleteShipping(each._id)} /> </button>
                          </td>
                        </tr>
                      )
                    })}

                  </tbody>}
              </table>
            </div>
            {shippingList.length > 0 ?
              <div className="table-footer d-flex justify-content-between">
                <div className="table-shorting">
                  <label>Demostración</label>
                  <Select showSearch placeholder={<b> {total <= 10 ? total : length}</b>} optionFilterProp="children"
                    onSelect={this.handleChangePageSize.bind(this)} 
                    value={this.state.page_ ? pagesize : (length <=pagesize ?length:pagesize)}
                    onSearch={this.handleChange_}>
                    <Option value={10}>10</Option>
                    <Option value={25}>25</Option>
                    <Option value={50}>50</Option>
                    <Option value={75}>75</Option>
                  </Select>
                  <label>Fuera de  {total} Métodos de envío</label>
                </div>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                </div>
              </div> : null}
          </div>
        </div>
      </Home>
    )
  }
}
const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(ShippingMethods)






