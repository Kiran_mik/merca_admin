import React, { Component } from 'react';
import Home from '../Home'
import axios from 'axios'
import { API_URL, IMAGE_URL } from '../../config/configs'
import swal from 'sweetalert'
import { toast } from 'react-toastify';
import SimpleReactValidator from 'simple-react-validator'
import * as actions from '../../actions';
import { connect } from "react-redux";


class AddSuppliers extends Component {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      logo: '',
      supplierName: '',
      supplierId_:'',
      address: '',
      country: '',
      state1: '',
      city: '',
      emailId: '',
      mobileNo: '',
      fax: '',
      errors: {
        
      },
      listOfStates: [],
      supplierId: ''
    }
  }

  componentDidMount() {
    this.getAllDetails()
  }
  validateForm = () => {
    let { supplierName,supplierId_, address,state1, city, emailId, mobileNo, errors } = this.state
    let formIsValid = true
    var pattern = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
    var mblpattern = new RegExp(/^\d{8,20}$/);

    if (!supplierName||supplierName.trim()==='') {
      formIsValid = false
      errors['supplierName'] = '* Se requiere el nombre del proveedor'
    }
    if (!supplierId_||supplierId_.trim()==='') {
      formIsValid = false
      errors['supplierId_'] = '* Se requiere identificación del proveedor'
    }
    if (!address||address.trim()==='') {
      formIsValid = false
      errors['address'] = '* La dirección es necesaria'
    }
    if (!state1||state1.trim()==='') {
      formIsValid = false
      errors['state1'] = '* Se requiere estado'
    }
    if (!city||city.trim()==='') {
      formIsValid = false
      errors['city'] = '* Se requiere ciudad'
    }
    // if (!emailId||emailId.trim()==='') {
    //   formIsValid = false
    //   errors['emailId'] = '* Email is required'
    // }
    if (!pattern.test(emailId)) {
      formIsValid = false
      if(!emailId||emailId.trim() === '' ){
        errors['emailId'] = '* EmailId es requerido'
      }else
      errors['emailId'] = '* Ingrese un correo electrónico válido'
    }
    // if (!mobileNo) {
    //   formIsValid = false
    //   errors['mobileNo'] = '* Mobile No is required'
    // }
    // if (!mobileNo) {
    //   formIsValid = false
    //   errors['mobileNo'] = '* Se requiere número de celular'
    // }else  if(mobileNo.length!=10){
    //   errors['mobileNo'] = '* Ingrese un número de teléfono celular válido de 10 dígitos'
    // }
    if (!mblpattern.test(mobileNo)) {
		  formIsValid = false
		  if(!mobileNo){
			errors['mobileNo'] = 'Se requiere número de celular'
		  }else
		  errors['mobileNo'] = 'Ingrese un número de teléfono celular válido 8 to 20'
		}
    // else formIsValid = true
    this.setState({ errors })
    return formIsValid

  }

  getAllDetails = () => {
    var id = this.props.match.params.sId;
    this.setState({ supplierId: id })
    if (id) {
      var body = { supplierId: id }
      let token = localStorage.getItem('token')
      var url = '/suppliers/getSupplierDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data) {
          this.setState({
            logo: data.data.logo,
            supplierName: data.data.supplierName,
            supplierId_:data.data.supplierId,
            address: data.data.address,
            country: data.data.country,
            state1: data.data.state,
            city: data.data.city,
            emailId: data.data.emailId,
            mobileNo: data.data.mobileNo,
            fax: data.data.fax,
          })
        }

      })
    }
  }
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }
  }

  // #################################### logo preview ################################
  fileChangedHandler(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file: file,
        filename: filename,
        imagePreviewUrl: reader.result,
        fileVisible: true
      })
    }
    reader.readAsDataURL(file)
  }

  // #################################### logo upload ################################
  metaFileUpload = () => {
    var { file } = this.state
    var token = localStorage.getItem('token')
    var formData = new FormData()
    formData.append('file', file)
    axios.post(API_URL + '/auth/fileUpload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: token
      }
    })
      .then(response => {
        let { data } = response.data
        this.setState({ logo: data.filepath, fileVisible: false, file: '' })
      })
      .catch(err => console.log(err))
  }

  // #################################### Add supplier ################################

  addSuppliers = () => {
    if (this.validateForm()) {
      let token = localStorage.getItem('token')
      let { logo, supplierName,supplierId_, address, city, emailId, mobileNo, fax, supplierId,state1 } = this.state
      var data = { logo, supplierName,supplierId:supplierId_, address, country: 'Argentina', state:state1, city, emailId, mobileNo, fax }
      if (supplierId != '') {
        data.id = supplierId
      }
      var body = data
      let url = "/suppliers/addSuppliers"
      let method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.statusCode === 432) {
          toast.error("Por favor, introduzca un correo electrónico válido", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }
        if (data.statusCode === 434) {
          toast.error("El número de celular debe tener 10 dígitos.", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }
        if (data.statusCode === 445) {
          toast.error("EmailId ya existe.", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }
        if (data.statusCode === 441) {
          toast.error("SupplierName ya existe.", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }
        if (data.statusCode === 457) {
          toast.error("SupplierId ya existe", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }
        if (data.statusCode === 228) {
          swal({
            title: "Agregado exitosamente!",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
                this.props.history.push('/supplier')
              // }
            })
        }
        if (data.statusCode === 219) {
          swal({
            title: "Actualizado con éxito !",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
                this.props.history.push('/supplier')
              // }
            })
        }
      })
    }
  }
  // #################################### StatesList ################################
  getIds = (e) => {
    let token = localStorage.getItem('token')
    axios({
      method: 'post',
      url: API_URL + '/states/statesDropDown',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      },
    }).then(response => {
      var data = response.data.data
      this.setState({ listOfStates: data })
    })
  }
  render() {
    let { logo, supplierName,supplierId_, address, country, state1, city, emailId, mobileNo, fax, listOfStates, errors, supplierId } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>{supplierId == undefined ? "Añadir " : "Editar "} proveedores</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}> Página principal</li>
                <li className='breadcrumb-item'>Master Management</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/supplier')}>Suppliers</li>
                <li className='breadcrumb-item active'>{supplierId == undefined ? "Añadir proveedores" : "Editar proveedor"}</li>
              </ul>
            </div>
          </div>
        </div>

        <div className='card animated fadeIn mb-2'>
          <div className="card-header" data-spy="affix" data-offset-top="100">
            <div className="button-continer text-right">
              <button type='button' className='btn btn-primary' onClick={this.addSuppliers}> <span>{supplierId == undefined ? "Añadir" : "Guardar"}</span> </button>
              <button type='button' className='btn btn-outline-primary' onClick={() => this.props.history.push('/supplier')} > Cancelar </button>
            </div>
          </div>
          <div className='card-body'>
            <form className='form-sample'>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Imagen :</label>
                <div className="drop-section mb-3 col-md-4">
                  <div className="form-group inputDnD mb-0">
                    <div className="upload-overlay d-flex justify-content-center align-items-center">
                      <div className="upload-wrapper">
                        <i className="fa fa-upload"></i>
                        <span><button type="button"  >Elige un archivo</button>  o arrástralo aquí</span>
                      </div>
                    </div>
                    <label className="sr-only" >Subir archivo</label>
                    <input type="file" className="form-control-file text-primary font-weight-bold" name='image'
                      id="file" accept="image/*" data-title="Drag and drop a file"
                      onChange={this.fileChangedHandler.bind(this)} />
                  </div>
                  {this.state.fileVisible ?
                    <span>
                      <div className='product-img'> <img src={this.state.imagePreviewUrl} alt={this.state.filename} /><span> <i className='fa fa-times-circle-o text-danger pull-right fa-2x'
                        onClick={() => this.setState({ fileVisible: !this.state.fileVisible, file: '' })} /></span> </div>
                      <span>{this.state.filename}</span>

                    </span> : null}
                  <div className="button-continer pull-right my-2">
                    <button className="btn btn-primary" type="button" onClick={() => this.metaFileUpload()} >Cargar imagen</button>

                  </div>
                </div>
                <div className="col-md-4">
                  <div className="row justify-content-between my-2">
                    <div className="col-md-12 select-image"><div className="product-img">
                      {logo ? <img src={IMAGE_URL + logo + "?" + Math.random()} alt={logo} /> : <img src="/assets/images/no-image-icon-4.png" />}
                    </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Nombre del proveedor<span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='supplierName'
                    placeholder='Ingrese el nombre del proveedor'
                    value={supplierName}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.supplierName} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Identificación del proveedor<span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='supplierId_'
                    placeholder='Ingrese la identificación del proveedor'
                    value={supplierId_}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.supplierName} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Habla a <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>

                  <textarea placeholder="Ingrese Habla a" className='form-control' name='address' value={address} onChange={this.handleChange}></textarea>
                  <span className="error-block"> {errors.address} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Country <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <select className='form-control' value={country} onChange={this.handleChange}>
                    <option value='Argentina'>Argentina</option>
                  </select>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Estado <span className="text-danger">*</span>:</label>
                <div className='col-md-8 co-sm-7'>
                  <select className='form-control' name='state1' value={state1} onFocus={this.getIds} onChange={(e, value) => this.setState({ state1: e.target.value })} >
                    <option value=''>{state1}</option>
                    {listOfStates.map((each, key) => {
                      return (
                        <option value={each.province} key={key}>{each.province}</option>
                      )
                    })}
                  </select>
                  <span className="error-block"> {state1 ? "" :errors.state1} </span>

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Ciudad <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='city'
                    placeholder='Ingrese Ciudad'
                    value={city}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.city} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Email <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  {this.state.supplierId == undefined ? <input
                    className='form-control'
                    type='text'
                    name='emailId'
                    placeholder='Ingrese Email'
                    value={emailId}
                    onChange={this.handleChange}
                  /> : <input
                      className='form-control'
                      type='text'
                      name='emailId'
                      placeholder='Enter Email'
                      value={emailId}
                      disabled
                      onChange={this.handleChange}
                    />}
                  <span className="error-block"> {errors.emailId} </span>

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Telefono no <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='number'
                    name='mobileNo'
                    placeholder='Ingrese el número de teléfono'
                    value={mobileNo}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.mobileNo} </span>

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Fax :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='number'
                    name='fax'
                    placeholder='Ingresar fax'
                    value={fax}
                    onChange={this.handleChange}
                  />
                </div>
              </div>

            </form>
          </div>
        </div>

      </Home>
    );
  }
}

export default connect(null, actions)(AddSuppliers);
