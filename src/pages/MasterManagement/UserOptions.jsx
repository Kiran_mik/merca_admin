import React, { Component } from 'react'
import Home from '../Home'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import swal from 'sweetalert'
import SimpleReactValidator from 'simple-react-validator'


class EditHomeSection extends Component {
    constructor(props) {
        super(props)
        this.validator = new SimpleReactValidator();
        this.state = {
            page: 1,
            pagesize: 10,
            question: '',
            isUpdate: false,
            answers: [
                {
                    answer: '',
                }
            ],
            errors: {}

        }
    }

    componentDidMount() {
        var permissions = this.props.permissionsList;
        if (permissions && permissions.role != 'Super Admin') {
            this.props.history.push('/dashboard')
        }
        this.homePageDetails()
    }

    validateForm = () => {
        let { question, errors } = this.state
        let formIsValid = true
        if (question == '' || question.trim() === '') {
            formIsValid = false
            errors['question'] = '* se requiere pregunta'
        }
                this.setState({ errors })
        return formIsValid
    }

    handleChange = (e) => {
        if (['answer'].includes(e.target.title)) {
            let answers = [...this.state.answers]
            answers[e.target.dataset.id][e.target.title] = e.target.value
            this.setState({ answers })
        } else {
            this.setState({ [e.target.name]: e.target.value, errors: Object.assign(this.state.errors, { question: "" }) })
        }
    }

    plus = () => {
        this.setState(prevState => ({
            answers: [
                ...prevState.answers,
                {
                    answer: '',
                }
            ]
        }))
        this.setState({ errors: Object.assign(this.state.errors, { answer: "" }) })
    }



    // ########################### add/update HomePageDetails #######################
    addPageDetails = (e) => {
        if (this.validateForm()) {
        let { question, answers } = this.state
        var answers_ = []
        answers.map(each => {
            if (each.answer === '' || each.answer.trim() === '') {
                return
            } else {
                answers_.push({ answer: each.answer })
            }
        })
        let body = { question, answers:answers_ }
        var token = localStorage.getItem("token")
        var url = '/options/addOptions'
        var method = 'post'
        var token = localStorage.getItem('token')
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
            let { data } = response
            if (data.status === 1) {
                swal("Opción añadida con éxito", '', 'success')
                this.homePageDetails()
            }
        })
        }

    }
    // ########################### HomePageDetails #######################
    homePageDetails = () => {
        var token = localStorage.getItem("token")
        var url = '/options/getOptionDetails'
        var method = 'get'
        var token = localStorage.getItem('token')
        this.props.commonApiCall(url, method, null, token, null, this.props, res => {
            let { data } = res
            if (data && data.data) {
                this.setState({
                    question: data.data.question,
                    answers: data.data.answers,
                    isUpdate: true,
                })
            }
        })
    }

    // ################ XXXXXX ################


    render() {
        let { question, answers, isUpdate, errors } = this.state
        return (
            <Home>
                <div className='row'>
                    <div className='col-md-12'>
                        <div className='page-header'>
                            <h3>Opciones para clientes</h3>
                            <ul className='breadcrumb '>
                                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                                <li className='breadcrumb-item'>Master Management</li>
                                <li className='breadcrumb-item active'>Opciones para clientes</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="card animated fadeIn mb-2">
                    <div className="card-header">
                        <h5 className='card-title'>Pregunta</h5>
                    </div>
                    <div className='card-body'>
                        <form className='form-sample'>
                            <div className='form-group row'>
                                <label className='col-lg-2 col-sm-3 col-form-label'>Pregunta :</label>
                                <div className='col-md-8 col-sm-7'>
                                    <input
                                        className='form-control'
                                        type='text'
                                        name='question'
                                        placeholder='ingrese la pregunta'
                                        value={question}
                                        onChange={this.handleChange}
                                    />
                                    <span className="error-block"> {errors.question} </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div className='card animated fadeIn mb-2'>
                    <div className='card-header'>
                        <h5 className='card-title'>Respuestas</h5>
                    </div>
                    <div className='card-body'>
                        <form className='form-sample'>
                            {answers.map((item, key) => {
                                let optionId = `optionId-${key}`
                                return (
                                    <div key={key} >
                                        <div className='form-group row'>
                                            <label className='col-lg-2 col-sm-3 col-form-label'><b>Option {key + 1}</b></label>
                                            <div className='col-md-8 col-sm-7'>
                                                <input
                                                    type='text'
                                                    name={optionId}
                                                    data-id={key}
                                                    id={optionId}
                                                    value={item.answer}
                                                    className='form-control'
                                                    title='answer'
                                                    placeholder='ingrese la opción'
                                                    onChange={this.handleChange}
                                                />
                                                {item.answer == '' ? <span className="error-block"> {errors.answer} </span> : null}
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                            <hr />
                            <div className='button-continer row justify-content-between'>
                                {answers.length > 1 || isUpdate ?
                                    <div className='button-group-container'>
                                        <button type='button' className='btn btn-success' onClick={this.addPageDetails} >Actualizar</button>
                                    </div> :
                                    <div className='button-group-container'>
                                        <button type='button' className='btn btn-primary' onClick={this.addPageDetails} ><i className='far fa-save' /> Guardar</button>
                                    </div>
                                }
                                {answers.length < 5 ? <button type='button' className='btn btn-primary float-right' onClick={this.plus} > <i className='fa fa-plus' /> Añadir </button> : null}
                            </div>
                        </form>

                        <div className='form-group row'>
                        </div>
                    </div>
                </div>


            </Home>
        )
    }
}

const mapStateToProps = state => ({
    permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(EditHomeSection)
