import React, { Component } from 'react';
import Home from '../Home'
import SimpleReactValidator from 'simple-react-validator'
import * as actions from '../../actions';
import { connect } from "react-redux";
import swal from 'sweetalert'
import queryString from 'query-string'
import { toast } from 'react-toastify';

class AddBrand extends Component {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      brandName: '',
      brandId_: '',
      brandId: '',
      errors: {
        brandName: ''
      },
    }

  }

  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.role != 'Super Admin') {
      this.props.history.push('/dashboard')
    }
    this.getAllDetails()
  }
  validateForm = () => {
    let { brandName, brandId_, errors } = this.state
    let formIsValid = true
    if (!brandName || brandName.trim() === '') {
      formIsValid = false
      errors['brandName'] = '* Se requiere marca'
    }
    if (!brandId_ || brandId_.trim() === '') {
      formIsValid = false
      errors['brandId_'] = '* Se requiere ID de marca'
    }
    // else formIsValid = true
    this.setState({ errors })
    return formIsValid
  }
  getAllDetails = () => {
    var url = this.props.location.search;
    var params = queryString.parse(url);
    var id = params.id;
    this.setState({ brandId: id })
    if (id) {
      var body = { brandId: id }
      let token = localStorage.getItem('token')
      var url = '/brands/getBrandDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data) {
          this.setState({
            brandName: data.data.brandName,
            brandId_: data.data.brandId
          })
        }

      })
    }
  }
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }
  }


  addBrand = () => {
    if (this.validateForm()) {
      let token = localStorage.getItem('token');
      var { brandName, brandId_, brandId } = this.state
      var data = { brandName, brandId: brandId_ }
      if (brandId != '') {
        data.id = brandId
      }
      var body = data
      let url = "/brands/addBrands"
      let method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.statusCode === 228) {
          swal({
            title: "Agregado exitosamente !",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
              this.props.history.push('/brand')
              // }
            })
        }
        if (data.statusCode === 219) {
          swal({
            title: "Actualizado con éxito!",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
              this.props.history.push('/brand')
              // }
            })
        }
        if (data.statusCode === 441) {
          toast.error("BrandName ya existe !", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }
        if (data.statusCode === 457) {
          toast.error("BrandId ya existe !", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }
        if (data.message === "Invalid token." || data.message === "Token is expired.") {
          swal('Sesión expirada', '', 'error')
            .then((willDelete) => {
              // if (willDelete) {
              localStorage.removeItem("token"); this.props.history.push('/');
              // }
            })
        }
      })
    }
  }

  render() {
    let { errors, brandId } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>{brandId == undefined ? "Agregar " : "Editar "} Brand</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}> Página principal</li>
                <li className='breadcrumb-item'>Master Management</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/brand')}>Marca</li>
                <li className='breadcrumb-item active'>{brandId == undefined ? "Agregar marca" : "Editar marca"}</li>
              </ul>
            </div>
          </div>
        </div>

        <div className='card animated fadeIn mb-2'>
          <div className="card-header" data-spy="affix" data-offset-top="100">
            <div className="button-continer text-right">
              <button type='button' className='btn btn-primary' onClick={this.addBrand}> <span>{brandId == undefined ? "Añadir" : "Guardar"}</span> </button>
              <button type='button' className='btn btn-outline-primary' onClick={() => this.props.history.push('/brand')} > Cancelar </button>
            </div>
          </div>
          <div className='card-body'>
            <form className='form-sample'>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Nombre de la marca <span className="text-danger">*</span> : </label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='brandName'
                    placeholder='Ingrese el nombre de la marca'
                    value={this.state.brandName}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.brandName} </span>

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>ID de marca <span className="text-danger">*</span> : </label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='brandId_'
                    placeholder='Enter brand Id'
                    value={this.state.brandId_}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.brandId_} </span>
                </div>
              </div>
            </form>
          </div>
        </div>

      </Home>
    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(AddBrand)
