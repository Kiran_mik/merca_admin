import React, { Component } from 'react'
import { connect } from 'react-redux'
import Pagination from 'rc-pagination'
import swal from 'sweetalert'
import * as actions from '../../actions'
import 'antd/dist/antd.css'
import { Select } from 'antd'
import { isEmpty } from "lodash"
import Home from '../Home'
import Toggle from 'react-toggle'
import { API_URL, CSV_URL, EXCEL_URL } from '../../config/configs'
import { BeatLoader } from 'react-spinners';
import Loader from '../Loader'
import $ from 'jquery';
const FileDownload = require('js-file-download')

class Brand extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize:10,
      brandListing: [],
      sortData: { brandName: false, isActive: false },
      sort: {},
      length: 0,
      total: 0,
      listOfBrands: [],
      selectedBrand: [],
      loading: true,
      page_:false,
      download:false
    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.role!='Super Admin') {
      this.props.history.push('/dashboard')
    }
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').stop().slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').stop().slideToggle();
    });
    $('[data-toggle="tooltip"]').tooltip();

    this.listOfBrands()
  }

  // ##############################  Listing #############################
  listOfBrands = (e) => {
    let { page, pagesize, selectedBrand,sort } = this.state
    var data = {pagesize: pagesize }
    if(e){
      data.page=1
    }else{
      data.page=page
    }
    if (selectedBrand != '') {
      data.brandName = selectedBrand
    }
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    var body = data
    let token = localStorage.getItem('token')
    var url = '/brands/brandListing'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      let data1 = response.data.data
      if (response.data.status === 1) {
        this.setState({ brandListing: data.brandListing, total: data.total, length: data.brandListing.length, loading: false })
      } else {
        this.setState({ brandListing: [] })
      }
      if(e=='filter'){
        this.setState({page:1})
      }
    })
  }
    //*********** SORTING ************************//
    onSort = (column) => {
      let { sortData } = this.state;
      var element, value;
      for (const key in sortData) {
        if (key == column) {
          sortData[key] = !sortData[key];
          element = key;
          value = -1
          if (sortData[key]) {
            value = 1
          }
          this.setState({
            sort: { [element]: value }
          }, () => {
            this.listOfBrands();
          });
          this.setState({ sortData });
        }
        else {
          sortData[key] = false
          element = key;
          value = 1;
        }
      }
      this.setState({ sortData });
    }
  

  resetListing = (e) => {
    var url = '/brands/brandListing'
    var method = 'post'
    var { page, pagesize } = this.state
    var body = { page: page, pagesize: pagesize }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      let data1 = response.data.data
      if (response.data.status === 1) {
        this.setState({ brandListing: data.brandListing, total: data.total, length: data.brandListing.length, selectedBrand: [] })
      } else {
        this.setState({ brandListing: [] })
      }
    }
    )
  }

  //* ********** DELETE    ************************//
  deleteBrand = uid => {
    var delArr = this.state.multipleDelete;
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          var body = { brandId: [uid] }
          let token = localStorage.getItem('token')
          var url = '/brands/deleteBrands'
          var method = 'post'
          this.props.commonApiCall(url, method, body, token, null, this.props, response => {
            let { data } = response
            if (data.status == 1) {
              swal('Borrado exitosamente', '', 'success')
            }
            this.listOfBrands()
          })
            .catch(err => console.error(err))
        }
      });
  }



  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize
    }, () => this.listOfBrands());


  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 }, () => this.listOfBrands());
  }
  handleChange_ = (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1, page_: true }, () => this.listOfBrands());
    } else {
      this.setState({ pagesize: this.state.length, page: 1 }, () => this.listOfBrands());
    }
  }

  gettingDetails = (Id) => {
    this.props.history.push('/addbrand?id=' + Id)
  }

  // ########################### for Search #########################
  getIds = (e) => {
    let token = localStorage.getItem('token')
    var body = { type: "brandName", searchText: e }
    var url = '/brands/brandFieldsList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var data = response.data.data
      var listOfBrands = []
      data.map((each, key) => {
        listOfBrands.push(each.brandName)
        this.setState({ listOfBrands })
      })

    })
  }

  // ########################### Change Status #########################
  changeStatus(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { brandId: [Id], isActive: status }
    var urlkey = '/brands/changeBrandStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        swal('Actualizado con éxito !', '', 'success')
        this.listOfBrands()
      } else if (data.message === 'Invalid token') {
        this.props.history.push('/')
      } else {
        swal(data.message, '', 'error')
      }
    })
  }

  // ########################### download CSV #########################

  downloadCSV(type) {
    let token = localStorage.getItem('token')
    this.setState({download:true})
    let data = { filteredFields: ["brandId","brandName", "isActive"], brandsArray: [] }
    this.state.brandListing.map((each) => {
      return (
        data.brandsArray.push(each._id)
      )
    })
    var body = data
    var url = '/brands/' + type
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (data) {
        this.setState({download:false})
        if (type === 'downloadBrandsCsvFile') {
          window.open(CSV_URL + data.filePathAndName, '_blank');
          // FileDownload(CSV_URL  +  data.filePathAndName, data.filePathAndName);
        } else {
          window.open(EXCEL_URL + data.filePathAndName, '_blank');
          // FileDownload(EXCEL_URL  +  data.filePathAndName, data.filePathAndName);
        }
      }
    })
  }



  render() {
    const Option = Select.Option
    let { pagesize, page, sortData, total, brandListing, length, listOfBrands, selectedBrand } = this.state
    const filteredBrand = listOfBrands.filter(o => !selectedBrand.includes(o));

    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Marca</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item'>Master Management</li>
                <li className='breadcrumb-item active'>Marca</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
       { this.state.download ?<Loader/> :null}
        <div className='card animated fadeIn'>
          <div className="card-header">
            <div className="row data-filter justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  <div className="dropdown">
                    <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                      <span>Herramientas</span>
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadBrandsExcelFile")}>Exportar a Excel</a>
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadBrandsCsvFile")}>Exportar a CSV</a>
                    </div>
                  </div>
                  <button type="button" className="btn btn-primary" onClick={() => this.props.history.push('/addbrand')}><i className="fa fa-plus" /> <span>Agregar marca</span></button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre de la marca :</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre de la marca"
                    value={selectedBrand}
                    onChange={(selectedBrand) => this.setState({ selectedBrand })}
                    onSearch={(e) => this.getIds(e)}
                    onFocus={(e) => this.getIds(e)}
                    style={{ width: '100%' }}
                  >
                    {filteredBrand.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
            </div>
            <hr />
            <div className="pull-right filter-button">

              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={()=>this.listOfBrands('filter')}>
              Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetListing}>
              Reiniciar
            </button>
            </div>
          </div>
          <div className='card-body'>

            <div className='table-responsive'>
              <table className='table dataTable with-image row-border hover custom-table table-striped'>
                <thead>
                  <tr>
                    <th width="50%" sortable-column="brandName" onClick={this.onSort.bind(this, 'brandName')}>Nombre de la marca  <i aria-hidden='true' className={(sortData['brandName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> 
                    <th width='25%'>ID de marca</th>
                    <th width="25%" sortable-column="isActive" onClick={this.onSort.bind(this, 'isActive')}>Estado<i aria-hidden='true' className={(sortData['isActive']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> 
                    <th>Comportamiento</th>
                  </tr>
                </thead>
                {brandListing.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                  <tbody>
                    {brandListing.map((each, id) => {
                      return (
                        <tr key={id}>
                          <td>{each.brandName}</td>
                          <td>{each.brandId}</td>
                          <td><Toggle checked={each.isActive} className='custom-classname' onChange={() => this.changeStatus(each.isActive, each._id)} /></td>
                          <td>
                            <button > <i className='fa fa-edit text-primary' data-toggle="tooltip" title="Edit" onClick={() => this.gettingDetails(each._id)} /> </button>
                            <button > <i className='fa fa-trash text-danger' data-toggle="tooltip" title="Delete" onClick={() => this.deleteBrand(each._id)} /> </button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>}
              </table>
            </div>
            {brandListing.length > 0 ?
              <div className="table-footer d-flex justify-content-between">
                <div className="table-shorting">
                  <label>Demostración</label>
                  <Select showSearch placeholder={<b> {total <= 10 ? total : length}</b>} optionFilterProp="children"
                    onSelect={this.handleChangePageSize.bind(this)} 
                    value={this.state.page_ ? pagesize : (total <= 10? total : length)}
                    onSearch={this.handleChange_}>
                    <Option value={10}>10</Option>
                    <Option value={25}>25</Option>
                    <Option value={50}>50</Option>
                    <Option value={75}>75</Option>
                  </Select>
                  <label>Fuera de  {total} Marcas</label>
                </div>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale/>
                </div>
              </div> : null}
          </div>
        </div>
      </Home>
    )
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(Brand)
