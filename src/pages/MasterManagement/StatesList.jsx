import React, { Component } from 'react'
import { connect } from 'react-redux'
import Pagination from 'rc-pagination'
import swal from 'sweetalert'
import * as actions from '../../actions'
import 'antd/dist/antd.css'
import { Select } from 'antd'
import Home from '../Home'
import { isEmpty } from "lodash"
import Toggle from 'react-toggle'
import _ from 'lodash'
import { API_URL, CSV_URL, EXCEL_URL } from '../../config/configs'
import $ from 'jquery';
import { BeatLoader } from 'react-spinners';
import Laoder from '../Loader'
const FileDownload = require('js-file-download')

class States extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 10,
      statesListing: [],
      best_sellers: false,
      multipleDelete: [],
      searchItem: '',
      total: 23,
      sort: {},
      sortData: { province: false, capital: false, hasc: false },
      province: true,
      capital: true,
      hasc: true,
      status: true,
      length: '',
      listOfCapitals: [],
      selectedCapital: [],
      listOfProvince: [],
      selectedProvince: [],
      listOfHasc: [],
      selectedHasc: [],
      loading: true,
      page_:false,
      download:false
    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.role!='Super Admin') {
      this.props.history.push('/dashboard')
    }
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').stop().slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').stop().slideToggle();
    });
    $('[data-toggle="tooltip"]').tooltip();

    this.listOfStates('pagesize')
  }


  // ##############################  Listing #############################
  listOfStates = (e) => {
    let { page, pagesize, sort, selectedCapital, selectedHasc, selectedProvince } = this.state
    var data = {}
    if (e==='filter') {
      data.page = 1
    } else if(e==='pagesize') {
      data=data
    }else{
      data.page = page
    }
    if(e==='pagesize'){
      data=data
    }else{
      data.pagesize=pagesize
    }
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (selectedCapital != '') {
      data.capital = selectedCapital
    }
    if (selectedProvince != '') {
      data.province = selectedProvince
    }
    if (selectedHasc != '') {
      data.hasc = selectedHasc
    }
    var body = data
    let token = localStorage.getItem('token')
    var url = '/states/stateListing'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      let data1 = response.data.data
      if (response.data.status === 1) {
        this.setState({ statesListing: data.stateListing, total: data.total, length: data.stateListing.length, loading: false })
        if (data1.manageStateListing) {
          this.setState({
            province: data1.manageStateListing.province,
            capital: data1.manageStateListing.capital,
            hasc: data1.manageStateListing.hasc,
            pagesize: data1.manageStateListing.pageSize,

          })
        }
      } else {
        this.setState({ statesListing: [] })
      }
    })
  }

  //* **********  RESET LISTING  ************************//
  resetListing = (e) => {
    var url = '/states/stateListing'
    var method = 'post'
    var { page, pagesize } = this.state
    var body = { page: page, pagesize: pagesize }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (data1.status === 0) {
        this.setState({ userListing: [] })
      }
      else
        this.setState({ statesListing: data.stateListing, total: data.total, length: data.stateListing.length })
      if (data.manageStateListing) {
        this.setState({
          province: data.manageStateListing.province,
          capital: data.manageStateListing.capital,
          hasc: data.manageStateListing.hasc,
          selectedHasc: [],
          selectedCapital: [],
          selectedProvince: []
        })
      }
    }
    )
  }

  //* ********** SORTING ************************//
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState(
          {
            sort: { [element]: value }
          },
          () => {
            this.listOfStates()
          }
        )
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }

  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize
    }, () => this.listOfStates());
  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 },()=>this.setTableRows(), () => this.listOfStates());
  }
  handleChange_ = (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1,  },()=>this.setTableRows(), () => this.listOfStates());
    } 
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.listOfStates());
    // }
  }

  //* ********** DELETE State   ************************//
  deleteState = uid => {
    var delArr = this.state.multipleDelete;
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          var body = { stateId: [uid] }
          var url = '/states/deleteStates'
          var method = 'post'
          var token = localStorage.getItem('token')
          this.props.commonApiCall(url, method, body, token, null, this.props, response => {
            let { data } = response
            if (data.status == 1) {
              swal(data.message, '', 'success')
            } else {
              swal(data.message, '', 'error')
            }
            this.listOfStates()
          })
            .catch(err => console.error(err))
        }
      });
  }
  gettingDetails = (Id) => {
    this.props.history.push(`/editstates/${Id}`)
  }


  // ########################### Set Table Rows #########################

  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { province, capital, hasc,pagesize } = this.state
    var body = { province, capital, hasc ,pageSize:pagesize}
    var url = '/states/setStateFilter'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      if (response) {
        this.listOfStates()
        if(e){
          swal('Updated successfully !', '', 'success')
        }
      }
    })
  }


  // ########################### getProductName for Search #########################
  getIds = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: [name], searchText: e }
    var url = '/states/stateFieldsList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (name === 'province') {
        var unArr = _.uniqBy(data, (e) => {
          return e.province.toString();
        });
        this.setState({ listOfProvince: unArr })
      }
      if (name === 'capital') {
        var capArr = _.uniqBy(data, (e) => {
          return e.capital.toString();
        });
        this.setState({ listOfCapitals: capArr })
      }
      if (name === 'hasc') {
        var hasArr = _.uniqBy(data, (e) => {
          return e.hasc.toString();
        });
        this.setState({ listOfHasc: hasArr })
      }

    })
  }
  // ########################### download CSV #########################
  downloadCSV(type) {
    let { province, capital, hasc, statesListing } = this.state
    let token = localStorage.getItem('token')
    let data = { filteredFields: [], statesArray: [] }
    this.setState({download:true})
    if (province) {
      data.filteredFields.push('province')
    };
    if (capital) {
      data.filteredFields.push('capital')
    };
    if (hasc) {
      data.filteredFields.push('hasc')
    };
    statesListing.map((each) => {
      return (
        data.statesArray.push(each._id)
      )
    })
    var body = data
    var url =  '/states/' + type
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (data) {
        this.setState({ download: false, multipleDelete: [] })
        if (type === 'downloadStateCsvFile') {
          window.open(CSV_URL + data.filePathAndName, '_blank');
          // FileDownload(CSV_URL  +  data.filePathAndName, data.filePathAndName);

        } else {
          window.open(EXCEL_URL + data.filePathAndName, '_blank');
          // FileDownload(EXCEL_URL  +  data.filePathAndName, data.filePathAndName);
        }
      }
    })
  }
  // ########################### Change Status #########################
  changeStatus(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { stateId: [Id], isActive: status }
    var urlkey = '/states/changeStateStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        if (status == false) {
          swal('Estado desactivado con éxito !', '', 'success')
        }
        else {
          swal('Estado activado con éxito!', '', 'success')
        }
        this.listOfStates()
      } else if (data.message === 'Invalid token') {
        this.props.history.push('/')
      } else {
        swal(data.message, '', 'error')
      }
    })
  }


  render() {
    const Option = Select.Option
    let self = this
    let { statesListing, pagesize, page, sortData, total, province, capital, hasc, length, status,
      selectedCapital, selectedHasc, selectedProvince, listOfCapitals, listOfProvince, listOfHasc } = this.state

    const filteredHasc1 = listOfHasc.map((each) => { return each.hasc });
    const filteredHasc = filteredHasc1.filter(o => !selectedHasc.includes(o));

    const filteredProvince1 = listOfProvince.map((each) => { return each.province });
    const filteredProvince = filteredProvince1.filter(o => !selectedProvince.includes(o));

    const filteredCapital1 = listOfCapitals.map((each) => { return each.capital });
    const filteredCapital = filteredCapital1.filter(o => !selectedCapital.includes(o));
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3> Estados</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item'>Master Management</li>
                <li className='breadcrumb-item active'>Estados</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />

        <div className='card animated fadeIn'>
          <div className="card-header">
            <div className="row data-filter justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  <div className="dropdown">
                    <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                      <span>Herramientas</span>
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadStateExcelFile")}>Exportar a Excel</a>
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadStateCsvFile")}>Exportar a CSV</a>
                    </div>
                  </div>
                  <button type="button" className="btn btn-primary" onClick={() => this.props.history.push('/addstates')}><i className="fa fa-plus" /> <span> Add States</span></button>
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ province: !this.state.province })} checked={province} /><span></span>Provincia</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ capital: !this.state.capital })} checked={capital} /><span></span>Capital</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ hasc: !this.state.hasc })} checked={hasc} /><span></span>Hasc (código de subdivisión)</label></li>
            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.listOfStates} >
            Reiniciar
            </button>

            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({ province: true, capital: true, hasc: true })}>
            Seleccionar todo
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={()=>this.setTableRows('rows')}>
            Guardar
            </button>
          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Provincia</label>
                  <Select
                    mode="multiple"
                    placeholder="Entrar provincia"
                    value={selectedProvince}
                    onChange={(selectedProvince) => this.setState({ selectedProvince })}
                    isOptionUnique="true"
                    onSearch={(e) => this.getIds(e, 'province')}
                    onFocus={(e) => this.getIds(e, 'province')}
                    style={{ width: '100%' }}
                  >
                    {filteredProvince.map((item) => (
                      <Select.Option key={item} value={item} >
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Capital</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingresar capital"
                    value={selectedCapital}
                    onChange={(selectedCapital) => this.setState({ selectedCapital })}
                    // onInputKeyDown={(e)=>this.getIds(e,'capital')}
                    onSearch={(e) => this.getIds(e, 'capital')}
                    onFocus={(e) => this.getIds(e, 'capital')}
                    style={{ width: '100%' }}
                  >
                    {filteredCapital.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label> Hasc (código de subdivisión)</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese Hasc"
                    value={selectedHasc}
                    onChange={(selectedHasc) => this.setState({ selectedHasc })}
                    onSearch={(e) => this.getIds(e, 'hasc')}
                    onFocus={(e) => this.getIds(e, 'hasc')}
                    style={{ width: '100%' }}
                  >
                    {filteredHasc.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
            </div>
            <hr />
            <div className="pull-right filter-button">
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={()=>this.listOfStates('filter')}>
              Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetListing}>
              Reiniciar
            </button>
            </div>
          </div>
          <div className='card-body'>
            <div className='table-responsive'>
              <table className='table dataTable with-image row-border hover custom-table table-striped'>
                <thead>
                  <tr>
                    {province ? <th sortable-column="province" onClick={this.onSort.bind(this, 'province')} >
                    Provincia <i aria-hidden='true' className={(sortData['province']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {capital ? <th>Capital</th> : null}
                    {hasc ? <th> Hasc (código de subdivisión) </th> : null}
                    {status ? <th> Estado </th> : null}
                    <th>Comportamiento</th>
                  </tr>
                </thead>
                {statesListing.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                  <tbody>
                    {statesListing.map((each, id) => {
                      return (
                        <tr key={id}>
                          {province ? <td>{each.province}</td> : null}
                          {capital ? <td>{each.capital}</td> : null}
                          {hasc ? <td>{each.hasc}</td> : null}
                          {status ? <td><Toggle checked={each.isActive} className='custom-classname' onChange={() => this.changeStatus(each.isActive, each._id)} /></td> : null}
                          <td>  <button
                          >
                            <i className='fa fa-edit text-primary' data-toggle="tooltip" title="Edit" onClick={() => this.gettingDetails(each._id)} />
                          </button>
                            <button >
                              <i className='fa fa-trash text-danger' data-toggle="tooltip" title="Delete" onClick={() => this.deleteState(each._id)} />
                            </button></td>
                        </tr>
                      )
                    })}

                  </tbody>}
              </table>
            </div>
            {statesListing.length > 0 ?
              <div className="table-footer d-flex justify-content-between">
                <div className="table-shorting">
                  <label>Demostración</label>
                  <Select showSearch placeholder={<b> {total < 10 ? total : length}</b>} optionFilterProp="children"
                    onSelect={this.handleChangePageSize.bind(this)} 
                    value={this.state.page_ ? pagesize : (length <=pagesize ?length:pagesize)}
                    onSearch={this.handleChange_}>
                    <Option value={10}>10</Option>
                    <Option value={25}>25</Option>
                    <Option value={50}>50</Option>
                    <Option value={75}>75</Option>
                  </Select>
                  <label>Fuera de {total} Estados</label>
                </div>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale/>
                </div>
              </div> : null}
          </div>
        </div>
      </Home>
    )
  }
}

// export default connect(null, actions)(States);
const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(States)
