import React, { Component } from 'react'
import { connect } from 'react-redux'
import Pagination from 'rc-pagination'
import swal from 'sweetalert'
import * as actions from '../../actions'
import 'antd/dist/antd.css'
import { Select } from 'antd'
import Home from '../Home'
import Toggle from 'react-toggle'
import { API_URL, IMAGE_URL, CSV_URL, EXCEL_URL } from '../../config/configs'
import _ from 'lodash'
import $ from 'jquery';
import { isEmpty } from "lodash"
import { BeatLoader } from 'react-spinners';
import Loader from '../Loader'
const FileDownload = require('js-file-download')

class Suppliers extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 10,
      total: 0,
      suppliersList: [],
      supplierName: true,
      supplierId: true,
      logo: true,
      address: true,
      email: true,
      phoneNo: true,
      fax: true,
      status: true,
      length: '',
      sort: {},
      sortData: { supplierName: false, supplierId: false, address: false, emailId: false, isActive: false },
      listOfSuppliers: [],
      selectedSupplier: [],
      listOfEmails: [],
      selectedEmails: [],
      listOfFax: [],
      selectedFax: [],
      listOfPhoneNo: [],
      selectedPhoneNo: [],
      listOfAddress: [],
      selectedAddress: [],
      multipleDelete: [],
      selectedOption: 'Selecciona aquí',
      selectAll: false,
      loading: true,
      page_: false,
      download: false

    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.role != 'Super Admin') {
      this.props.history.push('/dashboard')
    }
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').stop().slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').stop().slideToggle();
    });
    $('[data-toggle="tooltip"]').tooltip();

    this.listOfSuppliers('pagesize')
  }



  // ##############################  Listing #############################
  listOfSuppliers = (e) => {
    let { page, pagesize, sort, selectedEmails, selectedPhoneNo, selectedFax, selectedSupplier, selectedAddress } = this.state
    var data = { pagesize }
    if (e === 'filter') {
      data.page = 1
    } else if (e === 'pagesize') {
      data = data
    } else {
      data.page = page
    }
    if (e === 'pagesize') {
      data = data
    } else {
      data.pagesize = pagesize
    }
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (selectedEmails != '') {
      data.emailId = selectedEmails
    }
    if (selectedPhoneNo != '') {
      data.mobileNo = selectedPhoneNo
    }
    if (selectedFax != '') {
      data.fax = selectedFax
    }
    if (selectedSupplier != '') {
      data.supplierName = selectedSupplier
    }
    if (selectedAddress != '') {
      data.address = selectedAddress
    }

    var body = data
    let token = localStorage.getItem('token')
    var url = '/suppliers/supplierListing'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      let data1 = response.data.data
      if (response.data.status === 1) {
        this.setState({ suppliersList: data.supplierListing, total: data.total, length: data.supplierListing.length, loading: false })
        if (data && data.supplierListing.length <= pagesize) {
          this.setState({ page_: false })
        }

        if (data1.manageSupplierListing) {
          this.setState({
            supplierName: data1.manageSupplierListing.supplierName,
            supplierId: data1.manageSupplierListing.supplierId,
            logo: data1.manageSupplierListing.logo,
            address: data1.manageSupplierListing.address,
            email: data1.manageSupplierListing.emailId,
            phoneNo: data1.manageSupplierListing.mobileNo,
            fax: data1.manageSupplierListing.fax,
            status: data1.manageSupplierListing.status,
            pagesize: data1.manageSupplierListing.pageSize
          })
        }
      } else {
        this.setState({ supplierListing: [] })
      }

    })
  }

  //* ********** SORTING ************************//
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState(
          {
            sort: { [element]: value }
          },
          () => {
            this.listOfSuppliers()
          }
        )
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }
  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize
    }, () => this.listOfSuppliers());
    this.setState({ selectAll: false }, () => this.selectAllcheck())

  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 }, () => this.setTableRows(), () => this.listOfSuppliers());
  }
  handleChange_ = (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1 }, () => this.setTableRows(), () => this.listOfSuppliers());
    }
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.listOfSuppliers());
    // }
  }

  // ########################### Set Table Rows #########################

  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { supplierName, supplierId, logo, address, email, phoneNo, fax, status, pagesize } = this.state
    var body = { supplierName, supplierId, logo, address, emailId: email, mobileNo: phoneNo, fax, status, pageSize: pagesize }
    var url = '/suppliers/setSupplierFilter'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      if (response) {
        this.listOfSuppliers()
        if (e) {
          swal('Actualizado con éxito !', '', 'success')
        }
      }
    })
  }

  // ########################### Change Status #########################
  changeStatus(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { supplierId: [Id], isActive: status }
    var urlkey = '/suppliers/changeSupplierStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        if (status == false) {
          swal('Proveedor desactivado con éxito !', '', 'success')
        }
        else {
          swal(' Proveedor activado con éxito !', '', 'success')
        }
        this.listOfSuppliers()
      } else if (data.message === 'Invalid token') {
        this.props.history.push('/')
      } else {
        swal(data.message, '', 'error')
      }
    })
  }
  //* ********** DELETE    ************************//
  deleteSupplier = uid => {
    var delArr = this.state.multipleDelete;
    swal({
      title: "¿Estás seguro?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {

          if (delArr.length > 0) {
            var body = { supplierId: delArr }
          }
          else {
            var body = { supplierId: [uid] }
          }
          let token = localStorage.getItem('token')
          var url = '/suppliers/deleteSuppliers'
          var method = 'post'
          this.props.commonApiCall(url, method, body, token, null, this.props, response => {
            let { data } = response
            if (data.status == 1) {
              swal('Borrado exitosamente', '', 'success')
            }
            this.listOfSuppliers()
          })
            .catch(err => console.error(err))
        }
      });
  }

  gettingDetails = (Id) => {
    this.props.history.push(`/editsuppliers/${Id}`)
  }


  // ###########################  for Search #########################
  getIds = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: [name], searchText: e }
    var url = '/suppliers/supplierFieldsList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (response && response.data && response.data.data) {
        if (name === 'supplierName') {
          var sArr = _.uniqBy(data, (e) => {
            return e.supplierName.toString();
          });
          this.setState({ listOfSuppliers: sArr })
        }
        if (name === 'emailId') {
          var eArr = _.uniqBy(data, (e) => {
            return e.emailId.toString();
          });
          this.setState({ listOfEmails: eArr })
        }
        if (name === 'mobileNo') {
          var phArr = _.uniqBy(data, (e) => {
            return e.mobileNo;
          });
          this.setState({ listOfPhoneNo: phArr })

        }
        if (name === 'fax') {
          var fArr = _.uniqBy(data, (e) => {
            return e.fax;
          });
          this.setState({ listOfFax: fArr })
        }
        if (name === 'address') {
          var aArr = _.uniqBy(data, (e) => {
            return e.address.toString();
          });
          this.setState({ listOfAddress: aArr })

        }
      }
    })
  }


  //* **********  RESET LISTING  ************************//
  resetListing = (e) => {
    var url = '/suppliers/supplierListing'
    var method = 'post'
    var { page, pagesize } = this.state
    var body = { page: page, pagesize: pagesize }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      let data1 = response.data.data
      if (response.data.status === 1) {
        this.setState({
          suppliersList: data.supplierListing, total: data.total, length: data.supplierListing.length, selectedEmails: [],
          selectedFax: [],
          selectedPhoneNo: [],
          selectedAddress: [],
          selectedSupplier: []
        })
        if (data1.manageSupplierListing) {
          this.setState({
            supplierName: data1.manageSupplierListing.supplierName,
            supplierId: data1.manageSupplierListing.supplierId,
            logo: data1.manageSupplierListing.logo,
            address: data1.manageSupplierListing.address,
            email: data1.manageSupplierListing.emailId,
            phoneNo: data1.manageSupplierListing.mobileNo,
            fax: data1.manageSupplierListing.fax,
            status: data1.manageSupplierListing.status,

          })
        }
      } else {
        this.setState({ supplierListing: [] })
      }
    }
    )
  }
  // ########################### download CSV #########################
  downloadCSV(type, array) {
    let { supplierName, logo, address, selectedEmails, selectedPhoneNo, selectedFax, selectedSupplier, selectedAddress, email, phoneNo, fax, supplierId, multipleDelete } = this.state
    let token = localStorage.getItem('token')
    this.setState({ downlaod: true })
    if (array === "totalList") {
      var data = { filteredFields: ["logo", "supplierName", "supplierId", "address", "emailId", "mobileNo", 'fax'] }
    }
    if (array === "filteredList") {
      var data = { filteredFields: [] }
      if (supplierName) {
        data.filteredFields.push('supplierName')
      };
      if (logo) {
        data.filteredFields.push('logo')
      };
      if (supplierId) {
        data.filteredFields.push('supplierId')
      };
      if (address) {
        data.filteredFields.push('address')
      };
      if (email) {
        data.filteredFields.push('emailId')
      }
      if (phoneNo) {
        data.filteredFields.push('mobileNo')
      }
      if (fax) {
        data.filteredFields.push('fax')
      };
      if (multipleDelete.length > 0) {
        data.suppliersArray = multipleDelete
      }
      if (selectedEmails != '') {
        data.emailId = selectedEmails
      }
      if (selectedPhoneNo != '') {
        data.mobileNo = selectedPhoneNo
      }
      if (selectedFax != '') {
        data.fax = selectedFax
      }
      if (selectedSupplier != '') {
        data.supplierName = selectedSupplier
      }
      if (selectedAddress != '') {
        data.address = selectedAddress
      }
    }
    var body = data
    var url = '/suppliers/' + type
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      this.setState({ download: false, multipleDelete: [] })
      if (data) {
        if (type === 'downloadSupplierCsvFile') {
          window.open(CSV_URL + data.filePathAndName, '_blank');
          // FileDownload(CSV_URL  +  data.filePathAndName, data.filePathAndName);
        } else {
          window.open(EXCEL_URL + data.filePathAndName, '_blank');
          // FileDownload(EXCEL_URL  +  data.filePathAndName, data.filePathAndName);
        }
      }
    })
  }


  checkArray(_id) {
    let { multipleDelete } = this.state
    if (multipleDelete.includes(_id)) {
      return true
    } else {
      return false
    }
  }

  onCheckbox(_id, val) {
    var delarray = this.state.multipleDelete
    if (!delarray.includes(_id)) {
      delarray.push(_id)
    } else {
      delarray.splice(delarray.indexOf(_id), 1)
    }
    if (delarray.length != this.state.suppliersList.length) {
      this.setState({ checked: false })
    }
    if (this) this.setState({ multipleDelete: delarray })
  }

  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { suppliersList } = this.state
    if (this.state.selectAll) {
      suppliersList.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      suppliersList.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }

  applyfilter = () => {
    var delArr = this.state.multipleDelete
    if (this.state.selectedOption == 'delete') {
      if (delArr.length > 0) {
        this.deleteSupplier(...delArr)
      }
    }
    if (this.state.selectedOption == 'active') {
      if (delArr.length > 0) {
        var body = { supplierId: delArr, isActive: true }
      }
    }
    if (this.state.selectedOption == 'inActive') {
      if (delArr.length > 0) {
        var body = { supplierId: delArr, isActive: false }
      }
    }
    var url = '/suppliers/changeSupplierStatus'
    var method = 'post'
    var token = localStorage.getItem('token')
    if (delArr.length > 0) {
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          this.listOfSuppliers()
          this.setState({ multipleDelete: [], selectedOption: 'Select here' })
          swal('Actualizado con éxito!', '', 'success')
        }
      })
    } else {
      this.setState({ selectedOption: 'Select here' })
      swal('Select atleast one supplier name', '', 'info')
    }

  }
  render() {
    const Option = Select.Option
    let { suppliersList, pagesize, page, sortData, total, supplierName, logo, address, email, phoneNo, fax, status, supplierId,
      length, selectedSupplier, listOfSuppliers, listOfEmails, selectedEmails, selectedPhoneNo, listOfPhoneNo,
      selectedFax, listOfFax, selectedAddress, listOfAddress, selectedOption } = this.state


    const filteredSuppliers1 = listOfSuppliers.map((each) => { return each.supplierName });
    const filteredSuppliers = filteredSuppliers1.filter(o => !selectedSupplier.includes(o));

    const filteredEmails1 = listOfEmails.map((each) => { return each.emailId });
    const filteredEmails = filteredEmails1.filter(o => !selectedEmails.includes(o));


    const filteredFax1 = listOfFax.map((each) => { return each.fax });
    const filteredFax = filteredFax1.filter(o => !selectedFax.includes(o));
    const filteredPhoneNo1 = listOfPhoneNo.map((each) => { return each.mobileNo });
    const filteredPhoneNo = filteredPhoneNo1.filter(o => !selectedPhoneNo.includes(o));
    const filteredAddress1 = listOfAddress.map((each) => { return each.address });
    const filteredAddress = filteredAddress1.filter(o => !selectedPhoneNo.includes(o));




    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Proveedoras</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item'>Master Management</li>
                <li className='breadcrumb-item active'>Proveedoras</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
        {this.state.download ? <Loader /> : null}
        <div className='card animated fadeIn'>
          <div className="card-header">
            <div className="row data-filter justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  <Select
                    showSearch
                    placeholder={<b>Seleccionar</b>}
                    optionFilterProp="children"
                    value={selectedOption}
                    onSelect={(value) => this.setState({ selectedOption: value })}
                    className="applyselect"
                  >
                    <Option value="active">Activo</Option>
                    <Option value="inActive">Inactivo</Option>
                    <Option value="delete" >Eliminar</Option>
                  </Select>
                  <button type="button" className="btn btn-primary ml-0 apply_btn" onClick={this.applyfilter}>Aplicar</button>
                  <div className="dropdown">
                    <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                      <span>Herramientas</span>
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadSupplierExcelFile", "totalList")}>Exportar a Excel</a>
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadSupplierCsvFile", "totalList")}>Exportar a CSV</a>
                    </div>
                  </div>
                  <button type="button" className="btn btn-primary" onClick={() => this.props.history.push('/addsuppliers')}><i className="fa fa-plus" /> <span> Añadir proveedores</span></button>
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ supplierName: !this.state.supplierName })} checked={supplierName} /><span></span>Nombre del proveedor</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ supplierId: !this.state.supplierId })} checked={supplierId} /><span></span>Identificación del proveedor</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ logo: !this.state.logo })} checked={logo} /><span></span>Logo</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ address: !this.state.address })} checked={address} /><span></span>Dirección</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ email: !this.state.email })} checked={email} /><span></span>Email</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ phoneNo: !this.state.phoneNo })} checked={phoneNo} /><span></span>Telefono no</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ fax: !this.state.fax })} checked={fax} /><span></span>Fax</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ status: !this.state.status })} checked={status} /><span></span>Estado</label></li>

            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.listOfSuppliers} >
              Reiniciar
            </button>

            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({ supplierName: true, logo: true, address: true, email: true, phoneNo: true, fax: true, status: true, })} >
              Seleccionar todo
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setTableRows('rows')}>
              Guardar
            </button>

          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre del proveedor</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre del proveedor"
                    value={selectedSupplier}
                    onChange={(selectedSupplier) => this.setState({ selectedSupplier })}
                    onSearch={(e) => this.getIds(e, 'supplierName')}
                    onFocus={(e) => this.getIds(e, 'supplierName')}
                    style={{ width: '100%' }}
                  >
                    {filteredSuppliers.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>             </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Dirección seleccionada</label>
                  <Select
                    mode="multiple"
                    placeholder="Dirección seleccionada"
                    value={selectedAddress}
                    onChange={(selectedAddress) => this.setState({ selectedAddress })}
                    onSearch={(e) => this.getIds(e, 'address')}
                    onFocus={(e) => this.getIds(e, 'address')}

                    style={{ width: '100%' }}
                  >
                    {filteredAddress.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>            </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Email</label>
                  <Select
                    mode="multiple"
                    placeholder="Enter Email"
                    value={selectedEmails}
                    onChange={(selectedEmails) => this.setState({ selectedEmails })}
                    onSearch={(e) => this.getIds(e, 'emailId')}
                    onFocus={(e) => this.getIds(e, 'emailId')}
                    style={{ width: '100%' }}
                  >
                    {filteredEmails.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>            </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Telefono no</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el número de teléfono"
                    value={selectedPhoneNo}
                    onChange={(selectedPhoneNo) => this.setState({ selectedPhoneNo })}
                    onSearch={(e) => this.getIds(e, 'mobileNo')}
                    onFocus={(e) => this.getIds(e, 'mobileNo')}
                    style={{ width: '100%' }}
                  >
                    {filteredPhoneNo.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>             </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Fax</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingresar fax"
                    value={selectedFax}
                    onChange={(selectedFax) => this.setState({ selectedFax })}
                    onSearch={(e) => this.getIds(e, 'fax')}
                    onFocus={(e) => this.getIds(e, 'fax')}
                    style={{ width: '100%' }}
                  >
                    {filteredFax.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>             </div>
              </div>
            </div>
            <hr />
            <div className="pull-right filter-button">
              <div className="dropdown ml-2">
                <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                  <span>Herramientas</span>
                </button>
                <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                  <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadSupplierExcelFile", "filteredList")}>Exportar a Excel</a>
                  <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadSupplierCsvFile", "filteredList")}>Exportar a CSV</a>
                </div>
              </div>
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.listOfSuppliers('filter')}>
                Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetListing} >
                Reiniciar
            </button>
            </div>
          </div>
          <div className='card-body'>

            <div className='table-responsive'>
              <table className='table dataTable with-image row-border hover custom-table table-striped'>
                <thead>
                  <tr>
                    <th><div className="checkbox">
                      <label>
                        <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} /><span></span>
                        <i className="input-helper" />
                      </label>
                    </div>
                    </th>
                    {supplierName ? <th sortable-column="supplierName" onClick={this.onSort.bind(this, 'supplierName')}> Nombre del proveedor
                    <i aria-hidden='true' className={(sortData['supplierName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                    {supplierId ? <th sortable-column="supplierId" onClick={this.onSort.bind(this, 'supplierId')}> Identificación del proveedor
                    <i aria-hidden='true' className={(sortData['supplierId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                    {logo ? <th>Logo</th> : null}
                    {address ? <th sortable-column="address" onClick={this.onSort.bind(this, 'address')}>
                      Habla a
                     <i aria-hidden='true' className={(sortData['address']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {email ? <th sortable-column="emailId" onClick={this.onSort.bind(this, 'emailId')} >
                      Email
                     <i aria-hidden='true' className={(sortData['emailId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {phoneNo ? <th > Telefono no </th> : null}
                    {fax ? <th > Fax </th> : null}
                    {status ? <th sortable-column="isActive" onClick={this.onSort.bind(this, 'isActive')}>
                      Estado
                      <i aria-hidden='true' className={(sortData['isActive']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    <th> Comportamiento</th>
                  </tr>
                </thead>
                {suppliersList.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                  <tbody>
                    {suppliersList.map((each, id) => {
                      return (
                        <tr key={id}>
                          <td>
                            <div className='checkbox'>
                              <label>
                                <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray(each._id)} onChange={() => this.onCheckbox(each._id)} /><span></span>

                                <i className='input-helper' />
                              </label>
                            </div>
                          </td>
                          {supplierName ? <td>{each.supplierName} </td> : null}
                          {supplierId ? <td>{each.supplierId} </td> : null}
                          {logo ? <td><div className="thumb-img"><img src={each.logo ? IMAGE_URL + each.logo : "assets/images/Cart default.png"} alt={each.supplierName} /></div></td> : null}
                          {address ? <td>{each.address}<br></br>{each.city}<br></br>{each.state}<br></br>{each.country}</td> : null}
                          {email ? <td>{each.emailId}</td> : null}
                          {phoneNo ? <td>{each.mobileNo}</td> : null}
                          {fax ? <td>{each.fax != '' ? each.fax : "-"}</td> : null}
                          {status ? <td><Toggle checked={each.isActive} className='custom-classname' onChange={() => this.changeStatus(each.isActive, each._id)} /></td> : null}
                          <td>
                            <button > <i className='fa fa-edit text-primary' data-toggle="tooltip" title="View" onClick={() => this.gettingDetails(each._id)} /> </button>
                            <button > <i className='fa fa-trash text-danger' data-toggle="tooltip" title="Delete" onClick={() => this.deleteSupplier(each._id)} /> </button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>}
              </table>
            </div>
            {suppliersList.length > 0 ?
              <div className="table-footer d-flex justify-content-between">
                <div className="table-shorting">
                  <label>Demostración</label>
                  <Select showSearch placeholder={<b> {total <= 10 ? total : length}</b>} optionFilterProp="children"
                    onSelect={this.handleChangePageSize.bind(this)}
                    value={this.state.page_ ? pagesize : (length <= pagesize ? length : pagesize)}
                    onSearch={this.handleChange_}>
                    <Option value={10}>10</Option>
                    <Option value={25}>25</Option>
                    <Option value={50}>50</Option>
                    <Option value={75}>75</Option>
                  </Select>
                  <label>Fuera de {total} proveedores</label>
                </div>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                </div>
              </div> : null}
          </div>
        </div>
      </Home>
    )
  }
}


const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(Suppliers)