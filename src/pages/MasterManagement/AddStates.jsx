import React, { Component } from 'react';
import Home from '../Home'
import swal from 'sweetalert'
import 'antd/dist/antd.css'
import SimpleReactValidator from 'simple-react-validator'
import * as actions from '../../actions';
import { connect } from "react-redux";


class AddStates extends Component {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      province: '',
      capital: '',
      hasc: '',
      stateId: '',
      errors: {
        province: '',
        hasc: '',
      },
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.role!='Super Admin') {
      this.props.history.push('/dashboard')
    }
    this.getAllDetails()
  }
  validateForm = () => {
    let { province, hasc, errors } = this.state
    let formIsValid = true
    if (!province||province.trim()==='') {
      formIsValid = false
      errors['province'] = '* Se requiere provincia'
    }
    if (!hasc||hasc.trim()==='') {
      formIsValid = false
      errors['hasc'] = '* Se requiere hasc'
    }
    // else formIsValid = true
    this.setState({ errors })
    return formIsValid
  }

  getAllDetails = () => {
    var id = this.props.match.params.rId;
    this.setState({ stateId: id })
    if (id) {
      var body = { stateId: id }
      let token = localStorage.getItem('token')
      var url = '/states/getStateDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data) {
          this.setState({
            province: data.data.province,
            capital: data.data.capital,
            hasc: data.data.hasc
          })
        }

      })
    }
  }

  addStates = () => {
    if (this.validateForm()) {
      let token = localStorage.getItem('token');
      var { province, capital, hasc, stateId } = this.state
      var data = { province, capital, hasc }
      if (stateId != '') {
        data.stateId = stateId
      }
      var body = data
      let url = "/states/addStates"
      let method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.statusCode === 228) {
          swal({
            title: "Agregado exitosamente !",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
                this.props.history.push('/states')
              // }
            })
        }
        if (data.statusCode === 219) {
          swal({
            title: "Actualizado con éxito!",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
                this.props.history.push('/states')
              // }
            })
        }
      })
    }
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }

  }

  render() {
    let { province, capital, hasc, errors, stateId } = this.state

    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>{stateId == undefined ? "Agregar " : "Editar "} estado</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}> Página principal</li>
                <li className='breadcrumb-item'>Master Management</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/states')}>States</li>
                <li className='breadcrumb-item active'>{stateId == undefined ? "Agregar estado" : "Editar estado"}</li>
              </ul>
            </div>
          </div>
        </div>

        <div className='card animated fadeIn mb-2'>
          <div className="card-header" data-spy="affix" data-offset-top="100">
            <div className="button-continer text-right">
              <button type='button' className='btn btn-primary' onClick={this.addStates} > <span>{stateId == undefined ? "Add" : "Guardar"}</span> </button>
              <button type='button' className='btn btn-outline-primary' onClick={() => this.props.history.push('/states')} > Cancelar </button>
            </div>
          </div>
          <div className='card-body'>
            <form className='form-sample'>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Provincia <span className="text-danger">*</span> : </label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='province'
                    placeholder='Entrar provincia'
                    value={province}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.province} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Capital<span className="text-danger"></span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='capital'
                    placeholder='Ingresar capital'
                    value={capital}
                    onChange={this.handleChange}
                  />

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Hasc (código de subdivisión) <span className="text-danger">*</span> : </label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='hasc'
                    placeholder='Ingrese Hasc (código de subdivisión)'
                    value={hasc}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.hasc} </span>
                </div>
              </div>
            </form>
          </div>
        </div>
      </Home>
    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(AddStates)
