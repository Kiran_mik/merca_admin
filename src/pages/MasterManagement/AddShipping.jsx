import React, { Component } from 'react';
import Home from '../Home'
import SimpleReactValidator from 'simple-react-validator'
import * as actions from '../../actions';
import { connect } from "react-redux";
import swal from 'sweetalert'
class AddShipping extends Component {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      shippingName: '',
      trackingURL: '',
      shippingId: '',
      errors: {
        shippingName: '',
        trackingURL: '',
      },
    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.role!='Super Admin') {
      this.props.history.push('/dashboard')
    }
    this.getAllDetails()
  }
  getAllDetails = () => {
    var id = this.props.match.params.sId;
    this.setState({ shippingId: id })
    if (id) {
      var body = { shippingId: id }
      let token = localStorage.getItem('token')
      var url = '/shipping/getShippingDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data) {
          this.setState({
            shippingName: data.data.shippingName,
            trackingURL: data.data.trackingUrl
          })
        }
      })
    }
  }

  validateForm = () => {
    let { shippingName, trackingURL, errors } = this.state
    let formIsValid = true
    if (!shippingName||shippingName.trim()==='') {
      formIsValid = false
      errors['shippingName'] = '* Se requiere nombre de envío'
    }
    if (!trackingURL||trackingURL.trim()==='') {
      formIsValid = false
      errors['trackingURL'] = '* Se requiere URL de seguimiento'
    }
    // else formIsValid = true
    this.setState({ errors })
    return formIsValid
  }
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }
  }
  addShipping = () => {
    if (this.validateForm()) {
      let token = localStorage.getItem('token');
      var { shippingName, trackingURL, shippingId } = this.state
      var data = { shippingName, trackingUrl: trackingURL }
      if (shippingId != '') {
        data.shippingId = shippingId
      }
      var body = data
      let url="/shipping/addShippingDetails"
      let method='post'
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          let { data } = response
          if (data.statusCode === 228) {
          swal({
            title: "Agregado exitosamente !!",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
                this.props.history.push('/shippingMethods')
              // }
            })
        }
        if (data.statusCode === 219) {
          swal({
            title: "Actualizado con éxito!",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
                this.props.history.push('/shippingMethods')
              // }
            })
        }
        })
    }
  }


  render() {
    let { trackingURL, shippingName, errors, shippingId } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>{shippingId == undefined ? "Agregar" : "Editar"} método de envío</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}> Página principal</li>
                <li className='breadcrumb-item'>Métodos de envío</li>
                <li className='breadcrumb-item active'>{shippingId == undefined ? "Agregar envío" : "Editar envío"} </li>
              </ul>
            </div>
          </div>
        </div>
        <div className='card animated fadeIn mb-2'>
          <div className="card-header" data-spy="affix" data-offset-top="100">
            <div className="button-continer text-right">
              <button type='button' className='btn btn-primary' onClick={this.addShipping}> <span>{shippingId == undefined ? "Añadir" : "Guardar"}</span> </button>
              <button type='button' className='btn btn-outline-primary' onClick={() => this.props.history.push('/shippingMethods')} > Cancelar </button>
            </div>
          </div>
          <div className='card-body'>
            <form className='form-sample'>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Nombre de envío <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='shippingName'
                    placeholder='Ingrese el nombre del envío'
                    value={shippingName}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.shippingName} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>URL de seguimiento <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='trackingURL'
                    placeholder='Ingresar URL de seguimiento'
                    value={trackingURL}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.trackingURL} </span>
                </div>
              </div>
            </form>
          </div>
        </div>
      </Home>
    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(AddShipping)
