import React, { Component } from 'react';
import Home from '../Home'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import SimpleReactValidator from 'simple-react-validator'
import swal from 'sweetalert'



class EditCms extends Component {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      description: '',
      metaDescription: '',
      metaTitle: '',
      metaKeyword: '',
      pageName: '',
      cmsId: '',
      errors: {
        description: '',
        metaDescription: '',
        metaTitle: '',
        metaKeyword: '',
        pageName: '',
      }
    }
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }
  }
  validateForm = () => {
    let { description, metaDescription, metaTitle, metaKeyword, pageName, errors } = this.state
    let formIsValid = true
    if (!description||description.trim()==='') {
      formIsValid = false
      errors['description'] = '* Descripción requerida'
    }
    if (!metaDescription||metaDescription.trim()==='') {
      formIsValid = false
      errors['metaDescription'] = '* Se requiere meta descripción'
    }
    if (!metaTitle||metaTitle.trim()==='') {
      formIsValid = false
      errors['metaTitle'] = '* Se requiere meta título'
    }
    if (!metaKeyword||metaKeyword.trim()==='') {
      formIsValid = false
      errors['metaKeyword'] = '* Se requiere meta palabra clave'
    }
    if (!pageName||pageName.trim()==='') {
      formIsValid = false
      errors['pageName'] = '* Se requiere el nombre de la página'
    }
    // else formIsValid = true
    this.setState({ errors })
    return formIsValid
  }


  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission &&permissions.rolePermission.cmsPagesAccess&& permissions.rolePermission.cmsPagesAccess.view === false) {
      this.props.history.push('/dashboard')
    }
    var id = this.props.match.params.cId;
    this.setState({ cmsId: id })
    if (id) {
      var body = { cmsId: id }
      let token = localStorage.getItem('token')
      var url = '/cms/cmsDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data) {
          this.setState({
            description: data.data.description,
            metaDescription: data.data.metaDescription,
            metaTitle: data.data.metaTitle,
            metaKeyword: data.data.metaKeyword,
            pageName: data.data.pageName
          })
        }

      })
    }
  }

  updateCmsPage = () => {
    if (this.validateForm()) {
      var { description, metaDescription, metaTitle, metaKeyword, pageName, cmsId } = this.state
      var body = { description, metaDescription, metaTitle, metaKeyword, pageName, cmsId }
      let token = localStorage.getItem('token')
      var url = '/cms/updateCms'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          swal({
            title: "Actualizado con éxito! !",
            icon: "success",
          })
            .then((willDelete) => {
              if (willDelete) {
                this.props.history.push('/cmsListing')
              }
            })
        }

      })
    }
  }

  render() {
    var { description, metaDescription, metaTitle, metaKeyword, pageName, errors } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Editar {pageName} </h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Home</li>
                <li className="breadcrumb-item" >Gestión de contenido</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/cmsListing')}>CMS Management </li>
                <li className='breadcrumb-item active'>Editar {pageName} </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card animated fadeIn mb-2">
          <div className="card-header">
            <h5 className='card-title'>Metadatos</h5>
          </div>
          <div className='card-body'>
            <form className='form-sample'>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'> Meta título <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='metaTitle'
                    placeholder='Ingrese Meta título'
                    value={metaTitle}
                    onChange={this.handleChange}
                  />
                  <span className="notes">Máximo 70 caracteres.</span>
                  <span className="error-block"> {errors.metaTitle} </span>

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>
                Metadescripción : <span className="text-danger">*</span> :
                </label>
                <div className='col-md-8 co-sm-7'>
                  <textarea
                    className='form-control'
                    rows="10" cols="50"
                    type='text'
                    name='metaDescription'
                    placeholder='Introducir meta descripción'
                    value={metaDescription}
                    onChange={this.handleChange}
                  />
                  <span className="notes">Máximo 10 palabras es adecuado</span>
                  <span className="error-block"> {errors.metaDescription} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Meta palabra clave <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <textarea
                    className='form-control'
                    type='text'
                    name='metaKeyword'
                    placeholder='Introducir Meta palabra clave'
                    value={metaKeyword}
                    onChange={this.handleChange}
                  />
                  <span className="notes">Máximo 156 caracteres es adecuado</span>
                  <span className="error-block"> {errors.metaKeyword} </span>

                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="card animated fadeIn mb-2">
          <div className="card-header">
            <h5 className='card-title'>Detalle de la página</h5>
          </div>
          <div className='card-body'>
            <form className='form-sample'>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'> Nombre de la página <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='pageName'
                    placeholder='Introducir Nombre de la página'
                    value="Plan intelligently"
                    value={pageName}
                    onChange={this.handleChange}
                    disabled
                  />
                  <span className="error-block"> {errors.pageName} </span>
                </div>
              </div>

              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'> Descripción <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <CKEditor
                    className='ck-editor__editable '
                    columns={40}
                    editor={ClassicEditor}
                    data={description}
                    onInit={editor => {
                    }}
                    onChange={(event, editor) => {
                      const data = editor.getData()
                      this.setState({ description: data })
                    }}
                  />
                  <span className="error-block"> {errors.description} </span>
                </div>
              </div>
              <hr />
              <div className="form-group">
                <div className="button-group-container float-right">
                  <button type="button" className="btn btn-primary" onClick={this.updateCmsPage}>
                    <span>Actualizar</span>
                  </button>
                  <button type="button" className="btn btn-outline-primary" onClick={() => this.props.history.push('/cmsListing')}>
                    <span>Cancelar</span>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </Home>

    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(EditCms)

