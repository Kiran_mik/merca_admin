import React, { Component } from 'react'
import Home from '../Home'
import 'rc-pagination/assets/index.css'
import { Select } from 'antd'
import 'antd/dist/antd.css'
import * as actions from '../../actions'
import { connect } from 'react-redux'
import $ from 'jquery';
class CmsPages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cmsListing: [],
      cmsPagesAccess: {}
    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission &&permissions.rolePermission.cmsPagesAccess&& permissions.rolePermission.cmsPagesAccess.view === false) {
      this.props.history.push('/dashboard')
    }
    if (permissions.rolePermission) {
      let { cmsPagesAccess } = permissions.rolePermission
      this.setState({ cmsPagesAccess: cmsPagesAccess })
    }
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });

    this.cmsDetails()
  }
  cmsDetails = () => {
    var url = '/cms/cmsListing'
    var method = 'post'
    var body = { page: 1, pagesize: 5 }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data.status === 1) {
        this.setState({ cmsListing: data.data.cmsListing })
      }
    })
  }


  render() {
    var { cmsListing, cmsPagesAccess } = this.state
    const Option = Select.Option;
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>CMS Management</h3>
              <ul className='breadcrumb' >
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" >Gestión de contenido</li>
                <li className='breadcrumb-item active'>CMS Management </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card animated fadeIn">
          <div className="card-body">
            <div className="table-responsive">
              <table className="table dataTable with-image row-border hover custom-table table-striped" >
                <thead>
                  <tr>
                    <th>Nombre de la página </th>
                    <th>Descripción</th>
                    {cmsPagesAccess.edit ? <th> Comportamiento</th> : null}
                  </tr>
                </thead>
                <tbody>
                  {cmsListing.map((each, id) => {
                    let description = each.description
                    return (
                      <tr key={id}>
                        <td>{each.pageName}</td>
                        <td className="text-truncate" style={{ 'maxWidth': 250 }}>{description.replace(/<.+?>/g, '')}</td>
                        {cmsPagesAccess.edit ? <td><button onClick={() => this.props.history.push(`/cmsview/${each._id}`)}><i className="fa fa-pencil-square-o text-primary" data-toggle="tooltip" title="Edit"></i></button></td> : null}
                      </tr>
                    )
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </Home>
    )
  }
}



const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(CmsPages)
