import React, { Component } from 'react'
import Home from '../../Home'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import * as actions from '../../../actions'
import { connect } from 'react-redux'
import swal from 'sweetalert'
import Toggle from 'react-toggle'
import { isEmpty } from "lodash"
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios'
import { API_URL, IMAGE_URL } from '../../../config/configs'
import { Select } from 'antd';
import StarRatings from 'react-star-ratings';
import 'antd/dist/antd.css';
import $ from 'jquery';
var _ = require('lodash');
const { OptGroup } = Select;
toast.configure({ autoClose: 1000, draggable: false });
class AddProduct extends Component {
  constructor(props) {
    super(props)
    this.state = {
      productId: '',
      productName: '',
      productName1: '',
      sku: '',
      customUrl: '',
      description: '',
      shortDescription: '',
      price: '',
      minQuantity: 1,
      size: '',
      salePrice: '',
      salePriceErr: false,
      weight: '',
      taxable: false,
      freeDelivery: true,
      preOrder: false,
      allowedDiscount: false,
      discountValue: '',
      publish: true,
      tab1: true,

      tab2: false,
      imagesArr: [],
      additionalImages: [],
      productImage: '',
      singleThImg: '',
      singleSmImg: '',
      productLabel: '',
      imagePreviewUrl: '',
      fileVisible: false,
      fileDisplay: false,
      multiView: false,
      multibeforeUpload: true,
      url: [],

      tab3: false,
      stockQuantity: '',
      // availability: '2 working days',

      tab4: false,
      parentCategoryNames: [],
      parentCatName: [],
      parentCatDisplay: '',
      bestSeller: true,
      featuredProduct: false,
      storeName: '',
      brandName: '',
      supplierName: '',
      listOfbrands: [],
      listofSuppliers: [],
      brandId: undefined,
      supplierId: '',
      prrentIds: [],
      relCat: [],


      tab5: false,
      tab5respo: false,
      relateProduct: '',
      relatedCategoryNames: [],
      relatedProducts: [],
      selectedProduct: [],
      selectedCategory: [],
      relatedItemsDetails: [],
      multipleDelete: [],
      page: 1,
      pagesize: 10,
      sort: {},
      sortData: { price: false, SKU: false, productName: false, categoryName: false, salePrice: false, stockQuantity: false },

      tab6: false,
      metaTitle: '',
      metaKeyword: '',
      metaDescription: '',

      tab7: false,
      reviewListing: [],
      sort1: {},
      sortData1: { rating: false, reviewerName: false, productName: false, description: false, isActive: false },


      errors: {
        productName: '',
        sku: '',
        price: '',
        weight: ''
      },
      imagesArray: [
        {
          imgName: '',
          imgLabel: '',
          imgSequence: 1
        }
      ]
    }
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }
  }

  handleChangeSaleprice = event => {
    this.setState({ [event.target.name]: event.target.value })
    if (Number(this.state.price) < Number(event.target.value)) {
      this.setState({ salePriceErr: true })
    } else if (event.target.value == '' || Number(this.state.price) > Number(event.target.value)) {
      this.setState({ salePriceErr: false })
    }
  }


  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission && permissions.rolePermission.productsAccess && permissions.rolePermission.productsAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }
    this.getallDetails()
    this.listOfReviews()
    $('.card-header').affix({ offset: { top: 150 } });

  }

  // ############################## Get Details #############################
  getallDetails = () => {
    var id = this.props.match.params.pId;
    if (this.state.productId) {
      id = this.state.productId
    }
    if (id) {
      var body = { productId: id }
      let token = localStorage.getItem('token')
      var url = '/products/getProductDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let data1 = response.data.data
        let data = data1[0]
        if (data.discount) {
          var discountVal = data.discount
        }
        if (data.tempImgArr && data.tempImgArr.length == 0) {
          if(data.parentCategoryName.length===3){
            this.setState({
              parentCatDisplay: data.parentCategoryName[2]
            })
          }else if(data.parentCategoryName.length===2){
            this.setState({
              parentCatDisplay: data.parentCategoryName[1]
            })
          }else{
            this.setState({
              parentCatDisplay: data.parentCategoryName[0]
            })
          }
          this.setState({
            productId: data._id,
            // tab1
            productName: data.productName,
            sku: data.SKU,
            customUrl: data.customUrl,
            description: data.description,
            shortDescription: data.shortDescription,
            price: data.price,
            salePrice: data.salePrice,
            weight: data.weight,
            taxable: data.taxable,
            freeDelivery: data.freeDelivery,
            preOrder: data.preOrder,
            allowedDiscount: data.allowedDiscount,
            discountValue: discountVal,
            publish: data.publish,
            minQuantity: data.minQuantity,
            size: data.size,
            //tab2
            productName1: data.productImage,
            productLabel: data.productLabel,
            additionalImages: data.images,
            multiView: true,
            //tab3
            stockQuantity: data.stockQuantity,
            // availability: data.availability,
            //tab4
            parentCategoryNames: [],
            // parentCatDisplay: data.parentCategoryName,
            parentCatName: data.parentCategory,
            // selectedProduct:data.parentCategoryList,
            bestSeller: data.bestSeller,
            featuredProduct: data.featuredProduct,
            storeName: data.storeName,
            brandName: data.brandName,
            supplierName: data.supplierName,
            brandId: data.brandId,
            supplierId: data.supplierId,
            //tab5
            relatedItemsDetails: data.relatedItems,
            // selectedProduct:data.relatedItems

            // tab6
            metaTitle: data.metaTitle,
            metaKeyword: data.metaKeyword,
            metaDescription: data.metaDescription,

          })
        } else {
          if(data.parentCategoryName.length===3){
            this.setState({
              parentCatDisplay: data.parentCategoryName[2]
            })
          }else if(data.parentCategoryName.length===2){
            this.setState({
              parentCatDisplay: data.parentCategoryName[1]
            })
          }else{
            this.setState({
              parentCatDisplay: data.parentCategoryName[0]
            })
          }
          this.setState({
            productId: data._id,
            // tab1
            productName: data.productName,
            sku: data.SKU,
            customUrl: data.customUrl,
            description: data.description,
            shortDescription: data.shortDescription,
            price: data.price,
            salePrice: data.salePrice,
            weight: data.weight,
            taxable: data.taxable,
            freeDelivery: data.freeDelivery,
            preOrder: data.preOrder,
            allowedDiscount: data.allowedDiscount,
            discountValue: discountVal,
            publish: data.publish,
            minQuantity: data.minQuantity,
            size: data.size,
            //tab2
            productName1: data.productImage,
            productLabel: data.productLabel,
            additionalImages: data.tempImgArr,
            multiView: true,
            //tab3
            stockQuantity: data.stockQuantity,
            // availability: data.availability,
            //tab4
            parentCategoryNames: [],
            // parentCatDisplay: data.parentCategoryName,
            parentCatName: data.parentCategory,
            // selectedProduct:data.parentCategoryList,
            bestSeller: data.bestSeller,
            featuredProduct: data.featuredProduct,
            storeName: data.storeName,
            brandName: data.brandName,
            supplierName: data.supplierName,
            brandId: data.brandId,
            supplierId: data.supplierId,
            //tab5
            relatedItemsDetails: data.relatedItems,
            // selectedProduct:data.relatedItems

            // tab6
            metaTitle: data.metaTitle,
            metaKeyword: data.metaKeyword,
            metaDescription: data.metaDescription,

          })
        }
        this.getRelatedProductDetails()
      }
      )

    }
  }
  // ############################## URL generator #############################
  urlGenerator = () => {
    let { errors } = this.state
    var url = '/products/addProducts'
    var method = 'post'
    let { productName, productId } = this.state
    let data = {}
    if (productId == '') {
      data.productName = productName
    }
    if (productId != '') {
      data.product = productName
      data.productId = productId
    }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data = response.data.data
      if (data) {
        errors['customUrl'] = ''
      }
      if (productId != '') {
        this.setState({
          customUrl: data.customUrl,
          errors
        })
      } else
        this.setState({
          productId: data._id,
          customUrl: data.customUrl,
          errors
        })
    }
    )
  }
  // ################################# Main Image Preview #################################
  fileChangedHandler(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file: file,
        filename: filename,
        imagePreviewUrl: reader.result,
        fileVisible: true
      })
    }
    reader.readAsDataURL(file)
  }
  // ################################# Main Image Upload #################################
  singleFileUpload = () => {
    this.setState({ productName1: '' })
    if (this.state.productId != '') {
      var { productId } = this.state
      var token = localStorage.getItem('token')
      var formData = new FormData()
      formData.append('productId', productId)
      formData.append('file', this.state.file)
      axios
        .post(API_URL + '/auth/fileUploadForProduct', formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: token
          }
        })
        .then(response => {
          let { data } = response.data
          if (data) {
            this.setState({
              productName1: data.productPics[0].normalImage, singleThImg: data.productPics[0].thumbnailImage, singleSmImg: data.productPics[0].compressedImage,
              productLabel: data.productPics[0].normalImage.slice(0, -4), fileVisible: !this.state.fileVisible, fileDisplay: true
            })
          }
        })
        .catch(err => console.log(err))
    } else {
      swal('Proporcione información del artículo', '', 'error')
    }
  }
  // ################################# Delete Image after upload #################################
  deleteProductImage = (e) => {
    swal({ title: 'Are you sure?', icon: 'warning', buttons: true, dangerMode: true }).then(willDelete => {
      if (willDelete) {
        var body = { productId: this.state.productId }
        var url = '/products/deleteProductImage'
        var method = 'post'
        var token = localStorage.getItem('token')
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          let { data } = response
          if (data.status === 1) {
            this.setState({
              productName1: '',
              productLabel: '',
            })
          }
          this.getallDetails()
        })
        swal('Borrado exitosamente', '', 'success')
      }
    })
  }
  // ################################# Additional Image Preview #################################
  fileChangedHandler1 = (e) => {
    this.setState({ multibeforeUpload: true })
    var imagesArr = this.state.imagesArr;
    var self = this
    var files = e.target.files;
    this.setState({ imgs_Arr: files })
    for (var i = 0, f; f = files[i]; i++) {
      if (!f.type.match('image.*')) {
        continue;
      }
      var reader = new FileReader();
      reader.onload = (function (theFile) {
        return function (e) {
          imagesArr.push({ image: e.target.result, url: theFile.name })
          self.setState({ imagesArr, multiple_img: true })
        };
      })(f);
      reader.readAsDataURL(f);
    }
  }
  // ################################# Delete Additional Images before upload ######################
  imagesSplice(key) {
    var imagesArr = this.state.imagesArr;
    var imgs_Arr = Object.values(this.state.imgs_Arr)
    imagesArr.splice(key, 1);
    imgs_Arr.splice(key, 1)
    this.setState({ imgs_Arr, imagesArr })
    // this.setState({ imagesArr,multiple_img:!this.state.multiple_img });
  }
  // ########################### deleteAdditionalImages #########################
  deleteAdditionalImages(id, each) {
    swal({ title: 'Are you sure?', icon: 'warning', buttons: true, dangerMode: true }).then(willDelete => {
      if (willDelete) {
        if (this.state.productId != '') {
          var { productId } = this.state
          let token = localStorage.getItem('token')
          var body = { productId: productId, imageId: each }
          var url = '/products/deleteAdditionalImages'
          var method = 'post'
          this.props.commonApiCall(url, method, body, token, null, this.props, response => {
            let { data } = response
            if (data.status === 1) {
              this.setState({ additionalImages: this.state.additionalImages })
              this.getallDetails()
            }
          }
          )
        }
      }
    })
  }
  // ################################# Additional Image Upload #################################
  multipleFileUpload = () => {
    this.setState({ additionalImages: [] })
    if (this.state.productId != '') {
      this.setState({ multiView: true })
      var { productId } = this.state
      var token = localStorage.getItem('token')
      var imagefile = this.state.imgs_Arr
      var additionalImages = this.state.additionalImages;
      var formData = new FormData()
      for (const file of imagefile) {
        formData.append('file', file, file.name)
      }
      formData.append('productId', productId)
      axios
        .post(API_URL + '/auth/uploadMultipleImages', formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: token
          }
        })
        .then(response => {
          let { data } = response
          var pics = data.data.productPics
          pics.map(each => {
            additionalImages.push(each)
          })
          this.setState({ additionalImages: data.data.productPics, multiView: true, multibeforeUpload: !this.state.multibeforeUpload, imagesArr: [], multiple_img: !this.state.multiple_img })
        })
        .catch(err => console.log(err))
    } else {
      swal('Proporcione información del artículo', '', 'error')
    }
  }
  // #################################### Parent Category Listing ################################
  getCategoryName = () => {
    let token = localStorage.getItem('token')
    axios({
      method: 'get',
      url: API_URL + '/categories/catLevels',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      }
    }).then(response => {
      var data = response.data
      if (data.data) {
        this.setState({ parentCategoryNames: data.data })
      }
      // })
    })
  }
  // ########################### Brand and Supplier Listing #########################
  handleSearch = (value, name) => {
    let token = localStorage.getItem('token')
    var body = { type: name, searchText: value }
    axios({
      method: 'post',
      url: API_URL + '/products/productFieldsList',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      },
      data: body
    }).then(response => {
      var data = response.data.data
      if (response && response.data && response.data.data) {
        if (name === 'brandName') {
          this.setState({ listOfbrands: data })
        }
        if (name === 'supplierName') {
          this.setState({ listofSuppliers: data })
        }
      }
    })
  }
  // ########################### Related Category Listing #########################
  relatedCatagory = e => {
    let token = localStorage.getItem('token')
    var body = { searchText: e }
    axios({
      method: 'post',
      url: API_URL + '/categories/getAllCategories',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      },
      data: body
    }).then(response => {
      var data = response.data.data

      var list = []
      data.map((each) => {
        list.push(each.categoryName)
      })
      this.setState({ relatedCategoryNames: list, relCat: data })
    })
  }
  // ########################### Related Product Listing #########################
  relatedProductsArr = e => {
    let token = localStorage.getItem('token')
    var { selectedCategory, productId } = this.state
    var data = { productId: productId }
    data.searchText = e
    if (selectedCategory.length > 0) {
      var NamesArray = []
      selectedCategory.map((each1) => {
        this.state.relCat.map((each) => {
          if (each.categoryName.toString() == each1) {
            NamesArray.push(each._id)
          }
        })
      })
      data.categoryName = NamesArray
    }
    var body = data
    axios({
      method: 'post',
      url: API_URL + '/products/productListForRelatedProducts',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      },
      data: body
    }).then(response => {
      var { data } = response.data
      if (data && data.length > 0) {
        var pArr = _.uniqBy(data, (e) => {
          return e.productName.toString();
        });
        this.setState({ relatedProducts: pArr, relatedProductsObj: data })

      } else {
        this.setState({ relatedProducts: [] })
      }
    })
  }
  // ########################### Get Rel_product Details #########################
  getRelatedProductDetails = () => {

    let { productId, page, pagesize, sort } = this.state
    var data = { productId, page, pagesize }
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    var body = data
    let token = localStorage.getItem('token')
    var url = '/products/relatedProductsListing'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data

      if (data) {
        this.setState({ relatedItemsDetails: data.productlisting })
      }
    }
    )
  }
  // ########################### adding RelatedProducts #########################
  addingRelatedProducts = () => {
    if (this.state.productId != '') {
      var { productId, selectedProductS } = this.state
      let token = localStorage.getItem('token')
      var body = { productId: productId, tab5: true, relatedProduct: selectedProductS }
      var url = '/products/addProducts'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.data) {
          this.setState({ tab5respo: true, selectedProduct: [], selectedCategory: [] })
          this.getRelatedProductDetails()
        }
      }
      )
    } else {
      swal('Proporcione información del artículo', '', 'error')
    }
  }
  // ########################### delete Related Products #########################
  deleteRelatedProducts = (id) => {
    var delArr = this.state.multipleDelete
    var { productId } = this.state

    swal({ title: 'Are you sure?', icon: 'warning', buttons: true, dangerMode: true }).then(willDelete => {
      if (willDelete) {
        if (delArr.length > 0) {
          var body = { productId: productId, id: delArr }
        } else {
          var body = { productId: productId, id: [id] }
        }
        var url = '/products/deleteRelatedProduct'
        var method = 'post'
        var token = localStorage.getItem('token')
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          let { data } = response
          this.getRelatedProductDetails()
          this.setState({ multipleDelete: [], selectedOption: [] })
        }
        )
        swal('Borrado exitosamente', '', 'success')
      }
    })
  }
  // ########################### Sorting Related Products #########################
  onSort = (column) => {
    let { sortData } = this.state;
    var element, value;
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key];
        element = key;
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState({
          sort: { [element]: value }
        }, () => {
          this.getRelatedProductDetails();
        });
        this.setState({ sortData });
      }
      else {
        sortData[key] = false
        element = key;
        value = 1;
      }
    }
    this.setState({ sortData });
  }
  // ############################## Change Review Status #############################
  change_reviewStatus(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { reviewId: [Id], isActive: status }
    var urlkey = '/review/changeReviewStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        swal(data.message, '', 'success')
        this.listOfReviews()
      } else {
        swal(data.message, '', 'error')
      }
    })
  }
  // ############################## Review Listing #############################
  listOfReviews = (e) => {
    var id = this.props.match.params.pId;
    let { page, pagesize, sort1 } = this.state
    var data = { page, pagesize }
    if(e){
      data.productName =[e]
    }else{
      data.productName = [this.state.productName]
    }
    if (!isEmpty(sort1)) {
      data.sort = sort1
    }
    var body = data
    let token = localStorage.getItem('token')
    var url = '/review/reviewListing'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      if (response.data.status === 1) {
        this.setState({ reviewListing: data.reviewListing, total: data.total, length: data.reviewListing.length })
      } else {
        this.setState({ reviewListing: [] })

      }
    })
  }
  // ########################### Delete Review #########################
  deleteReview = (uid) => {
    swal({ title: "¿Estás seguro?", icon: "warning", buttons: true, dangerMode: true, })
      .then((willDelete) => {
        if (willDelete) {
          var body = { reviewId: [uid] }
          let token = localStorage.getItem('token')
          axios({
            method: 'POST',
            url: API_URL + '/review/deleteReviews',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': token
            },
            data: JSON.stringify(body)
          })
            .then(res => {
              this.listOfReviews()
            })
          swal('Borrado exitosamente', '', 'success')
        }
      });
  }

  // ################################# Fields_Sorting #################################
  onSort1 = (column) => {
    let { sortData1 } = this.state;
    var element, value;
    for (const key in sortData1) {
      if (key == column) {
        sortData1[key] = !sortData1[key];
        element = key;
        value = -1
        if (sortData1[key]) {
          value = 1
        }
        this.setState({
          sort1: { [element]: value }
        }, () => {
          this.listOfReviews();
        });
        this.setState({ sortData1 });
      }
      else {
        sortData1[key] = false
        element = key;
        value = 1;
      }
    }
    this.setState({ sortData1 });
  }
  validateFormTab1 = () => {
    let { productName, sku, price, customUrl, weight, errors } = this.state
    let formIsValid = true
    if (!sku || sku.trim() === '') {
      formIsValid = false
      errors['sku'] = '* SKU es requerido'
    }
    if (!productName || productName.trim() === '') {
      formIsValid = false
      errors['productName'] = '* Se requiere el nombre del producto'
    }

    if (!customUrl) {
      formIsValid = false
      errors['customUrl'] = '* Se requiere URL personalizada'
    }
    if (!price) {
      formIsValid = false
      errors['price'] = '* Precio es obligatorio'
    }
    if (!weight) {
      formIsValid = false
      errors['weight'] = '* Se requiere peso'
    }
    this.setState({ errors })
    return formIsValid

  }

  validateFormTab3 = () => {
    let { stockQuantity, errors } = this.state
    let formIsValid = true
    if (!stockQuantity) {
      formIsValid = false
      errors['stockQuantity'] = '* Se requiere cantidad de stock'
    }
    // if (!availability) {
    //   formIsValid = false
    //   errors['availability'] = '* Availability is required'
    // }
    this.setState({ errors })
    return formIsValid
  }

  validateFormTab4 = () => {
    let { parentCatDisplay, brandId, supplierId, errors } = this.state
    let formIsValid = true
    if (!parentCatDisplay) {
      formIsValid = false
      errors['parentCatDisplay'] = '* Se requiere la categoría principal'
    }
    if (!brandId || brandId.trim() === '') {
      formIsValid = false
      errors['brandId'] = '* Se requiere marca'
    }
    if (!supplierId || supplierId.trim() === '') {
      formIsValid = false
      errors['supplierId'] = '* Se requiere el nombre del proveedor'
    }
    this.setState({ errors })
    return formIsValid
  }
  // ################################# ADD new Product #################################
  addProduct = () => {
    let token = localStorage.getItem('token')
    let {
      productId, productName, minQuantity, size, sku, customUrl, description, shortDescription, price, salePrice, weight, taxable, freeDelivery, preOrder, allowedDiscount, discountValue, publish, tab1,
      tab2, productLabel, productName1, singleSmImg, singleThImg,
      tab3, stockQuantity,
      tab4, parentCatName, bestSeller, featuredProduct, storeName, searchItem,
      tab5, selectedProduct, relateProduct, tab5respo, tab6, tab7,
      metaTitle, metaKeyword, metaDescription,
    } = this.state

    if (tab1) {
      if (!this.validateFormTab1()) {
        toast.error("¡Proporcione los campos obligatorios en la sección anterior!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        this.setState({ tab1: false, tab2: true })
      }
      if (this.validateFormTab1()) {
        var body = {
          productName: productName,
          SKU: sku,
          customUrl: customUrl,
          description: description,
          shortDescription: shortDescription,
          price: price,
          salePrice: salePrice,
          weight: weight,
          taxable: taxable,
          size, minQuantity,
          freeDelivery: freeDelivery,
          preOrder: preOrder,
          allowedDiscount: allowedDiscount,
          publish: publish,
          discount: discountValue,
          productId: productId,
          tab1: true
        }
        var url = '/products/addProducts'
        var method = 'post'
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          let { data } = response
          if (data) {
            if (data.statusCode === 459) {
              toast.error("SKU ya existe!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
            } else
              this.setState({ tab1: false, tab2: true })
          }
        }
        )
      }
    } else if (tab2) {
      this.setState({ tab2: false, tab3: true })
      if (this.state.productId != '') {
        var additionalImages = _.map(this.state.additionalImages, (object, index) => {
          return {
            imgName: object.normalImage ? object.normalImage : object.imgName,
            thumbnailImage: object.thumbnailImage,
            compressedImage: object.compressedImage,
            imgLabel: object.label ? object.label : object.imgLabel,
            imgSequence: object.sequence_number ? object.sequence_number : object.imgSequence
          }
        })

        var body = {
          productId: productId,
          productImage: productName1,
          productThumbnailImage: singleThImg,
          productCompressedImage: singleSmImg,
          productLabel: productLabel,
          tab2: true,
          imagesArray: additionalImages
        }
        var url = '/products/addProducts'
        var method = 'post'
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          let { data } = response
          if (data) {
            this.setState({ tab2: false, tab3: true })
            this.getallDetails()
          }
        }
        )
      } else {
        swal('Proporcione información del artículo', '', 'error')
      }
    } else if (tab3) {
      if (!this.validateFormTab3()) {
        toast.error("¡Proporcione los campos obligatorios en la sección anterior!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        this.setState({ tab3: false, tab4: true })
      }
      if (this.validateFormTab3()) {
        if (this.state.productId != '') {
          var body = {
            productId: productId,
            tab3: true,
            stockQuantity: stockQuantity,
            // availability: availability
          }
          var url = '/products/addProducts'
          var method = 'post'
          this.props.commonApiCall(url, method, body, token, null, this.props, response => {
            let { data } = response
            if (data) {
              this.setState({ tab3: false, tab4: true })
            }
          }
          )
        } else {
          swal('Proporcione información del artículo', '', 'error')
        }
      }
    } else if (tab4) {
      if (!this.validateFormTab4()) {
        toast.error("¡Proporcione los campos obligatorios en la sección anterior!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        this.setState({ tab4: false, tab5: true })
      }
      if (this.validateFormTab4()) {
        if (this.state.productId != '') {
          var body = {
            productId: productId,
            tab4: true,
            parentCategoryNames: parentCatName,
            bestSeller: bestSeller,
            featuredProduct: featuredProduct,
            // storeName: storeName,
            brandId: this.state.brandId,
            supplierId: this.state.supplierId

          }
          var url = '/products/addProducts'
          var method = 'post'
          this.props.commonApiCall(url, method, body, token, null, this.props, response => {
            let { data } = response
            if (data) {
              this.setState({ tab4: false, tab5: true })
            }
          }
          )
        } else {
          swal('Proporcione información del artículo', '', 'error')
        }
      }
    } else if (tab5) {

      this.setState({ tab5: false, tab5respo: false, tab6: true })
    } else if (tab6) {
      this.setState({ tab6: false, tab7: true })
      if (this.state.productId != '') {
        var body = {
          productId: productId,
          tab6: true,
          metaTitle: metaTitle,
          metaKeyword: metaKeyword,
          metaDescription: metaDescription,
        }
        var url = '/products/addProducts'
        var method = 'post'
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          let { data } = response
          if (data) {
            this.setState({ tab6: false, tab7: true })
            swal('Producto agregado exitosamente', '', 'success')
          }
        }
        )
      } else {
        swal('Proporcione información del artículo', '', 'error')
      }
    } else if (tab7) {

      this.props.history.push('/productlist')
    }
  }

  handleImageNameChange = (event, data) => {
    data.label = event.target.value
    this.setState({
      additionalImages: this.state.additionalImages
    })
  }

  handleSequenceChange = (event, data) => {
    data.sequence_number = event.target.value
    data.imgSequence = event.target.value
    this.setState({
      additionalImages: this.state.additionalImages
    })
  }

  checkArray(_id) {
    let { multipleDelete } = this.state
    if (multipleDelete.includes(_id)) {
      return true
    } else {
      return false
    }
  }

  handle_RelProdName = value => {
    let idsArray = [];
    this.state.relatedProductsObj.map((each) => {
      value.map((each1) => {
        if (each.productName === each1) {
          idsArray.push(each._id);
        }
      })
      this.setState({ selectedProductS: idsArray, selectedProduct: value })
    })
  };

  handle_RelCatName(value) {
    let { selectedCategory, } = this.state;
    selectedCategory = value;
    this.setState({ selectedCategory });
  };

  handle_parentCatName(value, id, id1) {
    let idsArray = [];
    let namesArray = [];
    this.state.parentCategoryNames.map((each) => {
      if (each._id.toString() === value) {
        idsArray.push(each._id);
        namesArray.push(each.categoryName)
      }
      let subCat = each.subCategoryArray;
      if (each && each.subCategoryArray && Array.isArray(each.subCategoryArray) && each.subCategoryArray.length) {
        subCat.map((each1) => {
          if (each1._id.toString() === value) {
            idsArray = [];
            namesArray = []
            idsArray.push(each._id, each1._id);
            namesArray.push(each.categoryName, each1.categoryName);
          } else {
            let finalSubCat = each1.subCategory;
            if (each1 && each1.subCategory && Array.isArray(each1.subCategory) && each1.subCategory.length) {
              finalSubCat.map((each2) => {
                if (each2._id.toString() === value) {
                  idsArray = [];
                  namesArray = []
                  idsArray.push(each._id, each1._id, each2._id);
                  namesArray.push(each.categoryName, each1.categoryName, each2.categoryName);
                }
              })
            }
          }
        })
      }
      if (value) {
        this.setState({
          errors: Object.assign(this.state.errors, { parentCatDisplay: "" })
        });
      }
    })
    this.setState({ parentCatName: idsArray, parentCatDisplay: value })
  }

  resetFields = () => {
    if (this.state.tab1) {
      this.setState({
        productName: '',
        sku: '',
        customUrl: '',
        description: '',
        shortDescription: '',
        price: '',
        salePrice: '',
        weight: '',
        discountValue: '',
      })
    }
    if (this.state.tab3) {
      this.setState({
        stockQuantity: '',
      })
    }
    if (this.state.tab4) {
      this.setState({
        parentCatDisplay: '',
        brandName: '',
        supplierName: ''
      })
    }
    if (this.state.tab6) {
      this.setState({
        metaTitle: '',
        metaKeyword: '',
        metaDescription: ''
      })
    }

  }

  render() {

    var {
      productName, sku, customUrl, description, minQuantity, size, shortDescription, price, salePrice, weight, taxable, freeDelivery, preOrder, allowedDiscount, discountValue, publish, productImage, additionalImages, stockQuantity, availability, productName1, productLabel, tab4, parentCategoryNames, searchItem, bestSeller, featuredProduct, storeName, relateProduct, relatedProduct,
      relatedCategoryNames, selectedCategory, relatedProducts, selectedProduct, relatedItemsDetails, listOfbrands, listofSuppliers, errors,
      metaTitle, metaKeyword, metaDescription, parentCatName, parentCatDisplay, multipleDelete, sortData, imagesArr, brandName, supplierName, reviewListing, sortData1,
    } = this.state
    const Option = Select.Option;
    const filteredOptions1 = relatedProducts.map((each) => { return each.productName });
    const filteredOptions = filteredOptions1.filter(o => !selectedProduct.includes(o));
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Agregar producto</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/productlist')}>Catalogo de producto</li>
                <li className="breadcrumb-item " onClick={() => this.props.history.push('/productlist')} > Productos</li>
                <li className='breadcrumb-item active'> Agregar producto </li>
              </ul>
            </div>
          </div>
        </div>
        <div className='card animated fadeIn mb-2'>
          <div className='card-header' data-spy='affix' data-offset-top='100'>
            <div className='button-continer text-right'>
              <button type='button' className='btn btn-light' onClick={this.resetFields}> <i className='fa fa-share' /> <span>Reiniciar</span> </button>
              <button className='btn btn-primary next-step' onClick={this.addProduct} > <i className='fa fa-check-circle' /> <span>Guardar Continuar</span> </button>
            </div>
          </div>
          <div className='card-body'>
            <ul className='nav nav-tabs' id='myTab' role='tablist'>
              <li className='nav-item'>
                <a className={this.state.tab1 ? 'nav-link active' : 'nav-link'} data-toggle='tab' href='#item' role='tab' aria-controls='item' aria-selected='false' onClick={() => this.setState({ tab1: true, tab2: false, tab3: false, tab4: false, tab5: false, tab6: false, tab7: false })} >Información del artículo </a>
              </li>
              <li className='nav-item'>
                <a className={this.state.tab2 ? 'nav-link active' : 'nav-link'} data-toggle='tab' href='#imagetab' role='tab' aria-controls='imagetab' aria-selected='false' onClick={() => this.setState({ tab1: false, tab2: true, tab3: false, tab4: false, tab5: false, tab6: false, tab7: false })} > Imágenes </a>
              </li>
              <li className='nav-item'>
                <a className={this.state.tab3 ? 'nav-link active' : 'nav-link'} data-toggle='tab' href='#inventorytab' role='tab' aria-controls='inventorytab' aria-selected='false' onClick={() => this.setState({ tab1: false, tab2: false, tab3: true, tab4: false, tab5: false, tab6: false, tab7: false })} > Información de inventario </a>
              </li>

              <li className='nav-item'>
                <a className={this.state.tab4 ? 'nav-link active' : 'nav-link'} data-toggle='tab' href='#additionaltab' role='tab' aria-controls='additionaltab' aria-selected='false' onClick={() => this.setState({ tab1: false, tab2: false, tab3: false, tab4: true, tab5: false, tab6: false, tab7: false })} > Información adicional </a>
              </li>

              <li className='nav-item'>
                <a className={this.state.tab5 ? 'nav-link active' : 'nav-link'} data-toggle='tab' href='#relatedtab' role='tab' aria-controls='relatedtab' aria-selected='false' onClick={() => this.setState({ tab1: false, tab2: false, tab3: false, tab4: false, tab5: true, tab6: false, tab7: false })}> Artículos relacionados </a>
              </li>
              <li className='nav-item'>
                <a className={this.state.tab6 ? 'nav-link active' : 'nav-link'} data-toggle='tab' href='#meta' role='tab' aria-controls='meta' aria-selected='false' onClick={() => this.setState({ tab1: false, tab2: false, tab3: false, tab4: false, tab5: false, tab6: true, tab7: false })} > Metadatos </a>
              </li>
              <li className='nav-item'>
                <a className={this.state.tab7 ? 'nav-link active' : 'nav-link'} data-toggle='tab' href='#reviewtab' role='tab' aria-controls='reviewtab' aria-selected='false' onClick={() => { this.listOfReviews(this.state.productName); this.setState({ tab1: false, tab2: false, tab3: false, tab4: false, tab5: false, tab6: false, tab7: true }) }}> Comentarios </a>
              </li>
            </ul>

            <div className='tab-content' id='myTabContent'>
              {/* tab1 */}
              <div className={this.state.tab1 ? 'tab-pane fade show active' : 'tab-pane fade'} id='item' role='tabpanel' aria-labelledby='itemtab' >
                <form className='form-sample'>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      SKU : <span className='text-danger'>*</span>
                    </label>
                    <div className='col-md-4 co-sm-3'>
                      <input
                        className='form-control'
                        type='text'
                        name='sku'
                        placeholder='Ingresar SKU'
                        onChange={this.handleChange}
                        value={sku}
                      />
                      <span className='error-block'>
                        {this.state.errors.sku}
                      </span>
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Nombre : <span className='text-danger'>*</span>
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <input
                        className='form-control'
                        type='text'
                        name='productName'
                        placeholder='Ingrese el nombre del producto'
                        value={productName}
                        onChange={this.handleChange}
                        onBlur={this.urlGenerator}
                      />
                      <span className='error-block'>
                        {this.state.errors.productName}
                      </span>
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      URL personalizada :<span className='text-danger'>*</span>
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <input
                        className='form-control'
                        type='text'
                        name='customUrl'
                        placeholder='Introducir URL'
                        value={customUrl}
                        onChange={this.handleChange}
                      />
                      <span className='error-block'>
                        {this.state.errors.customUrl}
                      </span>
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Descripción :
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <CKEditor
                        editor={ClassicEditor}
                        data={description}
                        onInit={editor => { }}
                        onChange={(event, editor) => {
                          const data = editor.getData()
                          this.setState({ description: data })
                        }}
                      />
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Breve descripción :
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <CKEditor
                        editor={ClassicEditor}
                        data={shortDescription}
                        onInit={editor => { }}
                        onChange={(event, editor) => {
                          const data = editor.getData()
                          this.setState({ shortDescription: data })
                        }}
                      />
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Precio : <span className='text-danger'>*</span>
                    </label>
                    <div className='col-md-4 co-sm-3'>
                      <input
                        className='form-control'
                        type='number'
                        name='price'
                        placeholder='Ingrese precio'
                        value={price}
                        onChange={this.handleChange}
                      />
                      <span className='error-block'> {this.state.errors.price} </span>
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Precio de venta :
                    </label>
                    <div className='col-md-4 co-sm-3'>
                      <input
                        className='form-control'
                        type='number'
                        name='salePrice'
                        placeholder='Ingrese precio de venta'
                        value={salePrice}
                        onChange={this.handleChangeSaleprice}
                      />
                      <span className='error-block'> {this.state.salePriceErr ? `Ingrese el monto < ${this.state.price}` : null} </span>
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Peso : <span className='text-danger'>*</span>
                    </label>
                    <div className='col-md-4 co-sm-3'>
                      <input
                        className='form-control'
                        type='text'
                        name='weight'
                        placeholder='Ingresar peso'
                        value={weight}
                        onChange={this.handleChange}
                      />
                      <span className='error-block'>
                        {this.state.errors.weight}
                      </span>
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Cantidad mínima para comprar:
                    </label>
                    <div className='col-md-4 co-sm-3'>
                      <input
                        className='form-control'
                        type='number'
                        name='minQuantity'
                        placeholder='Ingrese la cantidad mínima'
                        value={minQuantity}
                        onChange={this.handleChange}
                      />
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Talla :
                    </label>
                    <div className='col-md-4 co-sm-3'>
                      <input
                        className='form-control'
                        type='text'
                        name='size'
                        placeholder='Ingrese tamaño '
                        value={size}
                        onChange={this.handleChange}
                      />
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Imponible :
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <span className='react-switch'>
                        <Toggle
                          checked={taxable}
                          className='custom-classname'
                          onChange={() =>
                            this.setState({ taxable: !this.state.taxable })
                          }
                        />
                      </span>
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Entrega gratis :
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <span className='react-switch'>
                        <Toggle
                          checked={freeDelivery}
                          className='custom-classname'
                          onChange={() =>
                            this.setState({
                              freeDelivery: !this.state.freeDelivery
                            })
                          }
                        />
                      </span>
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Hacer un pedido :
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <span className='react-switch'>
                        <Toggle
                          checked={preOrder}
                          className='custom-classname'
                          onChange={() =>
                            this.setState({ preOrder: !this.state.preOrder })
                          }
                        />
                      </span>
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Descuento permitido :
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <span className='react-switch'>
                        <Toggle
                          checked={allowedDiscount}
                          className='custom-classname'
                          onChange={() =>
                            this.setState({
                              allowedDiscount: !this.state.allowedDiscount
                            })
                          }
                        />
                      </span>
                    </div>
                  </div>
                  {allowedDiscount ? (
                    <div className='form-group row'>
                      <label className='col-lg-2 col-sm-3 col-form-label'>
                        Descuento :
                      </label>
                      <div className='col-md-4 co-sm-7'>
                        <span className='react-switch'>
                          <input
                            className='form-control'
                            type='number'
                            name='discountValue'
                            placeholder='Ingrese el valor de descuento'
                            value={discountValue}
                            onChange={this.handleChange}
                          />
                        </span>
                      </div>
                    </div>
                  ) : null}

                  <div className='form-group row'>
                    <label
                      className='col-lg-2 col-sm-3 col-form-label'
                      htmlFor='material-switch'
                    >
                      Publicado :
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <Toggle
                        checked={publish}
                        className='custom-classname'
                        onChange={() =>
                          this.setState({ publish: !this.state.publish })
                        }
                      />
                    </div>
                  </div>
                </form>
              </div>
              {/* tab2 */}
              <div className={this.state.tab2 ? 'tab-pane fade show active' : 'tab-pane fade'} id='imagetab' role='tabpanel' aria-labelledby='imagetab' >
                <div className='row m-b-1'>
                  <div className='drop-section mb-3 col-md-6'>
                    <h5>Imagen principal</h5>
                    <div className='form-group inputDnD mb-0'>
                      <div className='upload-overlay d-flex justify-content-center align-items-center'>
                        <div className='upload-wrapper'>
                          <i className='fa fa-upload' />
                          <span> <button type='button'>Elige un archivo</button> o arrástralo aquí </span>
                        </div>
                      </div>
                      <label className='sr-only'>Subir archivo</label>
                      <input
                        type='file'
                        className='form-control-file text-primary font-weight-bold'
                        name='image'
                        id='file'
                        accept='image/*'
                        data-title='Drag and drop a file'
                        onChange={this.fileChangedHandler.bind(this)}
                      />
                      <span className="notes">El tamaño de imagen 400 * 400 es preferible</span>

                    </div>
                    <div className='row justify-content-between my-2'>
                      {this.state.fileVisible ? (
                        <div className='col-md-12 select-image'>
                          <span>
                            <div className='product-img'> <img src={this.state.imagePreviewUrl} /> </div>
                            <span>{this.state.filename}</span>
                          </span>
                          <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ fileVisible: !this.state.fileVisible })} />
                        </div>
                      ) : null}

                    </div>
                    {this.state.fileVisible ? (
                      <div className='button-continer pull-right mb-2'>
                        <button className='btn btn-primary' type='button' onClick={this.singleFileUpload} > Cargar imagen </button>
                      </div>
                    ) : null}
                    <div className='table-responsive mt-3'>
                      <table className='table dataTable with-image row-border hover custom-table'>
                        <thead>
                          <tr>
                            <th> Imagen </th>
                            <th>Label</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr className='animated fadeIn'>
                            <td className='product-img'>
                              {productName1 ? <img src={IMAGE_URL + productName1 + "?" + Math.random()} alt={productName1} /> : <img src="/assets/images/no-image-icon-4.png" />}
                            </td>
                            <td>
                              <input
                                className='form-control'
                                type='text'
                                name='productLabel'
                                value={productLabel}
                                placeholder='Label'
                                onChange={this.handleChange}
                              />
                            </td>
                            <td>
                              {productName1 ?
                                <button className='btn btn-outline-primary btn-border' onClick={this.deleteProductImage} >
                                  <i className='fa fa-close' /> <span>Remove</span>
                                </button> : null}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  {/* multi  images*/}
                  <div className='drop-section mb-3 col-md-6'>
                    <h5>Imagen adicional</h5>
                    <div className='form-group inputDnD mb-0'>
                      <div className='upload-overlay d-flex justify-content-center align-items-center'>
                        <div className='upload-wrapper'>
                          <i className='fa fa-upload' />
                          <span> <button type='button'>Elige un archivo</button> arrástralo aquí</span>
                        </div>
                      </div>
                      <label className='sr-only'>Subir archivo</label>
                      <input
                        type='file'
                        className='form-control-file text-primary font-weight-bold'
                        name='image'
                        id='file1'
                        accept='image/*'
                        multiple
                        data-title='Drag and drop a file'
                        onChange={this.fileChangedHandler1.bind(this)}
                      />
                      <span className="notes">El tamaño de imagen 400 * 400 es preferible</span>
                    </div>

                    {this.state.multibeforeUpload ?
                      <div className='row justify-content-between my-2'>
                        {this.state.imagesArr && Array.isArray(this.state.imagesArr) && this.state.imagesArr.length ? this.state.imagesArr.map((each, id) => {
                          return (<div className='col-md-12 select-image' key={id}>
                            <span>
                              <div className='product-img'>
                                <img src={each && each.image ? each.image : undefined} />
                              </div>
                              <span>{each.url}</span>
                            </span>
                            <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={this.imagesSplice.bind(this, id)} />
                          </div>
                          )
                        }) : undefined}
                      </div>
                      : null}
                    {this.state.multiple_img ?
                      <div className='button-continer  pull-right   mb-2'>
                        <button className='btn btn-primary' type='button' onClick={this.multipleFileUpload}> Cargar imagen </button>
                      </div>
                      : null}
                    {this.state.multiView ?
                      <div className='table-responsive  mt-3'>
                        <table className='table dataTable with-image row-border hover custom-table'>
                          <thead>
                            <tr>
                              <th>Imagen </th>
                              <th>Label</th>
                              <th sortable-column='firstname'> Sequence </th>
                            </tr>
                          </thead>

                          <tbody>
                            {additionalImages && additionalImages.length > 0 && additionalImages.map((each, id) => {
                              return (
                                <tr className='animated fadeIn' key={id}>
                                  <td className='product-img'>
                                    <img
                                      src={each.imgName ? IMAGE_URL + each.imgName : "/assets/images/no-image-icon-4.png"}
                                      alt={each.imgName}
                                    />
                                  </td>
                                  <td>
                                    <input
                                      className='form-control'
                                      type='text'
                                      name='thumbnailImage'
                                      value={each.label ? each.label : each.imgLabel}
                                      placeholder="Enter Label"
                                      onChange={(event) => this.handleImageNameChange(event, each)}
                                    />
                                  </td>
                                  <td>
                                    <input
                                      className='form-control'
                                      type='text'
                                      name='option'
                                      placeholder='Sort Order'
                                      value={each.imgSequence ? each.imgSequence : each.sequence_number}
                                      // onChange={this.handleChange}
                                      onChange={(event) => this.handleSequenceChange(event, each)}
                                    />
                                  </td>
                                  <td>
                                    <button className='btn btn-outline-primary btn-border' onClick={() => this.deleteAdditionalImages(id, each._id)}> <i className='fa fa-close' /> Remove </button>
                                  </td>
                                </tr>
                              )
                            })}
                          </tbody>
                        </table>
                      </div>

                      : null}



                  </div>
                </div>
              </div>
              {/* tab3 */}
              <div className={this.state.tab3 ? 'tab-pane fade show active' : 'tab-pane fade'} id='inventorytab' role='tabpanel' aria-labelledby='inventorytab' >
                <form className='form-sample'>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Cantidad de stock :<span className='text-danger'>*</span>
                    </label>
                    <div className='col-md-4 co-sm-3'>
                      <input
                        className='form-control'
                        type='number'
                        name='stockQuantity'
                        placeholder='Introducir cantidad de stock '
                        value={stockQuantity}
                        onChange={this.handleChange}
                      />
                      <span className='error-block'>
                        {this.state.errors.stockQuantity}
                      </span>
                    </div>
                  </div>

                  {/* <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Availability : <span className='text-danger'>*</span>
                    </label>
                    <div className='col-md-4 co-sm-7'>
                      <select
                        className='form-control'
                        value={availability}
                        onChange={e =>
                          this.setState({ availability: e.target.value })
                        }
                      >
                        <option value='2 working days'>2 working days</option>
                        <option value='4 working days'>4 working days</option>
                        <option value='6 working days'>6 working days</option>
                        <option value='10 working days'>10 working days</option>
                      </select>
                    </div>
                  </div>
                  <span className='error-block'>
                    {this.state.errors.availability}
                  </span> */}
                </form>
              </div>
              {/* tab6 */}
              <div className={this.state.tab6 ? 'tab-pane fade show active' : 'tab-pane fade'} id='meta' role='tabpanel' aria-labelledby='metatab' >
                <form className='form-sample'>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Meta título
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <input
                        className='form-control'
                        type='text'
                        name='metaTitle'
                        value={metaTitle}
                        placeholder='Enter Title'
                        onChange={this.handleChange}
                      />
                      <span className='notes'> Máximo 70 caracteres es adecuado</span>
                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Meta palabra clave:
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <textarea
                        className='form-control'
                        type='text'
                        name='metaKeyword'
                        value={metaKeyword}
                        placeholder='Introduzca palabra clave meta'
                        onChange={this.handleChange}
                      />
                      <span className='notes'>
                        Máximo 10 palabras es adecuado
                      </span>
                    </div>
                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Metadescripción:
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <textarea
                        className='form-control'
                        type='text'
                        name='metaDescription'
                        value={metaDescription}
                        placeholder='Ingrese la descripción'
                        onChange={this.handleChange}
                      />
                      <span className='notes'>
                        Máximo 156 caracteres es adecuado
                      </span>
                    </div>
                  </div>
                </form>
              </div>
              {/* tab4 */}
              <div className={this.state.tab4 ? 'tab-pane fade show active' : 'tab-pane fade'} id='additionaltab' role='tabpanel' aria-labelledby='additionaltab' >
                <form className='form-sample'>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Categoría principal:<span className='text-danger'>*</span>
                    </label>

                    <div className='col-md-8 co-sm-7'>

                      <Select
                        placeholder="Parent Category"
                        value={parentCatDisplay}
                        onChange={(value, key) => this.handle_parentCatName(value, key)}
                        onSearch={this.getCategoryName}
                        onFocus={this.getCategoryName}
                      >
                        {this.state.parentCategoryNames.map((each, id) => {
                          return (
                            <OptGroup>
                              <Option value={each._id} key={id}> {each.categoryName} </Option>
                              {each && each.subCategoryArray && Array.isArray(each.subCategoryArray) && each.subCategoryArray.length ? each.subCategoryArray.map((subCat, _id) => {
                                return (
                                  <OptGroup>
                                    <Option value={subCat._id} key={_id}> -{subCat.categoryName} </Option>
                                    {subCat && subCat.subCategory && Array.isArray(subCat.subCategory) && subCat.subCategory.length ? subCat.subCategory.map((subsub, _id) => {
                                      return (
                                        <Option value={subsub._id} key={_id}> --{subsub.categoryName} </Option>
                                      )
                                    }) : undefined}
                                  </OptGroup>
                                )
                              }) : undefined
                              }
                            </OptGroup>

                          )
                        })}
                      </Select>
                      <span className='error-msg'>{errors.parentCatDisplay}</span>

                    </div>

                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Mejor vendido :
                    </label>
                    <div className='col-md-8 co-sm-7 round-radio d-flex align-items-center'>
                      <span className='react-switch'>
                        <Toggle
                          checked={bestSeller}
                          className='custom-classname'
                          onChange={() =>
                            this.setState({
                              bestSeller: !this.state.bestSeller
                            })
                          }
                        />
                      </span>

                      {/* <div className="radio">
                      <label>
                      <input type="radio" name="optradio" checked={featuredProduct ? false : true} onChange={() =>
                        this.setState({
                          bestSeller: !this.state.bestSeller,
                          featuredProduct: !this.state.featuredProduct
                        })} />
                        <div className="inner">
                          <span></span>
                        </div>
                      </label>
                    </div> */}

                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Producto destacado:
                    </label>
                    <div className='col-md-8 co-sm-7 round-radio d-flex align-items-center'>
                      <span className='react-switch'>
                        <Toggle
                          checked={featuredProduct}
                          className='custom-classname'
                          onChange={() =>
                            this.setState({
                              featuredProduct: !this.state.featuredProduct
                            })
                          }
                        />
                      </span>
                      {/* <div className="radio">
                      <label>
                      <input type="radio" name="optradio" checked={bestSeller ? false : true} onChange={() =>
                        this.setState({
                          featuredProduct: !this.state.featuredProduct,
                          bestSeller: !this.state.bestSeller
                        })
                      } />
                        <div className="inner">
                          <span></span>
                        </div>
                      </label>
                    </div> */}

                    </div>
                  </div>
                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Nombre de la marca :<span className='text-danger'>*</span>
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <Select
                        showSearch
                        placeholder="Ingrese el nombre de la marca"
                        value={this.state.brandName}
                        onChange={(brandName) => this.setState({ brandName: brandName, brandId: brandName, errors: Object.assign(this.state.errors, { brandName: "", brandId: '' }) })}
                        onSearch={(e) => this.handleSearch(e, 'brandName')}
                        onFocus={(e) => this.handleSearch(e, 'brandName')}
                        style={{ width: '100%' }}
                        filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {listOfbrands.map((item, key) => (
                          <Select.Option key={key} value={item._id}>
                            {item.brandName}
                          </Select.Option>
                        ))}
                      </Select>
                      <span className='error-msg'>{errors.brandId}</span>
                    </div>



                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label'>
                      Nombre del proveedor :<span className='text-danger'>*</span>
                    </label>
                    <div className='col-md-8 co-sm-7'>
                      <Select
                        showSearch
                        placeholder="Ingrese el nombre del proveedor"
                        value={supplierName}
                        onChange={(supplierName) => this.setState({ supplierName, supplierId: supplierName, errors: Object.assign(this.state.errors, { supplierName: "", supplierId: '' }) })}
                        // onInputKeyDown={(e) => this.handleSearch(e,'supplierName')}
                        onSearch={(e) => this.handleSearch(e, 'supplierName')}
                        onFocus={(e) => this.handleSearch(e, 'supplierName')}

                        style={{ width: '100%' }}
                        filterOption={(input, option) =>
                          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {listofSuppliers.map((item, key) => (
                          <Select.Option key={key} value={item._id}>
                            {item.supplierName}
                          </Select.Option>
                        ))}
                      </Select>
                      <span className='error-msg'>{errors.supplierId}</span>

                    </div>
                  </div>

                  <div className='form-group row'>
                    <label className='col-lg-2 col-sm-3 col-form-label' />
                  </div>
                  <hr />
                </form>
              </div>
              {/* tab7 */}
              <div className={this.state.tab7 ? 'tab-pane fade show active' : 'tab-pane fade'} id='reviewtab' role='tabpanel' aria-labelledby='reviewtab' >
                <div className='table-responsive'>
                  {reviewListing.length > 0 ?
                    <table className='table dataTable with-image row-border hover custom-table'>
                      <thead>
                        <tr>
                          <th sortable-column="rating" onClick={this.onSort1.bind(this, 'rating')}>Clasificación
                      <i aria-hidden="true" className={(sortData1['rating']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i></th>
                          <th sortable-column="reviewerName" onClick={this.onSort1.bind(this, 'reviewerName')}>Nombre del revisor
                        <i aria-hidden="true" className={(sortData1['reviewerName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i>
                          </th>
                          <th sortable-column="productName" onClick={this.onSort1.bind(this, 'productName')}>nombre del producto
                       <i aria-hidden="true" className={(sortData1['productName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i>
                          </th>
                          <th sortable-column="description" onClick={this.onSort1.bind(this, 'description')} >Comentarios
                        <i aria-hidden="true" className={(sortData1['description']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i>
                          </th>
                          <th sortable-column="isActive" onClick={this.onSort1.bind(this, 'isActive')}>Estado
                      <i aria-hidden="true" className={(sortData1['isActive']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i>
                          </th>
                          <th> Acción</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          reviewListing.map((each, Key) => {
                            return (
                              <tr key={Key} className="animated fadeIn">

                                <td>  <StarRatings
                                  starRatedColor="orange"
                                  rating={each.rating}
                                  starDimension="20px"
                                  starSpacing="5px"
                                  numberOfStars={5}
                                  name='rating'
                                /></td>
                                <td><p className="mb-1 d-block" >{each.reviewerName}</p>{each.emailId}{each.createdAt}<p className="text-danger" onClick={() => this.props.history.push(`/editreview/${each._id}`)}>[Editar reseña]</p></td>
                                <td>{each.productName}<a className="mt-1 d-block" href={each.productLink}>Visita la página</a> </td>
                                <td><b >{each.title}</b><br /><span className="text-truncate cu-w mt-1">{each.description}</span></td>
                                <td><label>
                                  <Toggle
                                    checked={each.isActive}
                                    className='custom-classname'
                                    onChange={() => this.change_reviewStatus(each.isActive, each._id)}
                                  />
                                </label>
                                </td>
                                <td>
                                  <button onClick={() => this.props.history.push(`/editreview/${each._id}`)} data-toggle="tooltip" title="Edit"><i className="fa fa-pencil-square-o text-primary" aria-hidden="true"></i></button>
                                  <button onClick={() => this.deleteReview(each._id)} data-toggle="tooltip" title="Delete">< i className="fa fa-trash text-danger" aria-hidden="true"></i></button>
                                </td>
                              </tr>
                            );
                          })
                        }
                      </tbody>
                    </table> :

                    <div>No se encontraron comentarios</div>}
                </div>
              </div>
              {/* tab5 */}
              <div className={this.state.tab5 ? 'tab-pane fade show active' : 'tab-pane fade'} id='relatedtab' role='tabpanel' aria-labelledby='relatedtab' >
                <div className='row add-new'>
                  <div className='form-group col-lg-4 col-sm-4'>
                    <Select
                      mode="multiple"
                      placeholder="Ingrese el nombre de la categoría"
                      value={selectedCategory}
                      onChange={(value) => this.handle_RelCatName(value)}
                      // onInputKeyDown={(e) => this.relatedCatagory(e)}
                      onSearch={(e) => this.relatedCatagory(e)}
                      onFocus={(e) => this.relatedCatagory(e)}

                      style={{ width: '100%' }}
                    >
                      {relatedCategoryNames.map((item, key) => (
                        <Select.Option key={key} value={item}>
                          {item}
                        </Select.Option>
                      ))}
                    </Select>
                  </div>
                  <div className='form-group col-lg-4 col-sm-4'>
                    <Select
                      mode="multiple"
                      placeholder="Enter Product Name"
                      value={selectedProduct}
                      onChange={(value) => this.handle_RelProdName(value)}
                      // onInputKeyDown={(e) => this.relatedProductsArr(e)}
                      onSearch={(e) => this.relatedProductsArr(e)}
                      onFocus={(e) => this.relatedProductsArr(e)}

                      style={{ width: '100%' }}
                    >
                      {filteredOptions.map(item => (
                        <Select.Option key={item} value={item}>
                          {item}
                        </Select.Option>
                      ))}
                    </Select>
                  </div>
                  <button className='btn btn-primary' onClick={this.addingRelatedProducts}>
                    <i className='fa fa-plus' />
                    <span>Add</span>
                  </button>
                </div>
                {this.state.multipleDelete.length > 0 ?
                  <button type="button" className="btn btn-danger animated fadeIn" onClick={() => this.deleteRelatedProducts(...multipleDelete)}><i className="fa fa-trash"></i> Delete Selected</button>
                  : null}
                <div className='table-responsive'>
                  {relatedItemsDetails ?
                    <table className='table dataTable with-image row-border hover custom-table'>
                      <thead>
                        <tr>
                          {/* <th width='5%'>
                            <div className='checkbox'>
                              <label>
                                <input
                                  type='checkbox'
                                  className='form-check-input'
                                  id='deleteCheckbox'
                                />
                                <span />
                                <i className='input-helper' />
                              </label>
                            </div>
                          </th> */}
                          <th sortable-column="SKU" onClick={this.onSort.bind(this, 'SKU')}>SKU <i aria-hidden="true" className={(sortData['SKU']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i> </th>
                          <th> Imagen </th>
                          <th sortable-column="productName" onClick={this.onSort.bind(this, 'productName')}>nombre del producto<i aria-hidden="true" className={(sortData['productName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i> </th>
                          <th sortable-column="categoryName" onClick={this.onSort.bind(this, 'categoryName')}>Categoría <i aria-hidden="true" className={(sortData['categoryName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i> </th>
                          <th sortable-column="price" onClick={this.onSort.bind(this, 'price')}>Precio <i aria-hidden="true" className={(sortData['price']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i> </th>
                          <th sortable-column="salePrice" onClick={this.onSort.bind(this, 'salePrice')}>Precio de venta<i aria-hidden="true" className={(sortData['salePrice']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i> </th>
                          <th sortable-column="stockQuantity" onClick={this.onSort.bind(this, 'stockQuantity')}>Cantidad avl<i aria-hidden="true" className={(sortData['stockQuantity']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i> </th>
                          <th width='10%'> Action</th>
                        </tr>
                      </thead>
                      <tbody>

                        {relatedItemsDetails.map((each, key) => {
                          return (

                            <tr className='animated fadeIn' key={key}>
                              {/*  <td>
                                <div className='checkbox'>
                                  <label>
                                    <input
                                      type='checkbox'
                                      className='form-check-input'
                                      name='foo'
                                      checked={this.checkArray(each._id)}
                                      onChange={() => this.onCheckbox(each._id)}
                                    />
                                    <span />
                                    <i className='input-helper' />
                                  </label>
                                </div>
                              </td> */}
                              <td>{each.SKU}</td>
                              <td>
                                <div className='thumb-img'>
                                  <img
                                    src={IMAGE_URL + each.productImage}
                                    alt='pic'
                                  />
                                </div>
                              </td>
                              <td>{each.productName}</td>
                              <td>{each.categoryName}</td>
                              <td>{each.price}</td>
                              <td>{each.salePrice}</td>
                              <td>{each.stockQuantity}</td>
                              <td>
                                <button onClick={() => this.deleteRelatedProducts(each._id)}> <i className='fa fa-trash text-danger' aria-hidden='true' /> </button>
                              </td>
                            </tr>
                          )
                        })}

                      </tbody>
                    </table>
                    : null}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home>
    )
  }
}



const mapStateToProps = state => ({
  Categorydetails: state.commonReducers.editedDetails,
  permissionsList: state.admindata.rolePermissions,

});

export default connect(mapStateToProps, actions)(AddProduct) 