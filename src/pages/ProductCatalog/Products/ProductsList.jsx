
import React, { Component } from 'react';
import "bootstrap-less";
import { connect } from "react-redux";
import Toggle from 'react-toggle'
// import 'rc-pagination/assets/index.css';
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import Pagination from 'rc-pagination';
import axios from 'axios'
import _ from "lodash"
import { BeatLoader } from 'react-spinners';
import { API_URL, IMAGE_URL, CSV_URL, EXCEL_URL } from '../../../config/configs'
import swal from 'sweetalert';
import { Select } from 'antd';
import 'antd/dist/antd.css';
import * as actions from '../../../actions';
import Home from '../../Home';
import $ from 'jquery';
import { isEmpty } from "lodash"
import Loader from '../../Loader'
const FileDownload = require('js-file-download')

class ProductManagement extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      pagesize: 25,
      productList: [],
      sku: true,
      catName: true,
      price: true,
      salePrice: true,
      quantity: true,
      prodName: true,
      brandName: true,
      supplierName: true,
      searchCategory: '',
      published: true,
      multipleDelete: [],
      selectedOption: 'Select here',
      total: null,
      selectedProduct: [],
      selectedCategory: [],
      mainCategoryNames: [],
      mainProductNames: [],
      selectedBrand: [],
      listOfBrands: [],
      selectedSku: [],
      selectedSupplier: [],
      listOfSuppliers: [],
      searchBrandslist: [],
      SKUNames: [],
      min_price: "",
      max_price: "",
      minSalePrice: "",
      maxSalePrice: "",
      minStockQuantity: "",
      maxStockQuantity: "",
      selectedCategory1: [],
      selectAll: false,
      sortData: { price: false, SKU: false, productName: false, categoryName: false, salePrice: false, stockQuantity: false, brandName: false, supplierName: false, publish: false },
      sort: {},
      loading: true,
      selectedBrandS: [],
      searchSupplierslist: [],
      searchSupplierS: [],
      length: 0,
      productsAccess: {},
      loading: true,
      publishedvalue: '',
      page_: false,
      download: false,
      skuText: '',
      productText:''


    };
  }



  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission && permissions.rolePermission.productsAccess && permissions.rolePermission.productsAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }

    if (permissions.rolePermission) {
      let { productsAccess } = permissions.rolePermission
      this.setState({ productsAccess: productsAccess })
    }
    this.productListing('pagesize')
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').stop().slideToggle();
    });

    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').stop().slideToggle();
    });
    $('[data-toggle="tooltip"]').tooltip();
  }


  //*********** SORTING ************************//
  onSort = (column) => {
    let { sortData } = this.state;
    var element, value;
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key];
        element = key;
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState({
          sort: { [element]: value }
        }, () => {
          this.productListing();
        });
        this.setState({ sortData });
      }
      else {
        sortData[key] = false
        element = key;
        value = 1;
      }
    }
    this.setState({ sortData });
  }

  // ############################## Product Listing ######################
  productListing = (e) => {
    var url = '/products/productListing'
    var method = 'post'
    var { page, pagesize, selectedProduct, skuText,productText, sort, publishedvalue, searchCategoryS, selectedBrandS, searchSupplierS, searchCategory, selectedCategory1, selectedCategory, selectedSku, min_price, max_price, minSalePrice, maxSalePrice, minStockQuantity, maxStockQuantity } = this.state;
    let data = {}
    if (e === 'filter') {
      data.page = 1
    } else if (e === 'pagesize') {
      data = data
    } else {
      data.page = page
    }
    
    if (this.state.product_text) {
      data.type = 'productName'
      data.searchText = productText
    }
    if (this.state.sku_text) {
      data.type = 'SKU'
      data.searchText = skuText
    }
    if (e === 'pagesize') {
      data = data
    } else {
      data.pagesize = pagesize
    }
    let price = {}
    let salePrice = {}
    let stockQuantity = {}
    data.price = price;
    data.salePrice = salePrice
    data.stockQuantity = stockQuantity
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    // if (selectedProduct != '') {
    //   data.productName = selectedProduct;
    // }
    if (selectedBrandS != '') {
      data.brandName = selectedBrandS;
    }
    if (searchSupplierS != '') {
      data.supplierName = searchSupplierS;
    }
    if (searchCategoryS != '') {
      data.categoryName = searchCategoryS;
    }
    // if (selectedSku != '') {
    //   data.SKUName = selectedSku;
    // }
    if (publishedvalue != '') {
      data.status = publishedvalue
    }
    if (min_price != '') {
      price.minPrice = JSON.parse(min_price);
    }
    if (min_price == '') {
      price.minPrice = null;
    }
    if (max_price != '') {
      price.maxPrice = JSON.parse(max_price);
    }
    if (max_price == '') {
      price.maxPrice = null;
    }
    if (minSalePrice != '') {
      salePrice.minSalePrice = JSON.parse(minSalePrice);
    }
    if (minSalePrice == '') {
      salePrice.minSalePrice = null;
    }
    if (maxSalePrice != '') {
      salePrice.maxSalePrice = JSON.parse(maxSalePrice);
    }
    if (maxSalePrice == '') {
      salePrice.maxSalePrice = null;
    }
    if (minStockQuantity != '') {
      stockQuantity.minStockQuantity = JSON.parse(minStockQuantity);
    }
    if (minStockQuantity == '') {
      stockQuantity.minStockQuantity = null;
    }
    if (maxStockQuantity != '') {
      stockQuantity.maxStockQuantity = JSON.parse(maxStockQuantity);
    }
    if (maxStockQuantity == '') {
      stockQuantity.maxStockQuantity = null;
    }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      let data1 = response.data.data
      if (response.data.status === 1) {
        this.setState({ productList: data.productlisting, total: data.total, loading: false, length: data.productlisting.length, loading: false, selectedOption: 'Select here' })
        if (data && data.productlisting.length <= pagesize) {
          this.setState({ page_: false })
        }
        if (data1.manageProductListing) {
          this.setState({
            sku: data1.manageProductListing.SKU,
            catName: data1.manageProductListing.categoryName,
            prodName: data1.manageProductListing.productName,
            price: data1.manageProductListing.price,
            salePrice: data1.manageProductListing.salePrice,
            // quantity:data1.manageProductListing.fax,
            pagesize: data1.manageProductListing.pageSize,
            published: data1.manageProductListing.publish,
            brandName: data1.manageProductListing.brandName,
            supplierName: data1.manageProductListing.supplierName

          })
        }
      }
      else if (e == 'filter') {
        this.setState({ page: 1 })
      }
      else {
        this.setState({ supplierListing: [] })
      }
    })
  }


  // ############################## Reset Listing ######################
  resetListing = (e) => {
    var url = '/products/productListing'
    var method = 'post'
    var { page, pagesize } = this.state
    var body = { page: page, pagesize: pagesize }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data.status === 0) {
        this.setState({ productList: [] })
      } else {
        this.setState({
          productList: data.data.productlisting, total: data.data.total, loading: false, skuText: '',product_text:false,sku_text:false,productText:'', length: data.data.productlisting.length, selectedProduct: [], selectedCategory: [], selectedSku: [], selectedBrand: [], searchCategoryS: [], selectedBrandS: [], searchSupplierS: [], selectedSupplier: [],
          min_price: "", max_price: "", minSalePrice: "", maxSalePrice: "", minStockQuantity: "", maxStockQuantity: "", total: data.data.total, publishedvalue: 'Select',
        })
      }
    }
    )
  }


  // ########################### Bulk Actions #########################

  applyfilter = () => {
    var delArr = this.state.multipleDelete
    if (this.state.selectedOption == 'Delete') {
      if (delArr.length > 0) {
        this.deleteProduct(...delArr)
      }
    }
    if (this.state.selectedOption == 'Publish') {
      var body = { productId: delArr, publish: true }
    }
    if (this.state.selectedOption == 'Un-publish') {
      var body = { productId: delArr, publish: false }
    }
    var url = '/products/changeProductStatus'
    var method = 'post'
    var token = localStorage.getItem('token')
    if (delArr.length > 0) {
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status == 1) {
          if (this.state.selectedOption == 'Un-publish') {
            swal('Productos no publicados con éxito', '', 'success')
          }
          else {
            swal('Productos publicados con éxito', '', 'success')
          }
          this.productListing()
        }
        this.setState({ multipleDelete: [], selectedOption: 'Selecciona aquí' })
      })
    } else {
      this.setState({ selectedOption: 'Selecciona aquí' })
      swal('Select atleast one product', '', 'info')
    }

  }

  // ########################### Product Publish  #########################
  productPublish(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { productId: [Id], publish: status }
    var urlkey = '/products/changeProductStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        if (status == false) {
          swal('Desactivado con éxito !', '', 'success')
        }
        else {
          swal('Publicado con éxito !', '', 'success')
        }
        this.productListing()
      }
      else {
        swal(data.message, '', 'error')
      }
    })
  }

  // ########################### Delete Product #########################
  deleteProduct = id => {
    var delArr = this.state.multipleDelete
    swal({ title: '¿Estás seguro?', icon: 'warning', buttons: true, dangerMode: true }).then(willDelete => {
      if (willDelete) {
        if (delArr.length > 0) {
          var body = { productId: delArr }
        } else {
          var body = { productId: [id] }
        }
        var url = '/products/deleteProduct'
        var method = 'post'
        var token = localStorage.getItem('token')
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          let { data } = response
          this.productListing()
          this.setState({ multipleDelete: [], selectedOption: 'Select here' })
        }
        )
        swal('Borrado exitosamente', '', 'success')
      }
    })
  }

  // ########################### Set Table Rows #########################

  setTableRows = (e) => {

    var token = localStorage.getItem('token')
    let { sku, catName, prodName, price, salePrice, quantity, published, brandName, supplierName, pagesize } = this.state
    var body = { SKU: sku, salePrice: salePrice, price: price, productName: prodName, categoryName: catName, stockQuantity: quantity, publish: published, brandName, supplierName, pageSize: pagesize }
    var url = '/products/setFilter'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      if (response) {
        this.productListing()
        if (e) {
          swal('Actualizado con éxito!', '', 'success')
        }
      }
    })
  }

  // ########################### Reset Table Rows #########################

  reSetTableRows = () => {
    this.setState({ sku: true, catName: true, prodName: true, price: true, salePrice: true, quantity: true, published: true })
  }





  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }



  // ########################### PAGINATION #########################

  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize,
      pagination_: false
    }, () => this.productListing());
    this.setState({ selectAll: false }, () => this.selectAllcheck())

  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 }, () => this.setTableRows(), () => this.productListing());
  }
  handleChange_ = (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1, page_: true }, () => this.setTableRows(), () => this.productListing());
    }
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.productListing());
    // }
  }

  // ########################### for Search #########################
  getIds = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: name, searchText: e }
    var url = '/products/productFieldsList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (response && response.data && response.data.data) {
        if (name === 'productName') {
          var pNArr = _.uniqBy(data, (e) => {
            return e.productName.toString();
          });
          this.setState({ mainProductNames: pNArr })
        }
        if (name === 'SKU') {
          var sKArr = _.uniqBy(data, (e) => {
            return e.SKU.toString();
          });
          this.setState({ SKUNames: sKArr })
        }
        if (name === 'brandName') {
          var bRArr = _.uniqBy(data, (e) => {
            return e.brandName.toString();
          });
          this.setState({ listOfBrands: bRArr, searchBrandslist: data })
        }
        if (name === 'supplierName') {
          var supArr = _.uniqBy(data, (e) => {
            return e.supplierName.toString();
          });
          this.setState({ listOfSuppliers: supArr, searchSupplierslist: data })

        }
      }
    })
  }

  // ########################### getcategory for Search #########################
  getcategoryName = e => {
    let token = localStorage.getItem('token')
    var body = { searchText: e }
    var url = '/categories/getAllCategories'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      var catArr = _.uniqBy(data, (e) => {
        return e.categoryName.toString();
      });
      this.setState({ mainCategoryNames: catArr, searchCatlist: data })
    })
  }


  // ########################### download CSV #########################

  downloadCSV = (type, array) => {
    let { sku, catName, prodName, price, salePrice, quantity, published, supplierName, brandName, productList, selectedProduct, multipleDelete,
      selectedBrandS, searchSupplierS, searchCategory, selectedCategory1, selectedCategory, selectedSku, min_price, max_price, minSalePrice, maxSalePrice, minStockQuantity, maxStockQuantity } = this.state
    let token = localStorage.getItem('token')
    this.setState({ download: true })
    if (array === "totalList") {
      var data = { filteredFields: ["productImage", "productId", "SKU", "productName", "publish", "price", "salePrice", "categoryName", "stockQuantity", "brandName", "supplierName"] }
    }

    if (array === "filteredList") {
      var data = { filteredFields: ["productImage"] }
      let price1 = {}
      let salePrice1 = {}
      let stockQuantity1 = {}
      data.price = price1;
      data.salePrice = salePrice1;
      data.stockQuantity = stockQuantity1

      if (sku) {
        data.filteredFields.push('SKU')
      };
      if (catName) {
        data.filteredFields.push('categoryName')
      };
      if (prodName) {
        data.filteredFields.push('productName')
      };
      if (price) {
        data.filteredFields.push("price")
      };
      if (salePrice) {
        data.filteredFields.push("salePrice")
      };
      if (quantity) {
        data.filteredFields.push("stockQuantity")
      };
      if (published) {
        data.filteredFields.push("publish")
      }
      if (brandName) {
        data.filteredFields.push("brandName")
      }
      if (supplierName) {
        data.filteredFields.push("supplierName")
      }
      if (multipleDelete.length > 0) {
        data.productArray = multipleDelete
      }
      if (selectedProduct != '') {
        data.productName = selectedProduct
      }
      if (selectedBrandS != '') {
        data.brandName = selectedBrandS
      }
      if (searchSupplierS != '') {
        data.supplierName = searchSupplierS
      }
      if (selectedCategory != '') {
        data.categoryName = selectedCategory
      }
      if (selectedSku != '') {
        data.SKUName = selectedSku
      }
      if (min_price != '') {
        price1.minPrice = JSON.parse(min_price)
      }
      if (min_price == '') {
        price1.minPrice = null
      }
      if (max_price != '') {
        price1.maxPrice = JSON.parse(max_price)
      }
      if (max_price == '') {
        price1.maxPrice = null
      }
      if (minSalePrice != '') {
        salePrice1.minSalePrice = JSON.parse(minSalePrice)
      }
      if (minSalePrice == '') {
        salePrice1.minSalePrice = null
      }
      if (maxSalePrice != '') {
        salePrice1.maxSalePrice = JSON.parse(maxSalePrice)
      }
      if (maxSalePrice == '') {
        salePrice1.maxSalePrice = null
      }
      if (minStockQuantity != '') {
        stockQuantity1.minStockQuantity = JSON.parse(minStockQuantity)
      }
      if (minStockQuantity == '') {
        stockQuantity1.minStockQuantity = null
      }
      if (maxStockQuantity != '') {
        stockQuantity1.maxStockQuantity = JSON.parse(maxStockQuantity)
      }
      if (maxStockQuantity == '') {
        stockQuantity1.maxStockQuantity = null
      }

    }
    var body = data
    var url = '/products/' + type
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (data) {
        this.setState({ multipleDelete: [], download: false })
        if (type === 'downloadCsvFile') {
          window.open(CSV_URL + data.filePathAndName, '_blank');
          // FileDownload(CSV_URL  +  data.filePathAndName, data.filePathAndName);
        } else {
          window.open(EXCEL_URL + data.filePathAndName, '_blank');
          // FileDownload(EXCEL_URL  +  data.filePathAndName, data.filePathAndName);
        }
        // swal("Downloaded Successfully",'','success')
      }
    })
  }



  editProduct(id) {
    this.props.history.push(`/editProduct/${id}`)
  }
  checkArray(_id) {
    let { multipleDelete } = this.state
    if (multipleDelete.includes(_id)) {
      return true
    } else {
      return false
    }
  }

  onCheckbox(_id, val) {
    var delarray = this.state.multipleDelete
    if (!delarray.includes(_id)) {
      delarray.push(_id)
    } else {
      delarray.splice(delarray.indexOf(_id), 1)
    }
    if (delarray.length != this.state.productList.length) {
      this.setState({ checked: false })
    }
    if (this) this.setState({ multipleDelete: delarray })
  }
  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { productList } = this.state
    if (this.state.selectAll) {
      productList.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      productList.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }

  handleChangeBrand = (value) => {
    let idsArray = [];
    this.state.searchBrandslist.map((each) => {
      value.map((each1) => {
        if (each.brandName === each1) {
          idsArray.push(each._id);
        }
      })
      this.setState({ selectedBrandS: idsArray, selectedBrand: value })
    })
  }
  handleChangeCategory = (value) => {
    let idsArray = [];
    this.state.searchCatlist.map((each) => {
      value.map((each1) => {
        if (each.categoryName === each1) {
          idsArray.push(each._id);
        }
      })
      this.setState({ searchCategoryS: idsArray, selectedCategory: value })
    })
  }

  handleChangeSupplier = (value) => {
    let idsArray = [];
    this.state.searchSupplierslist.map((each) => {
      value.map((each1) => {
        if (each.supplierName === each1) {
          idsArray.push(each._id);
        }
      })
      this.setState({ searchSupplierS: idsArray, selectedSupplier: value })
    })
  }
  // ######################### XXXX #########################
  testing = (e) => {
    if (e.key === 'Enter') {
      this.setState({sku_text:true,product_text:false},()=>this.productListing('skuText'))
    }
  }
  testing1 = (e) => {
    if (e.key === 'Enter') {
      this.setState({product_text:true,sku_text:false},()=>this.productListing('productText'))
    }
  }
  render() {
    let { total, productsAccess, pagesize, page, sku, prodName, catName, price, salePrice, quantity, published, productList, mainProductNames, mainCategoryNames, selectedProduct, brandName,
      supplierName, selectedBrand, listOfBrands, selectedSupplier, listOfSuppliers, length, publishedvalue,
      selectedCategory, selectedSku, skuText,productText, SKUNames, min_price, max_price, minSalePrice, maxSalePrice, minStockQuantity, maxStockQuantity, sortData } = this.state
    const Option = Select.Option;

    const filteredOptions1 = mainProductNames.map((each) => { return each.productName });
    const filteredOptions = filteredOptions1.filter(o => !selectedProduct.includes(o));

    const filteredOptions2i = SKUNames.map((each) => { return each.SKU });
    const filteredOptions2 = filteredOptions2i.filter(o => !selectedSku.includes(o));

    const filteredBrands1 = listOfBrands.map((each) => { return each.brandName });
    const filteredBrands = filteredBrands1.filter(o => !selectedBrand.includes(o));

    const filteredSupplier1 = listOfSuppliers.map((each) => { return each.supplierName });
    const filteredSupplier = filteredSupplier1.filter(o => !selectedSupplier.includes(o));

    const filteredCat1 = mainCategoryNames.map((each) => { return each.categoryName });
    const filteredCat = filteredCat1.filter(o => !selectedCategory.includes(o));
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Productos</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" > Catalogo de producto</li>
                <li className="breadcrumb-item active" > Productos</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
        {this.state.download ? <Loader /> : null}
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="row data-filter justify-content-between">
              {productList.length > 0 ?
                <div className="table-footer mt-0 col-md-6">
                  <label>Demostración</label>
                  <Select showSearch placeholder={<b> {total < 25 ? total : length}</b>} optionFilterProp="children"
                    onSelect={this.handleChangePageSize.bind(this)}
                    value={this.state.page_ ? pagesize : (length <= pagesize ? length : pagesize)}
                    onSearch={this.handleChange_} >
                    <Option value={25}>25</Option>
                    <Option value={50}>50</Option>
                    <Option value={75}>75</Option>
                    <Option value={100}>100</Option>
                  </Select>
                  <label>Fuera de  <b>{total}</b> productos</label>
                  {/* <div className="pagination-list">
                    <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                  </div> */}
                </div> : null}
              <div className="col-md-6 text-md-right">
                <div className="button-continer text-right">
                  {productsAccess.status == false && productsAccess.delete == false ? null :
                    <>
                      {productsAccess.status && productsAccess.delete ?
                        <Select
                          showSearch
                          placeholder={<b>Seleccionar</b>}
                          optionFilterProp="children"
                          onSelect={(value) => this.setState({ selectedOption: value })}
                          className="applyselect"
                        >
                          <Option value="Publish">Activar</Option>
                          <Option value="Un-publish">Anular publicación</Option>
                          <Option value="Delete">Eliminar</Option>
                        </Select> : <>{productsAccess.delete ?
                          <Select
                            showSearch
                            placeholder={<b>Seleccionar</b>}
                            optionFilterProp="children"
                            onSelect={(value) => this.setState({ selectedOption: value })}
                            className="applyselect"
                          >
                            <Option value="Delete">Eliminar</Option>
                          </Select> : <Select
                            showSearch
                            placeholder={<b>Seleccionar</b>}
                            optionFilterProp="children"
                            onSelect={(value) => this.setState({ selectedOption: value })}
                            className="applyselect"
                          >
                            <Option value="Publish">Activar</Option>
                            <Option value="Un-publish">Anular publicación</Option>
                          </Select>} </>}

                      <button type="button" className="btn btn-primary ml-0 apply_btn" onClick={this.applyfilter}>Aplicar</button></>}
                  {productsAccess.create ? <button type="button" className="btn btn-primary" onClick={() => this.props.history.push('/addProduct')}><i className="fa fa-plus" /> <span> Agregar producto</span></button> : null}
                  {productsAccess.download ? <div className="dropdown">
                    <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                      <span>Herramientas</span>
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadExcelFile", "totalList")}>Exportar a Excel</a>
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadCsvFile", "totalList")}>Exportar a CSV</a>
                    </div>
                  </div> : null}
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ sku: !this.state.sku })} checked={sku} /><span></span>SKU</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ prodName: !this.state.prodName })} checked={prodName} /><span></span>Nombre</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ catName: !this.state.catName })} checked={catName} /><span></span>Categoría</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ price: !this.state.price })} checked={price} /><span></span>Precio</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ salePrice: !this.state.salePrice })} checked={salePrice} /><span></span>Precio de venta</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ quantity: !this.state.quantity })} checked={quantity} /><span></span>Cantidad de stock</label></li>
              {productsAccess.status ? <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ published: !this.state.published })} checked={published} /><span></span>Publicado</label></li> : null}
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ brandName: !this.state.brandName })} checked={brandName} /><span></span>Nombre de la marca</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ supplierName: !this.state.supplierName })} checked={supplierName} /><span></span>Nombre del proveedor</label></li>

            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary  ml-2" type="button" onClick={this.reSetTableRows}>
              Reiniciar
            </button>
            <button className="nav-link pull-right btn btn-primary" type="button" onClick={() => this.setTableRows('rows')}>
              Guardar
            </button>


          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Category</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre de la categoría"
                    value={selectedCategory}
                    onChange={(value) => this.handleChangeCategory(value)}
                    onSearch={(e) => this.getcategoryName(e)}
                    onFocus={(e) => this.getcategoryName(e)}
                    style={{ width: '100%' }}
                  >
                    {filteredCat.map((item, key) => (
                      <Select.Option key={key} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>SKU</label>
                  {/* <Select
                    mode="multiple"
                    placeholder="Introduce SKU"
                    value={selectedSku}
                    onChange={(selectedSku) => this.setState({ selectedSku })}
                    onSearch={(e) => this.getIds(e, 'SKU')}
                    onFocus={(e) => this.getIds(e, 'SKU')}
                    style={{ width: '100%' }}
                  >
                    {filteredOptions2.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select> */}
                  <input type="text" className="form-control" placeholder="Introduce SKU" onChange={this.handleChange} name='skuText' value={skuText} onKeyDown={this.testing} />
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Producto</label>
                  <input type="text" className="form-control" placeholder="Ingrese el nombre del producto" onChange={this.handleChange} name='productText' value={productText} onKeyDown={this.testing1} />
                  {/* <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre del producto"
                    value={selectedProduct}
                    onChange={(selectedProduct) => this.setState({ selectedProduct })}
                    onSearch={(e) => this.getIds(e, 'productName')}
                    onFocus={(e) => this.getIds(e, 'productName')}

                    style={{ width: '100%' }}
                  >
                    {filteredOptions.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select> */}
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Precio</label>
                  <div className="row">
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="min_price" placeholder="Min Price" value={min_price} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="max_price" placeholder="Max Price" value={max_price} onChange={this.handleChange} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Precio de venta</label>
                  <div className="row">
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="minSalePrice" placeholder="Min Price" value={minSalePrice} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="maxSalePrice" placeholder="Max Price" value={maxSalePrice} onChange={this.handleChange} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Cantidad</label>
                  <div className="row">
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="minStockQuantity" placeholder="Min Quantitiy" value={minStockQuantity} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-6">
                      <input className="form-control" type="number" name="maxStockQuantity" placeholder="Max Quantitiy" value={maxStockQuantity} onChange={this.handleChange} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre de la marca</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre de la marca"
                    value={selectedBrand}
                    onChange={(value) => this.handleChangeBrand(value)}
                    onSearch={(e) => this.getIds(e, 'brandName')}
                    onFocus={(e) => this.getIds(e, 'brandName')}
                    style={{ width: '100%' }}
                  >
                    {filteredBrands.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre del proveedor :</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre del proveedor"
                    value={selectedSupplier}
                    onChange={(value) => this.handleChangeSupplier(value)}
                    onSearch={(e) => this.getIds(e, 'supplierName')}
                    onFocus={(e) => this.getIds(e, 'supplierName')}
                    style={{ width: '100%' }}
                  >
                    {filteredSupplier.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Estado</label>
                  <select className="form-control" onChange={(e) => this.setState({ publishedvalue: e.target.value })} value={publishedvalue}>
                    <option value=''>Seleccionar</option>
                    <option value='published'>Publicado</option>
                    <option value='unPublished'>Sin Activar</option>
                  </select>
                </div>
              </div>

            </div>
            <hr />
            <div className="pull-right filter-button">
              {productsAccess.download ?
                <div className="dropdown ml-2">
                  <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                    <span>Herramientas</span>
                  </button>
                  <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                    <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadExcelFile", "filteredList")}>Exportar a Excel</a>
                    <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadCsvFile", "filteredList")}>Exportar a CSV</a>
                  </div>
                </div> : null}
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.productListing('filter')} > Aplicar filtro </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={() => this.resetListing()} > Reiniciar </button>
            </div>

          </div>
          <div className="card-body">
            <div className="animated fadeIn">
              <div className="table-responsive">
                <table className="table dataTable with-image row-border hover custom-table" >
                  <thead>
                    <tr>
                      {productsAccess.status == false && productsAccess.delete == false ? null :
                        <th width="5%">
                          <div className="checkbox">
                            <label>
                              <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} /><span></span>
                              <i className="input-helper" />
                            </label>
                          </div>
                        </th>}
                      {this.state.sku ? <th sortable-column="SKU" onClick={this.onSort.bind(this, 'SKU')}>SKU <i aria-hidden='true' className={(sortData['SKU']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                      <th>Imagen </th>
                      {this.state.prodName ? <th sortable-column="productName" onClick={this.onSort.bind(this, 'productName')}>Nombre <i aria-hidden='true' className={(sortData['productName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                      {this.state.catName ? <th sortable-column="categoryName" onClick={this.onSort.bind(this, 'categoryName')}>Categoria <i aria-hidden='true' className={(sortData['categoryName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                      {brandName ? <th sortable-column="brandName" onClick={this.onSort.bind(this, 'brandName')}>Nombre de la marca <i aria-hidden='true' className={(sortData['brandName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                      {supplierName ? <th sortable-column="supplierName" onClick={this.onSort.bind(this, 'supplierName')}>Nombre del proveedor <i aria-hidden='true' className={(sortData['supplierName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                      {this.state.price ? <th sortable-column="price" onClick={this.onSort.bind(this, 'price')}>Precio <i aria-hidden='true' className={(sortData['price']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                      {this.state.salePrice ? <th sortable-column="salePrice" onClick={this.onSort.bind(this, 'salePrice')}>Precio de venta <i aria-hidden='true' className={(sortData['salePrice']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                      {this.state.quantity ? <th sortable-column="stockQuantity" onClick={this.onSort.bind(this, 'stockQuantity')}>Cantidad avl <i aria-hidden='true' className={(sortData['stockQuantity']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                      {this.state.published && productsAccess.status ? <th sortable-column="publish" onClick={this.onSort.bind(this, 'publish')}>Publicado <i aria-hidden='true' className={(sortData['publish']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                      {productsAccess.edit == false && productsAccess.delete == false ? null : <th width="10%"> Comportamiento</th>}
                    </tr>
                  </thead>
                  {productList.length === 0 && this.state.loading === false ? <tbody><tr><td>No se encontraron productos</td></tr></tbody> :
                    <tbody>
                      {productList.map((each, id) => {
                        let prodImage = (each.productImage) ? (IMAGE_URL + each.productImage) : "../assets/images/no-imagefound.jpg"
                        return (
                          <tr className="animated fadeIn" key={id}>
                            {productsAccess.status == false && productsAccess.delete == false ? null :
                              <td>
                                <div className="checkbox">
                                  <label>
                                    <input type='checkbox' className='form-check-input' checked={this.checkArray(each._id)} onChange={() => this.onCheckbox(each._id)} /><span></span>
                                    <i className="input-helper" />
                                  </label>
                                </div>
                              </td>}
                            {this.state.sku ? <td>{each.SKU}</td> : null}
                            <td >
                              <div className="thumb-img">
                                <img src={prodImage} alt={each.prodName} />
                              </div>
                            </td>
                            {this.state.prodName ? <td className="text-truncate" style={{ 'maxWidth': 250 }}>{each.productName}{each.prodName}</td> : null}
                            {this.state.catName ? <td>{each.categoryName[0]} <br></br>{each.categoryName[1] ? <>-{each.categoryName[1]} </> : null} <br></br>{each.categoryName[2] ? <> --{each.categoryName[2]} </> : null}</td> : null}
                            {brandName ? <td>{each.brandName}</td> : null}
                            {supplierName ? <td>{each.supplierName}</td> : null}
                            {this.state.price ? <td>${each.price ? each.price.toFixed(2) : "-"}</td> : null}
                            {this.state.salePrice ? <td>${each.salePrice ? each.salePrice.toFixed(2) : "-"}</td> : null}
                            {this.state.quantity ? <td>{each.stockQuantity}</td> : null}
                            {this.state.published && productsAccess.status ? <td>
                              <Toggle checked={each.publish} className='custom-classname' onChange={() => this.productPublish(each.publish, each._id)} /></td> : null}
                            {productsAccess.edit == false && productsAccess.delete == false ? null :
                              <td>
                                {productsAccess.edit ? <button onClick={() => this.editProduct(each._id)} data-toggle="tooltip" title="Edit"><i className="fa fa-pencil-square-o text-primary" aria-hidden="true"></i></button> : null}
                                {productsAccess.delete ? <button onClick={() => this.deleteProduct(each._id)} data-toggle="tooltip" title="Delete">< i className="fa fa-trash text-danger" aria-hidden="true" ></i></button> : null}
                              </td>}
                          </tr>
                        )
                      })}
                    </tbody>}

                </table>
              </div>
            </div>
            {productList.length > 0 ?
              <div className="table-footer d-flex justify-content-between">
                <div className="table-shorting">
                  <label>Demostración</label>
                  <Select showSearch placeholder={<b> {total < 25 ? total : length}</b>} optionFilterProp="children"
                    onSelect={this.handleChangePageSize.bind(this)}
                    value={this.state.page_ ? pagesize : (length <= pagesize ? length : pagesize)}
                    onSearch={this.handleChange_}>
                    <Option value={25}>25</Option>
                    <Option value={50}>50</Option>
                    <Option value={75}>75</Option>
                    <Option value={100}>100</Option>
                  </Select>
                  <label>Fuera de  <b>{total}</b> productos</label>
                </div>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} />
                </div>
              </div> : null}
          </div>
        </div>


      </Home >
    );
  }
}
const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,

});


export default connect(mapStateToProps, actions)(ProductManagement);
