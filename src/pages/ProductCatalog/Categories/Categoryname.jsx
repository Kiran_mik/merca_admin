import React, { PureComponent } from 'react'
import Home from '../../Home'
import Toggle from 'react-toggle'
import axios from 'axios'
import { API_URL, IMAGE_URL } from '../../../config/configs'
import { connect } from 'react-redux'
import * as actions from '../../../actions'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import SimpleReactValidator from 'simple-react-validator'
import swal from 'sweetalert'
import { toast } from 'react-toastify';
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";


class EditCategory extends PureComponent {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      categoryName: '',
      customUrl: '',
      publish: true,
      categoryId: '',
      cat_id: '',
      image: '',
      icon: '',
      description: '',
      metaTitle: '',
      metaKeyword: '',
      metaDescription: '',
      fileVisible: false,
      fileVisible1: false,
      errors: {
        categoryName: '',
        customUrl: ''
      },
      src: null,
      crop: {
        x: 10,
        y: 10,
        unit: 'px',
        width: 400,
        height: 275,

      }
    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission && permissions.rolePermission.productCategoryAccess && permissions.rolePermission.productCategoryAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }
    var id = this.props.match.params.cId;
    this.setState({ cat_id: id })
    if (id) {
      var body = { categoryId: id }
      let token = localStorage.getItem('token')
      var url = '/categories/getCategoriesDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let data = response.data.data
        if (data) {
          this.setState({
            categoryName: data.categoryName,
            customUrl: data.customUrl,
            publish: data.publish,
            categoryId: data._id,
            image: data.image,
            icon: data.icon,
            description: data.description,
            metaTitle: data.metaTitle,
            metaKeyword: data.metaKeyword,
            metaDescription: data.metaDescription,
          })
        }

      })
    }

  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }

  }
  validateForm = () => {
    let { categoryName, customUrl, errors } = this.state
    let formIsValid = true
    if (!categoryName) {
      formIsValid = false
      errors['categoryName'] = '* Category name is required'
    }
    if (!customUrl) {
      formIsValid = false
      errors['customUrl'] = '* Custom URL is required'
    }
    this.setState({ errors })
    return formIsValid
  }
  urlGenerator = () => {
    let { categoryName, categoryId } = this.state
    let data = {}
    if (categoryId === '') {
      data.categoryName = categoryName
    }
    if (categoryId !== '') {
      data.category = categoryName
      data.categoryId = categoryId
    }
    var body = data
    var token = localStorage.getItem('token')
    var url = '/categories/addCategory'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var { data } = response
      if (data.statusCode === 441) {
        toast.error("Name already exists !", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
      }
      else {
        if (categoryId !== '') {
          this.setState({
            customUrl: data.data.customUrl,
          })
        } else
          this.setState({
            categoryId: data.data._id,
            customUrl: data.data.customUrl,
            errors: {}
          })
      }
    })
  }


  // #################################### Add Category ################################

  addCategory = () => {
    if (this.validateForm()) {
      let token = localStorage.getItem('token');
      var { categoryName, cat_id, customUrl, publish, categoryId, image, icon, description, metaTitle, metaKeyword, metaDescription } = this.state
      var body = { categoryName, customUrl, publish, categoryId, image, icon, description, metaTitle, metaKeyword, metaDescription }
      var url = '/categories/addCategory'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          if (cat_id !== undefined) {
            var title = "Category Updated Successfully !"
          } else {
            var title = "Category Added Successfully !"
          }
          swal({ title: title, icon: "success", })
            .then((willDelete) => {
              if (willDelete) {
                this.props.history.push('/categorylist')
              }
            })
        }

      })
    }
  }



  // #################################  Image&Icon Preview #################################

  fileChangedHandlerImg(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file: file,
        filename: filename,
        imagePreviewUrl: reader.result,
        fileVisible: true
      })
    }
    reader.readAsDataURL(file)
  }


  fileChangedHandlerIcon(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file1: file,
        filename1: filename,
        iconPreviewUrl: reader.result,
        fileVisible1: true
      })
    }
    reader.readAsDataURL(file)
  }


  b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
  // #################################  #################################
  metaFileUpload = (name) => {
    var ImageURL = this.state.b64Img;
    var block = ImageURL.split(";");
    var contentType = block[0].split(":")[1];// In this case "image/gif"
    var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."
    var blob = this.b64toBlob(realData, contentType);
    if (name) {
      var { file, file1, categoryId } = this.state
      this.setState({ file })
      var token = localStorage.getItem('token')
      var formData = new FormData()
      formData.append('imageType', [name])
      formData.append('categoryId', categoryId)
      if (name === "image") {
        formData.append('file', blob)
      }
      else if (name === "icon") {
        formData.append('file', file1)
      }
      else {
        formData.append('file', file)
      }
      axios
        .post(API_URL + '/auth/fileUploadForCategoryImage', formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: token
          }
        })
        .then(response => {
          let { data } = response.data
          if (data) {
            if (name === "image") {
              this.setState({ image: data.filepath, fileVisible: false, file: '', src: null, croppedImageUrl: null })
            }
            else if (name === "icon") {
              this.setState({ icon: data.filepath, fileVisible1: false, file1: '' })
            }
            else {
              console.log('image not found');
            }
          }
        })
        .catch(err => console.log(err))
    }
  }
  onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () =>
        this.setState({ src: reader.result })
      );
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  onImageLoaded = image => {
    this.imageRef = image;
  };

  onCropComplete = crop => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        "newFile.jpeg"
      );
      this.setState({ croppedImageUrl });
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );


    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = canvas.toDataURL('image/jpeg');
        var item_image = this.fileUrl.replace(/^data:image\/(png|jpg);base64,/, "");
        this.setState({ b64Img: item_image })
        resolve(this.fileUrl);
      }, "image/jpeg");
    });
  }


  render() {
    var { crop, croppedImageUrl, src, categoryName, errors, cat_id, customUrl, publish, image, icon, description, metaTitle, metaKeyword, metaDescription } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>{cat_id !== undefined ? 'Edit Category' : 'Añadir categoría'}</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Home</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/categorylist')}>Product Catalog</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/categorylist')}>Categories</li>
                <li className='breadcrumb-item active'>{cat_id !== undefined ? 'Edit Category' : 'Añadir categoría'}</li>
              </ul>
            </div>
          </div>
        </div>

        <div className='card fadeIn'>
          <div className="card-header" data-spy="affix" data-offset-top="100">
            <div className="button-continer text-right">
              <button type='button' className='btn btn-primary' onClick={this.addCategory}> <span>{cat_id !== undefined ? 'Save' : 'Add'}</span> </button>
              <button type='button' className='btn btn-outline-primary' onClick={() => this.props.history.push('/categorylist')} > Cancel </button>
            </div>
          </div>
          <div className='card-body'>
            <form className='form-sample'>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3  col-form-label'>
                  Name <span className='text-danger'>*</span> :
                  </label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='categoryName'
                    placeholder='Ingrese el nombre de la categoría'
                    value={categoryName}
                    onChange={this.handleChange}
                    onBlur={this.urlGenerator}
                  />
                  <span className="error-block"> {errors.categoryName} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Custom URL <span className='text-danger'>*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='customUrl'
                    placeholder='Enter Custom URL'
                    value={customUrl}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.customUrl} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>
                  Image :
                  </label>
                <div >
                  <div className="form-group inputDnD mb-0">
                    <div className="upload-overlay d-flex justify-content-center align-items-center">
                      <div className="upload-wrapper">
                        <i className="fa fa-upload"></i>
                        <span><button type="button"  >Choose a file</button> or drag it here</span>
                      </div>
                    </div>
                    <label className="sr-only" >File Upload</label>
                    <input type="file" className="form-control-file text-primary font-weight-bold" name='image' id="file" accept="image/*"
                      data-title="Drag and drop a file" onChange={this.onSelectFile} />
                    <span className="notes">'400*275' image size is preferable</span>
                  </div>

                  <div className="App">
                    <div >
                      {src && (
                        <ReactCrop
                          className='reactcrop'
                          src={src}
                          crop={crop}
                          minHeight={100}
                          maxHeight={275}
                          minWidth={100}
                          maxWidth={400}
                          locked={true}
                          onImageLoaded={this.onImageLoaded}
                          onComplete={this.onCropComplete}
                          onChange={this.onCropChange}
                        />

                      )}
                      <div className="croped-img">
                        {croppedImageUrl && (
                          <img alt="Crop" style={{ maxWidth: "100%" }} src={croppedImageUrl} />
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="button-continer pull-right my-2">
                    <button className="btn btn-primary" type="button" onClick={this.metaFileUpload.bind(this, "image")}> Upload Image</button>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="row justify-content-between">
                    <div className="col-md-12 select-image"><div className="product-img">
                      {image ? <img src={IMAGE_URL + image + "?" + Math.random()} alt={image} /> : <img src="/assets/images/no-image-icon-4.png" alt="no-image-found" />}
                    </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>
                  Icon :
                  </label>
                <div className="drop-section mb-3 col-md-4">
                  <div className="form-group inputDnD mb-0">
                    <div className="upload-overlay d-flex justify-content-center align-items-center">
                      <div className="upload-wrapper">
                        <i className="fa fa-upload"></i>
                        <span><button type="button"  >Choose a file</button> or drag it here</span>
                      </div>
                    </div>
                    <label className="sr-only" >File Upload</label>
                    <input type="file" className="form-control-file text-primary font-weight-bold" name='image' id="file" accept="image/*"
                      data-title="Drag and drop a file" onChange={this.fileChangedHandlerIcon.bind(this)} />
                  </div>
                  {this.state.fileVisible1 ?
                    <span>
                      <div className='product-img'> <img src={this.state.iconPreviewUrl} alt={this.state.filename1} /><span> <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ fileVisible1: !this.state.fileVisible1, file1: '' })} /></span> </div>
                      <span>{this.state.filename1}</span>
                    </span> : null}
                  <div className="button-continer pull-right my-2">
                    <button className="btn btn-primary" type="button" onClick={this.metaFileUpload.bind(this, "icon")}> Upload Icon</button>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="row justify-content-between my-2">
                    <div className="col-md-12 select-image"><div className="product-img">
                      {icon ? <img src={IMAGE_URL + icon + "?" + Math.random()} alt={icon} /> : <img src="/assets/images/no-image-icon-4.png" alt={icon} />}</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>
                  Description :
                </label>
                <div className='col-md-8 co-sm-7'>
                  <CKEditor
                    editor={ClassicEditor}
                    data={description}
                    onChange={(event, editor) => {
                      const data = editor.getData()
                      this.setState({ description: data })
                    }}
                  />

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'> Meta Title : </label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='metaTitle'
                    placeholder='Enter Title'
                    value={metaTitle}
                    onChange={this.handleChange}
                  />

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'> Meta Keyword : </label>
                <div className='col-md-8 co-sm-7'>
                  <textarea
                    className='form-control'
                    type='text'
                    name='metaKeyword'
                    placeholder='Enter Meta Keyword'
                    value={metaKeyword}
                    onChange={this.handleChange}
                  />

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'> Meta Description : </label>
                <div className='col-md-8 co-sm-7'>
                  <textarea
                    className='form-control'
                    type='text'
                    name='metaDescription'
                    placeholder='Enter Meta Keyword'
                    value={metaDescription}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label' htmlFor='material-switch' > Published : </label>
                <div className='col-md-8 co-sm-7'>
                  <Toggle
                    checked={publish}
                    className='custom-classname'
                    onChange={() => this.setState({ publish: !this.state.publish })}
                  />
                </div>
              </div>
            </form>
          </div>
        </div>
      </Home>
    )
  }
}


const mapStateToProps = state => ({
  Categorydetails: state.commonReducers.editedDetails,
  permissionsList: state.admindata.rolePermissions,

});

export default connect(mapStateToProps, actions)(EditCategory)


