import React, { Component } from 'react'
import Home from '../../Home'
import axios from 'axios'
import { connect } from 'react-redux'
import Toggle from 'react-toggle'
import { IMAGE_URL, CSV_URL, EXCEL_URL } from '../../../config/configs'
import swal from 'sweetalert'
import { Select } from 'antd';
import 'antd/dist/antd.css';
import * as actions from '../../../actions'
import $ from 'jquery';
import { isEmpty } from "lodash"
import 'rc-pagination/assets/index.css';
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import Pagination from 'rc-pagination';
import { BeatLoader } from 'react-spinners';
import Loader from '../../Loader'
window.jQuery = $;
window.$ = $;
global.jQuery = $;

const FileDownload = require('js-file-download')
class CategoryList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      categorieslist: [],
      page: 1,
      pagesize: 10,
      selectAll: false,
      total: null,
      length: undefined,
      view: "Lista",
      changeButton: "Vista de la lista",
      multipleDelete: [],
      selectedOption: 'Selecciona aquí',
      image: true,
      name: true,
      customUrl: true,
      categoryId: true,
      publish: true,
      sidebar: true,
      scrollbar: true,
      selectedCatagory: [],
      mainCatagoryNames: [],
      publishedvalue: '',
      sortData: { categoryName: false, publish: false, categoryId: false, sideView: false, frontView: false, position: true },
      sort: {position:1},
      productCategoryAccess: {},
      loading: true,
      page_: false,
      download:false
    }
  }
  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission && permissions.rolePermission.productCategoryAccess && permissions.rolePermission.productCategoryAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }

    if (permissions.rolePermission) {
      let { productCategoryAccess } = permissions.rolePermission
      this.setState({ productCategoryAccess: productCategoryAccess })
    }
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });
    this.getAllCategories('pagesize')
  }

  // ########################### PAGINATION #########################

  // paginationChange(page, pagesize) {
  //   this.setState({
  //     page: page,
  //     pagesize: pagesize,
  //   }, () => this.getAllCategories());
  //   this.setState({ selectAll: false }, () => this.setTableRows(), () => this.selectAllcheck())
  // }

  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize,
    }, () => this.getAllCategories());
    this.setState({ selectAll: false }, () => this.selectAllcheck())
  }

  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 }, () => this.setTableRows(), () => this.getAllCategories());
  }

  handleChange_ = (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1, page_: true }, () => this.setTableRows(), () => this.getAllCategories());
    }
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.getAllCategories());
    // }
  }
  // #################################### Get Allcatagories ################################
  getAllCategories = (e) => {
    var { page, pagesize, selectedCatagory, publishedvalue, sort } = this.state
    var url = '/categories/categoryListing'
    var method = 'post'
    var data = {}
    if (e === 'filter') {
      data.page = 1
    } else if (e === 'pagesize') {
      data = data
    } else {
      data.page = page
    }
    if (e === 'pagesize') {
      data = data
    } else {
      data.pagesize = pagesize
    }
    if (selectedCatagory != '') {
      data.categoryName = selectedCatagory
    }
    if (publishedvalue != '') {
      data.publish = publishedvalue
    }
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      let data1 = data.data
      if (data.status === 0) {
        this.setState({ categorieslist: [] })
      } else {
        this.setState({ categorieslist: data.data.categorylisting, total: data.data.total, loading: false, length: data.data.categorylisting.length, })
        if (data.data && data.data.categorylisting.length <= pagesize) {
          this.setState({ page_: false })
        }
        if (data1.manageCategoryListing) {
          this.setState({
            image: data.data.manageCategoryListing.image,
            sidebar: data.data.manageCategoryListing.sideView,
            scrollbar: data.data.manageCategoryListing.frontView,
            name: data.data.manageCategoryListing.categoryName,
            categoryId: data.data.manageCategoryListing.categoryId,
            customUrl: data.data.manageCategoryListing.customUrl,
            publish: data.data.manageCategoryListing.publish,
            pagesize: data.data.manageCategoryListing.pageSize
          })
        }
      }
      if (e == 'filter') {
        this.setState({ page: 1 })
      }
    }
    )
  }
  // ############################## Reset Listing ######################
  resetListing = (e) => {
    var url = '/categories/categoryListing'
    var method = 'post'
    var { page, pagesize } = this.state
    var body = { page: page, pagesize: pagesize }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data.status === 0) {
        this.setState({ categorieslist: [] })
      } else {
        this.setState({ categorieslist: data.data.categorylisting, selectedCatagory: [], publishedvalue: '', total: data.data.total })
      }
    }
    )
  }

  // ########################### Catagory Status #########################
  changeCatStatus(status, Id, type) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { categoryId: [Id], [type]: status }
    if (type === 'publish') {
      body.isPublish = true
    } else if (type === 'sideView') {
      body.isSideView = true
    } else if (type === 'frontView') {
      body.isFrontView = true
    }
    var urlkey = '/categories/changeCategoryStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        if (status == false) {
          swal('Desactivado con éxito!', '', 'success')
        }
        else {
          swal('Publicado con éxito!', '', 'success')
        }
        this.getAllCategories()
      } else if (data.message === 'Invalid token') {
        this.props.history.push('/')
      } else {
        swal(data.message, '', 'error')
      }
    })
  }



  checkArray(_id) {
    let { multipleDelete } = this.state
    if (multipleDelete.includes(_id)) {
      return true
    } else {
      return false
    }
  }

  onCheckbox(_id, val) {
    var delarray = this.state.multipleDelete
    if (!delarray.includes(_id)) {
      delarray.push(_id)
    } else {
      delarray.splice(delarray.indexOf(_id), 1)
    }
    if (delarray.length != this.state.categorieslist.length) {
      this.setState({ checked: false })
    }
    if (this) this.setState({ multipleDelete: delarray })
  }


  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { categorieslist } = this.state
    if (this.state.selectAll) {
      categorieslist.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      categorieslist.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }


  applyfilter = () => {
    var delArr = this.state.multipleDelete
    if (this.state.selectedOption == 'Delete') {
      if (delArr.length > 0) {
        this.deleteCategory(...delArr)
      }
    }
    if (this.state.selectedOption == 'Publish') {
      if (delArr.length > 0) {
        var body = { categoryId: delArr, publish: true ,isPublish:true}
      }
    }
    if (this.state.selectedOption == 'Un-publish') {
      if (delArr.length > 0) {
        var body = { categoryId: delArr, publish: false,isPublish:true }
      }
    }
    var url = '/categories/changeCategoryStatus'
    var method = 'post'
    var token = localStorage.getItem('token')
    if (delArr.length > 0) {
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status == 1) {
          if (this.state.selectedOption == 'Un-publish') {
            swal('Categorías no publicadas con éxito.', '', 'success')
          }
          else {
            swal('Categorías publicadas con éxito!', '', 'success')
          }
          this.getAllCategories()
        }
        this.setState({ multipleDelete: [], selectedOption: 'Selecciona aquí' })
      })
    } else {
      this.setState({ selectedOption: 'Selecciona aquí' })
      swal('Seleccione al menos una categoría', '', 'info')
    }
  }





  // #################################### Delete Category ################################
  deleteCategory = (cId) => {
    var delArr = this.state.multipleDelete
    swal({ title: "Are you sure?", icon: "warning", buttons: true, dangerMode: true, }).then((willDelete) => {
      if (willDelete) {
        if (delArr.length > 0) {
          var body = { categoryId: delArr }
        } else {
          var body = { categoryId: [cId] }
        }
        var token = localStorage.getItem('token')
        var url = '/categories/deleteCategories'
        var method = 'post'
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          this.setState({ selectedOption: 'Selecciona aquí' })
          this.getAllCategories()
        })
        swal('Borrado exitosamente', '', 'success')
      }
    });
  }

  // #################################### Update Category ################################

  updateCategory = (cId) => {
    this.props.history.push(`/editcategory/${cId}`)

  }

  // #################################### Sub Category List ################################

  subcategorylist = (scId, scName) => {
    var body = { reviewId: scId, catName: scName }
    this.props.editDetails(body)
    this.props.history.push(`/subcategorylist/${scId}`)
  }

  // ########################### Set Table Rows #########################

  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { name, categoryId, image, sidebar, scrollbar, customUrl, publish, pagesize } = this.state
    var body = { image: true, categoryName: name, customUrl, sideView: sidebar, frontView: scrollbar, publish, categoryId, pageSize: pagesize }
    var url = '/categories/setFilterForCategory'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      if (response) {
        this.getAllCategories()
        if (e) {
          swal('Filas de tabla ¡Actualizadas con éxito!', '', 'success')
        }
      }
    })
  }




  changeGrid = () => {
    this.setState({ view: 'Lista', changeButton: 'Vista en cuadrícula' })
  }

  changeList = () => {
    this.setState({ view: 'Cuadrícula', changeButton: 'Vista de la lista' })
  }



  // ########################### getProductName for Search #########################
  getCatNames = e => {
    let token = localStorage.getItem('token')
    var body = { type: "category", searchText: e }
    var url = '/categories/categoryList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      var mainCatagoryNames = []
      data.map((each, key) => {
        mainCatagoryNames.push(each.categoryName)
        this.setState({ mainCatagoryNames })
      })
    })
  }

  handleChange1 = selectedCatagory => {
    this.setState({ selectedCatagory });
  };

  handleChange2 = (e) => {
    this.setState({ publishedvalue: e.target.value })
  }

  //*********** SORTING ************************//
  onSort = (column) => {
    let { sortData } = this.state;
    var element, value;
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key];
        element = key;
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState({
          sort: { [element]: value }
        }, () => {
          this.getAllCategories();
        });
        this.setState({ sortData });
      }
      else {
        sortData[key] = false
        element = key;
        value = 1;
      }
    }
    this.setState({ sortData });
  }



  // ########################### download CSV #########################

  downloadCSV(type, array) {
    let { name, customUrl, publish, sort, categoryId, selectedCatagory, scrollbar, sidebar, publishedvalue, multipleDelete } = this.state
    let token = localStorage.getItem('token')
    this.setState({download:false})
    if (array === "totalList") {
      var data = { filteredFields: ["image", "categoryName", "publish", "customUrl", "frontView", "sideView", "position", "color"] }
    }
    if (array === "filteredList") {
      var data = { filteredFields: ['image'] }
      if (name) {
        data.filteredFields.push('categoryName')
      };
      if (categoryId) {
        data.filteredFields.push('categoryId')
      };
      if (customUrl) {
        data.filteredFields.push('customUrl')
      };
      if (publish) {
        data.filteredFields.push('publish')
      };
      if (scrollbar) {
        data.filteredFields.push('frontView')
      };
      if (sidebar) {
        data.filteredFields.push('sideView')
      };
      if (selectedCatagory != '') {
        data.categoryName = selectedCatagory
      }
      if (publishedvalue != '') {
        data.publish = publishedvalue
      }
      if (multipleDelete.length > 0) {
        data.categoryArray = multipleDelete
      }
      if (!isEmpty(sort)) {
        data.sort = sort
      }
    }
    var body = data
    var url = '/categories/' + type
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (data) {
        this.setState({ multipleDelete: [], download: false })
        if (type === 'downloadCategoriesCsvFile') {
          window.open(CSV_URL + data.filePathAndName, '_blank');
        } else {
          window.open(EXCEL_URL + data.filePathAndName, '_blank');
        }
      }
    })
  }


  render() {
    let { categorieslist, productCategoryAccess, length, sidebar, scrollbar, selectedOption, categoryId, page, pagesize, total, image, customUrl, publish, name, mainCatagoryNames, selectedCatagory, publishedvalue, sortData } = this.state
    const Option = Select.Option;
    const filteredOptions = mainCatagoryNames.filter(o => !selectedCatagory.includes(o));
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Categorías</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" >Catálogo de productos</li>
                <li className="breadcrumb-item active">Categorías</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
        {this.state.download ? <Loader /> : null}
        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="row data-filter justify-content-between">
              {categorieslist.length > 0 ?
                <div className="table-footer mt-0 col-md-6">
                  <label>Demostración</label>
                  <Select showSearch placeholder={<b> {total <= 5 ? total : length}</b>} optionFilterProp="children"
                    onSelect={this.handleChangePageSize.bind(this)}
                    value={this.state.page_ ? pagesize : (length <= pagesize ? length : pagesize)}
                    onSearch={this.handleChange_} >
                    <Option value={5}>5</Option>
                    <Option value={10}>10</Option>
                    <Option value={15}>15</Option>
                    <Option value={50}>50</Option>
                  </Select>
                  <label>Fuera de las{total} categorías</label>
                  <div className="pagination-list">
                    <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                  </div>
                </div> : null}
              {/* <div className="col-md-12 text-md-right"> */}
              <div className="col-md-6 text-md-right">
                <div className="button-continer text-right">
                  {productCategoryAccess.status == false && productCategoryAccess.delete == false ? null :
                    <>
                      {productCategoryAccess.status && productCategoryAccess.delete ?
                        <Select
                          showSearch
                          placeholder={<b>Seleccionar</b>}
                          optionFilterProp="children"
                          value={selectedOption}
                          onSelect={(value) => this.setState({ selectedOption: value })}
                          className="applyselect"
                        >
                          <Option value="Publish">Activar</Option>
                          <Option value="Un-publish">Desactivar</Option>
                          <Option value="Delete" >Eliminar</Option>
                        </Select> : <>{productCategoryAccess.delete ?
                          <Select
                            showSearch
                            placeholder={<b>Seleccionar</b>}
                            optionFilterProp="children"
                            value={selectedOption}
                            onSelect={(value) => this.setState({ selectedOption: value })}
                            className="applyselect"
                          >
                            <Option value="Delete" >Eliminar</Option>
                          </Select> : <Select
                            showSearch
                            placeholder={<b>Seleccionar</b>}
                            optionFilterProp="children"
                            value={selectedOption}
                            onSelect={(value) => this.setState({ selectedOption: value })}
                            className="applyselect"
                          >
                            <Option value="Publish">Activar</Option>
                            <Option value="Un-publish">Desactivar</Option>
                          </Select>} </>}

                      <button type="button" className="btn btn-primary ml-0 apply_btn" onClick={this.applyfilter}>Aplicar</button></>}
                  {productCategoryAccess.create ?
                    <button type="button" className="btn btn-primary" onClick={() => this.props.history.push('/addcategory')}>
                      <i className="fa fa-plus" /> Añadir categoría</button> : null}
                  {productCategoryAccess.download ? <div className="dropdown">
                    <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                      <span>Herramientas</span>
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadCategoriesExcelFile", "totalList")}>Exportar a Excel</a>
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadCategoriesCsvFile", "totalList")}>Exportar a CSV</a>
                    </div>
                  </div> : null}
                  {this.state.view === "Cuadrícula" ?

                    <button type="button" className="btn btn-primary" onClick={this.changeGrid} > <i className="fa fa-bars"></i> <span>Lista</span> </button> :
                    <button type="button" className="btn btn-primary" onClick={this.changeList} ><i className="fa fa-th-large"></i><span>Cuadrícula</span> </button>
                  }
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ name: !this.state.name })} checked={name} /><span></span>Nombre</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ customUrl: !this.state.customUrl })} checked={customUrl} /><span></span>URL personalizada</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ categoryId: !this.state.categoryId })} checked={categoryId} /><span></span>Categoria ID</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ sidebar: !this.state.sidebar })} checked={sidebar} /><span></span>Barra lateral</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ scrollbar: !this.state.scrollbar })} checked={scrollbar} /><span></span>Barra de desplazamiento</label></li>

              {productCategoryAccess.status ? <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ publish: !this.state.publish })} checked={publish} /><span></span>Activar</label></li> : null}
            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.getAllCategories} > Reiniciar </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({ name: true, customUrl: true, publish: true, categoryId: true })} > Seleccionar todo </button>
            <button className="nav-link pull-right btn btn-primary" type="button" onClick={() => this.setTableRows('rows')}> Guardar </button>

          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre de la categoría"
                    value={selectedCatagory}
                    onChange={this.handleChange1}
                    onSearch={(e) => this.getCatNames(e)}
                    onFocus={(e) => this.getCatNames(e)}

                    style={{ width: '100%' }}
                  >
                    {filteredOptions.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Publicado:</label>
                  <select className="form-control" onChange={this.handleChange2} value={publishedvalue}>
                    <option value="">Selecciona aquí</option>
                    <option value="published">Publicado</option>
                    <option value="unPublished">Desactivado</option>
                  </select>
                </div>
              </div>
            </div>
            <hr />
            <div className="pull-right filter-button">
              {productCategoryAccess.download ?
                <div className="dropdown ml-2">
                  <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                    <span>Herramientas</span>
                  </button>
                  <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                    <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadCategoriesExcelFile", "filteredList")}>Exportar a Excel</a>
                    <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadCategoriesCsvFile", "filteredList")}>Exportar a CSV</a>
                  </div>
                </div> : null}
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.getAllCategories('filter')} > Aplicar filtro </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={() => this.resetListing()} > Reiniciar </button>
            </div>
          </div>
        

        {/* Data table start */} 
        {categorieslist.length === 0 && this.state.loading === false ? <div className="text-center p-2">No se encontraron registros</div> :
          <div>

            {this.state.view === "Cuadrícula" ?
              <div className="row product-list mt-3">
                {categorieslist.map((each, id) => {
                  let image = (each.image) ? (IMAGE_URL + each.image + "?" + Math.random()) : "/assets/images/no-imagefound.jpg"
                  return (
                    <div className={each.publish ? "col-lg-3 col-md-4 product-item unpublished-item" : "col-lg-3 col-md-4 product-item"} key={id}>
                      <div className="thumbnail" >
                        <div className="thumbnail-wrapper" >
                          <a>
                            {productCategoryAccess.status ? <div className="publish-icon" onClick={() => this.changeCatStatus(each.publish, each._id, 'publish')}><i className="fa fa-power-off"></i></div> : null}
                            {productCategoryAccess.status == false && productCategoryAccess.delete == false ? null :
                              <div className="checkbox">
                                <label>
                                  <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray(each._id)} onChange={() => this.onCheckbox(each._id)} /><span></span>
                                  <i className="input-helper" />
                                </label>
                              </div>}

                            {productCategoryAccess.edit == false && productCategoryAccess.delete == false ? null :
                              <div className="product-setting">
                                {productCategoryAccess.edit ? <i className="fa fa-edit" onClick={() => this.updateCategory(each._id)} ></i> : null}
                                {productCategoryAccess.delete ? <i className="fa fa-trash" onClick={() => this.deleteCategory(each._id)} ></i> : null}
                              </div>}
                          </a>
                          <div className="image-wrapper" onClick={() => this.subcategorylist(each._id, each.categoryName)}>
                            <img src={image} alt={each.categoryName} />
                          </div>
                        </div>
                        <div className="caption">
                          <h5 className="caption-heading">{each.categoryName}</h5>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>

              :
              
                <div className="card-body">
                  <div className='animated fadeIn'>
                    <div className='row'>
                      <div className='col-md-12 mb-2'>
                      </div>
                    </div>
                    <div className='table-responsive'>
                      <table className='table dataTable with-image row-border hover custom-table'>
                        <thead>
                          <tr>
                            {productCategoryAccess.status == false && productCategoryAccess.delete == false ? null :
                              <th><div className="checkbox">
                                <label>
                                  <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} /><span></span>
                                  <i className="input-helper" />
                                </label>
                              </div>
                              </th>}
                            {image ? <th> Imagen </th> : null}
                            {name ? <th sortable-column="categoryName" onClick={this.onSort.bind(this, 'categoryName')}>Nombre de la categoría <i aria-hidden='true' className={(sortData['categoryName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                            <th sortable-column="position" onClick={this.onSort.bind(this, 'position')}> Posición <i aria-hidden='true' className={(sortData['position']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /> </th>
                            {categoryId ? <th sortable-column="categoryId" onClick={this.onSort.bind(this, 'categoryId')}>Categoria ID <i aria-hidden='true' className={(sortData['categoryId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                            {customUrl ? <th>URL personalizada </th> : null}
                            {sidebar ? <th sortable-column="sideView" onClick={this.onSort.bind(this, 'sideView')}>Barra lateral <i aria-hidden='true' className={(sortData['sideView']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                            {scrollbar ? <th sortable-column="frontView" onClick={this.onSort.bind(this, 'frontView')}>Barra de desplazamiento <i aria-hidden='true' className={(sortData['frontView']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                            {publish && productCategoryAccess.status ? <th sortable-column="publish" onClick={this.onSort.bind(this, 'publish')}>Publicado <i aria-hidden='true' className={(sortData['publish']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                            {productCategoryAccess.edit == false && productCategoryAccess.delete == false ? null : <th>Comportamiento</th>}
                          </tr>
                        </thead>
                        <tbody>

                          {categorieslist.map((each, id) => {
                            let image = (each.image) ? (IMAGE_URL + each.image + "?" + Math.random()) : "../assets/images/no-imagefound.jpg"
                            return (
                              <tr className='animated fadeIn' key={id}>
                                {productCategoryAccess.status == false && productCategoryAccess.delete == false ? null :
                                  <td width="10%"><div className="checkbox">
                                    <label>
                                      <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray(each._id)} onChange={() => this.onCheckbox(each._id)} /><span></span>
                                      <i className="input-helper" />
                                    </label>
                                  </div></td>}
                                {image ? <td> <div className='thumb-img'> <a onClick={() => this.subcategorylist(each._id, each.categoryName)}><img src={image} alt={each.categoryName} /></a> </div> </td> : null}
                                {name ? <td><a href="javascript:void();" onClick={() => this.subcategorylist(each._id, each.categoryName)}>{each.categoryName}</a></td> : null}
                                <td>{each.position ? each.position : '-'}</td>
                                {categoryId ? <td>{each.categoryId}</td> : null}
                                {customUrl ? <td>{each.customUrl}</td> : null}
                                {sidebar ? <td><Toggle checked={each.sideView} className='custom-classname' onChange={() => this.changeCatStatus(each.sideView, each._id, 'sideView')} /></td> : null}
                                {scrollbar ? <td><Toggle checked={each.frontView} className='custom-classname' onChange={() => this.changeCatStatus(each.frontView, each._id, 'frontView')} /></td> : null}
                                {publish && productCategoryAccess.status ? <td><Toggle checked={each.publish} className='custom-classname' onChange={() => this.changeCatStatus(each.publish, each._id, 'publish')} /></td> : null}
                                {productCategoryAccess.edit == false && productCategoryAccess.delete == false ? null : <td>
                                  {productCategoryAccess.edit ?
                                    <button onClick={() => this.updateCategory(each._id)}>
                                      <i className='fa fa-pencil-square-o text-primary' aria-hidden='true' />
                                    </button> : null}
                                  {productCategoryAccess.delete ? <button onClick={() => this.deleteCategory(each._id)}>
                                    <i className='fa fa-trash text-danger' aria-hidden='true' />
                                  </button> : null}
                                </td>}
                              </tr>
                            )
                          })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
            }
          </div>}
          </div>

      </Home >
    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(CategoryList);
