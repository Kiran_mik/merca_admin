import React, { PureComponent } from 'react'
import Home from '../../Home'
import Toggle from 'react-toggle'
import axios from 'axios'
import { API_URL, IMAGE_URL } from '../../../config/configs'
import { connect } from 'react-redux'
import * as actions from '../../../actions'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import SimpleReactValidator from 'simple-react-validator'
import swal from 'sweetalert'
import { toast } from 'react-toastify';
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";


class EditCategory extends PureComponent {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      categoryName: '',
      customUrl: '',
      publish: true,
      sidebar:true,
      scrollbar:true,
      categoryId: '',
      categoryId_:'',
      color:'',
      position:'',
      cat_id: '',
      image: '',
      icon: '',
      description: '',
      metaTitle: '',
      metaKeyword: '',
      metaDescription: '',
      fileVisible: false,
      fileVisible1: false,
      errors: {},
      src: null,
      bannerSrc: null,
      crop: {
        x: 10,
        y: 10,
        unit: 'px',
        width: 400,
        height: 275,
      },
      cropBanner: {
        x: 10,
        y: 10,
        unit: 'px',
        width: 1200,
        height: 300,

      }
    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission && permissions.rolePermission.productCategoryAccess && permissions.rolePermission.productCategoryAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }
    var id = this.props.match.params.cId;
    this.setState({ cat_id: id })
    if (id) {
      var body = { categoryId: id }
      let token = localStorage.getItem('token')
      var url = '/categories/getCategoriesDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let data = response.data.data
        if (data) {
          this.setState({
            categoryName: data.categoryName,
            color:data.color,
            position:data.position,
            scrollbar:data.frontView,
            sidebar:data.sideView,
            categoryId_:data.categoryId,
            customUrl: data.customUrl,
            publish: data.publish,
            categoryId: data._id,
            image: data.image,
            ban_image: data.categoryImage,
            icon: data.icon,
            metaTitle: data.metaTitle,
            metaKeyword: data.metaKeyword,
            metaDescription: data.metaDescription,
          })
          if (data.description != undefined) {
            this.setState({ description: data.description })
          } else {
            this.setState({ description: '' })
          }
        }

      })
    }

  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.name === 'categoryName') {
      this.setState({ catname_: false });
    }
    if (event.target.name === 'customUrl') {
      this.setState({ caturl_: false });
    }
    if (event.target.name === 'categoryId_') {
      this.setState({ catId_: false });
    }
    if (event.target.name === 'position') {
      this.setState({ position_: false });
    }

  }
  // validateForm = () => {
  //   let { categoryName, customUrl, errors } = this.state
  //   let formIsValid = true
  //   if (!categoryName) {
  //     formIsValid = false
  //     errors['categoryName'] = '* Category name is required'
  //   }
  //   if (!customUrl) {
  //     formIsValid = false
  //     errors['customUrl'] = '* Custom URL is required'
  //   }
  //   this.setState({ errors })
  //   return formIsValid
  // }
  urlGenerator = () => {
    var url = '/categories/addCategory'
    let { categoryName, categoryId } = this.state
    let data = {}
    if (categoryId === '') {
      data.categoryName = categoryName
    }
    if (categoryId !== '') {
      data.category = categoryName
      data.categoryId = categoryId
    }
    var body = data
    var token = localStorage.getItem('token')
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var { data } = response
      if (data.statusCode === 441) {
        toast.error("Nombre ya existe !", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
      }
      else {
        if (categoryId !== '') {
          this.setState({
            customUrl: data.data.customUrl,
            caturl_: false
          })
        } else
          this.setState({
            categoryId: data.data._id,
            customUrl: data.data.customUrl,
            errors: {},
            caturl_: false
          })
      }
    })
  }


  // #################################### Add Category ################################

  addCategory = () => {
    // if (this.validateForm()) {
    let token = localStorage.getItem('token');
    var { categoryName,categoryId_,position, cat_id, customUrl, publish,sidebar,scrollbar,color, categoryId, image, icon, ban_image, description, metaTitle, metaKeyword, metaDescription } = this.state
    if (!categoryName || categoryName.trim() === '') {
      this.setState({ catname_: true })
    }
    if (!categoryId_ || categoryId_.trim() === '') {
      this.setState({ catId_: true })
    }
    if (position==='') {
      this.setState({ position_: true })
    }
    if (!customUrl || customUrl.trim() === '') {
      this.setState({ caturl_: true })
    }
    if (categoryName.trim() != '' && customUrl.trim() != '') {
      var body = { categoryName, customUrl, publish,color,position,sideView:sidebar,frontView:scrollbar, categoryId:categoryId_, id:categoryId, image, icon, categoryImage: ban_image, description, metaTitle, metaKeyword, metaDescription }
      var url = '/categories/addCategory'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          if (cat_id !== undefined) {
            var title = "Categoría actualizada con éxito!"
          } else {
            var title = "Categoría añadida con éxito!"
          }
          swal({ title: title, icon: "success", })
            .then((willDelete) => {
              // if (willDelete) {
              this.props.history.push('/categorylist')
              // }
            })
        }
        if (data.statusCode === 457) {
          toast.error("¡El Id. De categoría ya existe!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }
      })
    }
  }



  // #################################  Image&Icon Preview #################################

  fileChangedHandlerImg(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file: file,
        filename: filename,
        imagePreviewUrl: reader.result,
        fileVisible: true
      })
    }
    reader.readAsDataURL(file)
  }


  fileChangedHandlerIcon(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file1: file,
        filename1: filename,
        iconPreviewUrl: reader.result,
        fileVisible1: true
      })
    }
    reader.readAsDataURL(file)
  }


  b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
  // #################################  #################################
  metaFileUpload = (name) => {
    if (name === 'image') {
      var ImageURL = this.state.b64Img;
      var block = ImageURL.split(";");
      var contentType = block[0].split(":")[1];// In this case "image/gif"
      var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."
    } else if (name === "categoryImage") {
      var ImageURL = this.state.b64Img1;
      var block = ImageURL.split(";");
      var contentType = block[0].split(":")[1];// In this case "image/gif"
      var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."
    }
    if (name === "image" || name === "categoryImage") {
      var blob = this.b64toBlob(realData, contentType);
    }
    if (name) {
      var { file, file1, categoryId } = this.state
      this.setState({ file })
      var token = localStorage.getItem('token')
      var formData = new FormData()
      formData.append('imageType', [name])
      formData.append('categoryId', categoryId)
      if (name === "icon") {
        formData.append('file', file1)
      }

      else if (name === "image") {
        formData.append('file', blob)
      }
      else if (name === "categoryImage") {
        formData.append('file', blob)
      }
      axios
        .post(API_URL + '/auth/fileUploadForCategoryImage', formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: token
          }
        })
        .then(response => {
          let { data } = response.data
          // setTimeout(()=>{
          if (data) {
            if (name === "image") {
              this.setState({ image: data.filepath, fileVisible: false, file: '', src: null, croppedImageUrl: null })
            }
            else if (name === "icon") {
              this.setState({ icon: data.filepath, fileVisible1: false, file1: '' })
            }
            else if (name === "categoryImage") {
              this.setState({ ban_image: data.filepath, croppedImageUrl1: null, bannerSrc: null })
            }
            else {
              console.log('image not found');
            }
          }
        // },500)
        })
        .catch(err => console.log(err))
    }
  }

  onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () =>
        this.setState({ src: reader.result })
      );
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  onImageLoaded = image => {
    this.imageRef = image;
  };

  onCropComplete = crop => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        "newFile.jpeg"
      );
      this.setState({ croppedImageUrl });
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );


    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = canvas.toDataURL('image/jpeg');
        var item_image = this.fileUrl.replace(/^data:image\/(png|jpg);base64,/, "");
        this.setState({ b64Img: item_image })
        resolve(this.fileUrl);
      }, "image/jpeg");
    });
  }





  onSelectFile1 = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader1 = new FileReader();
      reader1.addEventListener("load", () =>
        this.setState({ bannerSrc: reader1.result })
      );
      reader1.readAsDataURL(e.target.files[0]);
    }
  };
  onImageLoaded1 = (image) => {
    this.imageRef1 = image;
  };

  onCropComplete1 = (crop) => {
    this.makeClientCrop1(this.state.cropBanner);
  };

  onCropChange1 = (cropBanner) => {
    this.setState({ cropBanner: cropBanner });
  };

  async makeClientCrop1(cropBanner) {
    if (this.imageRef1 && cropBanner.width && cropBanner.height) {
      const croppedImageUrl1 = await this.getCroppedImg1(
        this.imageRef1,
        cropBanner,
        "newFile.jpeg"
      );
      this.setState({ croppedImageUrl1 });
    }
  }

  getCroppedImg1(image, crop, fileName) {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );


    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = canvas.toDataURL('image/jpeg');
        var item_image = this.fileUrl.replace(/^data:image\/(png|jpg);base64,/, "");
        this.setState({ b64Img1: item_image })
        resolve(this.fileUrl);
      }, "image/jpeg");
    });
  }
  render() {
    var { crop, cropBanner, croppedImageUrl,sidebar,scrollbar, croppedImageUrl1,color,position, src, bannerSrc, categoryName,categoryId_, errors, cat_id, customUrl, publish, image, icon,
       description, metaTitle, metaKeyword, metaDescription } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>{cat_id !== undefined ? 'Editar categoria' : 'Añadir categoría'}</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/categorylist')}>Catalogo de producto</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/categorylist')}>Categorías</li>
                <li className='breadcrumb-item active'>{cat_id !== undefined ? 'Editar categoria' : 'Añadir categoría'}</li>
              </ul>
            </div>
          </div>
        </div>

        <div className='card fadeIn'>
          <div className="card-header" data-spy="affix" data-offset-top="100">
            <div className="button-continer text-right">
              <button type='button' className='btn btn-primary' onClick={this.addCategory}> <span>{cat_id !== undefined ? 'Guardar' : 'Añadir'}</span> </button>
              <button type='button' className='btn btn-outline-primary' onClick={() => this.props.history.push('/categorylist')} > Cancelar </button>
            </div>
          </div>
          <div className='card-body'>
            <form className='form-sample'>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3  col-form-label'>
                Nombre <span className='text-danger'>*</span> :
                  </label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='categoryName'
                    placeholder='Ingrese el nombre de la categoría'
                    value={categoryName}
                    onChange={this.handleChange}
                    onBlur={this.urlGenerator}
                  />
                  <span className="error-block"> {this.state.catname_ ? "* Se requiere nombre de categoría" : null} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3  col-form-label'>
                Categoria ID <span className='text-danger'>*</span> :
                  </label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='categoryId_'
                    placeholder='Ingresar ID de categoría'
                    value={categoryId_}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {this.state.catId_ ? "* Se requiere ID de categoría" : null} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3  col-form-label'>
                Color :
                  </label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='color'
                    placeholder='Ingresar color'
                    value={color}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3  col-form-label'>
                Posición <span className='text-danger'>*</span> :
                  </label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='number'
                    name='position'
                    placeholder='Ingresar Posición'
                    value={position}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {this.state.position_ ? "* Se requiere Posición" : null} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>URL personalizada <span className='text-danger'>*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='customUrl'
                    placeholder='Ingresar URL personalizada'
                    value={customUrl}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {this.state.caturl_ ? "* Se requiere URL de categoría" : null} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-12 col-form-label'>
                Imagen :
                  </label>
                <div className="col-lg-4 col-sm-12">
                  <div className="form-group inputDnD mb-0">
                    <div className="upload-overlay d-flex justify-content-center align-items-center">
                      <div className="upload-wrapper">
                        <i className="fa fa-upload"></i>
                        <span><button type="button"  >Elige un archivo</button> o arrástralo aquí</span>
                      </div>
                    </div>
                    <label className="sr-only" >Subir archivo</label>
                    <input type="file" className="form-control-file text-primary font-weight-bold" name='image' id="file" accept="image/*"
                      data-title="Drag and drop a file" onChange={this.onSelectFile} />
                    <span className="notes">Es preferible un tamaño de imagen de 400 * 275</span>
                  </div>
                  <div className="button-continer pull-right my-2">
                    <button className="btn btn-primary" type="button" onClick={this.metaFileUpload.bind(this, "image")}> Cargar imagen</button>
                  </div>
                </div>
                <div className="col-md-4 col-sm-12">
                  <div className="row justify-content-between">
                    <div className="col-md-12 select-image"><div className="product-img">
                      {image ? <img src={IMAGE_URL + image + "?" + Math.random()} alt={image} /> : <img src="/assets/images/no-image-icon-4.png" alt="no-image-found" />}
                    </div>
                    </div>
                  </div>
                </div>
                <div className="App mt-3 col-12">
                  <div className="close-icn">
                    {src && (
                      <>
                        <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ src: '' })} />
                        <ReactCrop
                          className='reactcrop'
                          src={src}
                          crop={crop}
                          minHeight={100}
                          maxHeight={275}
                          minWidth={100}
                          maxWidth={400}
                          locked={true}
                          onImageLoaded={this.onImageLoaded}
                          onComplete={this.onCropComplete}
                          onChange={this.onCropChange}
                        />
                        <div className="croped-img mt-3">
                          {croppedImageUrl && (
                            <img alt="Crop" style={{ maxWidth: "100%" }} src={croppedImageUrl} />
                          )}
                        </div>
                      </>)}
                  </div>
                </div>
              </div>


              <div className='form-group row'>
                <label className='col-lg-2 col-sm-12 col-form-label'>
                Imagen de banner:
                  </label>
                <div className="col-lg-4 col-sm-12">
                  <div className="form-group inputDnD mb-0">
                    <div className="upload-overlay d-flex justify-content-center align-items-center">
                      <div className="upload-wrapper">
                        <i className="fa fa-upload"></i>
                        <span><button type="button"  >Elige un archivo</button> o arrástralo aquí</span>
                      </div>
                    </div>
                    <label className="sr-only" >Subir archivo</label>
                    <input type="file" className="form-control-file text-primary font-weight-bold" name='image' id="file" accept="image/*"
                      data-title="Drag and drop a file" onChange={this.onSelectFile1} />
                    <span className="notes">Es preferible un tamaño de imagen de 1200*300</span>
                  </div>
                  <div className="button-continer pull-right my-2">
                    <button className="btn btn-primary" type="button" onClick={this.metaFileUpload.bind(this, "categoryImage")}> Cargar imagen</button>
                  </div>
                </div>
                <div className="col-md-4 col-sm-12">
                  <div className="row justify-content-between">
                    <div className="col-md-12 select-image"><div className="product-img">
                      {this.state.ban_image ? <img src={IMAGE_URL + this.state.ban_image + "?" + Math.random()} alt={image} /> : <img src="/assets/images/no-image-icon-4.png" alt="no-image-found" />}
                    </div>
                    </div>
                  </div>
                </div>
                <div className="App mt-3 col-12">
                  <div className="close-icn">
                    {bannerSrc && (
                      <>
                        <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ bannerSrc: '' })} />
                        <ReactCrop
                          className='reactcrop'
                          src={bannerSrc}
                          crop={cropBanner}
                          minHeight={100}
                          maxHeight={300}
                          minWidth={100}
                          maxWidth={1200}
                          locked={true}
                          onImageLoaded={this.onImageLoaded1}
                          onComplete={this.onCropComplete1}
                          onChange={this.onCropChange1}
                        />
                        <div className="croped-img mt-3">
                          {croppedImageUrl1 && (
                            <img alt="Crop" style={{ maxWidth: "100%" }} src={croppedImageUrl1} />
                          )}
                        </div>
                      </>
                    )}
                  </div>
                </div>
              </div>

              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>
                Ícono:
                  </label>
                <div className="drop-section mb-3 col-md-4">
                  <div className="form-group inputDnD mb-0">
                    <div className="upload-overlay d-flex justify-content-center align-items-center">
                      <div className="upload-wrapper">
                        <i className="fa fa-upload"></i>
                        <span><button type="button"  >Elige un archivo</button> o arrástralo aquí</span>
                      </div>
                    </div>
                    <label className="sr-only" >Subir archivo</label>
                    <input type="file" className="form-control-file text-primary font-weight-bold" name='image' id="file" accept="image/*"
                      data-title="Drag and drop a file" onChange={this.fileChangedHandlerIcon.bind(this)} />
                  </div>
                  {this.state.fileVisible1 ?
                    <span>
                      <div className='product-img'> <img src={this.state.iconPreviewUrl} alt={this.state.filename1} />
                        <span> <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ fileVisible1: !this.state.fileVisible1, file1: '' })} /></span> </div>
                      <span>{this.state.filename1}</span>
                    </span> : null}
                  <div className="button-continer pull-right my-2">
                    <button className="btn btn-primary" type="button" onClick={this.metaFileUpload.bind(this, "icon")}> Cargar Ícono</button>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="row justify-content-between my-2">
                    <div className="col-md-12 select-image"><div className="product-img">
                      {icon ? <img src={IMAGE_URL + icon + "?" + Math.random()} alt={icon} /> : <img src="/assets/images/no-image-icon-4.png" alt={icon} />}</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>
                Descripción :
                </label>
                <div className='col-md-8 co-sm-7'>
                  <CKEditor
                    editor={ClassicEditor}
                    data={description}
                    onChange={(event, editor) => {
                      const data = editor.getData()
                      this.setState({ description: data })
                    }}
                  />

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'> Meta título: </label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='metaTitle'
                    placeholder='Meta título'
                    value={metaTitle}
                    onChange={this.handleChange}
                  />

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'> Meta palabra clave: </label>
                <div className='col-md-8 co-sm-7'>
                  <textarea
                    className='form-control'
                    type='text'
                    name='metaKeyword'
                    placeholder='Introduzca palabra clave meta'
                    value={metaKeyword}
                    onChange={this.handleChange}
                  />

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'> Metadescripción : </label>
                <div className='col-md-8 co-sm-7'>
                  <textarea
                    className='form-control'
                    type='text'
                    name='metaDescription'
                    placeholder='Introduzca palabra clave meta'
                    value={metaDescription}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label' htmlFor='material-switch' >Publicado: </label>
                <div className='col-md-8 co-sm-7'>
                  <Toggle
                    checked={publish}
                    className='custom-classname'
                    onChange={() => this.setState({ publish: !this.state.publish })}
                  />
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label' htmlFor='material-switch' >Mostrar en la barra lateral: </label>
                <div className='col-md-8 co-sm-7'>
                  <Toggle
                    checked={sidebar}
                    className='custom-classname'
                    onChange={() => this.setState({ sidebar: !this.state.sidebar })}
                  />
                </div>
              </div><div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label' htmlFor='material-switch' >Mostrar en la barra de desplazamiento: </label>
                <div className='col-md-8 co-sm-7'>
                  <Toggle
                    checked={scrollbar}
                    className='custom-classname'
                    onChange={() => this.setState({ scrollbar: !this.state.scrollbar })}
                  />
                </div>
              </div>
            </form>
          </div>
        </div>
      </Home >
    )
  }
}


const mapStateToProps = state => ({
  Categorydetails: state.commonReducers.editedDetails,
  permissionsList: state.admindata.rolePermissions,

});

export default connect(mapStateToProps, actions)(EditCategory)


