import React, { Component } from 'react';
import Home from '../Home'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import axios from 'axios'
import { Select } from 'antd';
import { API_URL, IMAGE_URL, CSV_URL } from '../../config/configs'
import swal from 'sweetalert'
import { FadeLoader } from 'react-spinners';
import Loader from '../Loader'
var Head = [[
  'productName', 'metaTitle', 'metaKeyword', 'metaDescription', 'relatedItems', 'featuredProduct', 'bestSeller', 'parentCategory', 'additionalImage', 'brandName', 'supplierName',
  'price', 'stockQuantity', 'weight', 'publish', 'allowedDiscount', 'preOrder', 'freeDelivery', 'taxable', 'salePrice', 'description', 'shortDescription',
  'availability', 'discount'
]];
class BulkUploads extends Component {
  constructor(props) {
    super(props)
    this.state = {
      productsAccess: {},
      tab1: true,
      file: {},
      filename: '',
      imagesArr: [],
      bulkFiles: [],
      loading: null,
      errors_: [],
      upload: false,
      image_err: [],
      warnings_: [],
      exist_: [],
      url_:[],

    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions.rolePermission) {
      let { productsAccess } = permissions.rolePermission
      this.setState({ productsAccess: productsAccess })
    }
  }




  fileChangedHandler(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file: file,
        filename: file.name,
      })
    }
    reader.readAsDataURL(file)
  }


  // ################################# CSV Preview && Upload #################################
  CsvFileHandler(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    if (file) {
      let filename = event.target.files[0].name
      reader.onloadend = () => {
        this.setState({
          file: file,
          filename: filename,
          imagePreviewUrl: reader.result,
        })
      }
      reader.readAsDataURL(file)
    }
  }

  CsvFileUpload = (val) => {
    var { file } = this.state
    var token = localStorage.getItem('token')
    this.setState({ upload: true })
    var formData = new FormData()
    if (this.state.csvVal) {
      formData.append('updateProducts', true)
    }
    if(this.state.csv_val){
      formData.append('updateProducts', false)
    }
    formData.append('file', file)
    let ext = file.name.split(".");
    let n = ext.length;
    if (ext[n - 1] == "zip") {
      var API = '/auth/uploadZipFile'
    } else {
      var API = '/products/bulkUploadProduct'
    }


    axios
      .post(API_URL + API, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: token
        }
      })
      .then(response => {
        let { data } = response
        if (data.status === 1 && data.statusCode === 219 || data.statusCode === 206) {
          swal("Subido correctamente", "", "success")
          this.setState({ file: {}, filename: '', upload: false,exist_:'' })
        }
        if (data.statusCode === 435) {
          swal("La URL del producto debe ser única", "", "warning")
          if (response.data && response.data.data) {
            let { data } = response.data
            this.setState({ url_: data, upload: false })
          }
        }
        if (data.statusCode === 241) {
          swal({ title: data.data.length + " " + "los productos con el mismo nombre ya existen, ¿desea actualizarlos?", icon: 'warning', buttons: true, dangerMode: true }).then(willDelete => {
            if (willDelete) {
              this.setState({ csvVal: true })
              // this.CsvFileUpload()
              if (response.data && response.data.data) {
                let { data } = response.data
                this.setState({ exist_: data, upload: false })
              }
            } 
            else if(!willDelete){
              this.setState({ csvVal: false,csv_val:true })
              // this.CsvFileUpload('csvval')
              if (response.data && response.data.data) {
                let { data } = response.data
                this.setState({ exist_: data, upload: false })
              }
            }
            else {
              this.setState({ file: {}, filename: '', upload: false })
            }
          })
        }
        if (data.statusCode === 228) {
          swal("Subido correctamente", "", "success")
          this.setState({ file: {}, filename: '', upload: false ,exist_:[]})
        }
        if (data.statusCode === 460) {
          swal("Algunos detalles no encontrados", "", "warning")
          if (response.data && response.data.data) {
            let { data } = response.data
            this.setState({ errors_: data, upload: false })
          }
        }
        if (data.statusCode === 451) {
          swal("por favor llene todos los campos requeridos", "", "warning")
          if (response.data && response.data.data) {
            let { data } = response.data
            this.setState({ warnings_: data, upload: false })
          }
        }
        if (data.message === "Invalid token." || data.message === "Token is expired.") {
          swal('Session Expired', '', 'error')
            .then((willDelete) => {
              // if (willDelete) {
              localStorage.removeItem("token"); this.props.history.push('/');
              // }
            })
        }
        // if (data.status === 0) {
        //   swal(data.message, "", "error")
        // }
        this.setState({ csvVal: false, upload: false })
      })
      .catch(err => console.log(err))
  }

  // ################################# Bulk Image Preview #################################
  fileChangedHandler1 = (e) => {
    this.setState({ multibeforeUpload: true })
    let files = e.target.files;
    var imagesArr = this.state.imagesArr;
    var fileStorage = this.state.bulkFiles
    for (let i = 0; i < files.length; i++) {
      let url = URL.createObjectURL(files[i]);
      let img = new Image();
      img.src = url;
      document.body.appendChild(img);
      img.onload = function () {
        URL.revokeObjectURL(this.src);
      }
      fileStorage.push(files[i])
      imagesArr.push({ image: img.src, url: files[i].name })
      this.setState({ imagesArr: imagesArr, bulkFiles: fileStorage })
    }
  }

  bulkFileUpload = () => {
    this.setState({ upload: true })
    var { bulkFiles } = this.state
    var token = localStorage.getItem('token')
    var imagefile = document.querySelector('#file1')
    var formData = new FormData()
    for (const file of imagefile.files) {
      formData.append('file', file)
    }
    axios
      .post(API_URL + '/auth/uploadBulkMultipleImages', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: token
        }
      })
      .then(response => {
        let { data } = response
        if (data.status === 1) {
          if (data.data && data.data.length > 0) {
            this.setState({ image_err: data.data, upload: false })
            swal("Algunas imágenes están cargadas", "", "warning")
          } else {
            this.setState({ multibeforeUpload: !this.state.multibeforeUpload, imagesArr: [], upload: false })
            swal("Subido correctamente", "", "success")
          }
        }
        if (data.message === "Invalid token." || data.message === "Token is expired.") {
          swal('Session Expired', '', 'error')
            .then((willDelete) => {
              // if (willDelete) {
              localStorage.removeItem("token"); this.props.history.push('/');
              // }
            })
        }
      })
      .catch(err => console.log(err))
  }


  loadimage = () => {
    return this.state.imagesArr && Array.isArray(this.state.imagesArr) && this.state.imagesArr.length ? this.state.imagesArr.map((each, id) => {
      return (<div className='col-md-2 col-sm-4 select-image text-center select-image-product text-center' key={id}>

        <span>
          <div className='product-img'> <img src={each && each.image ? each.image : undefined} alt={each.image} /> </div>
          <span>{each.url}</span>
        </span>
      </div>

      )
    }) : undefined
  }


  sampleCSVdownload = () => {
    let token = localStorage.getItem('token')
    axios({
      method: 'get',
      url: API_URL + '/products/downloadSampleCsv',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
    })
      .then(res => {
        let { data } = res
        if (data && data.data)
          window.open(CSV_URL + data.data.filePathAndName, '_blank');
      })
  }
  render() {
    let { productsAccess, filename, image_err, loading, exist_,url_, errors_, warnings_ } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Subidas masivas</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/productlist')}>Catalogo de producto</li>
                <li className="breadcrumb-item  active" onClick={() => this.props.history.push('/productlist')} >Subidas masivas</li>
              </ul>
            </div>
          </div>
        </div>
        {this.state.upload ? <Loader /> : null}
        <div className='card animated fadeIn'>
          <div className='card-body'>
            <ul className='nav nav-tabs' id='myTab' role='tablist'>
              {productsAccess.addBulkProduct ?
                <li className='nav-item'>
                  <a className={this.state.tab1 ? 'nav-link active' : 'nav-link'} data-toggle='tab' href='#addproduct' role='tab' aria-controls='item' aria-selected='false'  >Agregar producto </a>
                </li> : null}
              {productsAccess.bulkimageUpload ? <li className='nav-item'>
                <a className={this.state.tab2 ? 'nav-link active' : 'nav-link'} data-toggle='tab' href='#updateimage' role='tab' aria-controls='imagetab' aria-selected='false'  >Cargar imágenes </a>
              </li> : null}
              <li className="push-right"><button className="btn btn-primary mr-3" onClick={this.sampleCSVdownload}>Descargar Sample CSV</button> </li>
            </ul>
            <div className='tab-content' id='myTabContent'>
              <div className={this.state.tab1 ? 'tab-pane fade show active' : 'tab-pane fade'} id='addproduct' role='tabpanel' aria-labelledby='itemtab' >
                <div className="table-responsive">
                  <table className="table dataTable with-image row-border hover custom-table table-striped" >
                    <thead>
                      <tr>
                        <th>Nombre del campo
                      </th>
                        <th >¿Es obligatorio? (Si no)
                      </th>
                        <th sortable-column="emailId">Es único (sí / no)

                        </th>
                        <th sortable-column="mobile" >¿Cómo / qué mencionar?

                        </th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr className="animated fadeIn">
                        <td>nombre del producto</td>
                        <td>sí</td>
                        <td>sí</td>
                        <td>Para mostrar el nombre del producto <br />
                          Eg: Raybon Sunglasses </td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>SKU</td>
                        <td>sí</td>
                        <td>sí</td>
                        <td>SKU del producto <br />
                          Eg: SKU-prod-1 </td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Images</td>
                        <td>sí</td>
                        <td>sí</td>
                        <td>Imagen principal del producto, imágenes adicionales del producto <br />
                          Eg: SKU-prod-1,SKU-prod-11,SKU-prod-12,SKU-prod-13..... </td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Categoría principal</td>
                        <td>sí</td>
                        <td>no</td>
                        <td>Categoría principal, subcategoría, subcategoría (debe estar separada por comas)<br />
                          Eg: Electronics,Computers,Accessories
                        </td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Identificación del proveedor</td>
                        <td>sí</td>
                        <td>no</td>
                        <td>ID del proveedor para mostrar el nombre del proveedor en la página de detalles del producto.<br />
                          Eg: sup-cro-1</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>ID de marca</td>
                        <td>sí</td>
                        <td>no</td>
                        <td>brandId de la marca para mostrar el nombre de brandName en la página de detalles del producto.<br />
                          Eg: brand-nik-1</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Peso</td>
                        <td>sí</td>
                        <td>no</td>
                        <td>Peso de un artículo en particular para mostrar en la página de detalles del producto.<br />
                          Eg: 220</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Descripción</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Describiendo sobre características, detalles de un artículo en particular.<br />
                          Eg: A laptop that is designed for professionals .....</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Breve descripción</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Describiendo sobre la información del artículo y detalles esenciales.<br />
                          Eg: A laptop that is designed for professionals .....</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Precio</td>
                        <td>sí</td>
                        <td>no</td>
                        <td>Costo del articulo <br />
                          Eg: 10</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Precio de venta</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Costo del artículo después del descuento aplicado<br />
                          Eg: 5 (Note:Must be less than Price)</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Artículos relacionados</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Sugerir los elementos relacionados con ese nombre / características (usar SKU de productos ya existentes) separados por comas<br />
                          Eg:sku-pal-469,sku-pal-468, </td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Meta título</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Metatítulo que describe el contenido de una página web para los motores de búsqueda.<br />
                          Eg: Some Content here</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Meta palabra clave</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Eg: Algunos contenidos aquí</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Algunos contenidos aquí</td>
                        <td>no</td>
                        <td>no</td>
                        <td>El atributo es una breve descripción de una página que aparece debajo del título en SERPs<br />
                          Eg: Some Content here</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Descuento</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Eg: 10 (Note:Si el descuento permitido es verdadero)</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Cantidad de stock</td>
                        <td>sí</td>
                        <td>sí</td>
                        <td>Unidades de artículo disponibles<br />
                          Eg: 10</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Disponibilidad</td>
                        <td>sí</td>
                        <td>sí</td>
                        <td>
                          Eg: 2 días hábiles</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Producto destacado</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Elementos que promocionar (para mostrar en la sección de productos destacados en la página de inicio)<br />
                          Eg: Verdadero Falso</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Mejor vendido</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Productos más vendidos basados ​​en pedidos anteriores (para mostrar en la sección Best seller en la página de inicio)<br />
                          Eg: Verdadero Falso</td>
                      </tr>

                      <tr className="animated fadeIn">
                        <td>Hacer un pedido</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Disponible para artículos que aún no se han lanzado<br />
                          Eg: Verdadero Falso</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Entrega gratis</td>
                        <td>no</td>
                        <td>no</td>
                        <td> Eg: Verdadero Falso</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Imponible</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Eg: Verdadero Falso</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Descuento permitido</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Eg: Verdadero Falso</td>
                      </tr>
                      <tr className="animated fadeIn">
                        <td>Activar</td>
                        <td>no</td>
                        <td>no</td>
                        <td>Si el elemento 'verdadero' se mostrará en el sitio web, si el elemento 'verdadero' no se mostrará<br />
                          Eg: Verdadero Falso</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                {errors_.length > 0 ? <h6>Errores : </h6> : null}

                {errors_.length > 0 && errors_.map(each => {
                  return (
                    <ul style={{ color: 'red' }}>
                      <li>Línea no:{each.line} - {each.type} extraviado</li>
                    </ul>
                  )
                })}

                {warnings_.length > 0 ? <h6>Por favor llene todos los campos requeridos : </h6> : null}

                {warnings_.length > 0 && warnings_.map(each => {
                  return (
                    <ul style={{ color: 'red' }}>
                      <li>Línea no:{each.line} - {each}</li>
                    </ul>
                  )
                })}
                
                {exist_.length > 0 ? <h6>Productos con el mismo nombre ya existen : </h6> : null}

                {exist_.length > 0 && exist_.map(each => {
                  return (
                    <ul style={{ color: 'red' }}>
                      <li>{each.SKU} </li>
                    </ul>
                  )
                })}
                
                {url_.length > 0 ? <h6>La URL del producto debe ser única : </h6> : null}

                {url_.length > 0 && url_.map(each => {
                  return (
                    <ul style={{ color: 'red' }}>
                      <li>Line no:{each.line} </li>
                    </ul>
                  )
                })}


                <div className='form-group inputDnD mt-5'>
                  <div className='upload-overlay d-flex justify-content-center align-items-center'>
                    <div className='upload-wrapper'>
                      <i className='fa fa-upload' />
                      <span> <button type='button'>Elige un archivo</button> o arrástralo aquí </span>
                    </div>
                  </div>
                  <label className='sr-only'>Subir archivo</label>
                  <input type="file" className="form-control-file text-primary font-weight-bold" name='image' id="file" accept=".csv,.zip"
                    data-title="Drag and drop a file" onChange={this.CsvFileHandler.bind(this)} />
                </div>
                {this.state.fileVisible ?
                  <span>
                    <div className='product-img'> <img src={this.state.imagePreviewUrl} alt={this.state.filename} /><span> <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ fileVisible: !this.state.fileVisible, file: '' })} /></span> </div>
                    <span>{this.state.filename}</span>
                  </span> : null}
                <div className='animated fadeIn'>
                  <p className='product-img'>
                    {filename != '' ? filename : null}
                  </p>
                  {filename != '' ?
                    <button className='btn btn-outline-primary btn-border' onClick={() => this.setState({ file: {}, filename: '' })}>
                      <i className='fa fa-close' /> <span>Remove</span>
                    </button> : null}
                </div>
                <div className='button-continer pull-right mb-2'>
                  <button className='btn btn-primary' type='button' onClick={this.CsvFileUpload} >  Cargar Csv/Zip </button>
                </div>
              </div>
              <div className={this.state.tab1 != true ? 'tab-pane fade show active' : 'tab-pane fade'} id='updateimage' role='tabpanel' aria-labelledby='itemtab' >
                <ol>
                  <li>Download the file (Download CSV for Uploaded Products) after uploading (Upload CSV)</li>
                  <li> Name the Images as per below instructions  Supported extensions for Images : jpeg, jpg, png</li>
                  <li>
                    To Add and Update existing (Variation / Non-variation) product's Main Image, we required ProductDefaultSKU.
                  <ul>sku-som-75.jpg
                      <li>ProductDefaultSKU : You will find it in Product listing under column name: SKU</li>
                      <li>Image name should be: ProductDefaultSKU.jpg</li>
                      <li>For example sku-som-75.jpg,sku-som-89.jpg,sku-del-94.jpg</li>
                    </ul>
                  </li>
                  <li>
                    To Add Slider Image for Non-Variation Product, we required ProductDefaultSKU:
                  <ul>
                      <li>ProductDefaultSKU : You will find it in Product listing under column name: images</li>
                      <li>Images name should be ProductDefaultSKU1.jpg, ProductDefaultSKU2.jpg……so on.</li>
                      <li>Example: sku-som-751.jpg, sku-som-752.jpg, sku-som-753.jpg .....so on</li>
                    </ul>
                  </li>
                  <li>To Add Slider Images for a Single Variation of Product, we required SKU Label:
                      <ul>
                      <li>ProductDefaultSKU:You will find it in Product listing under column name: productImage</li>
                      {/* <li>Images name should be Variation SKU-Label.1.jpg, SKU-Label.2.jpg, ….. so on.</li> */}
                      <li> Auto Generated SKU Label be like: SKU-112-1233.jpg, SKU-112-1234.jpg, SKU-112-1235.jpg...so on</li>
                      {/* <li> For User Defined SKU Label be like: LSGB.1.jpg, LSGB.2.jpg, LSGB.3.jpg... so on</li> */}
                      {/* <li>Dont need to change the p</li> */}
                    </ul>
                  </li>
                  {/* <li>To Add Slider Images for Multiple Variations of Product, we required individual variations SKU Label:
                      <ul>
                      <li>SKU Label : Appear in pop up model when click on sí under column name: Is Variation</li>
                      <li>Images name should be : SKU-Label1.1.jpg, SKU-Label2.1.jpg, SKU-Label3.1.jpg</li>
                      <li>For example:*   To rename the slider image for First Variation SKU-Label1.1.jpg, SKU-Label1.2.jpg, SKU-Label1.3.jpg...so on</li>
                      <li>To rename the slider image for Second Variations SKU-Label2.1.jpg, SKU-Label2.2.jpg, SKU-Label2.3.jpg...so on</li>
                      <li>For Example, A product has 3 variations as follows: SKU-111-100, SKU-111-101, SKU-111-102.</li>
                      <li>SKU-111-100.1.jpg, SKU-111-100.2.jpg, SKU-111-100.3.jpg, ……. So on</li>
                      <li>SKU-111-101.1.jpg, SKU-111-101.2.jpg, SKU-111-101.3.jpg, ……….So on</li>
                      <li>SKU-111-102.1.jpg, SKU-111-102.2.jpg, SKU-111-102.3.jpg, ……..So on</li>
                    </ul>
                  </li> */}
                  <li>Name the images with the name that is given in (images and productImage columns)</li>
                  {/* <li>Generating Archive.zip file: (NOTE: Do not put images in any folder) Select all the named Images and compress them then the Archive.zip file will be generated.</li> */}
                </ol>
                {image_err.length > 0 ? <h6>Errores : </h6> : null}
                {image_err.length > 0 && image_err.map(each => {
                  return (
                    <ul style={{ color: 'red' }}>
                      <li>Nombre de la imágen:{each}</li>
                    </ul>
                  )
                })}

                <div className='form-group row'>
                  <div className='tab-content' id='myTabContent'>
                    {/* tab1 */}
                    {/* multi  images*/}
                    <div className='drop-section mb-3 col-md-12'>
                      <h5>Additional Image(s)</h5>
                      <div className='form-group inputDnD mb-0'>
                        <div className='upload-overlay d-flex justify-content-center align-items-center'>
                          <div className='upload-wrapper'>
                            <i className='fa fa-upload' />
                            <span> <button type='button'>Choose a file</button> or drag it here </span>
                          </div>
                        </div>
                        <label className='sr-only'>File Upload</label>
                        <input
                          type='file'
                          className='form-control-file text-primary font-weight-bold'
                          name='image'
                          id='file1'
                          accept='image/*'
                          multiple
                          data-title='Drag and drop a file'
                          onChange={this.fileChangedHandler1}
                        />
                      </div>


                    </div>


                    {this.state.multibeforeUpload ?
                      <div className='row my-2'>
                        {this.loadimage()}
                      </div>
                      : null}

                    {/* <FadeLoader sizeUnit={"px"} size={20} color={'#2472DC'} loading={loading} /> */}


                    {this.state.multibeforeUpload ?
                      <div className="col-md-12">
                        <div className='button-continer  pull-right   mb-2'>
                          <button className='btn btn-primary' type='button' onClick={this.bulkFileUpload}>
                            Upload Image
                      </button>
                        </div>
                      </div>
                      : null}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Home>
    );
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(BulkUploads)
