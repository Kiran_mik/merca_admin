import React, { Component } from 'react'
import Home from '../../Home'
import axios from 'axios'
import { connect } from 'react-redux'
import * as actions from '../../../actions'
import { API_URL, IMAGE_URL, CSV_URL, EXCEL_URL } from '../../../config/configs'
import swal from 'sweetalert'
import Toggle from 'react-toggle'
import { isEmpty } from "lodash"
import { Select } from 'antd';
import 'rc-pagination/assets/index.css';
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import Pagination from 'rc-pagination';
import 'antd/dist/antd.css';
import { BeatLoader } from 'react-spinners';
import Loader from '../../Loader'

import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;
const FileDownload = require('js-file-download')
class SubCategoryManagement extends Component {
  constructor(props) {
    super(props)
    this.state = {
      subCategorieslist: [],
      multipleDelete: [],
      parentId: '',
      selectAll: false,
      view: "Lista",
      changeButton: "Vista de la lista",
      image: true,
      name: true,
      customUrl: true,
      categoryId: true,
      parentCategory: true,
      publish: true,
      mainCatagoryNames: [],
      selectedCatagory: [],
      publishedvalue: '',
      parentCategoryName: '',
      categoryList: [],
      parentId2: '',
      sortData: { categoryName: false, publish: false, categoryId: false },
      sort: {},
      page: 1,
      pagesize: 5,
      total: null,
      loading: true,
      productCategoryAccess: {},
      selectedOption: 'Selecciona aquí',
      page_:false,
      download:false
    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission && permissions.rolePermission.productCategoryAccess && permissions.rolePermission.productCategoryAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }

    if (permissions.rolePermission) {
      let { productCategoryAccess } = permissions.rolePermission
      this.setState({ productCategoryAccess: productCategoryAccess })
    }
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').slideToggle();
    });

    this.getSubcategory('pagesize')
    this.parentCatName()
    this.getCategoryName()
  }
  // ############################### Sub categoryList ###############################
  getSubcategory = (e) => {
    let { publishedvalue, parentId2, selectedCatagory, sort, page, pagesize } = this.state
    var id = this.props.match.params.scId;
    this.setState({ parentId: id })
    var token = localStorage.getItem('token')
    var data = {type: "subCategory1" }
    if (e === 'filter') {
      data.page = 1
    } else if (e === 'pagesize') {
      data = data
    } else {
      data.page = page
    }
    if (e === 'pagesize') {
      data = data
    } else {
      data.pagesize = pagesize
    }
    if (publishedvalue != '') {
      data.publish = publishedvalue
    }
    if (parentId2 != '') {
      data.parentCategory = [parentId2]
    } else {
      data.parentCategory = [id]
    }
    if (selectedCatagory != '') {
      data.categoryName = selectedCatagory
    }
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    var body = data
    var url = '/categories/subCategoryListing'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      let data1 = data.data
      if (data.status === 0) {
        this.setState({ subCategorieslist: [] })
      } else {
        this.setState({ subCategorieslist: data.data.categorylisting, total: data1.total, loading: false, length: data.data.categorylisting.length })
        if (data.data&&data.data.categorylisting.length <= pagesize) {
          this.setState({ page_: false })
        }
        if (data1.manageSubCategory1Listing) {
          this.setState({
            name: data1.manageSubCategory1Listing.categoryName, customUrl: data1.manageSubCategory1Listing.customUrl,
            categoryId: data1.manageSubCategory1Listing.categoryId,
            parentCategory: data1.manageSubCategory1Listing.parentCategory, publish: data1.manageSubCategory1Listing.publish,
            pagesize:data1.manageSubCategory1Listing.pageSize
          })
        }
      }
      if (e == 'filter') {
        this.setState({ page: 1 })
      }
    })
  }

  // ############################## Reset Listing ######################
  resetListing = (e) => {
    var { page, pagesize, parentId } = this.state
    var url = '/categories/subCategoryListing'
    var method = 'post'
    var body = { page: page, pagesize: pagesize, type: "subCategory1", parentCategory: [this.props.match.params.scId] }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      let { data } = response
      if (data.status === 0) {
        this.setState({ subCategorieslist: [] })

      } else {
        this.setState({ subCategorieslist: data.data.categorylisting, publishedvalue: '', selectedCatagory: [], parentCategoryName: this.props.match.params.scId, parentId2: '', total: data.data.total })
      }

    }
    )
  }
  // #################################### Delete Category ################################
  deleteCategory = (cId) => {
    var delArr = this.state.multipleDelete
    swal({ title: "Are you sure?", icon: "warning", buttons: true, dangerMode: true, }).then((willDelete) => {
      if (willDelete) {
        if (delArr.length > 0) {
          var body = { categoryId: delArr }
        } else {
          var body = { categoryId: [cId] }
        }
        var token = localStorage.getItem('token')
        var url = '/categories/deleteCategories'
        var method = 'post'
        this.props.commonApiCall(url, method, body, token, null, this.props, response => {
          if (response) {
            this.setState({ selectedOption: 'Selecciona aquí' })
            this.getSubcategory()
          }
        })
        swal('Borrado exitosamente', '', 'success')
      }
    });
  }
  changeGrid = () => {
    this.setState({ view: 'Lista', changeButton: 'Vista en cuadrícula' })
  }

  changeList = () => {
    this.setState({ view: 'Cuadrícula', changeButton: 'Vista de la lista' })
  }
  checkArray(_id) {
    let { multipleDelete } = this.state
    if (multipleDelete.includes(_id)) {
      return true
    } else {
      return false
    }
  }
  applyfilter = () => {
    var delArr = this.state.multipleDelete
    if (this.state.selectedOption == 'Delete') {
      if (delArr.length > 0) {
        this.deleteCategory(...delArr)
      }
    }
    if (this.state.selectedOption == 'Publish') {
      if (delArr.length > 0) {
        var body = { categoryId: delArr, publish: true ,isPublish:true }
      }
    }
    if (this.state.selectedOption == 'Un-publish') {
      if (delArr.length > 0) {
        var body = { categoryId: delArr, publish: false,isPublish:true }
      }
    }
    var url = '/categories/changeCategoryStatus'
    var method = 'post'
    var token = localStorage.getItem('token')
    if(delArr.length>0){
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response
      if (data.status == 1) {
        if (this.state.selectedOption == 'Un-publish') {
          swal('SubCategorías no publicadas con éxito!', '', 'success')
        }
        else {
          swal('SubCategorías publicadas con éxito!', '', 'success')
        }
        this.getSubcategory()
      }
      this.setState({ multipleDelete: [], selectedOption: 'Selecciona aquí' })
    })
  }else{
    this.setState({ selectedOption: 'Selecciona aquí' })
    swal('Seleccione al menos una categoría','','info')
  }

  }

  onCheckbox(_id, val) {
    var delarray = this.state.multipleDelete
    if (!delarray.includes(_id)) {
      delarray.push(_id)
    } else {
      delarray.splice(delarray.indexOf(_id), 1)
    }
    if (delarray.length != this.state.subCategorieslist.length) {
      this.setState({ checked: false })
    }
    if (this) this.setState({ multipleDelete: delarray })
  }



  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { subCategorieslist } = this.state
    if (this.state.selectAll) {
      subCategorieslist.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      subCategorieslist.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }
  // ########################### Catagory Status #########################
  changeCatStatus(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { categoryId: [Id], publish: status,isPublish:true }
    var urlkey = '/categories/changeCategoryStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        if (status == false) {
          swal('Desactivado con éxito!', '', 'success')
        }
        else {
          swal('Publicado con éxito!', '', 'success')
        }
        this.getSubcategory()
      } else if (data.message === 'Invalid token') {
        this.props.history.push('/')
      } else {
        swal(data.message, '', 'error')
      }
    })
  }


  // #################################### Update Category ################################


  updateSubCategory = (cId) => {
    this.props.history.push(`/editsubcategory/${cId}`)

  }

  // #################################### Sub Category List ################################

  subcategorylist = (scId, scName) => {
    this.props.editDetails(this.state.parentId)
    this.props.history.push(`/ssCatagoryList/${scId}`)
  }


  // ########################### Set Table Rows #########################

  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { name, image, customUrl, publish, parentCategory, categoryId,pagesize } = this.state
    var body = { image: true, categoryName: name, customUrl, publish, parentCategory:true, categoryId,pageSize:pagesize,type: "subCategory1" }
    var url = '/categories/setFilterForSubCategory1'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      if (response) {
        this.getSubcategory()
        if(e){
          swal('Filas de tabla ¡Actualizadas con éxito!', '', 'success')
        }
      }
    })
  }
  // ########################### getProductName for Search #########################
  getCatNames = e => {
    let token = localStorage.getItem('token')
    var data = { type: "subCategory1", searchText: e }
    if (this.state.parentId2 != '') {
      data.parentCategory = this.state.parentId2
    } else {
      data.parentCategory = this.state.parentId
    }
    var body = data
    var url = '/categories/categoryList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      var mainCatagoryNames = []
      if (data.length > 0) {
        data.map((each, key) => {
          mainCatagoryNames.push(each.categoryName)
          this.setState({ mainCatagoryNames })
        })
      } else {
        this.setState({ mainCatagoryNames: [] })
      }
    })
  }

  handleChange2 = (e) => {
    this.setState({ publishedvalue: e.target.value })
  }


  parentCatName = () => {
    var id = this.props.match.params.scId;
    this.setState({ parentCategory: id })
    let token = localStorage.getItem('token');
    var body = { categoryId: id }
    var url = '/categories/getParentCategoryName'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      this.setState({ parentCategoryName: data.categoryName })
    })
  }
  // #################################### Parent Category Name ################################


  getCategoryName = (e) => {
    let token = localStorage.getItem('token')
    var body = { type: 'category' }
    var url = '/categories/categoryList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (response && response.data && response.data.data) {
        this.setState({ categoryList: data })
      }
    })
  }
  handleChange3(value) {
    let { parentCategoryName } = this.state;
    parentCategoryName = value;
    this.setState({ parentCategoryName, parentId2: parentCategoryName, parentId: parentCategoryName });
  }
  handleChange1 = selectedCatagory => {
    this.setState({ selectedCatagory });
  };


  //*********** SORTING ************************//
  onSort = (column) => {
    let { sortData } = this.state;
    var element, value;
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key];
        element = key;
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState({
          sort: { [element]: value }
        }, () => {
          this.getSubcategory();
        });
        this.setState({ sortData });
      }
      else {
        sortData[key] = false
        element = key;
        value = 1;
      }
    }
    this.setState({ sortData });
  }
  // ########################### PAGINATION #########################

  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize,
    }, () => this.getSubcategory());
    this.setState({ selectAll: false }, () => this.selectAllcheck())

  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 },() => this.setTableRows());
  }
  handleChange_= (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1 ,page_:true},() => this.setTableRows());
    } 
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.getSubcategory());
    // }
  }

  // ########################### download CSV #########################

  downloadCSV(type, array) {
    let { name, customUrl, publish, subCategorieslist, multipleDelete, parentCategory, categoryId, publishedvalue, parentId2, parentId, selectedCatagory, sort } = this.state
    let token = localStorage.getItem('token')
    this.setState({download:true})
    if (array === "totalList") {
      var data = { filteredFields: ["image", "categoryName", "publish", "categoryId", "customUrl", "parentCategory"], type: "subCategory1" }
      if (parentId2 != '') {
        data.parentCategory = [parentId2]
      } else {
        data.parentCategory = [parentId]
      }
    }
    if (array === "filteredList") {
      var data = { filteredFields: [], type: "subCategory1" }
      if (name) {
        data.filteredFields.push('categoryName')
      };
      if (customUrl) {
        data.filteredFields.push('customUrl')
      };
      if (publish) {
        data.filteredFields.push('publish')
      };
      if (categoryId) {
        data.filteredFields.push('categoryId')
      }
      if (multipleDelete.length > 0) {
        data.subCategoryArray = multipleDelete
      }
      if (parentCategory) {
        data.filteredFields.push('parentCategory')
      };
      if (publishedvalue != '') {
        data.publish = publishedvalue
      }
      // if (parentId2 != '') {
      //   data.parentCategory = [parentId2]
      // } else {
      //   data.parentCategory = [parentId]
      // }
      if (selectedCatagory != '') {
        data.categoryName = selectedCatagory
      }
      if (!isEmpty(sort)) {
        data.sort = sort
      }
    }
    var body = data
    var url = '/categories/' + type
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (data) {
        this.setState({ multipleDelete: [], download: false })
        if (type === 'downloadSubCategoriesCsvFile') {
          window.open(CSV_URL + data.filePathAndName, '_blank');
          // FileDownload(CSV_URL  +  data.filePathAndName, data.filePathAndName);
        } else {
          window.open(EXCEL_URL + data.filePathAndName, '_blank');
          // FileDownload(EXCEL_URL  +  data.filePathAndName, data.filePathAndName);
        }
        // swal("Downloaded Successfully",'','success')
      }
    })
  }

  render() {
    const Option = Select.Option;
    let { subCategorieslist, sortData, parentCategoryName, categoryList, selectedOption, parentId, image, page, pagesize, total, length, productCategoryAccess,
      customUrl, publish, categoryId, name, parentCategory, mainCatagoryNames, selectedCatagory, publishedvalue } = this.state
    const filteredOptions = mainCatagoryNames.filter(o => !selectedCatagory.includes(o));
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Gestión por categorías</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/categorylist')}>Catálogo de productos</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/categorylist')} > Categorías</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/subcategorylist')} >Subcategorías</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
        {this.state.download ? <Loader /> : null}
        <div className='card animated fadeIn'>
          <div className="card-header">
            <div className="row data-filter justify-content-between">
              {subCategorieslist.length > 0 ?
                <div className="table-footer mt-0">
                  <label>Demostración</label>
                  <Select showSearch placeholder={<b> {total < 5 ? total : length}</b>} optionFilterProp="children"
                    onSelect={this.handleChangePageSize.bind(this)} 
                    value={this.state.page_ ? pagesize : (length <=pagesize ?length:pagesize)}
                    onSearch={this.handleChange_}
                     >
                    <Option value={5}>5</Option>
                    <Option value={10}>10</Option>
                    <Option value={15}>15</Option>
                    <Option value={50}>50</Option>
                  </Select>
                  <label>Fuera de las{total} categorías</label>
                  <div className="pagination-list">
                    <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                  </div>
                </div> : null}
              <div className="col-lg-7 col-md-12 text-md-right">
                <div className="button-continer text-right">
                  {productCategoryAccess.status == false && productCategoryAccess.delete == false ? null :
                    <>
                      {productCategoryAccess.status && productCategoryAccess.delete ?
                        <Select
                          showSearch
                          placeholder={<b>Select</b>}
                          optionFilterProp="children"
                          value={selectedOption}
                          onSelect={(value) => this.setState({ selectedOption: value })}
                          className="applyselect"
                        >
                          <Option value="Publish">Activar</Option>
                          <Option value="Un-publish">Anular publicación</Option>
                          <Option value="Delete" >Eliminar</Option>
                        </Select> : <>{productCategoryAccess.delete ?
                          <Select
                            showSearch
                            placeholder={<b>Select</b>}
                            optionFilterProp="children"
                            value={selectedOption}
                            onSelect={(value) => this.setState({ selectedOption: value })}
                            className="applyselect"
                          >
                            <Option value="Delete" >Eliminar</Option>
                          </Select> : <Select
                            showSearch
                            placeholder={<b>Select</b>}
                            optionFilterProp="children"
                            value={selectedOption}
                            onSelect={(value) => this.setState({ selectedOption: value })}
                            className="applyselect"
                          >
                            <Option value="Publish">Activar</Option>
                            <Option value="Un-publish">Anular publicación</Option>
                          </Select>} </>}

                      <button type="button" className="btn btn-primary ml-0 apply_btn" onClick={this.applyfilter}>Aplicar</button></>}
                  {productCategoryAccess.create ?
                    <button
                      type='button'
                      className='btn btn-primary'
                      onClick={() => this.props.history.push(`/addsubcategory/${parentId}`)}
                    >
                      <i className='fa fa-plus' /> Añadir subcategoría
            </button> : null}
                  {productCategoryAccess.download ? <div className="dropdown">
                    <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                      <span>Herramientas</span>
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadSubCategoriesExcelFile", "totalList")}>Exportar a Excel</a>
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadSubCategoriesCsvFile", "totalList")}>Exportar a CSV</a>
                    </div>
                  </div> : null}
                  {this.state.view === "Cuadrícula" ?

                    <button type="button" className="btn btn-primary" onClick={this.changeGrid} > <i className="fa fa-bars"></i> <span>Lista</span> </button> :
                    <button type="button" className="btn btn-primary" onClick={this.changeList} ><i className="fa fa-th-large"></i><span>Cuadrícula</span> </button>
                  }
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ name: !this.state.name })} checked={name} /><span></span>Nombre</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ customUrl: !this.state.customUrl })} checked={customUrl} /><span></span>URL personalizada</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ categoryId: !this.state.categoryId })} checked={categoryId} /><span></span>Categoria ID</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ parentCategory: !this.state.parentCategory })} checked={parentCategory} /><span></span>Categoría principal</label></li>
              {productCategoryAccess.status ? <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ publish: !this.state.publish })} checked={publish} /><span></span>Activar</label></li> : null}

            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.getSubcategory}> Reiniciar </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({ name: true, customUrl: true, publish: true, parentCategory: true,categoryId:true })}> Seleccionar todo </button>
            <button className="nav-link pull-right btn btn-primary" type="button" onClick={()=>this.setTableRows('rows')}> Guardar </button>


          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre de subcategoría</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese el nombre de la categoría"
                    value={selectedCatagory}
                    onChange={this.handleChange1}
                    onSearch={(e) => this.getCatNames(e)}
                    onFocus={(e) => this.getCatNames(e)}

                    style={{ width: '100%' }}
                  >
                    {filteredOptions.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>


              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Categoría principal:</label>
                  <div className='col-md-8 co-sm-7'>
                    <Select
                      placeholder="Categoría principal"
                      style={{ width: 250 }}
                      value={parentCategoryName}
                      onChange={(value) => this.handleChange3(value)}
                      onInputKeyDown={this.getCategoryName} >
                      {categoryList.map((each, id) => {
                        return (
                          <Option value={each._id} key={id}>{each.categoryName} </Option>
                        )
                      })}
                    </Select>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Publicado</label>
                  <select className="form-control" onChange={this.handleChange2} value={publishedvalue}>
                    <option value="">Select Option</option>
                    <option value="published">Publicado</option>
                    <option value="unPublished">Sin Activar</option>
                  </select>
                </div>
              </div>
            </div>
            <hr />
            <div className="pull-right filter-button">
              {productCategoryAccess.download ?
                <div className="dropdown ml-2">
                  <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                    <span>Herramientas</span>
                  </button>
                  <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                    <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadSubCategoriesExcelFile", "filteredList")}>Exportar a Excel</a>
                    <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadSubCategoriesCsvFile", "filteredList")}>Exportar a CSV</a>
                  </div>
                </div> : null}
              <button className="nav-link pull-right btn btn-outline-primary  ml-2" type="button" onClick={this.resetListing}> Reiniciar </button>
              <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.getSubcategory('filter')} > Aplicar filtro </button>
            </div>
          </div>
       
        {/* Data table start */}
        {subCategorieslist.length === 0 && this.state.loading === false ? <div className="text-center p-2"><b>No se encontraron registros</b></div> :
          <div>

            {this.state.view === "Cuadrícula" ?
              <div className="row product-list mt-3">
                {subCategorieslist.map((each, id) => {
                  let image = (each.image) ? (IMAGE_URL + each.image + "?" + Math.random()) : "/assets/images/no-imagefound.jpg"
                  return (
                    <div className={each.publish ? "col-lg-3 col-md-4 product-item unpublished-item" : "col-lg-3 col-md-4 product-item"} key={id}>
                      <div className="thumbnail"  >
                        <div className="thumbnail-wrapper" >
                          <a>
                            {productCategoryAccess.status ? <div className="publish-icon" onClick={() => this.changeCatStatus(each.publish, each._id)}><i className="fa fa-power-off"></i></div> : null}
                            {productCategoryAccess.delete == false && productCategoryAccess.status == false ? null :
                              <div className="checkbox">
                                <label>
                                  <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray(each._id)} onChange={() => this.onCheckbox(each._id)} /><span></span>
                                  <i className="input-helper" />
                                </label>
                              </div>}
                            {productCategoryAccess.edit == false && productCategoryAccess.delete == false ? null :
                              <div className="product-setting">
                                {productCategoryAccess.edit ? <i className="fa fa-edit" onClick={() => { this.updateSubCategory(each._id) }} ></i> : null}
                                {productCategoryAccess.delete ? <i className="fa fa-trash" onClick={() => { this.deleteCategory(each._id) }} ></i> : null}
                              </div>}
                          </a>
                          <div className="image-wrapper" onClick={() => this.subcategorylist(each._id, each.categoryName)}>
                            <img src={image} alt={each.categoryName} />
                          </div>
                        </div>
                        <div className="caption">
                          <h5 className="caption-heading">{each.categoryName}</h5>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>

              :
              
                <div className="card-body">
                  <div className='animated fadeIn'>
                    <div className='row'>
                      <div className='col-md-12 mb-2'>
                      </div>
                    </div>
                    <div className='table-responsive'>
                      <table className='table dataTable with-image row-border hover custom-table'>
                        <thead>
                          <tr>
                            {productCategoryAccess.delete == false && productCategoryAccess.status == false ? null :
                              <th><div className="checkbox">
                                <label>
                                  <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} /><span></span>
                                  <i className="input-helper" />
                                </label>
                              </div>
                              </th>}
                            {image ? <th> Imagen </th> : null}
                            {name ? <th sortable-column="categoryName" onClick={this.onSort.bind(this, 'categoryName')}>Nombre <i aria-hidden='true' className={(sortData['categoryName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                            {categoryId ? <th> ID de subcategoría </th> : null}
                            {customUrl ? <th>URL personalizada </th> : null}
                            {parentCategory ? <th>Categoría principal </th> : null}
                            {publish && productCategoryAccess.status ? <th sortable-column="publish" onClick={this.onSort.bind(this, 'publish')}>Activar <i aria-hidden='true' className={(sortData['publish']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} /></th> : null}
                            {productCategoryAccess.edit == false && productCategoryAccess.delete == false ? null : <th>Actions</th>}
                          </tr>
                        </thead>
                        <tbody>

                          {subCategorieslist.map((each, id) => {
                            let image = (each.image) ? (IMAGE_URL + each.image + "?" + Math.random()) : "/assets/images/no-imagefound.jpg"
                            return (
                              <tr className='animated fadeIn' key={id}>
                                {productCategoryAccess.delete == false && productCategoryAccess.status == false ? null :
                                  <td width="10%"><div className="checkbox">
                                    <label>
                                      <input type="checkbox" className="form-check-input" id='deleteCheckbox' checked={this.checkArray(each._id)} onChange={() => this.onCheckbox(each._id)} /><span></span>
                                      <i className="input-helper" />
                                    </label>
                                  </div></td>}
                                {image ? <td> <div className='thumb-img'> <a onClick={() => this.subcategorylist(each._id, each.categoryName)}><img src={image} alt={each.categoryName} /></a> </div> </td> : null}
                                {name ? <td><a href="javascript:void();" onClick={() => this.subcategorylist(each._id, each.categoryName)}>{each.categoryName}</a></td> : null}
                                {categoryId ? <td>{each.customUrl}</td> : null}
                                {customUrl ? <td>{each.categoryId}</td> : null}
                                {parentCategory ? <td>{each.parentCategory}</td> : null}
                                {publish && productCategoryAccess.status ? <td><Toggle checked={each.publish} className='custom-classname' onChange={() => this.changeCatStatus(each.publish, each._id)} /></td> : null}

                                {productCategoryAccess.edit == false && productCategoryAccess.delete == false ? null : <td>
                                  {productCategoryAccess.edit ? <button onClick={() => { this.updateSubCategory(each._id) }}>
                                    <i className='fa fa-pencil-square-o text-primary' aria-hidden='true' />
                                  </button> : null}
                                  {productCategoryAccess.delete ? <button onClick={() => { this.deleteCategory(each._id) }}>
                                    <i className='fa fa-trash text-danger' aria-hidden='true' />
                                  </button> : null}
                                </td>}
                              </tr>
                            )
                          })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              
            }
          </div>}
          </div>



        {/* </div> */}
      </Home>
    )
  }
}

const mapStateToProps = state => ({
  SubCategoryDetails: state.commonReducers.editedDetails,
  permissionsList: state.admindata.rolePermissions,

})

export default connect(mapStateToProps, actions)(SubCategoryManagement)

