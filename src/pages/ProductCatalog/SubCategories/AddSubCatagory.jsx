import React, { Component } from 'react'
import Home from '../../Home'
import Toggle from 'react-toggle'
import axios from 'axios'
import { API_URL, IMAGE_URL } from '../../../config/configs'
import { connect } from 'react-redux'
import * as actions from '../../../actions'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import { Select } from 'antd';
import swal from 'sweetalert'
import { toast } from 'react-toastify';
class InsertSubCategory extends Component {
  constructor(props) {
    super(props)
    this.state = {
      categoryName: '',
      categoryId_:'',
      customUrl: '',
      categoryId: '',
      parentCategoryName: '',
      publish: true,
      parentCategory: '',
      image: '',
      description: '',
      metaTitle: '',
      metaKeyword: '',
      metaDescription: '',
      fileVisible: false,
      categoryList: [],
      searchItem: '',
      errors: {
      }
    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission && permissions.rolePermission.productCategoryAccess && permissions.rolePermission.productCategoryAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }

    this.parentCatName()
    this.getCategoryName()
  }


  parentCatName = () => {
    var id = this.props.match.params.scId;
    this.setState({ parentCategory: id })
    let token = localStorage.getItem('token');
    var body = { categoryId: id }
    var url = '/categories/getParentCategoryName'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data

      this.setState({ parentCategoryName: data.categoryName })
    })
  }

  validateForm = () => {
    let { categoryName,categoryId_, customUrl, parentCategoryName, errors } = this.state
    let formIsValid = true
    if (!categoryName || categoryName.trim() === '') {
      formIsValid = false
      errors['categoryName'] = '* Se requiere nombre de categoría'
    }
    if (!categoryId_ || categoryId_.trim() === '') {
      formIsValid = false
      errors['categoryId_'] = '* Se requiere ID de categoría'
    }
    if (!customUrl || customUrl.trim() === '') {
      formIsValid = false
      errors['customUrl'] = '* Se requiere URL de categoría'
    }
    if (!parentCategoryName || parentCategoryName.trim() === '') {
      formIsValid = false
      errors['parentCategoryName'] = '* Se requiere categoría principal'
    }
    // else formIsValid = true
    this.setState({ errors })
    return formIsValid
  }


  urlGenerator = () => {
    let { categoryName, parentCategory, categoryId } = this.state
    var data = { parentCategory, type: "subCategory1" }
    if (categoryId !== '') {
      data.categoryId = categoryId
      data.category = categoryName
    }
    if (categoryId === '') {
      data.categoryName = categoryName
    }
    var body = data
    var token = localStorage.getItem('token')
    var url = '/categories/addSubCategory'
    var method = 'post'
    if (categoryName.trim() != '') {
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        var { data } = response
        if (data.statusCode === 441) {
          toast.error("Nombre ya existe !", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }
        if (data.statusCode === 228) {
          this.setState({
            customUrl: data.data.customUrl,
            categoryId: data.data._id,
            errors: {}

          })
        }
      })
    }

  }


  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }

  }


  fileUploadSubmit = () => {
    var token = localStorage.getItem('token')
    var formData = new FormData()
    var imagefile = document.querySelector('#file')
    formData.append('file', imagefile.files[0])
    axios
      .post(API_URL + '/auth/fileUpload', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: token
        }
      })
      .then(response => {
        let { data } = response

        this.setState({ image: data.data.filepath })
      })
      .catch(err => console.log(err))
  }

  fileChangedHandler(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file: file,
        filename: filename,
        imagePreviewUrl: reader.result,
        fileVisible: true
      })
    }
    reader.readAsDataURL(file)
  }




  // #################################### Add SubCategory ################################

  addSubCategory = () => {
    if (this.validateForm()) {
      let token = localStorage.getItem('token')
      let { categoryName, categoryId,categoryId_, customUrl, image, metaTitle, metaDescription, metaKeyword, description, publish, parentCategory } = this.state
      var data = { type: "subCategory1", categoryName, customUrl, id:categoryId,categoryId:categoryId_, image, metaTitle, metaDescription, metaKeyword, description, publish }
      data.parentCategory = parentCategory
      var body = data
      var url = '/categories/addSubCategory'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          swal({
            title: "Subcategoría añadida con éxito!",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
              this.props.history.push(`/subcategorylist/${parentCategory}`)
              // }
            })
        }
        if (data.statusCode === 457) {
          toast.error("¡El Id. De categoría ya existe!", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
        }
      })
    }

  }



  // #################################### Parent Category Name ################################


  getCategoryName = (e) => {
    let token = localStorage.getItem('token')
    var body = { type: 'category' }
    var url = '/categories/categoryList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      var data = response.data.data
      if (response && response.data && response.data.data) {
        this.setState({ categoryList: data })
      }
    })
  }

  metaFileUpload() {
    var { categoryId, file } = this.state
    var token = localStorage.getItem('token')
    var formData = new FormData()
    formData.append('imageType', "image")
    formData.append('categoryId', categoryId)
    formData.append('file', file)
    axios.post(API_URL + '/auth/fileUploadForCategoryImage', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: token
      }
    })
      .then(response => {
        let { data } = response.data
        this.setState({ image: data.filepath, fileVisible: false, file: '' })
      })
      .catch(err => console.log(err))

  }

  handleChange2(value) {
    let { parentCategoryName } = this.state;
    parentCategoryName = value;
    this.setState({ parentCategoryName, parentCategory: parentCategoryName });
  }
  // ################################

  render() {

    const Option = Select.Option;
    let { errors, categoryName,categoryId_, customUrl, parentCategoryName, categoryList, image, metaTitle, metaDescription, metaKeyword, description, publish, parentCategory } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Gestión por categorías</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')} > Página principal </li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/categorylist')}>Catálogo de productos</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/categorylist')} > Categorías</li>
                <li className='breadcrumb-item active'>Añadir subcategoría </li>
              </ul>
            </div>
          </div>
        </div>

        <div className='card animated fadeIn'>
          <div className="card-header" data-spy="affix" data-offset-top="100">
            <div className="button-continer text-right">
              <button type='button' className='btn btn-primary' onClick={this.addSubCategory} > <span>Añadir</span> </button>
              <button type='button' className='btn btn-outline-primary' onClick={() => this.props.history.push(`/subcategorylist/${parentCategory}`)} > Cancelar </button>
            </div>
          </div>
          <div className='card-body'>
            <div className='card-center-block'>
              <form className='form-sample'>
                <div className='form-group row'>
                  <label className='col-lg-2 col-sm-3 col-form-label'>Nombre <span className="text-danger">*</span></label>
                  <div className='col-md-8 co-sm-7'>
                    <input
                      className='form-control'
                      type='text'
                      name='categoryName'
                      placeholder='Ingrese el nombre de la categoría'
                      value={categoryName}
                      onChange={this.handleChange}
                      onBlur={this.urlGenerator}
                    />
                    <span className="error-block"> {errors.categoryName} </span>
                  </div>
                </div>
                <div className='form-group row'>
                  <label className='col-lg-2 col-sm-3 col-form-label'>Categoria ID <span className="text-danger">*</span></label>
                  <div className='col-md-8 co-sm-7'>
                    <input
                      className='form-control'
                      type='text'
                      name='categoryId_'
                      placeholder='Ingrese el nombre de la categoría'
                      value={categoryId_}
                      onChange={this.handleChange}
                      onBlur={this.urlGenerator}
                    />
                    <span className="error-block"> {errors.categoryId_} </span>
                  </div>
                </div>
                <div className='form-group row'>
                  <label className='col-lg-2 col-sm-3 col-form-label'>URL personalizada <span className="text-danger">*</span></label>
                  <div className='col-md-8 co-sm-7'>
                    <input
                      className='form-control'
                      type='text'
                      name='customUrl'
                      placeholder='Ingresar URL personalizada'
                      value={customUrl}
                      onChange={this.handleChange}
                    />
                    <span className="error-block"> {errors.customUrl} </span>
                  </div>
                </div>
                <div className='form-group row'>
                  <label className='col-lg-2 col-sm-3 col-form-label'>
                  Categoría principal <span className="text-danger">*</span>
                  </label>
                  <div className='col-md-8 co-sm-7'>
                    <Select
                      placeholder="Categoría principal"
                      style={{ width: 600 }}
                      value={parentCategoryName}
                      onChange={(value) => this.handleChange2(value)}
                      onInputKeyDown={this.getCategoryName} >
                      {categoryList.map((each, id) => {
                        return (
                          <Option value={each._id} key={id}>{each.categoryName} </Option>
                        )
                      })}
                    </Select>
                    <span className="error-block"> {errors.parentCategoryName} </span>

                  </div>
                </div>
                <div className='form-group row'>
                  <label className='col-lg-2 col-sm-3 col-form-label'>Imagen</label>
                  <div className="drop-section mb-3 col-md-4">
                    <div className="form-group inputDnD mb-0">
                      <div className="upload-overlay d-flex justify-content-center align-items-center">
                        <div className="upload-wrapper">
                          <i className="fa fa-upload"></i>
                          <span><button type="button"  >Elige un archivo</button> o arrástralo aquí</span>
                        </div>
                      </div>
                      <label className="sr-only" >Subir archivo</label>
                      <input type="file" className="form-control-file text-primary font-weight-bold" name='image'
                        id="file" accept="image/*" data-title="Drag and drop a file"
                        onChange={this.fileChangedHandler.bind(this)} />
                    </div>
                    {this.state.fileVisible ?
                      <span>
                        <div className='product-img'> <img src={this.state.imagePreviewUrl} alt={this.state.imagePreviewUrl} /></div>
                          <span> <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ fileVisible: !this.state.fileVisible, file: '' })} /></span>
                        
                        <span>{this.state.filename}</span>
                      </span> : null}
                    <div className="button-continer pull-right my-2">
                      <button className="btn btn-primary" type="button" onClick={() => this.metaFileUpload()} >Cargar imagen</button>

                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="row justify-content-between my-2">
                      <div className="col-md-12 select-image"><div className="product-img">
                        {image ? <img src={IMAGE_URL + image + "?" + Math.random()} alt={image} /> : <img src="/assets/images/no-image-icon-4.png" alt="no-image-found" />}
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='form-group row'>
                  <label className='col-lg-2 col-sm-3 col-form-label'>Descripción:</label>
                  <div className='col-md-8 co-sm-7'>
                    <CKEditor
                      editor={ClassicEditor}
                      data={description}
                      onInit={editor => {
                      }}
                      onChange={(event, editor) => {
                        const data = editor.getData()
                        this.setState({ description: data })
                      }}
                    />
                  </div>
                </div>
                <div className='form-group row'>
                  <label className='col-lg-2 col-sm-3 col-form-label'>Meta título:</label>
                  <div className='col-md-8 co-sm-7'>
                    <input
                      className='form-control'
                      type='text'
                      name='metaTitle'
                      placeholder='Meta título:'
                      value={metaTitle}
                      onChange={this.handleChange}
                    />

                  </div>
                </div>
                <div className='form-group row'>
                  <label className='col-lg-2 col-sm-3 col-form-label'>
                  Meta palabra clave:
                  </label>
                  <div className='col-md-8 co-sm-7'>
                    <textarea
                      className='form-control'
                      type='text'
                      name='metaKeyword'
                      placeholder='Introduzca palabra clave meta'
                      value={metaKeyword}
                      onChange={this.handleChange}
                    />

                  </div>
                </div>
                <div className='form-group row'>
                  <label className='col-lg-2 col-lg-2 col-sm-3 col-form-label'>
                  Metadescripción :
                  </label>
                  <div className='col-md-8 co-sm-7'>
                    <textarea
                      className='form-control'
                      type='text'
                      name='metaDescription'
                      placeholder='Introduzca palabra clave meta'
                      value={metaDescription}
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <div className='form-group row'>
                  <label
                    className='col-lg-2 col-sm-3 col-form-label'
                    htmlFor='material-switch'
                  >
                    Publicado: <span className="text-danger">*</span>
                  </label>
                  <div className='col-md-8 co-sm-7'>
                    <Toggle
                      checked={publish}
                      className='custom-classname'
                      onChange={() => this.setState({ publish: !this.state.publish })}
                    />
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Home>
    )
  }
}


const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(InsertSubCategory)