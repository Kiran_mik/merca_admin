import React, { Component } from 'react';
import Home from '../Home';
import * as actions from '../../actions'
import { API_URL, IMAGE_URL } from '../../config/configs'
import { connect } from 'react-redux'
import { BeatLoader } from 'react-spinners';

class HomeSection extends Component {
    route(path) {
        this.props.history.push(path);
    }
    state = {
        page: 1,
        pagesize: 50,
        homepagelist: [],
        homeBannerAccess: {},
        loading: true


    }
    componentDidMount() {
        var permissions = this.props.permissionsList
        if (permissions && permissions.rolePermission && permissions.rolePermission.homeBannerAccess && permissions.rolePermission.homeBannerAccess.view === false) {
            this.props.history.push('/dashboard')
        }

        if (permissions.rolePermission) {
            let { homeBannerAccess } = permissions.rolePermission
            this.setState({ homeBannerAccess: homeBannerAccess })
        }
        this.homePageDetails()

    }

    // #################################### Get HomePageList ################################
    homePageDetails = () => {
        var token = localStorage.getItem("token")
        var url = '/home/homePageDetails'
        var method = 'get'
        var token = localStorage.getItem('token')
        this.props.commonApiCall(url, method, null, token, null, this.props, response => {
            let { data } = response.data
            let list = data[0].sectionData
            if (data && list) {
                this.setState({ homepagelist: list, loading: false })
            }
        })
    }
    // ####################################

    render() {
        let { homepagelist, homeBannerAccess } = this.state
        return (
            <Home>
                <div className="row">
                    <div className="col-md-12">
                        <div className="page-header">
                            <h3>Gestión de la sección de inicio</h3>
                            <ul className="breadcrumb ">
                                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                                <li className="breadcrumb-item" >Gestión de contenido</li>
                                <li className="breadcrumb-item active" >Gestión de la sección de inicio</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />

                <div className="card animated fadeIn">
                    <div className='card-header'>
                        <div className="data-filter" data-spy="affix" data-offset-top="100">
                            <div className="row">
                                <div className="col-md-6">
                                    <h5 className="mt-2 card-title" >Sección deslizante </h5>
                                </div>
                                <div className="col-md-6">
                                    <div className="button-continer text-right">
                                        {homeBannerAccess.edit ? <button type="button" className="btn btn-primary" onClick={() => this.props.history.push('/edithomesection')}><i className="fa fa-pencil-square-o" /><span>{homepagelist.length >0 ?"Edit":"Add"}</span> </button> : null}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {homepagelist.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                        <div className="card-body">
                            {homepagelist.map((one, id) => {
                                let image = (one.bannerImage) ? (IMAGE_URL + one.bannerImage) : "../assets/images/no-imagefound.jpg"
                                return (
                                    <div key={id} >
                                        <h5><b>Diapositiva{id + 1}</b></h5>
                                        <hr />
                                        <div className='my-5' >
                                            <form className="form-sample">
                                                <div className="form-group row">
                                                    <label className="col-lg-2 col-sm-3 col-form-label">Título :</label>
                                                    <div className="col-md-8 col-sm-7">
                                                        <p>{one.title}</p>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-lg-2 col-sm-3 col-form-label">Descripción :</label>
                                                    <div className="col-md-8 col-sm-7">
                                                        <p>{one.description}</p>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-lg-2 col-sm-3 col-form-label">Posición :</label>
                                                    <div className="col-md-8 col-sm-7">
                                                        <p>{one.position?one.position:'-'}</p>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-lg-2 col-sm-3 col-form-label">Color :</label>
                                                    <div className="col-md-8 col-sm-7">
                                                        <p>{one.color?one.color:'-'}</p>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-lg-2 col-sm-3 col-form-label">Imagen de banner :</label>
                                                    <div className="col-md-8 col-sm-7 banner-section">
                                                        <img src={image} alt={one.title} />
                                                    </div>
                                                </div>
                                                
                                                <div className="form-group row">
                                                    <label className="col-lg-2 col-sm-3 col-form-label">Imagen de banner celular:</label>
                                                    <div className="col-md-8 col-sm-7 banner-section mobile-banner-img">
                                                        <div className="inner-image ">
                                                            <img src={one.mobileBannerImage ? IMAGE_URL + one.mobileBannerImage : "/assets/images/no-image-icon-4.png"} alt={one.mobileBannerImage} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-group row">
                                                    <label className="col-lg-2 col-sm-3 col-form-label">URL del banner:</label>
                                                    <div className="col-md-8 col-sm-7">
                                                        <a href={one.bannerUrl ? one.bannerUrl : "javascript:;"} target="_blank" className="mt-1 d-block">{one.bannerUrl}</a> 
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>}
                </div>
            </Home>
        );
    }
}


const mapStateToProps = state => ({
    permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(HomeSection)