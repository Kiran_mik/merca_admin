import React, { Component } from 'react'
import Home from '../Home'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import axios from 'axios'
import { API_URL, IMAGE_URL } from '../../config/configs'
import SimpleReactValidator from 'simple-react-validator'
import swal from 'sweetalert'
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import { BeatLoader } from 'react-spinners';


class EditHomeSection extends Component {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      page: 1,
      pagesize: 10,
      homepagelist: [
        {
          categoryName: '',
          title: '',
          description: '',
          color: '',
          position: '',
          bannerImage: '',
          mobileBannerImage: '',
          bannerUrl: '',

        }
      ],
      metatitle: '',
      metadescription: '',
      metakeyword: '',
      photo: '',
      homeId: '',
      isUpdate: false,
      errors: {
        metadescription: '',
        metatitle: '',
        metakeyword: '',
        homepagelist: {
          title: '',
          bannerUrl: ''
        }
      },
      homeBannerAccess: {},

      src: null,
      mob_src: null,
      crop: {
        unit: 'px',
        width: 1500,
        height: 350,
      },
      cropBanner: {
        x: 10,
        y: 10,
        unit: 'px',
        width: 415,
        height: 275,
      },
      loading: true

    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission && permissions.rolePermission.homeBannerAccess && permissions.rolePermission.homeBannerAccess.view === false) {
      this.props.history.push('/dashboard')
    }

    if (permissions.rolePermission) {
      let { homeBannerAccess } = permissions.rolePermission
      this.setState({ homeBannerAccess: homeBannerAccess })
    }
    this.homePageDetails()
  }
  // componentWillMount() {
  //   this.validator = new SimpleReactValidator();
  // }
  handleChange = (e) => {
    if (['categoryName', 'title', 'color', 'position', 'description', 'bannerUrl', 'bannerImage'].includes(e.target.title)) {
      let homepagelist = [...this.state.homepagelist]
      homepagelist[e.target.dataset.id][e.target.title] = e.target.value
      this.setState({ homepagelist })
    } else {
      this.setState({ [e.target.name]: e.target.value })
    }
  }

  plus = () => {
    this.setState(prevState => ({
      homepagelist: [
        ...prevState.homepagelist,
        {
          categoryName: '',
          title: '',
          color: '',
          position: '',
          description: '',
          bannerImage: '',
          mobileBannerImage: '',
          bannerUrl: '',
          errors: {},
        }
      ]
    }))
  }

  delete(key) {
    swal({ title: "Are you sure?", icon: "warning", buttons: true, dangerMode: true, })
      .then((willDelete) => {
        if (willDelete) {
          var homepagelist = this.state.homepagelist;
          homepagelist.splice(key, 1);
          this.setState({ homepagelist })
          swal('Borrado exitosamente', '', 'success')
        }
      });

  }

  // ########################### add/update HomePageDetails #######################
  addHomePageDetails = (e) => {
    if (this.validator.allValid()) {
      e.preventDefault()
      let { metatitle, metadescription, metakeyword, homepagelist } = this.state
      let body = { metaTitle: metatitle, metaDescription: metadescription, metaKeyword: metakeyword, sectionData: homepagelist }
      var token = localStorage.getItem("token")
      var url = '/home/addHomePageDetails'
      var method = 'post'
      var token = localStorage.getItem('token')
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          swal({ title: "Inicio Banner (s) actualizado con éxito", icon: "success", })
            .then((willDelete) => {
              if (willDelete) {
                this.props.history.push('/homesection')
              }
            })
        }
      })
    }
    else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  }
  // ########################### Fileupload #######################
  fileUploadSubmit = (key, name) => {
    // var ImageURL = this.state.b64Img;
    // var block = ImageURL.split(";");
    // var contentType = block[0].split(":")[1];
    // var realData = block[1].split(",")[1];
    // var blob = this.b64toBlob(realData, contentType);
    var token = localStorage.getItem('token')


    if (name === 'banner') {
      var ImageURL = this.state.b64Img;
      var block = ImageURL.split(";");
      var contentType = block[0].split(":")[1];// In this case "image/gif"
      var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."
    } else if (name === "mobileBanner") {
      var ImageURL = this.state.b64Img1;
      var block = ImageURL.split(";");
      var contentType = block[0].split(":")[1];// In this case "image/gif"
      var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."
    }
    //  if(name==="image"||name==="categoryImage"){
    var blob = this.b64toBlob(realData, contentType);
    //  }





    var formData = new FormData();
    formData.append("file", blob);
    axios.post(API_URL + '/auth/fileUpload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': token
      }
    })
      .then((response) => {
        let { data } = response;
        var homepagelist_obj = this.state.homepagelist[key];
        if (name === 'banner') {
          homepagelist_obj["bannerImage"] = data.data.filepath;
          this.state.homepagelist[key] = homepagelist_obj
          this.setState({ homepagelist: this.state.homepagelist, croppedImageUrl: null, src: null });
        }
        else if (name === 'mobileBanner') {
          homepagelist_obj["mobileBannerImage"] = data.data.filepath;
          this.state.homepagelist[key] = homepagelist_obj
          this.setState({ homepagelist: this.state.homepagelist, croppedImageUrl1: null, mob_src: null });
        }

      })
      .catch(err => console.log(err));
  }
  // ########################### HomePageDetails #######################
  homePageDetails = () => {
    var token = localStorage.getItem("token")
    var url = '/home/homePageDetails'
    var method = 'get'
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, null, token, null, this.props, res => {
      let { data } = res
      if (data && data.data.length > 0) {
        this.setState({
          homeId: data.data[0]._id,
          metatitle: res.data.data[0].metaTitle,
          metadescription: res.data.data[0].metaDescription,
          metakeyword: res.data.data[0].metaKeyword,
          homepagelist: res.data.data[0].sectionData,
          isUpdate: true,
          loading: false
        })
      }
    })
  }

  // ################ XXXXXX ################

  onSelectFile = (e, item) => {
    var _ = require('lodash')
    var updateImage = _.map(this.state.homepagelist, obj => (obj.selected = false))
    item.selected = true
    this.setState({
      homepagelist: this.state.homepagelist,
    })
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () =>
        this.setState({ src: reader.result })
      );
      reader.readAsDataURL(e.target.files[0]);
    }
  };



  onImageLoaded = image => {
    this.imageRef = image;
  };

  onCropComplete = crop => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop, item) => {
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        "newFile.jpeg"
      );
      this.setState({ croppedImageUrl });
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = canvas.toDataURL('image/jpeg');
        var item_image = this.fileUrl.replace(/^data:image\/(png|jpg);base64,/, "");
        this.setState({ b64Img: item_image })
        resolve(this.fileUrl);
      }, "image/jpeg");
    });
  }

  onSelectFile1 = (e, item) => {
    var _ = require('lodash')
    var updateImage = _.map(this.state.homepagelist, obj => (obj.selected1 = false))
    item.selected1 = true
    this.setState({
      homepagelist: this.state.homepagelist,
    })
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () =>
        this.setState({ mob_src: reader.result })
      );
      reader.readAsDataURL(e.target.files[0]);
    }
  }; onImageLoaded1 = (image) => {
    this.imageRef1 = image;
  };

  onCropComplete1 = (crop) => {
    this.makeClientCrop1(this.state.cropBanner);
  };

  onCropChange1 = (cropBanner) => {
    this.setState({ cropBanner: cropBanner });
  };

  async makeClientCrop1(cropBanner) {
    if (this.imageRef1 && cropBanner.width && cropBanner.height) {
      const croppedImageUrl1 = await this.getCroppedImg1(
        this.imageRef1,
        cropBanner,
        "newFile.jpeg"
      );
      this.setState({ croppedImageUrl1 });
    }
  }

  getCroppedImg1(image, crop, fileName) {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );


    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = canvas.toDataURL('image/jpeg');
        var item_image = this.fileUrl.replace(/^data:image\/(png|jpg);base64,/, "");
        this.setState({ b64Img1: item_image })
        resolve(this.fileUrl);
      }, "image/jpeg");
    });
  }

  b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }



  render() {
    let { homepagelist, mob_src, cropBanner, homeBannerAccess, metatitle, metadescription, metakeyword, isUpdate, crop, croppedImageUrl, croppedImageUrl1, src } = this.state
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Editar banner de inicio </h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" >Gestión de contenido</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/homesection')}>Gestión de la sección de inicio</li>
                <li className="breadcrumb-item active" >Editar banner de inicio </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="card animated fadeIn mb-2">
          <div className="card-header">
            <h5 className='card-title'>Metadatos</h5>
          </div>
          <div className='card-body'>
            <form className='form-sample'>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Meta título :</label>
                <div className='col-md-8 col-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='metatitle'
                    placeholder='Introducir Meta título'
                    id='metatitle'
                    required
                    maxLength='70'
                    value={metatitle}
                    onChange={this.handleChange}
                  />
                  <span className="notes">Máximo 70 caracteres es adecuado </span>
                  {this.validator.message('metatitle', this.state.metatitle, 'required')}
                </div>
              </div>

              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>
                  Metadescripción :
                </label>
                <div className='col-md-8 col-sm-7'>
                  <textarea
                    className='form-control'
                    type='text'
                    name='metadescription'
                    placeholder='Ingrese la descripción'
                    id='metadescription'
                    required
                    maxLength='100'
                    value={metadescription}
                    onChange={this.handleChange}
                  />
                  <span className="notes">Máximo 100 caracteres.. </span>
                  {this.validator.message('metadescription', this.state.metadescription, 'required')}
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Meta palabra clave :</label>
                <div className='col-md-8 col-sm-7'>
                  <textarea
                    className='form-control'
                    type='text'
                    name='metakeyword'
                    placeholder='Introduzca palabra clave meta'
                    id='metakeyword'
                    required
                    maxLength='156'
                    value={metakeyword}
                    onChange={this.handleChange}
                  />
                  <span className="notes">Máximo 156 caracteres es adecuado. </span>
                  <p className="error-msg">{this.validator.message('metakeyword', this.state.metakeyword, 'required')}</p>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className='card animated fadeIn mb-2'>
          <div className='card-header'>
            <h5 className='card-title'>Sección deslizante </h5>
          </div>
          <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
          <div className='card-body'>
            <form className='form-sample'>
              {homepagelist.map((item, key) => {
                let categoryNameId = `categoryNameId-${key}`
                let titleId = `titleId-${key}`
                let positionId = `positionId-${key}`
                let colorId = `colorId-${key}`
                let descriptionId = `descriptionId-${key}`
                let bannerImageId = `bannerImageId-${key}`
                let mobileBannerImageId = `mobileBannerImageId-${key}`
                let bannerUrlId = `bannerUrl-${key}`
                return (
                  <div key={key} >
                    <div className="row justify-content-between">
                      <h5 className="col-md-10"><b>Slide{key + 1}</b></h5>
                      <div className="col-md-2 pull-right text-right">
                        {homeBannerAccess.delete ? <button type='button' className='btn btn-danger ' onClick={this.delete.bind(this, key)}> <i className='fa fa-trash' /> Eliminar </button> : null}
                      </div>
                    </div>
                    <hr />
                    <div className='my-5'>
                      <div className='form-group row'>
                        <label className='col-lg-2 col-sm-3 col-form-label'>Título :</label>
                        <div className='col-md-8 col-sm-7'>
                          <input
                            type='text'
                            name={titleId}
                            data-id={key}
                            id={titleId}
                            value={item.title}
                            className='form-control'
                            title='title'
                            placeholder='Ingrese Título'
                            onChange={this.handleChange}
                          />
                          {this.validator.message('title', homepagelist[key].title, 'required')}
                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-lg-2 col-sm-3 col-form-label'>Descripción :</label>
                        <div className='col-md-8 col-sm-7'>
                          <input
                            type='text'
                            name={descriptionId}
                            data-id={key}
                            id={descriptionId}
                            value={item.description}
                            className='form-control'
                            title='description'
                            placeholder='Ingrese Descripción'
                            onChange={this.handleChange}
                          />
                        </div>
                      </div>
                      {/* <div className='form-group row'>
                        <label className='col-lg-2 col-sm-3 col-form-label'>Posición :</label>
                        <div className='col-md-8 col-sm-7'>
                          <input
                            type='text'
                            name={positionId}
                            data-id={key}
                            id={positionId}
                            value={item.position}
                            className='form-control'
                            title='position'
                            placeholder='Ingresar posición'
                            onChange={this.handleChange}
                          />
                        </div>
                      </div> */}

                      <div className='form-group row'>
                        <label className='col-lg-2 col-sm-3 col-form-label'>Posición :</label>
                        <div className='col-md-8 co-sm-7'>
                          <select className="form-control col-md-4 mr-2"
                            placeholder={<h5><b>Seleccione uno</b></h5>}  
                             name={positionId}
                            data-id={key}
                            title='position'
                            id={positionId}
                            value={item.position} 
                            onChange={this.handleChange}>
                            <option value="">Seleccionar</option>
                            <option value="left">Left</option>
                            <option value="right">Right</option>
                          </select>
                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-lg-2 col-sm-3 col-form-label'>Color :</label>
                        <div className='col-md-8 col-sm-7'>
                          <input
                            type='text'
                            name={colorId}
                            data-id={key}
                            id={colorId}
                            value={item.color}
                            className='form-control'
                            title='color'
                            placeholder='Ingrese Color'
                            onChange={this.handleChange}
                          />
                        </div>
                      </div>
                      <div className='form-group row'>
                        <label className='col-lg-2 col-sm-3 col-form-label'>
                          Imagen de banner :
                        </label>
                        <div className="col-md-8 col-sm-7 banner-section">
                          <div className="inner-image ">
                            <img src={item.bannerImage ? IMAGE_URL + item.bannerImage : "/assets/images/no-image-icon-4.png"} alt={item.bannerImage} />
                          </div>

                          <div className="upload-file">
                            <label>
                              <input className="file-input" type="file" accept="image/*" data-id={key} title='bannerImage' name={bannerImageId} id={"file" + key} onChange={(e) => this.onSelectFile(e, item)} />
                            </label>
                            <a> <i className="fa fa-camera" aria-hidden="true" ></i> </a>
                          </div>

                          <span className="notes">El tamaño de la imagen del control deslizante debe ser '1500 * 350' </span>
                          {this.validator.message('bannerImage', homepagelist[key].bannerImage, 'required')}
                        </div>
                      </div>
                      <div className="row">
                        <div className='col-lg-2 col-sm-3'>
                        </div>
                        <div className="App">
                          {item.selected ? <>
                            {src && (
                              <ReactCrop
                                src={src}
                                crop={crop}
                                minHeight={100}
                                maxHeight={350}
                                minWidth={1500}
                                maxWidth={1500}
                                locked={true}
                                onImageLoaded={this.onImageLoaded}
                                onComplete={this.onCropComplete}
                                onChange={(e) => this.onCropChange(e, key, item)}
                                type="file"
                                data-id={key}
                                title='bannerImage'
                                name={bannerImageId}
                                accept="image/*"
                                id={"file" + key}
                              />
                            )}
                            {croppedImageUrl && (
                              <img alt="Crop" style={{ maxWidth: "100%" }} src={croppedImageUrl} />
                            )}
                            {src ? <button className="btn btn-primary" type="button" onClick={() => this.fileUploadSubmit(key, 'banner')}>Cargar imagen</button> : null}
                          </>
                            : null}
                        </div>
                      </div>

                      <div className='form-group row mt-4'>
                        <label className='col-lg-2 col-sm-3 col-form-label'> Imagen de banner celular: </label>
                        <div className="col-md-8 col-sm-7 banner-section mobile-banner-img">
                          <div className="inner-image ">
                            <img src={item.mobileBannerImage ? IMAGE_URL + item.mobileBannerImage : "/assets/images/no-image-icon-4.png"} alt={item.mobileBannerImage} />
                          </div>
                          <div className="upload-file">
                            <label>
                              <input className="file-input" type="file" accept="image/*" data-id={key} title='mobileBannerImage' name={mobileBannerImageId} id={"file" + key} onChange={(e) => this.onSelectFile1(e, item)} />

                            </label>
                            <a> <i className="fa fa-camera" aria-hidden="true" ></i> </a>
                          </div>
                          <span className="notes">El tamaño de la imagen del control deslizante debe ser '415 * 275' </span>
                          {this.validator.message('mobileBannerImage', homepagelist[key].mobileBannerImage, 'required')}
                        </div>
                      </div>
                      <div className="row">
                        <div className='col-lg-2 col-sm-3'>
                        </div>
                        <div className="close-icn">
                          {item.selected1 ? <>
                            {mob_src && (
                              <>
                                <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ mob_src: '' })} />

                                <ReactCrop
                                  src={mob_src}
                                  crop={cropBanner}
                                  minHeight={100}
                                  maxHeight={275}
                                  minWidth={415}
                                  maxWidth={415}
                                  locked={true}
                                  onImageLoaded={this.onImageLoaded1}
                                  onComplete={this.onCropComplete1}
                                  onChange={(e) => this.onCropChange1(e, key, item)}
                                  type="file"
                                  data-id={key}
                                  title='bannerImage'
                                  name={bannerImageId}
                                  accept="image/*"
                                  id={"file" + key}
                                />

                                {croppedImageUrl1 && (
                                  <img alt="Crop" style={{ maxWidth: "100%" }} src={croppedImageUrl1} />
                                )}
                                {mob_src ? <button className="btn btn-primary" type="button" onClick={() => this.fileUploadSubmit(key, 'mobileBanner')}>Cargar imagen</button> : null}
                              </>)}
                          </>
                            : null}
                        </div>
                      </div>

                      <div className='form-group row mt-4'>
                        <label className='col-lg-2 col-sm-3 col-form-label'> URL del banner : </label>
                        <div className='col-md-8 col-sm-7'>
                          <input
                            type='text'
                            name={bannerUrlId}
                            data-id={key}
                            id={bannerUrlId}
                            value={item.bannerUrl}
                            className='form-control'
                            title='bannerUrl'
                            placeholder='Enter bannerUrl'
                            onChange={this.handleChange}
                          />
                          <p style={{ color: 'red' }}>{this.validator.message('bannerUrl', homepagelist[key].bannerUrl, 'required')}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })}
              <hr />
              <div className='button-continer row justify-content-between'>
                {homepagelist.length > 1 || isUpdate ?
                  <div className='button-group-container'>
                    {homeBannerAccess.create ? <button type='button' className='btn btn-success' onClick={this.addHomePageDetails} >Actualizar</button> : null}
                  </div> :
                  <div className='button-group-container'>
                    <button type='button' className='btn btn-primary' onClick={this.addHomePageDetails} ><i className='far fa-save' /> Guardar</button>
                  </div>
                }
                {homeBannerAccess.create ? <button type='button' className='btn btn-primary float-right' onClick={this.plus} > <i className='fa fa-plus' /> Añadir </button> : null}
              </div>
            </form>
            <div className='form-group row'>
            </div>
          </div>
        </div>


      </Home>
    )
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(EditHomeSection)
