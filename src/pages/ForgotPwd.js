import React, { Component } from 'react';
import swal from 'sweetalert';
import axios from 'axios';
import { API_URL } from '../config/configs'

class ForgotPassword extends Component {
  constructor() {
    super();
    this.state = {
      emailId: '',
    }
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
  }

  //******************** Forgot password **************************/ 

  forgotPassword = (e) => {
    e.preventDefault();
    var body = { emailId: this.state.emailId }
    axios({
      "method": "post",
      url: API_URL + '/admins/forgotPasswordForAdmin',
      headers: {
        "Content-type": "application/json",
      },
      data: JSON.stringify(body)
    })
      .then(response => {
        let { data } = response
        if (data.status === 1) {
          swal('¡Por favor revise el correo electrónico registrado!', '', 'success');
        } else {
          swal('Ingrese su correo electrónico registrado', '', 'error');
        }
      })
      .catch((err) => console.log('err', err))
  }

  render() {
    let { emailId } = this.state;
    return (
      <div className="container-scroller">
        <div className="container-fluid page-body-wrapper full-page-wrapper">
          <div className="content-wrapper d-flex align-items-center auth login-full-bg">
            <div className="row w-100 margin-l-0">
              <div className="col-lg-6 col-sm-8 mx-auto authentication-form">
                <div className="login-header">
                  <img src='assets/images/logo.svg' alt='logo' />
                  <h2>Se te olvidó tu contraseña</h2>
                </div>
                <div className="auth-form-light text-left p-5">
                  <form autoComplete="off" onSubmit={this.forgotPassword} >
                    <h3 className="pb-2 text-center">Ingrese su dirección de correo electrónico a continuación</h3>
                    <div className="form-group" >
                      <input className="form-control" type="email" name="emailId" value={emailId} placeholder="Introduzca la dirección de correo electrónico" onChange={this.handleChange} />
                      <i className="fa fa-user" aria-hidden="true" />
                    </div>

                    <div className="mt-5 row">
                      <div className="col-md-6"> <button className="btn btn-block btn-outline-primary btn-lg font-weight-medium" type="submit" onClick={() => this.props.history.push('/')}>Cancelar</button></div>
                      <div className="col-md-6">  <button className="btn-block  btn btn-primary btn-lg font-weight-medium" type="button" onClick={this.forgotPassword}>Enviar</button></div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


export default ForgotPassword;
