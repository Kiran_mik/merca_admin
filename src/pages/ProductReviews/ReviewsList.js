import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import "bootstrap-less";
import { connect } from "react-redux";
import 'rc-pagination/assets/index.css';
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'
import Pagination from 'rc-pagination';
import { isEmpty } from "lodash"
import { BeatLoader } from 'react-spinners';
import { Select } from 'antd';
import 'antd/dist/antd.css';
import * as actions from '../../actions';
import StarRatings from 'react-star-ratings';
import Home from '../Home'
import Toggle from 'react-toggle'
import swal from 'sweetalert'
import { API_URL, CSV_URL, EXCEL_URL } from '../../config/configs'
import axios from 'axios'
import $ from 'jquery';
import _ from "lodash"

const FileDownload = require('js-file-download')


class ReviewManagement extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      pagesize: 20,
      multipleDelete: [],
      searchText: '',
      best_sellers: true,
      reviewListing: [],
      total: 0,
      length: '',
      rating: true,
      reviewerName: true,
      selectAll: false,
      productName: true,
      comments: true,
      status: true,
      sort: {},
      selectedOption: 'Select here',
      sortData: { rating: false, reviewerName: false, productName: false, description: false, isActive: false },
      selectedName: [],
      listOfNames: [],
      selectedProduct: [],
      listOfProducts: [],
      publishedvalue: '',
      max_rating: '',
      min_rating: '',
      listedProducts: [],
      loading: true,
      page_:false

    };
  }

  componentDidMount() {
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').stop().slideToggle();
    });

    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').stop().slideToggle();
    });
    $('[data-toggle="tooltip"]').tooltip();
    this.listOfReviews('pagesize')
  }


  // ############################## Review Listing #############################
  listOfReviews = (e) => {
    let { page, pagesize, sort, selectedName, selectedProduct, publishedvalue, min_rating, max_rating } = this.state
    var data = {  }
    if (e==='filter') {
      data.page = 1
    } else if(e==='pagesize') {
      data=data
    }else{
      data.page = page
    }
    if(e==='pagesize'){
      data=data
    }else{
      data.pagesize=pagesize
    }
    var rating = {}
    data.rating = rating
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (selectedName != '') {
      data.reviewerName = selectedName
    }
    if (selectedProduct != '') {
      data.productName = selectedProduct
    }
    if (publishedvalue != '') {
      data.status = publishedvalue
    }
    if (min_rating != '') {
      rating.minRating = JSON.parse(min_rating)
    }
    if (min_rating == '') {
      rating.minRating = null
    }
    if (max_rating != '') {
      rating.maxRating = JSON.parse(max_rating)
    }
    if (max_rating == '') {
      rating.maxRating = null
    }
    var body = data

    let token = localStorage.getItem('token')
    var url = '/review/reviewListing'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      let data1 = response.data.data
      if (response.data.status === 1) {
        this.setState({ reviewListing: data.reviewListing, total: data.total, length: data.reviewListing.length, loading: false })
        if (data.reviewListing.length <= pagesize) {
          this.setState({ page_: false })
        }
        if (data1.manageReviewListing) {
          this.setState({
            comments: data1.manageReviewListing.comments,
            rating: data1.manageReviewListing.rating,
            reviewerName: data1.manageReviewListing.reviewerName,
            productName: data1.manageReviewListing.productName,
            status: data1.manageReviewListing.status,
            pagesize:data1.manageReviewListing.pageSize,
          })
        }
      } else {
        this.setState({ reviewListing: [] })
      }
      if (e == 'filter') {
        this.setState({ page: 1 })
      }
    })
  }

  //* **********  RESET LISTING  ************************//
  resetListing = (e) => {
    var url = '/review/reviewListing'
    var method = 'post'
    var { page, pagesize } = this.state
    var body = { page: page, pagesize: pagesize }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let { data } = response.data
      let data1 = response.data.data
      if (response.data.status === 1) {
        this.setState({ reviewListing: data.reviewListing, total: data.total, length: data.reviewListing.length })
        if (data1.manageReviewListing) {
          this.setState({
            comments: data1.manageReviewListing.comments,
            rating: data1.manageReviewListing.rating,
            reviewerName: data1.manageReviewListing.reviewerName,
            productName: data1.manageReviewListing.productName,
            status: data1.manageReviewListing.status,
            selectedOption: 'Select',
            selectedProduct: [],
            selectedName: [],
            min_rating: '',
            max_rating: '',
            publishedvalue:''
          })
        }
      } else {
        this.setState({ reviewListing: [] })

      }
    }
    )
  }

  // ############################## Review Status #############################
  reviewStatus(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { reviewId: [Id], isActive: status }
    var urlkey = '/review/changeReviewStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        swal(data.message, '', 'success')
        this.listOfReviews()
      } else {
        swal(data.message, '', 'error')
      }
    })
  }

  // ########################### Delete Review #########################
  deleteReview = (uid) => {
    var delArr = this.state.multipleDelete;
    swal({ title: "Are you sure?", icon: "warning", buttons: true, dangerMode: true, })
      .then((willDelete) => {
        if (willDelete) {

          if (delArr.length > 0) {
            var body = { reviewId: delArr }
          }
          else {
            var body = { reviewId: [uid] }
          }
          let token = localStorage.getItem('token')
          var url = '/review/deleteReviews'
          var method = 'post'
          this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
            if (response) {
              this.listOfReviews()
            }
          })
          this.setState({ multipleDelete: [] })
          swal('Borrado exitosamente', '', 'success')
        }
      });
  }

  // ########################### Edit Review #########################
  updateReview = (Id) => {
    this.props.history.push(`/editreview/${Id}`)
  }


  onChangeSearchHandler = event => {
    let { name, value } = event.target

    this.setState({ [name]: value }, () => this.reviewListing())
  }

  checkArray(_id) {
    let { multipleDelete } = this.state;
    if (multipleDelete.includes(_id)) {
      return true;
    }
    else {
      return false;
    }
  }
  onCheckbox(_id, val) {

    var delarray = this.state.multipleDelete;
    if (!delarray.includes(_id)) {
      delarray.push(_id);
    }
    else {
      delarray.splice(delarray.indexOf(_id), 1);
    }
    if (delarray.length != this.state.reviewListing.length) {
      this.setState({ checked: false });
    }
    if (this)
      this.setState({ multipleDelete: delarray })
  }

  // ########################### PAGINATION #########################

  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize
    }, () => this.listOfReviews());
    this.setState({ selectAll: false }, () => this.selectAllcheck())
  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 },()=>this.setTableRows(), () => this.listOfReviews());
  }
  handleChange_ = (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1, page_: true },()=>this.setTableRows(), () => this.listOfReviews());
    } 
    // else {
    //   this.setState({ pagesize: this.state.length, page: 1 }, () => this.listOfReviews());
    // }
  }


  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { rating, reviewerName, productName, comments, status,pagesize } = this.state
    var body = { rating, reviewerName, productName, comments, status,pageSize:pagesize }
    var url = '/review/setReviewFilter'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      if (response) {
        this.listOfReviews()
        if(e){
          swal('Actualizado exitosamente !', '', 'success')
        }
      }
    })
  }
  //* ********** SORTING ************************//
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState(
          {
            sort: { [element]: value }
          },
          () => {
            this.listOfReviews()
          }
        )
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }
  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { reviewListing } = this.state
    if (this.state.selectAll) {
      reviewListing.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      reviewListing.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }
  applyfilter = () => {
    var delArr = this.state.multipleDelete
    if (this.state.selectedOption == 'Delete') {
      if (delArr.length > 0) {
        this.deleteReview(...delArr)
      }
    }
    if (this.state.selectedOption == 'Publish') {
      var body = { reviewId: delArr, isActive: true }
    }
    if (this.state.selectedOption == 'UnPublish') {
      var body = { reviewId: delArr, isActive: false }
    }
    var url = '/review/changeReviewStatus'
    var method = 'post'
    var token = localStorage.getItem('token')
    if (delArr.length > 0) {
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          this.listOfReviews()
          this.setState({ multipleDelete: [], selectedOption: 'Select here' })
          swal('Actualizado exitosamente', '', 'success')
        }
      })
    } else {
      this.setState({ selectedOption: 'Select here' })
      swal('Seleccione al menos una reseña', '', 'info')
    }

  }
  // ###########################  for Search #########################
  getIds = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { searchText: e }
    var url = '/review/' + [name]
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {

      var data = response.data.data
      if (response && response.data && response.data.data) {
        if (name === 'getReviewerNames') {
          var fNArr = _.uniqBy(data, (e) => {
            return e.reviewerName.toString();
          });
          this.setState({ listOfNames: fNArr })
        }
        if (name === 'getProductsNames') {
          var listOfProducts = []
          data.map((each, key) => {
            listOfProducts.push(each.productName)
            this.setState({ listOfProducts })
          })
          this.setState({ listedProducts: data })
        }
      }
    })
  }
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }
  // ########################### download CSV #########################


  downloadCSV(type, array) {
    let { rating, reviewerName, productName, comments, status, multipleDelete } = this.state
    let token = localStorage.getItem('token')
    if (array === "totalList") {
      var data = { filteredFields: ["reviewerName", "productName", "title", "description", "isActive", "rating"] }
    }

    if (array === "filteredList") {
      var data = { filteredFields: [] }
      if (rating) {
        data.filteredFields.push('rating')
      };
      if (reviewerName) {
        data.filteredFields.push('reviewerName')
      };
      if (productName) {
        data.filteredFields.push('productName')
      };
      if (status) {
        data.filteredFields.push('isActive')
      };
      if (comments) {
        data.filteredFields.push('description')
        data.filteredFields.push('title')
      };
      if (multipleDelete.length > 0) {
        data.reviewsArray = multipleDelete
      }
    }
    var body = data
    var url = '/review/' + type
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var data = response.data.data
      if (data) {
        if (type === 'downloadReviewsCsvFile') {
          window.open(CSV_URL + data.filePathAndName, '_blank');
          // FileDownload(CSV_URL  +  data.filePathAndName, data.filePathAndName);
        } else {
          window.open(EXCEL_URL + data.filePathAndName, '_blank');
          // FileDownload(EXCEL_URL  +  data.filePathAndName, data.filePathAndName);
        }
        // swal("Downloaded Successfully",'','success')
      }
    })
  }

  render() {
    const Option = Select.Option;
    let { reviewListing, page, pagesize, total, length, rating, reviewerName, productName, selectedOption, publishedvalue, max_rating, min_rating,
      sortData, comments, status, selectedName, listOfNames, selectedProduct, listOfProducts } = this.state
    const filteredNames1 = listOfNames.map((each) => { return each.reviewerName });
    const filteredNames = filteredNames1.filter(o => !selectedName.includes(o));



    const filteredProducts = listOfProducts.filter(o => !selectedProduct.includes(o));


    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Revisiones de productos</h3>
              <ul className="breadcrumb ">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item active">Revisiones de productos</li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />

        <div className="card animated fadeIn">
          <div className="card-header">
            <div className="row data-filter">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  <Select
                    showSearch
                    placeholder={<b>Seleccionar</b>}
                    optionFilterProp="children"
                    className="applyselect"
                    value={selectedOption}
                    onSelect={(value) => this.setState({ selectedOption: value })}
                  >
                    <Option value="Publish">Activar</Option>
                    <Option value="UnPublish">Anular publicación</Option>
                    <Option value="Delete">Eliminar</Option>
                  </Select>
                  <button type="button" className="btn btn-primary ml-0 apply_btn" onClick={this.applyfilter}>Aplicar</button>
                  <div className="dropdown">
                    <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                      <span>Herramientas</span>
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadReviewsExcelFile", 'totalList')}>Exportar a Excel</a>
                      <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadReviewsCsvFile", 'totalList')}>Exportar a CSV</a>
                    </div>
                  </div>
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
            <div className="item-list mt-3" id="itemlist">
              <ul className="row  mb-0">
                <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ rating: !this.state.rating })} checked={rating} /><span></span>Clasificación</label></li>
                <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ reviewerName: !this.state.reviewerName })} checked={reviewerName} /><span></span>Nombre del revisor</label></li>
                <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ productName: !this.state.productName })} checked={productName} /><span></span>Nombre del producto</label></li>
                <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ comments: !this.state.comments })} checked={comments} /><span></span>Comentarios</label></li>
                <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ status: !this.state.status })} checked={status} /><span></span>Estado</label></li>
              </ul>
              <hr />
              <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.listOfReviews} >
              Reiniciar
            </button>
              <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({ rating: true, reviewerName: true, productName: true, comments: true, status: true })}>
              Seleccionar todo
            </button>
              <button className="nav-link pull-right btn btn-primary" type="button" onClick={()=>this.setTableRows('rows')} >
              Guardar
            </button>

            </div>
            <div className="filter-list" id="filterlist">
              <div className="row">
                <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                  <div className="form-group">
                    <label>Clasificación</label>
                    <div className="row">
                      <div className="col-md-6">
                        <input className="form-control" type="number" name="min_rating" placeholder="From" value={min_rating} onChange={this.handleChange} />
                      </div>
                      <div className="col-md-6">
                        <input className="form-control" type="number" name="max_rating" placeholder="To" value={max_rating} onChange={this.handleChange} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                  <div className="form-group">
                    <label>Nombre del revisor</label>
                    <Select
                      mode="multiple"
                      placeholder="Nombre del revisor"
                      value={selectedName}
                      onChange={(val) => this.setState({ selectedName: val })}
                      onSearch={(e) => this.getIds(e, 'getReviewerNames')}
                      onFocus={(e) => this.getIds(e, 'getReviewerNames')}
                      style={{ width: '100%' }}
                    >
                      {filteredNames.map((item, id) => (
                        <Select.Option key={id} value={item}>
                          {item}
                        </Select.Option>
                      ))}
                    </Select>
                  </div>
                </div>
                <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                  <div className="form-group">
                    <label>Nombre del producto</label>
                    <Select
                      mode="multiple"
                      placeholder="Nombre del producto"
                      value={selectedProduct}
                      onChange={(value) => this.setState({ selectedProduct: value })}
                      onSearch={(e) => this.getIds(e, 'getProductsNames')}
                      onFocus={(e) => this.getIds(e, 'getProductsNames')}
                      style={{ width: '100%' }}
                    >
                      {filteredProducts.map(item => (
                        <Select.Option key={item} value={item}>
                          {item}
                        </Select.Option>
                      ))}

                    </Select>
                  </div>
                </div>

                <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                  <div className="form-group">
                    <label>Estado</label>
                    <select className="form-control" onChange={(e) => this.setState({ publishedvalue: e.target.value })} value={publishedvalue}>
                      <option value=''>Seleccionar</option>
                      <option value='published'>Publicado</option>
                      <option value='unPublished'>Sin Activar</option>
                    </select>
                  </div>
                </div>
              </div>
              <hr />
              <div className="pull-right filter-button">
                <div className="dropdown ml-2">
                  <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                    <span>Herramientas</span>
                  </button>
                  <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                    <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadReviewsExcelFile", 'filteredList')}>Exportar a Excel</a>
                    <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadReviewsCsvFile", 'filteredList')}>Exportar a CSV</a>
                  </div>
                </div>
                <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.listOfReviews('filter')}>
                Aplicar filtro
            </button>
                <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetListing} >
                Reiniciar
            </button>
              </div>
            </div>
          </div>
          <div className="card-body">

            {/* Data table start */}
            <div className="table-responsive">
              <table className="table dataTable with-image row-border hover custom-table table-striped">
                <thead>
                  <tr>
                    <th>
                      <div className='checkbox'>
                        <label>
                          <input type="checkbox" className="form-check-input" checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} />
                          <span />
                          <i className='input-helper' />
                        </label>
                      </div>
                    </th>

                    {rating ? <th sortable-column="rating" onClick={this.onSort.bind(this, 'rating')}>Clasificación
                      <i aria-hidden="true" className={(sortData['rating']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i></th> : null}

                    {reviewerName ? <th sortable-column="reviewerName" onClick={this.onSort.bind(this, 'reviewerName')}>Nombre del revisor
                        <i aria-hidden="true" className={(sortData['reviewerName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i>
                    </th> : null}
                    {productName ? <th sortable-column="productName" onClick={this.onSort.bind(this, 'productName')}>Nombre del producto
                       <i aria-hidden="true" className={(sortData['productName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i>
                    </th> : null}
                    {comments ? <th sortable-column="description" onClick={this.onSort.bind(this, 'description')} >Comentarios
                        <i aria-hidden="true" className={(sortData['description']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i>
                    </th> : null}
                    {status ? <th sortable-column="isActive" onClick={this.onSort.bind(this, 'isActive')}>Estado
                      <i aria-hidden="true" className={(sortData['isActive']) ? "fa fa-arrow-up" : "fa fa-arrow-down"}></i>
                    </th> : null}
                    <th> Action</th>
                  </tr>
                </thead>
                {reviewListing.length === 0 && this.state.loading === false ? <tbody><tr className="text-center p-3"><td >No se encontraron registros</td></tr></tbody> :
                  <tbody>
                    {
                      reviewListing.map((each, Key) => {
                        return (
                          <tr key={Key} className="animated fadeIn">
                            <td> <div className="checkbox">
                              <label>
                                <input type="checkbox" className="form-check-input" name="foo" checked={(this.checkArray(each._id)) ? true : false} onChange={() => this.onCheckbox(each._id)} />
                                <span></span>
                                <i className="input-helper" />
                              </label>
                            </div></td>
                            {rating ? <td>  <StarRatings
                              starRatedColor="orange"
                              rating={each.rating}
                              starDimension="20px"
                              starSpacing="5px"
                              numberOfStars={5}
                              name='rating'
                            /></td> : null}
                            {reviewerName ? <td><p className="mb-1 d-block" >{each.reviewerName}</p>{each.emailId}{each.createdAt}<p className="text-danger" onClick={() => this.updateReview(each._id)}>[Editar reseña]</p></td> : null}
                            {productName ? <td>{each.productName} <a href={each.productLink ? each.productLink : "javascript:;"} target="_blank" className="mt-1 d-block">Visita la página</a> </td> : null}
                            {comments ? <td><b >{each.title}</b><br /><span className="text-truncate cu-w mt-1">{each.description}</span></td> : null}
                            {status ? <td><label>
                              <Toggle
                                checked={each.isActive}
                                className='custom-classname'
                                onChange={() => this.reviewStatus(each.isActive, each._id)}
                              />
                            </label>
                            </td> : null}
                            <td>
                              <button onClick={() => this.updateReview(each._id)} data-toggle="tooltip" title="Edit"><i className="fa fa-pencil-square-o text-primary" aria-hidden="true"></i></button>
                              <button onClick={() => this.deleteReview(each._id)} data-toggle="tooltip" title="Delete">< i className="fa fa-trash text-danger" aria-hidden="true"></i></button>
                            </td>
                          </tr>
                        );
                      })
                    }

                  </tbody>}
              </table>
            </div>
            {reviewListing.length > 0 ?
              <div className="table-footer text-right">
                <label>Demostración</label>
                <Select showSearch placeholder={<b> {total < 20 ? total : length}</b>} optionFilterProp="children"
                  onSelect={this.handleChangePageSize.bind(this)}
                  value={this.state.page_ ? pagesize : (length <=pagesize ?length:pagesize)}
                  onSearch={this.handleChange_}>
                  <Option value={20}>20</Option>
                  <Option value={50}>50</Option>
                  <Option value={100}>100</Option>
                  <Option value={150}>150</Option>
                  <Option value={200}>200</Option>

                </Select>
                <label>Fuera de  {total} comentarios</label>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale />
                </div>
              </div> : null}
          </div>
        </div>
      </Home >

    );
  }
}



export default connect(null, actions)(ReviewManagement);
