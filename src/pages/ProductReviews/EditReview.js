import React, { Component } from 'react';
import Home from '../Home'
import StarRatings from 'react-star-ratings';
import Toggle from 'react-toggle'
import * as actions from '../../actions'
import { connect } from 'react-redux'
import axios from 'axios'
import { API_URL } from '../../config/configs'
import { Link } from 'react-router-dom';

import swal from 'sweetalert'
class EditReview extends Component {
  constructor(props) {
    super(props)
    this.state = {
      rating: undefined,
      reviewerName: '',
      email: '',
      dateOfReview: '',
      productName: '',
      commentTitle: '',
      description: '',
      productLink: '',
      id: '',
      errors: {
      },
      status1: true,
      isActive: true,
      review_Id: ''
    }
  }
  componentDidMount() {
    this.getReviewDetails()
  }
  // ############################## Getting Review Details #############################
  validateForm = () => {
    let { commentTitle, description, errors } = this.state
    let formIsValid = true
    if (!description || description.trim() === '') {
      formIsValid = false
      errors['description'] = '* Descripción requerida'
    }
    if (!commentTitle || commentTitle.trim() === '') {
      formIsValid = false
      errors['commentTitle'] = '* Se requiere el título del comentario'
    }
    // else formIsValid = true
    this.setState({ errors })
    return formIsValid
  }
  getReviewDetails = () => {
    var id = this.props.match.params.rId;
    this.setState({ review_Id: id })
    if (id) {
      var body = { reviewId: id }
      let token = localStorage.getItem('token')
      var url = '/review/getReviewDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response.data
        this.setState({
          rating: data.rating,
          reviewerName: data.reviewerName,
          email: data.emailId,
          dateOfReview: data.createdAt,
          productName: data.productName,
          isActive: data.isActive,
          commentTitle: data.title,
          description: data.description,
          productLink: data.productLink,
          id: data._id
        })

      })
    }
  }

  changingStatus(status) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { reviewId: [this.state.review_Id], isActive: status }
    var urlkey = '/review/changeReviewStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        swal(data.message, '', 'success')
        this.getReviewDetails()
      }
      else if (data.message === "Invalid token") {
        this.props.history.push('/')
      }
      else {
        swal(data.message, '', 'error')
      }
    })
  }


  // ############################## Update Review #############################

  updateReview = () => {
    if (this.validateForm()) {
      var token = localStorage.getItem('token')
      let { commentTitle, description, id } = this.state
      var body = { title: commentTitle, description: description, reviewId: id }
      var url = '/review/updateReview'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status === 1) {
          swal({
            title: 'Actualizado exitosamente',
            icon: "success",
            dangerMode: true,
          })
            .then(willDelete => {
              if (willDelete) {
                this.props.history.push('/reviewlist')
              }
            });
        }
      })
    }
  }




  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }

  }

  render() {
    var { rating, reviewerName, email, dateOfReview, productLink, productName, commentTitle, description, errors, isActive } = this.state
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Editar reseña</h3>
              <ul className='breadcrumb'>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/reviewlist')}>Revisiones de productos</li>
                <li className="breadcrumb-item active">Editar reseña </li>
              </ul>
            </div>
          </div>
        </div>
        <div className='card animated fadeIn'>
          <div className="card-body">
            <form className="form-sample">
              <div className="form-group row mb-0">
                <label className="col-lg-2 col-md-3 col-form-label">Calificación :</label>
                <div className="col-lg-8 col-md-7">
                  <StarRatings
                    starRatedColor="orange"
                    rating={rating}
                    starDimension="20px"
                    starSpacing="5px"
                    numberOfStars={5}
                    name='rating'
                  />
                </div>
              </div>
              <div className="form-group row  mb-0">
                <label className="col-lg-2 col-md-3 col-form-label">Nombre del revisor:</label>
                <div className="col-lg-8 col-md-7">
                  <p >{reviewerName}</p>
                </div>
              </div>
              <div className="form-group row  mb-0">
                <label className="col-lg-2 col-md-3 col-form-label">Email :</label>
                <div className="col-lg-8 col-md-7">
                  <p >{email}</p>
                </div>
              </div>
              <div className="form-group row  mb-0">
                <label className="col-lg-2 col-md-3 col-form-label">Fecha de revisión :</label>
                <div className="col-lg-8 col-md-7">
                  <p >{dateOfReview}</p>
                </div>
              </div>
              <div className="form-group row  mb-0">
                <label className="col-lg-2 col-md-3 col-form-label">Nombre del producto :</label>
                <div className="col-lg-8 col-md-7">
                  <a href={productLink ? productLink : "javascript:;"} target="_blank" className="mt-1 d-block"><u>{productName}</u></a>
                </div>
              </div>
              <div className="form-group row">
                <label className="col-lg-2 col-md-3 col-form-label">Estado:</label>
                <div className="col-lg-8 col-md-7">
                  <Toggle
                    checked={isActive}
                    className='custom-classname'
                    onChange={() => this.changingStatus(isActive)}
                  />
                </div>
              </div>
              <div className="form-group row">
                <label className="col-lg-2 col-md-3 col-form-label">Título del comentario:</label>
                <div className="col-lg-8 col-md-7">
                  <input className="form-control" type="text" name="commentTitle" placeholder="Ingresar Título del comentario" onChange={this.handleChange}
                    value={commentTitle} />
                  <span className="error-block"> {errors.commentTitle} </span>
                </div>
              </div>
              <div className="form-group row">
                <label className="col-lg-2 col-md-3 col-form-label">Descripción:</label>
                <div className="col-lg-8 col-md-7">
                  <textarea className="form-control" type="text" name="description" placeholder="Ingrese la descripción" onChange={this.handleChange}
                    value={description} />
                  <span className="error-block"> {errors.description} </span>
                </div>
              </div>
              <hr />
              <div className="form-group">
                <div className="button-group-container float-right">
                  <button type="button" className="btn btn-primary" onClick={this.updateReview}>
                    <span>Enviar</span>
                  </button>
                  <button type="button" className="btn btn-outline-primary" onClick={() => this.props.history.push('/reviewlist')}>Cancelar</button>
                </div>
              </div>
            </form>
          </div >
        </div >

      </Home>


    );
  }
}

const mapStateToProps = state => ({
  EditReview: state.commonReducers.editedDetails,
});

export default connect(mapStateToProps, actions)(EditReview);