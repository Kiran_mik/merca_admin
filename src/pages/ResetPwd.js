import React, { Component } from 'react'
import swal from 'sweetalert'
import axios from 'axios'
import { API_URL } from '../config/configs'
import { toast } from 'react-toastify';

const queryString = require('query-string')

class ResetPassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      newPassword: '',
      confirmPassword: '',
      error: {
        newPassword: '',
        confirmPassword: ''
      },
      confirmPasswordValidate: false,
      newPasswordValidate: false
    }
  }


  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value })
    if (this.state.newPassword) {
      var regexp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+/*-.])[A-Za-z\d@!@#$%^&*()_+/*-.]{6,}$/
      var newPasswordValidate = regexp.test(this.state.newPassword)
      newPasswordValidate
        ? this.setState({ newPasswordValidate: true })
        : this.setState({ newPasswordValidate: false })
    }
    if (this.state.newPassword !== this.state.confirmPassword) {
      this.setState({ confirmPasswordValidate: true })
    } else {
      this.setState({ confirmPasswordValidate: false })
    }
    if (event.target.value) {
      this.setState({
        error: Object.assign(this.state.error, { [event.target.name]: '' })
      })
    } else {
      this.setState({
        error: Object.assign(this.state.error, {
          [event.target.name]: 'Enter  ' + [event.target.name]
        })
      })
    }
  }

  confirmPasswordHandleChange = event => {
    if (event.target.value === this.state.newPassword) {
      this.setState({
        confirmPassword: event.target.value,
        confirmPasswordValidate: true
      })
    } else {
      this.setState({
        confirmPassword: event.target.value,
        confirmPasswordValidate: false
      })
    }
    if (event.target.value) {
      this.setState({
        error: Object.assign(this.state.error, { [event.target.name]: '' })
      })
    } else {
      this.setState({
        error: Object.assign(this.state.error, {
          [event.target.name]: 'Enter  ' + [event.target.name]
        })
      })
    }
  }


  resetpassword = () => {
    var self = this
    const { newPassword, confirmPassword } = this.state;
    const queryValues = queryString.parse(window.location.search)
    var body = { password: confirmPassword, token: queryValues.token }
    if (newPassword.trim() === '' || confirmPassword.trim() === '') {
      toast.error("Please Reset Password !", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
      return false
    }
    else if (newPassword !== confirmPassword) { 
      toast.error("Please provide confirm password same as password !", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
    }
    else {
      axios({
        method: 'post',
        url: API_URL + '/admins/resetPasswordForAdmin',
        headers: {
          'Content-Type': 'application/json'
        },
        data: JSON.stringify(body)
      })
        .then(function (response) {
          let { data } = response
          if (data.statusCode === 433) {
            toast.error("Password must be atleast 6 letters with Capital letters,Numbers,Special Characters !", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
          }
          if (data.statusCode === 234) {
            swal({
              title: 'You have successfully reset the password',
              icon: "success",
              dangerMode: true,
            })
              .then(willDelete => {
                // if (willDelete) {
                  self.props.history.push('/')
                // }
              });
          }
          if (data.statusCode === 416) {
            toast.error("Both passwords are same. Please choose another password !", { position: "bottom-right", autoClose: 2000, hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true });
          }
        })
    }
  }

  render() {
    let { newPassword, confirmPassword, error, confirmPasswordValidate, newPasswordValidate } = this.state
    return (
      <div className="container-scroller">
        <div className="container-fluid page-body-wrapper full-page-wrapper">
          <div className="content-wrapper d-flex align-items-center auth login-full-bg">
            <div className="row w-100 margin-l-0">
              <div className="col-lg-6 col-sm-8 mx-auto authentication-form">
                <div className="login-header">
                  <img src='assets/images/logo.svg' alt='logo' />
                  <h2>Reset Password </h2>
                </div>
                <div className="auth-form-light text-left p-5">
                  <form autoComplete="off"  >
                    <div className="form-group" >
                      <input
                        className='form-control'
                        type='password'
                        placeholder='Enter New Password'
                        name='newPassword'
                        value={newPassword}
                        onChange={this.handleChange}
                      />
                      {newPasswordValidate || !newPassword ? (<span className='error-block'> {error.newPassword} </span>) : null}
                    </div>

                    <div className="form-group" >
                      <input
                        className='form-control'
                        type="password"
                        placeholder='Confirm New Password'
                        id='confirmpassword'
                        name='confirmPassword'
                        value={confirmPassword}
                        required
                        onChange={this.confirmPasswordHandleChange}
                      />
                      {confirmPasswordValidate || !confirmPassword ? (<span className='error-block'>{error.confirmPassword} </span>) :
                        (<span className='error-block'> Password does not match{' '} </span>)}
                    </div>

                    <div className="mt-5 row">
                      <div className="col-md-6"> <button className="btn btn-block btn-outline-primary btn-lg font-weight-medium" type="submit" onClick={() => this.props.history.push('/')}>cancel</button></div>
                      <div className="col-md-6">  <button className="btn-block  btn btn-primary btn-lg font-weight-medium" type="button" onClick={this.resetpassword}>Submit</button></div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ResetPassword





