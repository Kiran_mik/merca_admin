import React, { Component } from 'react';
import Home from '../Home'
import Toggle from 'react-toggle'
import * as actions from '../../actions';
import { connect } from "react-redux";
import { API_URL, IMAGE_URL } from '../../config/configs'
import swal from 'sweetalert'
import SimpleReactValidator from 'simple-react-validator'
import axios from 'axios'
import { toast } from 'react-toastify';


class AddUser extends Component {
  constructor(props) {
    super(props)
    this.validator = new SimpleReactValidator();
    this.state = {
      photo: '',
      firstName: '',
      lastName: '',
      role: '',
      listOfRoles: [],
      emailId: '',
      mobileNo: '',
      status: true,
      userId: '',
      roleID: '',
      errors: {
        firstName: '',
        lastName: '',
        role: '',
        emailId: '',
        mobileNo: '',
      },
      isVerified:''
    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission && permissions.rolePermission.adminUsersAccess&& permissions.rolePermission.adminUsersAccess.view === false) {
      this.props.history.push('/dashboard')
    }

    this.getAllDetails()
    this.getAllRoles()
  }
  getAllRoles = () => {
    let token = localStorage.getItem('token');
    var url = '/role/getAllRole'
    var method = 'get'
    this.props.commonApiCall(url, method, null, token, null, this.props, response => {
      var { data } = response

      if (data.status === 1) {
        this.setState({ listOfRoles: data.data })
      }
    })
  }
  getAllDetails = () => {
    var id = this.props.match.params.aId;
    this.setState({ userId: id })
    if (id) {
      var body = { adminId: id }
      let token = localStorage.getItem('token')
      var url = '/admins/getSubAdminProfile'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data) {
          this.setState({
            photo: data.data.photo,
            roleID: data.data.roleId,
            firstName: data.data.firstName,
            lastName: data.data.lastName,
            role: data.data.role,
            roleID: data.data.roleId,
            emailId: data.data.emailId,
            mobileNo: data.data.mobileNo,
            status: data.data.isActive,
            roleID: data.data.roleId,
            isVerified:data.data.isVerified
          })
        }

      })
    }
  }
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
    // if (event.target.name === 'mobileNo') {
    //   var x = event.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
    //   event.target.value = x[1] + x[2] + x[3];
    //   this.setState({ [event.target.name]: event.target.value })
    // }
    if (event.target.value) {
      this.setState({
        errors: Object.assign(this.state.errors, { [event.target.name]: "" })
      });
    }

  }
  validateForm = () => {
    let { firstName, lastName, role, emailId, mobileNo, errors } = this.state
    let formIsValid = true
    var pattern = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
    var mblpattern = new RegExp(/^\d{8,20}$/);
    if (!firstName||firstName.trim()==='') {
      formIsValid = false
      errors['firstName'] = '* Se requiere el nombre'
    }
    if (!lastName||lastName.trim()==='') {
      formIsValid = false
      errors['lastName'] = '* Se requiere el apellido'
    }
    if (role=="") {
      formIsValid = false
      this.setState({roleReq:true})
    } 
    if (!pattern.test(emailId)) {
      formIsValid = false
      if(!emailId||emailId.trim() === '' ){
        errors['emailId'] = '* Correo electrónico es requerido'
      }else
      errors['emailId'] = '* Ingrese un correo electrónico válido'
    }
    // if (!mobileNo) {
    //   formIsValid = false
    //   errors['mobileNo'] = '* Se requiere número de celular'
    // }else  if(mobileNo.length!=10){
    //   errors['mobileNo'] = '* Ingrese un número de teléfono celular válido'
    // }
    if (!mblpattern.test(mobileNo)) {
		  formIsValid = false
		  if(!mobileNo){
			errors['mobileNo'] = 'Se requiere número de celular'
		  }else
		  errors['mobileNo'] = 'Ingrese un número de teléfono celular válido 8 to 20'
		}
    this.setState({ errors })
    return formIsValid
  }

  addAdminUser = () => {
    if (this.validateForm()) {
      let token = localStorage.getItem('token');
      var { photo, firstName, lastName, roleID, emailId, mobileNo, status, userId } = this.state
      var data = { firstName, lastName, role: roleID, emailId, mobileNo, photo, isActive: status }
      if (userId != '') {
        data.adminId = userId
      }
      var body = data
      var url = '/admins/addAdmin'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let { data } = response
        if (data.statusCode === 228) {
          swal({
            title: "Agregado exitosamente!",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
                this.props.history.push('/adminUsers')
              // }
            })
        }
        if (data.statusCode === 445) {
          swal("EmailId is already exists!",'', "warning")
        }
        if (data.statusCode === 219) {
          swal({
            title: "Actualizado con éxito!",
            icon: "success",
          })
            .then((willDelete) => {
              // if (willDelete) {
                this.props.history.push('/adminUsers')
              // }
            })
        }
      }
      )
    }
  }


  fileChangedHandlerImg(event) {
    let reader = new FileReader()
    let file = event.target.files[0]
    let filename = event.target.files[0].name
    reader.onloadend = () => {
      this.setState({
        file: file,
        filename: filename,
        imagePreviewUrl: reader.result,
        fileVisible: true
      })
    }
    reader.readAsDataURL(file)
  }

  imageFileUpload() {
    var { file } = this.state
    var token = localStorage.getItem('token')
    var formData = new FormData()
    formData.append('file', file)
    axios.post(API_URL + '/auth/fileUpload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: token
      }
    })
      .then(response => {

        let { data } = response.data
        this.setState({ photo: data.filepath, fileVisible: false, file: '' })
      })
      .catch(err => console.log(err))

  }

  userStatusChange(status) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { adminId: [this.state.userId], isActive: status }

    var urlkey = '/admins/changeAdminStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        this.getAllDetails()
      }
      else if (data.message === "Invalid token") {
        this.props.history.push('/')
      }
      else {
        swal(data.message, '', 'error')
      }
    })
  }
  handleChange1 = (value) => {
    this.setState({ role: value,roleReq:false })
    this.state.listOfRoles.map((each) => {
      if (each.role === value) {
        this.setState({ roleID: each._id })
      }
    })
  }
  resendMail = () => {
    var id = this.props.match.params.aId;
    var url = '/admins/resendVerificationForAdmin'
    var method = 'post'
    var data = { adminId:id}
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data = response.data
      if (data.status === 1) {
        // this.getDetails()
       swal('Correo de verificación enviado con éxito','','success')
      }
    }
    )
  }

  render() {

    let { photo, errors, listOfRoles, firstName,isVerified, lastName, role, emailId, mobileNo, status, userId,roleReq } = this.state
    console.log(this.state);
    
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Añadir nuevo usuario</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}> Página principal</li>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/adminUsers')}>Administración</li>
                <li className='breadcrumb-item active'>{userId != undefined ? 'Editar usuario administrador' : 'Añadir nuevo usuario'} </li>
              </ul>
            </div>
          </div>
        </div>

        <div className='card animated fadeIn mb-2'>
          <div className="card-header" data-spy="affix" data-offset-top="100">
            <div className="button-continer text-right">
              <button type='button' className='btn btn-primary' onClick={this.addAdminUser}> <span>Guardar</span> </button>
              <button type='button' className='btn btn-outline-primary' onClick={() => this.props.history.push('/adminUsers')} > Cancelar </button>
            </div>
          </div>
          <div className='card-body'>
            <form className='form-sample'>
              <div className='form-group row'>
                <label className='col-sm-3 col-lg-2 col-sm-3 col-form-label'>Imagen de perfil :</label>
                <div className="drop-section mb-3 col-md-4">
                  <div className="form-group inputDnD mb-0">
                    <div className="upload-overlay d-flex justify-content-center align-items-center">
                      <div className="upload-wrapper">
                        <i className="fa fa-upload"></i>
                        <span><button type="button"  >Elige un archivo </button> o arrástralo aquí</span>
                      </div>
                    </div>
                    <label className="sr-only" >Subir archivo</label>
                    <input type="file" className="form-control-file text-primary font-weight-bold" name='image' id="file" accept="image/*"
                      data-title="Drag and drop a file" onChange={this.fileChangedHandlerImg.bind(this)} />
                  </div>
                  {this.state.fileVisible ?
                    <span>
                      <div className='product-img'> <img src={this.state.imagePreviewUrl} /><span> <i className='fa fa-times-circle-o text-danger pull-right fa-2x' onClick={() => this.setState({ fileVisible: !this.state.fileVisible, file: '' })} /></span> </div>
                      <span>{this.state.filename}</span>

                    </span> : null}
                    {this.state.fileVisible ?
                  <div className="button-continer pull-right my-2">
                    <button className="btn btn-primary" type="button" onClick={() => this.imageFileUpload()} > Cargar imagen</button>
                  </div>:null
                    }
                </div>
                <div className="col-md-4">
                  <div className="row justify-content-between my-2">
                    <div className="col-md-12 select-image"><div className="product-img"><img src={photo ? IMAGE_URL + photo : "/assets/images/no-image-icon-4.png"} /></div></div>
                  </div>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Primer nombre <span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='firstName'
                    placeholder='Ingrese nombre'
                    value={firstName}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.firstName} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Apellido<span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='text'
                    name='lastName'
                    placeholder='Introduzca el apellido'
                    value={lastName}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.lastName} </span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Papel<span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <select className="form-control col-md-4 mr-2"
                    placeholder={<h5><b>Seleccione uno</b></h5>} value={role} onChange={(e) => this.handleChange1(e.target.value)}>
                    <option value="">Seleccionar</option>
                    {listOfRoles.map((each, id) => {
                      return (
                        <option value={each.role} key={id}>{each.role}</option>
                      )
                    })}
                  </select>
                  <span className="error-block"> {roleReq? "* Se requiere papel" : ''}</span>
                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>Email<span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  {userId == undefined ?
                    <input
                      className='form-control'
                      type='text'
                      name='emailId'
                      placeholder='Introduce tu correo electrónico'
                      value={emailId}
                      onChange={this.handleChange}
                    /> :
                    <input
                      className='form-control'
                      type='text'
                      name='emailId'
                      placeholder='Introduce tu correo electrónico'
                      value={emailId}
                      disabled
                    />}
                  <span className="error-block"> {errors.emailId} </span>

                </div>
              </div>
              <div className='form-group row'>
                <label className='col-lg-2 col-sm-3 col-form-label'>No celulares<span className="text-danger">*</span> :</label>
                <div className='col-md-8 co-sm-7'>
                  <input
                    className='form-control'
                    type='number'
                    name='mobileNo'
                    placeholder='Ingrese su celular No'
                    value={mobileNo}
                    onChange={this.handleChange}
                  />
                  <span className="error-block"> {errors.mobileNo} </span>

                </div>
              </div>
              <div className='form-group row'>
                <label
                  className='col-lg-2 col-sm-3 col-form-label'
                >
                  Estado <span className="text-danger">*</span> :
                  </label>
                <div className='col-md-8 co-sm-7'>
                  {userId ?
                    <Toggle
                      checked={status}
                      className='custom-classname'
                      onChange={() => this.userStatusChange(status)}
                    /> :
                    <Toggle
                      checked={status}
                      className='custom-classname'
                      onChange={() => this.setState({ status: !this.state.status })}
                    />}
                </div>
              </div>
              {userId === undefined||isVerified?null:<span className="form-group row justify-content-center" onClick={()=>this.resendMail()}><b style={{color:'blue'}}>Resend Verification mail</b></span>}

            </form>
          </div>
        </div>
      </Home>
    );
  }
}
const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(AddUser)
