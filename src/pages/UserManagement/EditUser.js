import React, { Component } from 'react'
import Home from '../Home'
import * as actions from '../../actions';
import { connect } from "react-redux";
// import ToggleButton from 'react-toggle-button'
import { IMAGE_URL } from '../../config/configs'
import swal from 'sweetalert'
import { isEmpty } from "lodash"
import Toggle from 'react-toggle'
import $ from 'jquery';
import Pagination from 'rc-pagination';
import moment from 'moment'
import { Select } from 'antd'
class EditUser extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      pagesize: 10,
      total: 0,
      length: 10,
      firstName: '',
      isActive: '',
      lastName: '',
      mobileNo: '',
      photo: '',
      emailId: '',
      isVerified:'',
      shippingAddresses: [],
      _id: '',
      usersAccess: {},
      orderListing: [],
      sort: {},
      sortData: { orderId: false, createdAt: false, count: false, discount: false, totalAmount: false, paymentType: false, orderStatus: false },

    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions && permissions.rolePermission && permissions.rolePermission.usersAccess && permissions.rolePermission.usersAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }

    if (permissions.rolePermission) {
      let { usersAccess } = permissions.rolePermission
      this.setState({ usersAccess: usersAccess })
    }

    $('[data-toggle="tooltip"]').tooltip();

    this.getDetails()
    this.orderListing()
  }
  getDetails = () => {
    var id = this.props.match.params.uId
    if (id) {
      var body = { userId: id }
      let token = localStorage.getItem('token')
      var url = '/user/gettingUserDetails'
      var method = 'post'
      this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
        let data = response.data.data
        if (data) {
          this.setState({
            firstName: data.firstName,
            isActive: data.isActive,
            lastName: data.lastName,
            mobileNo: data.mobileNo,
            isVerified:data.isVerified,
            photo: data.photo,
            emailId: data.emailId,
            _id: data._id,
          })
          if (data.shippingAddresses.length > 0) {
            this.setState({ shippingAddresses: data.shippingAddresses })
          }
        }

      })
    }
  }




  //* ********** SORTING ************************//
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState(
          {
            sort: { [element]: value }
          },
          () => {
            this.orderListing()
          }
        )
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }

  orderListing = () => {
    var id = this.props.match.params.uId
    var url = '/user/orderListing'
    var method = 'post'
    var { page, pagesize, sort } = this.state
    var data = { page: page, pagesize: pagesize, userId: id }
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (data1.status === 0) {
        this.setState({ orderListing: [] })
      }
      this.setState({ orderListing: data.orderListing, total: data.total, length: data.orderListing.length })
    }
    )
  }
  resendMail = () => {
    var id = this.props.match.params.uId
    var url = '/user/resendVerification'
    var method = 'post'
    var data = { userId:id}
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      console.log(data1);
      if (data1.status === 1) {
        this.getDetails()
       swal('Correo de verificación enviado con éxito','','success')
      }
    }
    )
  }





  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize
    }, () => this.orderListing());
  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 }, () => this.orderListing());
  }




  //* ********** EDIT USER STATUS  ************************//

  userStatusChange(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { userIds: [Id], isActive: status }
    var urlkey = '/user/changeUserStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        if (status == false) {
          swal('¡Usuario desactivado con éxito!', '', 'success')
        }
        else {
          swal('Usuario activado con éxito!', '', 'success')
        }
        this.getDetails()
      }
      else if (data.message === "Invalid token") {
        this.props.history.push('/')
      }
      else {
        swal(data.message, '', 'error')
      }
    })
  }

  render() {
    let { firstName, isActive, lastName, mobileNo, photo, emailId, _id, usersAccess, shippingAddresses,isVerified,
       orderListing, page, pagesize, total, length, sortData } = this.state
    const Option = Select.Option;
    return (
      <Home>
        <div className="row">
          <div className="col-md-12">
            <div className="page-header">
              <h3>Perfil del usuario</h3>
              <ul className="breadcrumb">
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className="breadcrumb-item" onClick={() => this.props.history.push('/usersList')}>Gestión de usuarios</li>
                <li className="breadcrumb-item active">Perfil del usuario</li>
              </ul>
            </div>
          </div>
        </div>
        <form className='form-sample'>
          <div className="row">
            <div className="col-md-3">
              <div className="card animated fadeIn mb-2">
                <div className='card-body'>

                  <div className="form-group row">
                    <div className="col-lg-12 text-center porfile-info">
                      <div className="user-profile-img">
                        <div className="inner-image">
                          <img src={photo ? IMAGE_URL + photo : "/assets/images/no-image-user.png"} alt={firstName} />
                        </div>
                      </div>
                      <h2>{firstName} {lastName}</h2>
                      <span><b>Email:</b>{emailId}</span><br />
                      <span><b>Celular:</b>{mobileNo ? mobileNo : 'No number'}</span>
                    </div>
                  </div>
                  <div className="form-group row justify-content-center">
                    <label className="col-form-label">Estado : </label>
                    <div className="active-toggle ml-1">
                      {usersAccess.edit ?
                        <Toggle
                          checked={isActive ? true : false}
                          className='custom-classname'
                          onChange={() =>
                            this.userStatusChange(isActive, _id)
                          }
                        /> : <Toggle
                          checked={isActive ? true : false}
                          className='custom-classname'
                          onChange={() => this.setState({ toggle_err: true })}
                        />}
                    </div>
                    <span className="error-block"> {this.state.toggle_err ? "No tiene permisos para cambiar el estado" : null} </span>
                  </div>
                 {isVerified?null:<span className="form-group row justify-content-center" onClick={()=>this.resendMail()}><b style={{color:'blue'}}>Resend Verification mail</b></span>}
                </div>
              </div>
            </div>

            <div className="col-md-9">
              <div className='card'>
                <div className='card-body'>
                  <ul className="nav nav-tabs" id="myTab" role="tablist">
                    <li className="nav-item active">
                      <a className="nav-link active" data-toggle="tab" href="#address" role="tab" aria-controls="item" aria-selected="false">Dirección</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" data-toggle="tab" href="#order" role="tab" aria-controls="imagetab" aria-selected="false">Orden</a>
                    </li>
                  </ul>
                  <div className="tab-content" id="myTabContent">
                    <div className="fade show active tab-pane" id="address" role="tabpanel" aria-labelledby="revenaddressue">
                      {shippingAddresses.length > 0 ?
                        <div className="form-group row">
                          {shippingAddresses.map((each, id) => {
                            return (
                              <div className="col-lg-6" key={id}>
                                <div className="bg-light address-list" >
                                  <h4>{each.name}</h4>
                                  <address>{each.addressLine1} ,<br />
                                    {each.addressLine2},<br />
                                    {each.city},<br />
                                    {each.phone},<br/>
                                    {each.state},<br/>
                                    {each.zipCode}
                                  </address>
                                  {each.defaultAddress ? <div className="text-right">
                                    <span className="border border-primary px-2 py-1 text-primary">Primario</span>
                                  </div> : null}
                                </div>

                              </div>
                            )
                          })}
                        </div> : <h6>No se encontraron direcciones</h6>}
                    </div>
                    <div className="fade tab-pane" id="order" role="tabpanel" aria-labelledby="order">
                      {orderListing.length > 0 ? <>
                        <div className='table-responsive'>
                          <table className='table dataTable with-image row-border hover custom-table table-striped'>
                            <thead>
                              <tr>
                                <th sortable-column="orderId" onClick={this.onSort.bind(this, 'orderId')} >
                                Solicitar ID
                                   <i aria-hidden='true' className={(sortData['orderId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                                </th>
                                <th sortable-column="createdAt" onClick={this.onSort.bind(this, 'createdAt')} >
                                Fecha de orden
                                   <i aria-hidden='true' className={(sortData['createdAt']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                                </th>



                                <th sortable-column="count" onClick={this.onSort.bind(this, 'count')} >
                                Cantidad
                                   <i aria-hidden='true' className={(sortData['count']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                                </th>



                                <th sortable-column="discount" onClick={this.onSort.bind(this, 'discount')} >
                                Descuento
                                   <i aria-hidden='true' className={(sortData['discount']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                                </th>



                                <th sortable-column="totalAmount" onClick={this.onSort.bind(this, 'totalAmount')} >
                                Total del pedido
                                   <i aria-hidden='true' className={(sortData['totalAmount']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                                </th>


                                <th sortable-column="paymentType" onClick={this.onSort.bind(this, 'paymentType')} >
                                Método de pago
                                   <i aria-hidden='true' className={(sortData['paymentType']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                                </th>


                                <th sortable-column="orderStatus" onClick={this.onSort.bind(this, 'orderStatus')} >
                                Estado del pedido
                                   <i aria-hidden='true' className={(sortData['orderStatus']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                                </th>

                                <th > Comportamiento</th>
                              </tr>
                            </thead>
                            <tbody>
                              {
                                orderListing && orderListing.length > 0 && orderListing.map((order, key) => {
                                  return (
                                    <tr className='animated fadeIn' key={key}>
                                      <td>
                                        {order.orderId}
                                      </td>
                                      <td>{moment(order.createdAt.slice(0, 10)).locale('es-es').format("DD/MM/YYYY")}</td>
                                      <td>{order.count}</td>
                                      <td>{order.discount}</td>
                                      <td>{order.totalAmount}</td>
                                      <td>{order.paymentType}</td>
                                      <td><span className="bg-warning status">{order.orderStatus}</span></td>
                                      <td>
                                        <button className="btn text-primary" onClick={() => this.props.history.push(`/orderview/${order._id}`)}>
                                          <i className="fa fa-eye" data-toggle="tooltip" title="View"></i>
                                        </button>
                                      </td>
                                    </tr>
                                  )
                                })
                              }

                            </tbody>
                          </table>
                        </div>
                        <div className="table-footer text-right">
                          <label>Demostración</label>
                          <Select showSearch placeholder={<b> {total <= length ? total : length}</b>} optionFilterProp="children"
                            onSelect={this.handleChangePageSize.bind(this)} onFocus={this.handleFocus} onBlur={this.handleBlur} >
                            <Option value={5}>5</Option>
                            <Option value={10}>10</Option>
                            <Option value={15}>15</Option>
                            <Option value={50}>50</Option>
                          </Select>
                          <label>Fuera de {total} Stock</label>
                          <div className="pagination-list">
                            <Pagination className="ant-pagination" pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} />
                          </div>
                        </div>
                      </> : <h6>No se encontraron pedidos</h6>}
                    </div>
                  </div>
                </div>
              </div >
            </div>
          </div >
        </form>
      </Home>

    );
  }
}
const mapStateToProps = state => ({
  user_details: state.user.userdatails,
  permissionsList: state.admindata.rolePermissions,

})

export default connect(mapStateToProps, actions)(EditUser);