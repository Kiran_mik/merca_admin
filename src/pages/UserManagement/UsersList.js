import React, { Component } from 'react'
import { connect } from 'react-redux'
import Pagination from 'rc-pagination';
import 'rc-pagination/assets/index.css';
import swal from 'sweetalert'
import * as actions from '../../actions'
import { isEmpty } from "lodash"
import 'antd/dist/antd.css'
import { Select } from 'antd'
import Home from '../Home'
import Toggle from 'react-toggle'
import _ from "lodash"
import { API_URL, IMAGE_URL, CSV_URL, EXCEL_URL } from '../../config/configs'
import { BeatLoader } from 'react-spinners';
import Loader from '../Loader'
import $ from 'jquery';
const FileDownload = require('js-file-download')

class UsersList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectAll: false,
      page: 1,
      pagesize: 50,
      userListing: [],
      best_sellers: false,
      multipleDelete: [],
      total: 0,
      sort: { createdAt: -1 },
      selectedOption: 'Selecciona aquí',
      sortData: { firstName: false, lastName: false, emailId: false, isVerified: false, isActive: false },
      ids: true,
      profileImage: true,
      firstName: true,
      lastName: true,
      emailId: true,
      mobileNo: true,
      verify: true,
      status: true,
      selectedId: [],
      listOfIds: [],
      selectedFname: [],
      listOfFnames: [],
      selectedLname: [],
      listOfLnames: [],
      selectedEmails: [],
      listOfEmails: [],
      selectedMobileno: [],
      listOfMobileno: [],
      publishedvalue: '',
      length: '',
      usersAccess: {},
      loading: true,
      page_: false,
      download: false
    }
  }

  componentDidMount() {
    var permissions = this.props.permissionsList;
    if (permissions && permissions.rolePermission && permissions.rolePermission.usersAccess && permissions.rolePermission.usersAccess.viewList === false) {
      this.props.history.push('/dashboard')
    }
    if (permissions.rolePermission) {
      let { usersAccess } = permissions.rolePermission
      this.setState({ usersAccess: usersAccess })
    }
    $('.filterlink').click(function () {
      $('#itemlist').hide();
      $('#filterlist').stop().slideToggle();
    });
    $('.listlink').click(function () {
      $('#filterlist').hide();
      $('#itemlist').stop().slideToggle();
    });
    $('[data-toggle="tooltip"]').tooltip();

    this.userListing('pagesize')
  }

  //* ********** SORTING ************************//
  onSort = column => {
    let { sortData } = this.state
    var element, value
    for (const key in sortData) {
      if (key == column) {
        sortData[key] = !sortData[key]
        element = key
        value = -1
        if (sortData[key]) {
          value = 1
        }
        this.setState({ sort: { [element]: value } }, () => { this.userListing() })
        this.setState({ sortData })
      } else {
        sortData[key] = false
        element = key
        value = 1
      }
    }
    this.setState({ sortData })
  }

  //* **********  USER LISTING  ************************//


  userListing = (e) => {
    var url = '/user/userListing'
    var method = 'post'
    var { page, pagesize, sort, selectedId, selectedFname, publishedvalue, selectedLname, selectedEmails, selectedMobileno } = this.state
    var data = {}
    if (e === 'filter') {
      data.page = 1
    } else if (e === 'pagesize') {
      data = data
    } else {
      data.page = page
    }
    if (e === 'pagesize') {
      data = data
    } else {
      data.pagesize = pagesize
    }
    if (!isEmpty(sort)) {
      data.sort = sort
    }
    if (selectedId != '') {
      data.userId = selectedId
    }
    if (selectedFname != '') {
      data.firstName = selectedFname
    }
    if (selectedLname != '') {
      data.lastName = selectedLname
    }
    if (selectedEmails != '') {
      data.emailId = selectedEmails
    }
    if (publishedvalue != '') {
      data.status = publishedvalue
    }
    if (selectedMobileno != '') {
      data.mobileNo = selectedMobileno
    }
    var body = data
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (data1.status === 0) {
        this.setState({ userListing: [] })
      }
      else {
        this.setState({ userListing: data.userListing, total: data.total, length: data.userListing.length, loading: false })
        if (data.userListing.length <= pagesize) {
          this.setState({ page_: false })
        }
      }
      if (e == 'filter') {
        this.setState({ page: 1 })
      }
      if (data.manageUserListing) {
        this.setState({
          ids: data.manageUserListing.userId,
          profileImage: data.manageUserListing.photo,
          firstName: data.manageUserListing.firstName,
          lastName: data.manageUserListing.lastName,
          emailId: data.manageUserListing.emailId,
          mobileNo: data.manageUserListing.mobileNo,
          verify: data.manageUserListing.emailVerify,
          status: data.manageUserListing.status,
          pagesize: data.manageUserListing.pageSize
        })
      }
    }
    )
  }
  //* **********  RESET LISTING  ************************//
  resetListing = (e) => {
    var url = '/user/userListing'
    var method = 'post'
    var { page, pagesize } = this.state
    var body = { page: page, pagesize: pagesize }
    var token = localStorage.getItem('token')
    this.props.commonApiCall(url, method, body, token, null, this.props, response => {
      let data1 = response.data
      let data = response.data.data
      if (data1.status === 0) {
        this.setState({ userListing: [] })
      }
      else
        this.setState({
          userListing: data.userListing, total: data.total,
          selectedId: [],
          selectedFname: [],
          publishedvalue: 'Select',
          selectedLname: [],
          selectedEmails: [],
          selectedMobileno: [],
          total: data.total, length: data.userListing.length
        })
      if (data.manageUserListing) {
        this.setState({
          ids: data.manageUserListing.userId,
          profileImage: data.manageUserListing.photo,
          firstName: data.manageUserListing.firstName,
          lastName: data.manageUserListing.lastName,
          emailId: data.manageUserListing.emailId,
          mobileNo: data.manageUserListing.mobileNo,
          verify: data.manageUserListing.emailVerify,
          status: data.manageUserListing.status,
        })
      }
    }
    )
  }

  //* ********** DELETE USER   ************************//
  deleteUser = uid => {
    var delArr = this.state.multipleDelete;
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          if (delArr.length > 0) {
            var body = { userId: delArr }
          }
          else {
            var body = { userId: [uid] }
          }
          let token = localStorage.getItem('token')
          var url = '/user/deleteUsers'
          var method = 'post'
          this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
            let { data } = response
            if (data.status == 1) {
              swal(data.message, '', 'success')
            } else {
              swal(data.message, '', 'error')
            }
            this.userListing()
            this.setState({ selectedOption: 'Selecciona aquí' })
          })
            .catch(err => console.error(err))
        }
      });
  }

  // ########################### Set Table Rows #########################

  setTableRows = (e) => {
    var token = localStorage.getItem('token')
    let { ids, profileImage, firstName, lastName, emailId, mobileNo, verify, status, pagesize } = this.state
    var body = { userId: ids, photo: profileImage, firstName, lastName, emailId, mobileNo, emailVerify: verify, status, pageSize: pagesize }
    var url = '/user/setUserFilter'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      if (response) {
        this.userListing()
        if (e) {
          swal('Actualizado con éxito !', '', 'success')
        }
      }
    })
  }
  //* ********** EDIT USER STATUS  ************************//

  userStatusChange(status, Id) {
    var status = !status
    var token = localStorage.getItem('token')
    var body = { userIds: [Id], isActive: status }
    var urlkey = '/user/changeUserStatus'
    this.props.changeStatus(body, token, urlkey, response => {
      let { data } = response
      if (data.status == 1) {
        if (status == false) {
          swal('Usuario desactivado con éxito !', '', 'success')
        }
        else {
          swal('Usuario activado con éxito!', '', 'success')
        }
        this.userListing()
      }
      else if (data.message === "Invalid token") {
        this.props.history.push('/')
      }
      else {
        swal(data.message, '', 'error')
      }
    })
  }

  //* **********  SINGLE USER DETAILS  ************************//
  gettingUserDetails = (Id) => {
    var body = { Id }
    this.props.editDetails(body)
    this.props.history.push(`/edituser/${Id}`)
  }



  checkArray(_id) {
    let { multipleDelete } = this.state;
    if (multipleDelete.includes(_id)) {
      return true;
    }
    else {
      return false;
    }
  }

  onCheckbox(_id, val) {
    var delarray = this.state.multipleDelete;
    if (!delarray.includes(_id)) {
      delarray.push(_id);
    }
    else {
      delarray.splice(delarray.indexOf(_id), 1);
    }
    if (delarray.length != this.state.userListing.length) {
      this.setState({ checked: false });
    }
    // if (this)
    this.setState({ multipleDelete: delarray })
  }

  // ########################### PAGINATION #########################

  paginationChange(page, pagesize) {
    this.setState({
      page: page,
      pagesize: pagesize
    }, () => this.userListing());
    this.setState({ selectAll: false }, () => this.selectAllcheck())
  }
  handleChangePageSize = (value) => {
    this.setState({ pagesize: value, page: 1 }, () => this.setTableRows(), () => this.userListing());
  }
  handleChange_ = (val) => {
    if (val) {
      this.setState({ pagesize: Number(val), page: 1, page_: true }, () => this.setTableRows(), () => this.userListing());
    }
    else {
      this.setState({ pagesize: this.state.length, page: 1 }, () => this.userListing());
    }
  }

  applyfilter = () => {
    var delArr = this.state.multipleDelete
    if (this.state.selectedOption == 'Delete') {
      if (delArr.length > 0) {
        this.deleteUser(...delArr)
      }
    }
    if (this.state.selectedOption == 'Active') {
      var body = { userIds: delArr, isActive: true }
    }
    if (this.state.selectedOption == 'InActive') {
      var body = { userIds: delArr, isActive: false }
    }
    var url = '/user/changeUserStatus'
    var method = 'post'
    var token = localStorage.getItem('token')
    if (delArr.length > 0) {
      this.props.commonApiCall(url, method, body, token, null, this.props, response => {
        let { data } = response
        if (data.status == 1) {
          if (this.state.selectedOption == 'InActive') {
            swal('¡Usuarios desactivados con éxito!', '', 'success')
          }
          else {
            swal('¡Usuarios activados con éxito!', '', 'success')
          }
          this.userListing()
        }
        this.setState({ multipleDelete: [], selectedOption: 'Selecciona aquí' })
      })
    } else {
      this.setState({ selectedOption: 'Selecciona aquí' })
      swal('Seleccione al menos un usuario', '', 'info')
    }
  }

  selectAllcheck = () => {
    var delarray = this.state.multipleDelete
    this.onCheckbox()
    var { userListing } = this.state
    if (this.state.selectAll) {
      userListing.map(each => {
        if (!delarray.includes(each._id)) {
          delarray.push(each._id)
        }
      })
    } else {
      userListing.map(each => {
        delarray.splice(delarray.indexOf(each._id), 1)
      })
    }
  }
  // ###########################  for Search #########################
  getIds = (e, name) => {
    let token = localStorage.getItem('token')
    var body = { type: [name], searchText: e }
    var url = '/user/userFieldsList'
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var data = response.data.data
      if (name === 'userId') {
        var idArr = _.uniqBy(data, (e) => {
          return e.userId.toString();
        });
        this.setState({ listOfIds: idArr })
      }
      if (name === 'firstName') {
        var fNArr = _.uniqBy(data, (e) => {
          return e.firstName.toString();
        });
        this.setState({ listOfFnames: fNArr })
      }
      if (name === 'lastName') {
        var lNArr = _.uniqBy(data, (e) => {
          return e.lastName.toString();
        });
        this.setState({ listOfLnames: lNArr })
      }
      if (name === 'emailId') {
        var listOfEmails = []
        data.map((each, key) => {
          listOfEmails.push(each.emailId)
          this.setState({ listOfEmails })
        })
      }

      if (name === 'mobileNo') {
        var mNArr = _.uniqBy(data, (e) => {
          return e.mobileNo;
        });
        this.setState({ listOfMobileno: mNArr })

      }

    })
  }



  // ########################### download CSV #########################
  downloadCSV(type, array) {
    let { ids, profileImage, firstName, lastName, emailId, mobileNo, verify, status, selectedId, selectedFname, publishedvalue, selectedLname, selectedEmails, selectedMobileno, multipleDelete } = this.state
    let token = localStorage.getItem('token')
    this.setState({ download: true })
    if (array === "totalList") {
      var data = { filteredFields: ["userId", "firstName", "lastName", "emailId", "mobileNo", "isVerified", "isActive", "photo"] }
    }

    if (array === "filteredList") {
      var data = { filteredFields: [] }
      if (ids) {
        data.filteredFields.push('userId')
      };
      if (firstName) {
        data.filteredFields.push('firstName')
      };
      if (lastName) {
        data.filteredFields.push('lastName')
      };
      if (profileImage) {
        data.filteredFields.push('photo')
      };
      if (emailId) {
        data.filteredFields.push('emailId')
      };
      if (mobileNo) {
        data.filteredFields.push('mobileNo')
      };
      if (verify) {
        data.filteredFields.push('isVerified')
      };
      if (status) {
        data.filteredFields.push('isActive')
      };
      if (multipleDelete.length > 0) {
        data.usersArray = multipleDelete
      }
      if (selectedId != '') {
        data.userId = selectedId
      }
      if (selectedFname != '') {
        data.firstName = selectedFname
      }
      if (selectedLname != '') {
        data.lastName = selectedLname
      }
      if (selectedEmails != '') {
        data.emailId = selectedEmails
      }
      if (publishedvalue != '') {
        data.status = publishedvalue
      }
      if (selectedMobileno != '') {
        data.mobileNo = selectedMobileno
      }
    }
    var body = data
    var url = '/user/' + type
    var method = 'post'
    this.props.commonApiCall(url, method, body, token, null, this.props, (response) => {
      var data = response.data.data
      if (data) {
        this.setState({ multipleDelete: [], download: false })
        if (type === 'downloadUserCsvFile') {
          window.open(CSV_URL + data.filePathAndName, '_blank');
        } else {
          window.open(EXCEL_URL + data.filePathAndName, '_blank');
        }
      }
    })
  }



  render() {
    const Option = Select.Option
    let { usersAccess, ids, profileImage, firstName, lastName, emailId, mobileNo, verify, status, userListing, pagesize, page, sortData, total, selectedOption,
      selectedId, listOfIds, selectedFname, listOfFnames, selectedLname, listOfLnames, selectedEmails, listOfEmails, selectedMobileno, listOfMobileno, publishedvalue, length } = this.state

    const filteredIds1 = listOfIds.map((each) => { return each.userId });
    const filteredIds = filteredIds1.filter(o => !selectedId.includes(o));

    const filteredFnames1 = listOfFnames.map((each) => { return each.firstName });
    const filteredFnames = filteredFnames1.filter(o => !selectedFname.includes(o));

    const filteredLnames1 = listOfLnames.map((each) => { return each.lastName });
    const filteredLnames = filteredLnames1.filter(o => !selectedLname.includes(o));

    const filteredEmails = listOfEmails.filter(o => !selectedEmails.includes(o));

    const filteredMobile1 = listOfMobileno.map((each) => { return each.mobileNo });
    const filteredMobile = filteredMobile1.filter(o => !selectedMobileno.includes(o));
    return (
      <Home>
        <div className='row'>
          <div className='col-md-12'>
            <div className='page-header'>
              <h3>Gestión de usuarios</h3>
              <ul className='breadcrumb '>
                <li className='breadcrumb-item' onClick={() => this.props.history.push('/dashboard')}>Página principal</li>
                <li className='breadcrumb-item active'>Gestión de usuarios </li>
              </ul>
            </div>
          </div>
        </div>
        <BeatLoader sizeUnit={"px"} size={30} color={'#2472DC'} loading={this.state.loading} />
        {this.state.download ? <Loader /> : null}
        <div className='card animated fadeIn'>
          <div className="card-header">
            <div className="row data-filter justify-content-end">
              <div className="col-md-12 text-md-right">
                <div className="button-continer text-right">
                  {usersAccess.delete == false && usersAccess.status == false ? null :
                    <>
                      {usersAccess.status && usersAccess.delete ?
                        <Select
                          showSearch
                          placeholder="Selecciona aquí"
                          optionFilterProp="children"
                          className="applyselect"
                          value={selectedOption}
                          onSelect={(value) => this.setState({ selectedOption: value })}
                        >
                          <Option value="Active">Activo</Option>
                          <Option value="InActive">Inactivo</Option>
                          <Option value="Delete">Eliminar</Option>
                        </Select> : <>{usersAccess.delete ?
                          <Select
                            showSearch
                            placeholder="select"
                            optionFilterProp="children"
                            className="applyselect"
                            value={selectedOption}
                            onSelect={(value) => this.setState({ selectedOption: value })}
                          >
                            <Option value="Delete">Eliminar</Option>
                          </Select> : <Select
                            showSearch
                            placeholder="select"
                            optionFilterProp="children"
                            className="applyselect"
                            value={selectedOption}
                            onSelect={(value) => this.setState({ selectedOption: value })}
                          >
                            <Option value="Active">Activo</Option>
                            <Option value="InActive">Inactivo</Option>
                          </Select>} </>}

                      <button type="button" className="btn btn-primary ml-0 apply_btn" onClick={this.applyfilter}>Aplicar</button></>}

                  {usersAccess.download ?
                    <div className="dropdown">
                      <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <span>Herramientas</span>
                      </button>
                      <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadUserExcelFile", "totalList")}>Exportar a Excel</a>
                        <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadUserCsvFile", "totalList")}>Exportar a CSV</a>
                      </div>
                    </div> : null}
                  <button type="button" className="nav-link  btn btn-teal listlink" >
                    <i className="fa fa-columns mr-0" aria-hidden="true" />
                  </button>
                  <button className="nav-link pull-right btn btn-teal filterlink" type="button" >
                    <i className="fa fa-filter mr-0" aria-hidden="true" />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="item-list mt-3" id="itemlist">
            <ul className="row  mb-0">
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ ids: !this.state.ids })} checked={ids} /><span></span>Número de DNI</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ profileImage: !this.state.profileImage })} checked={profileImage} /><span></span>Imagen de perfil</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ firstName: !this.state.firstName })} checked={firstName} /><span></span>Primer nombre</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ lastName: !this.state.lastName })} checked={lastName} /><span></span>Apellido</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ emailId: !this.state.emailId })} checked={emailId} /><span></span>Email</label></li>
              <li className="col-sm-3 checkbox"> <label><input type="checkbox" onChange={() => this.setState({ mobileNo: !this.state.mobileNo })} checked={mobileNo} /><span></span>Celular</label></li>
              <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ verify: !this.state.verify })} checked={verify} /><span></span>Verificación de correo electrónico</label></li>
              {usersAccess.status ? <li className="col-sm-3 checkbox"><label><input type="checkbox" onChange={() => this.setState({ status: !this.state.status })} checked={status} /><span></span>Estado</label></li> : null}
            </ul>
            <hr />
            <button className="nav-link pull-right btn btn-outline-primary ml-2" type="button" onClick={this.userListing}>
              Reiniciar
            </button>

            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setState({ ids: true, profileImage: true, firstName: true, lastName: true, emailId: true, mobileNo: true, verify: true, status: true })}>
              Seleccionar todo
            </button>
            <button className="nav-link pull-right btn btn-primary ml-2" type="button" onClick={() => this.setTableRows('rows')}>
              Guardar
            </button>

          </div>
          <div className="filter-list" id="filterlist">
            <div className="row">
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>ID de usuario</label>
                  <Select
                    mode="multiple"
                    placeholder="ID de usuario"
                    value={selectedId}
                    onChange={(val) => this.setState({ selectedId: val })}
                    onSearch={(e) => this.getIds(e, 'userId')}
                    onFocus={(e) => this.getIds(e, 'userId')}
                    style={{ width: '100%' }}
                  >
                    {filteredIds.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Nombre</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese nombre"
                    value={selectedFname}
                    onChange={(val) => this.setState({ selectedFname: val })}
                    onSearch={(e) => this.getIds(e, 'firstName')}
                    onFocus={(e) => this.getIds(e, 'firstName')}

                    style={{ width: '100%' }}
                    filterOption={(input, option) =>
                      option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {filteredFnames.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Apellido</label>
                  <Select
                    mode="multiple"
                    placeholder="Introduzca el apellido"
                    value={selectedLname}
                    onChange={(val) => this.setState({ selectedLname: val })}
                    onSearch={(e) => this.getIds(e, 'lastName')}
                    onFocus={(e) => this.getIds(e, 'lastName')}

                    style={{ width: '100%' }}
                  >
                    {filteredLnames.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Email</label>
                  <Select
                    mode="multiple"
                    placeholder="Ingrese correo electrónico"
                    value={selectedEmails}
                    onChange={(val) => this.setState({ selectedEmails: val })}
                    onSearch={(e) => this.getIds(e, 'emailId')}
                    onFocus={(e) => this.getIds(e, 'emailId')}

                    style={{ width: '100%' }}
                  >
                    {filteredEmails.map(item => (
                      <Select.Option key={item} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Celular</label>
                  <Select
                    mode="multiple"
                    placeholder="Entrar en el celular"
                    value={selectedMobileno}
                    onChange={(val) => this.setState({ selectedMobileno: val })}
                    onSearch={(e) => this.getIds(e, 'mobileNo')}
                    onFocus={(e) => this.getIds(e, 'mobileNo')}

                    style={{ width: '100%' }}
                  >
                    {filteredMobile.map((item, index) => (
                      <Select.Option key={index} value={item}>
                        {item}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="col-xl-3 col-lg-4 col-sm-6 col-12">
                <div className="form-group">
                  <label>Estado</label>
                  <select className="form-control" onChange={(e) => this.setState({ publishedvalue: e.target.value })} value={publishedvalue}>
                    <option value=''>Seleccionar</option>
                    <option value='active'>Activo</option>
                    <option value='inActive'>Inactivo</option>
                  </select>
                </div>
              </div>
            </div>
            <hr />
            <div className="pull-right filter-button">
              {usersAccess.download ?
                <div className="dropdown ml-2">
                  <button className="nav-link  btn btn-teal dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                    <span>Herramientas</span>
                  </button>
                  <div className="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                    <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadUserExcelFile", "filteredList")}>Exportar a Excel</a>
                    <a className="dropdown-item" href="#" onClick={this.downloadCSV.bind(this, "downloadUserCsvFile", "filteredList")}>Exportar a CSV</a>
                  </div>
                </div> : null}
              <button className="nav-link  btn btn-primary ml-2" type="button" onClick={() => this.userListing('filter')} >
                Aplicar filtro
            </button>
              <button className="nav-link  btn btn-outline-primary ml-2" type="button" onClick={this.resetListing} >
                Reiniciar
            </button>
            </div>
          </div>

          <div className='card-body'>
            <div className='table-responsive'>

              <table className='table dataTable with-image row-border hover custom-table table-striped'>
                <thead>
                  <tr>
                    {usersAccess.delete == false && usersAccess.status == false ? null :
                      <th>
                        <div className='checkbox'>
                          <label>
                            <input type="checkbox" className="form-check-input" checked={this.checkArray()} onChange={() => { this.setState({ selectAll: !this.state.selectAll }, () => this.selectAllcheck()) }} />
                            <span />
                            <i className='input-helper' />
                          </label>
                        </div>
                      </th>}

                    {ids ? <th>ID de usuario</th> : null}
                    {profileImage ? <th>Perfil</th> : null}
                    {firstName ? <th sortable-column="firstName" onClick={this.onSort.bind(this, 'firstName')} >
                      Primer nombre
                      <i aria-hidden='true' className={(sortData['firstName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {lastName ? <th sortable-column="lastName" onClick={this.onSort.bind(this, 'lastName')} >
                      Apellido
                      <i aria-hidden='true' className={(sortData['lastName']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {emailId ? <th sortable-column="emailId" onClick={this.onSort.bind(this, 'emailId')}>
                      Email
                      <i aria-hidden='true' className={(sortData['emailId']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {mobileNo ? <th > Celular <i aria-hidden='true' /> </th> : null}
                    {verify ? <th sortable-column="isVerified" onClick={this.onSort.bind(this, 'isVerified')}>
                      Verificación de correo electrónico
                      <i aria-hidden='true' className={(sortData['isVerified']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}
                    {status && usersAccess.status ? <th sortable-column="isActive" onClick={this.onSort.bind(this, 'isActive')}>
                      Estado
                    <i aria-hidden='true' className={(sortData['isActive']) ? "fa fa-arrow-up" : "fa fa-arrow-down"} />
                    </th> : null}

                    {usersAccess.viewDetails == false && usersAccess.delete == false ? null : <th> Comportamiento</th>}
                  </tr>
                </thead>
                {userListing.length === 0 && this.state.loading === false ?
                  <tbody><tr><td colSpan="10" style={{ padding: '30px', width: '100%' }}>No se encontraron registros </td></tr></tbody>
                  :
                  <tbody>
                    {userListing.map((user, Key) => {
                      return (
                        <tr key={user._id} key={Key} className='animated fadeIn'>
                          {usersAccess.delete == false && usersAccess.status == false ? null :
                            <td>
                              <div className='checkbox'>
                                <label>
                                  <input type="checkbox" className="form-check-input" checked={(this.checkArray(user._id)) ? true : false} onChange={() => this.onCheckbox(user._id)} />
                                  <span />
                                  <i className='input-helper' />
                                </label>
                              </div>
                            </td>}
                          {ids ? <td>{user.userId}</td> : null}
                          {profileImage ?
                            <td><div className="thumb-img"><img src={user.photo ? IMAGE_URL + user.photo : "assets/images/no-image-user.png"} alt="pic" /></div></td> : null}
                          {firstName ? <td onClick={() => this.gettingUserDetails(user._id)}><u>{user.firstName}</u> </td> : null}
                          {lastName ? <td> {user.lastName} </td> : null}
                          {emailId ? <td>{user.emailId} </td> : null}
                          {mobileNo ? <td>{user.mobileNo ? user.mobileNo : 'No Number'}</td> : null}
                          {/* {verify ? <td><i className="fa fa-check text-success"></i></td> : null} */}
                          {verify ? <td><i className={user.isVerified ? "fa fa-check text-success" : "fa fa-close text-danger"}></i></td> : null}
                          {status && usersAccess.status ?
                            <td>
                              <label>
                                <Toggle
                                  checked={user.isActive}
                                  className='custom-classname'
                                  onChange={() =>
                                    this.userStatusChange(user.isActive, user._id)
                                  }
                                />
                              </label>
                            </td> : null}
                          {usersAccess.viewDetails == false && usersAccess.delete == false ? null :
                            <td>
                              {usersAccess.viewDetails ?
                                <button
                                  onClick={() => this.gettingUserDetails(user._id)}
                                >
                                  <i className='fa fa-eye text-primary' aria-hidden='true' data-toggle="tooltip" title="View" />
                                </button> : null}
                              {usersAccess.delete ?
                                <button onClick={() => this.deleteUser(user._id)}>
                                  <i className='fa fa-trash text-danger' aria-hidden='true' data-toggle="tooltip" title="Delete" />
                                </button> : null}
                            </td>
                          }
                        </tr>
                      )
                    })}
                  </tbody>}

              </table>
            </div>
            {userListing.length > 0 ?
              <div className="table-footer text-right">
                <label>Demostración</label>
                <Select showSearch placeholder={<b> {total < 50 ? total : length}</b>} optionFilterProp="children"
                  onSelect={this.handleChangePageSize.bind(this)}
                  value={this.state.page_ ? pagesize : (length <= pagesize ? length : pagesize)}
                  onSearch={this.handleChange_}>
                  <Option value={50}>50</Option>
                  <Option value={75}>75</Option>
                  <Option value={100}>100</Option>
                  <Option value={125}>125</Option>
                  <Option value={150}>150</Option>
                </Select>
                <label>Fuera de  {total} usuarios</label>
                <div className="pagination-list">
                  <Pagination className="ant-pagination" showLessItems pageSize={pagesize} current={page} total={total} onChange={this.paginationChange.bind(this)} locale={true} />
                </div>
              </div> : null}
          </div>
        </div>

      </Home>
    )
  }
}

const mapStateToProps = state => ({
  permissionsList: state.admindata.rolePermissions,
});
export default connect(mapStateToProps, actions)(UsersList)
