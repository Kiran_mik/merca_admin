import {USER_DETAILS} from './types'
import axios from 'axios';
import {API_URL} from '../config/configs'

//**************************** USER DETAILS********************************************//
export const UserDetails =(body,token, callback) => async dispatch =>{
    try {
      let response = await axios({
        method: 'post',
        url:API_URL+'/user/gettingUserDetails',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token
        },
        data:body
      });
      let {data}=response;
      if(data.status == 1){
        let payload = {
          data : data.data
        }
        dispatch({ type: USER_DETAILS, payload });
      }
      callback(response);
      
    } catch (error) {
      throw error;
    }
  };
  