import axios from "axios";
import { API_URL } from "../config/configs";
import { EDIT_DETAILS,LOG_OUT } from "./types";
import swal from 'sweetalert';


// ################################# logout ###############################
export  const  Logout  = (props) =>  async  dispatch  => {
  const token = {token:localStorage.getItem("token")}
  axios({
    method: "post",
    url: API_URL + '/auth/logOut',
    headers: {
      "Content-Type": "application/json",
      "Authorization": localStorage.getItem('token')
    },
    data: JSON.stringify(token)
  })
  .then(response => {
    localStorage.removeItem('token','')
     props.history.push('/')
    dispatch({ type:  LOG_OUT, payload:  " " });
  })
};


// ################################# Change Status ###############################
export const changeStatus =(body,token,urlkey, callback) => async dispatch =>{
    try {
      let response = await axios({
        method: 'post',
        url:API_URL+"/"+urlkey,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token
        },
        data:JSON.stringify(body)
      });
      let {data}=response;
      callback(response);
    } catch (error) {
      throw error;
    }
  };


// ################################# EDIT DETAILS ###############################

export const editDetails =(body) => async dispatch =>{
  dispatch({ type: EDIT_DETAILS, payload:body });   
}
  



  /************************ METHOD = "POST" *************************************/

export const commonApiCall = (url, method, body, token, Type, props, callback) => async  dispatch => {
  if (method == 'post') {
    axios({
      method: "post",
      url: API_URL + url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: JSON.stringify(body)
    })
    .then(response => {
      let { data } = response;
      if (data.status === 1) {
        if(Type != null ) {
          var payload = { data: data.data}
          dispatch({ type: Type, payload });
        }
        callback(response)
      } 
      else if( data.message === "Invalid token."||data.message === "Token is expired."){
        swal('Session Expired','','error')
        .then((willDelete) => {
          // if (willDelete) {
          localStorage.removeItem("token"); props.history.push('/');
          // }
          })
       }
       else if (data.status === 0||data.statusCode===432||data.statusCode===434) {
        callback(response)
      } 
       
    })
      .catch(err => {
        console.log('err', err); 
      })
  } 
  /************************ METHOD = "GET" *************************************/
  if (method == 'get') {
     axios({
      method: "get",
      url: API_URL + url,
      headers: {
        "Content-Type": "application/json",
        "Authorization": token
      },
    })
      .then(response => {
        let  {data}  = response;
        if(data.message === "Invalid token."||data.message === "Token is expired."){
          swal('Session Expired','','error')
          .then((willDelete) => {
            // if (willDelete) {
            localStorage.removeItem("token"); props.history.push('/');
            // }
            })
        }else{
          callback(response);
        }
      })
      .catch(err => {
        console.log('err', err)
      });
  }
};
