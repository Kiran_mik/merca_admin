import axios from "axios";
import { API_URL } from "../config/configs";
import { CHANGE_PASSWORD, ADMIN_LOGIN } from "./types";


/****************** Admin change Password *************************/
export const changePassword = (body, callback) => async dispatch => {
  const token = localStorage.getItem("token")
  try {
    let response = await axios({
      method: 'post',
      url: API_URL + "/auth/changePassword",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      },
      data: JSON.stringify(body),

    });
    let { data } = response;
    if (data.status == 1) {
      await localStorage.setItem("user", JSON.stringify(data));
      let payload = {
        data: data.data

      }
      dispatch({
        type: CHANGE_PASSWORD,
        payload
      });
    }
    callback(response);
  } catch (error) {
    throw error;
  }
};



// ################################## ADMIN LOGIN ##########################

export const adminLogin = (data1, callback) => async dispatch => {
  try {
    let response = await axios.post(API_URL + '/admins/adminLogin', data1);
    let { data } = response;
    if (data.status === 1) {
      localStorage.setItem("token", data.data.token)
      let payload = {
        access_token: data.data.token,
        data: data.data,
        rolepermissions: data.data.rolePermission
      }
      dispatch({ type: ADMIN_LOGIN, payload });
    }
    callback(response);
  } catch (error) {
    throw error;
  }
};


