import {CHANGE_PASSWORD,ADMIN_LOGIN,EDITADMIN_PROFILE } from '../actions/types'
const INITIAL_STATE = {
  admindata: {},
  rolePermissions:{},
  editadmindata: {},
  editAdminProfileData:{}
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CHANGE_PASSWORD:
      return action.payload || false
      case EDITADMIN_PROFILE:
      return  Object.assign({},state,{editAdminProfileData:action.payload.data});
      case ADMIN_LOGIN:
      return  Object.assign({},state,{admindata:action.payload,rolePermissions:action.payload.data});
    default:
      return state
  }
}
