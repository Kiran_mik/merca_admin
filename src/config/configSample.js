module.exports = {
	API_URL: "http://mercadoapi.node.indianic.com/api",
	IMAGE_URL: "http://mercadoapi.node.indianic.com/public/upload/",
	CSV_URL: "http://mercadoapi.node.indianic.com/public/csv/",
	EXCEL_URL: "http://mercadoapi.node.indianic.com/public/excel/",
	PDF_URL:"http://mercadoapi.node.indianic.com/public/pdf/",
	PORT: 4155
	};
