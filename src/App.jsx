import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch,Redirect } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './store';
import AdminLogin from './pages/AdminLogin';
import ResetPassword from './pages/ResetPwd';
import AdminProfilePage from './pages/AdminProfilePage';
import Dashboard from './pages/Dashboard';
import UsersList from './pages/UserManagement/UsersList';
import UserAdmin from './pages/UserManagement/AdminUsers.jsx';
import EditUser from './pages/UserManagement/EditUser';
import AddUser from './pages/UserManagement/AddAdminUser';
import ProductsList from './pages/ProductCatalog/Products/ProductsList.jsx'
import AddProduct from './pages/ProductCatalog/Products/AddProduct.jsx';
import CategoryList from './pages/ProductCatalog/Categories/CategoryList';
import AddCategory from './pages/ProductCatalog/Categories/AddCategory'
import BulkUploads from './pages/ProductCatalog/BulkUploads'
import SubCategoriesList from './pages/ProductCatalog/SubCategories/SubCategoriesList.jsx';
import EditSubCategory from './pages/ProductCatalog/SubCategories/EditSubCategory';
import AddSubCategory from './pages/ProductCatalog/SubCategories/AddSubCatagory.jsx';
import ListingSSCatagories from './pages/ProductCatalog/SubSubCategories/ListingSSCatagories.jsx';
import AddssCategory from './pages/ProductCatalog/SubSubCategories/AddssCatagory.jsx';
import EditSsubCategory from './pages/ProductCatalog/SubSubCategories/EditSsCatagory.jsx';
import HomeSection from './pages/BannerSection/HomeBanner';
import EditHomeSection from './pages/BannerSection/EditHomeBanner';
import AdminForgotPassword from './pages/ForgotPwd';
import AdminChangePassword from './pages/ChangePwd';
import OrderManagement from './pages/OrderManagement/OrderList';
import PlaceOrderByAdmin from './pages/OrderManagement/PlaceOrderByAdmin';
import InvoiceListing from './pages/OrderManagement/InvoiceList'
import InvoiceView from './pages/OrderManagement/InvoiceView';
import NewInvoice from './pages/OrderManagement/Invoice_new';
import Shipments from './pages/OrderManagement/ShipmentList'
import ShipmentView from './pages/OrderManagement/ShipmentView'
import NewShipment from './pages/OrderManagement/Shipment_new'
import OrderView from './pages/OrderManagement/OrderView';
import CreditMemo from './pages/OrderManagement/CreditMemoList';
import NewCreditMemo from './pages/OrderManagement/CreditMemo_new';
import CreditMemoView from './pages/OrderManagement/CreditMemoView';
import Transactions from './pages/OrderManagement/TransactionList';
import TransactionView from './pages/OrderManagement/TransactionView';
import ReOrder from './pages/OrderManagement/ReOrder';
import EditOrder from './pages/OrderManagement/EditOrder';
import DiscountSettings from './pages/Settings/DiscountSettings';
import NotificationSettings from './pages/Settings/NotificationSettings';
import DefaultMeta from './pages/Settings/DefaultMeta';
import AccessManagement from './pages/ManageRoles/RolesList';
import EditPermissions from './pages/ManageRoles/EditPermissions';
import ProductReviews from './pages/ProductReviews/ReviewsList';
import EditReview from './pages/ProductReviews/EditReview'
import CmsView from './pages/CmsManagement/EditCms.jsx'
import CmsListing from './pages/CmsManagement/CmsListing'
import AdminEmails from './pages/EmailManagement/AdminEmails';
import PaymentSettings from './pages/Settings/PaymentSettings';
import SMTPsetting from './pages/EmailManagement/SMTPsetting';
import ShippingMethods from './pages/MasterManagement/ShippingList.jsx';
import AddShipping from './pages/MasterManagement/AddShipping.jsx';
import Suppleirs from './pages/MasterManagement/SuppliersList.jsx';
import AddSuppliers from './pages/MasterManagement/AddSuppliers';
import States from './pages/MasterManagement/StatesList.jsx';
import AddStates from './pages/MasterManagement/AddStates';
import Brand from './pages/MasterManagement/BrandList.jsx';
import AddBrand from './pages/MasterManagement/AddBrand';
import PageNotFound from './PageNotFound'
import UsersOptions from './pages/MasterManagement/UserOptions';




class App extends Component {
  constructor(props){
    super(props)
    this.state={
    }
  }
  componentDidMount(){
  }
  Authorization = () => {
    return localStorage.getItem('token') ? true : false
  }
  render() {
    const PrivateRoute = ({ component: Component, ...rest }) => (
      <Route {...rest} render={(props) => (
         this.Authorization()  ?  
        <Component {...props} /> 
         : <Redirect to='/' />
      )} />
    );

    const LoginCheckRoute = ({ component: Component, ...rest }) => (
      <Route {...rest} render={(props) => (
        this.Authorization()
          ? <Redirect to='/dashboard' />
          : <Component {...props} />
      )} />
    );
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Router>
            <Switch>
              <LoginCheckRoute exact path='/' component={AdminLogin} />
              <Route exact path="/forgotpassword" component={AdminForgotPassword} /> 
              <Route exact path="/resetpassword" component={ResetPassword} />
              <PrivateRoute exact path="/dashboard" component={Dashboard} />
              <PrivateRoute exact path="/usersList" component={UsersList} /> 
              <PrivateRoute exact path="/adminUsers" component={UserAdmin} />
              <PrivateRoute exact path="/edituser/:uId" component={EditUser} />
              <PrivateRoute exact path="/addAdminuser" component={AddUser} />
              <PrivateRoute exact path="/editAdminUser/:aId" component={AddUser} />
              <PrivateRoute exact path="/categorylist" component={CategoryList} />
              <PrivateRoute exact path="/addcategory" component={AddCategory} />
              <PrivateRoute exact path="/editcategory/:cId" component={AddCategory} />
              <PrivateRoute exact path="/subcategorylist/:scId" component={SubCategoriesList} />
              <PrivateRoute exact path="/addsubcategory/:scId" component={AddSubCategory} />
              <PrivateRoute exact path="/editsubcategory/:scId" component={EditSubCategory} />
              <PrivateRoute exact path="/addssCategory/:ssId" component={AddssCategory} />
              <PrivateRoute exact path="/ssCatagoryList/:ssId" component={ListingSSCatagories} />
              <PrivateRoute exact path="/editSsubcategory/:ssId" component={EditSsubCategory} />
              <PrivateRoute exact path="/productlist" component={ProductsList} />
              <PrivateRoute exact path="/addProduct" component={AddProduct} />
              <PrivateRoute exact path="/editProduct/:pId" component={AddProduct} />
              <PrivateRoute exact path="/bulkuploads" component={BulkUploads} />
              <PrivateRoute exact path="/adminprofilepage" component={AdminProfilePage} />
              <PrivateRoute exact path="/homesection" component={HomeSection} />
              <PrivateRoute exact path="/edithomesection" component={EditHomeSection} />
              <PrivateRoute exact path="/changePassword" component={AdminChangePassword} />
              <PrivateRoute exact path="/orderlisting" component={OrderManagement} />
              <PrivateRoute exact path="/placeOrderByAdmin" component={PlaceOrderByAdmin} />
              <PrivateRoute exact path="/orderview/:oId" component={OrderView} />
              <PrivateRoute exact path="/invoices" component={InvoiceListing} />
              <PrivateRoute exact path="/shipments" component={Shipments} />
              <PrivateRoute exact path="/newshipment/:oId" component={NewShipment} />
              <PrivateRoute exact path="/shipmentview/:oId/:sId" component={ShipmentView} />
              <PrivateRoute exact path="/invoiceView/:oId/:iId" component={InvoiceView} />
              <PrivateRoute exact path="/newinvoice/:oId" component={NewInvoice} />
              <PrivateRoute exact path="/creditmemo" component={CreditMemo} />
              <PrivateRoute exact path="/newcreditmemo/:oId" component={NewCreditMemo} />
              <PrivateRoute exact path="/creditmemoview/:oId/:cId" component={CreditMemoView} />
              <PrivateRoute exact path="/transactions" component={Transactions} />
              <PrivateRoute exact path="/transactionView/:oId/:tId" component={TransactionView} />
              <PrivateRoute exact path="/reorder/:oId" component={ReOrder} />
              <PrivateRoute exact path="/editOrder/:oId" component={EditOrder} />
              <PrivateRoute exact path="/reviewlist" component={ProductReviews} />
              <PrivateRoute exact path="/editreview/:rId" component={EditReview} />
              <PrivateRoute exact path="/roleslist" component={AccessManagement} />
              <PrivateRoute exact path="/editrole/:rId" component={EditPermissions} />
              <PrivateRoute exact path="/addrole" component={EditPermissions} />
              <PrivateRoute exact path="/discountsettings" component={DiscountSettings} />
              <PrivateRoute exact path="/notifications" component={NotificationSettings} />
              <PrivateRoute exact path="/defaultMeta" component={DefaultMeta} />
              <PrivateRoute exact path="/cmsview/:cId" component={CmsView} />
              <PrivateRoute exact path="/cmsListing" component={CmsListing} />
              <PrivateRoute exact path="/adminEmails" component={AdminEmails} />
              <PrivateRoute exact path="/smtpSettings" component={SMTPsetting} />
              <PrivateRoute exact path="/shippingMethods" component={ShippingMethods} />
              <PrivateRoute exact path="/addShipping" component={AddShipping} />
              <PrivateRoute exact path="/editShipping/:sId" component={AddShipping} />
              <PrivateRoute exact path="/supplier" component={Suppleirs} />
              <PrivateRoute exact path="/payments" component={PaymentSettings} />
              <PrivateRoute exact path="/addsuppliers" component={AddSuppliers} />
              <PrivateRoute exact path="/editsuppliers/:sId" component={AddSuppliers} />
              <PrivateRoute exact path="/states" component={States} />
              <PrivateRoute exact path="/addstates" component={AddStates} />
              <PrivateRoute exact path="/editstates/:rId" component={AddStates} />
              <PrivateRoute exact path="/brand" component={Brand} />
              <PrivateRoute exact path="/usersOptions" component={UsersOptions} />
              <PrivateRoute exact path="/addbrand" component={AddBrand} />

              <PrivateRoute path="/*" component={PageNotFound} />
            </Switch>
          </Router>
        </PersistGate>
      </Provider>
    );
  }
}


export default App
 
// const mapStateToProps = state => ({
//   permissionsList:state.admindata.rolePermissions,
// });
// export default connect( mapStateToProps, null )(App)