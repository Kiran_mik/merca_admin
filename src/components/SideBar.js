
import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux'
import * as actions from '../actions'
import { IMAGE_URL } from '../config/configs'

class Sidebar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      emailId: '',
      photo: '',
      adminUsersAccess: {},
      cmsPagesAccess: {},
      emailSettingsAccess: {},
      homeBannerAccess: {},
      ordersAccess: {},
      productCategoryAccess: {},
      productsAccess: {},
      rolesAccess: {},
      usersAccess: {},
      role:""
    };
    this.changeRoute = this.changeRoute.bind(this);
  }

  componentDidMount() {
    var permissions = this.props.permissionsList
    if (permissions.rolePermission) {
      let { adminUsersAccess, cmsPagesAccess, emailSettingsAccess, homeBannerAccess, ordersAccess, productCategoryAccess,
        productsAccess, rolesAccess, usersAccess } = permissions.rolePermission
      this.setState({
        adminUsersAccess: adminUsersAccess, usersAccess: usersAccess,
        cmsPagesAccess: cmsPagesAccess, emailSettingsAccess: emailSettingsAccess, homeBannerAccess: homeBannerAccess,
        ordersAccess: ordersAccess, productCategoryAccess: productCategoryAccess,
        productsAccess: productsAccess, rolesAccess: rolesAccess,
        role:permissions.role
      })
    }

    if (this.props.admindata.data) {
      if (this.props.editAdminProfileData && this.props.editAdminProfileData.data) {
        var { firstName, emailId, photo } = this.props.editAdminProfileData.data;
        this.setState({ userName: firstName, emailId: emailId, photo: photo })
      } else {
        var { firstName, email, photo } = this.props.admindata.data;
        this.setState({ userName: firstName, emailId: email, photo: photo })
      }
    }
  }
  changeRoute(page) {
    this.props.history.push(page);
  }
  render() {
    let { userName, photo, adminUsersAccess, cmsPagesAccess, emailSettingsAccess, homeBannerAccess, ordersAccess, productCategoryAccess,
      productsAccess, rolesAccess, usersAccess } = this.state
    let path = this.props.location.pathname;
    return (
      <nav className="sidebar sidebar-offcanvas" id="sidebar">
        <div className="text-center my-2 user-profile">
          <div className="thumb-img">
            <img src={photo != "" ? IMAGE_URL + photo : "/assets/images/no-image-user.png"} alt={userName} />
          </div>
          <span>{userName}</span>
        </div>
        <div className="sidebar-wrapper">
          <ul className="nav panel-group" id="accordionExample">
            <li className='nav-item'>
              <a className="nav-link" onClick={this.changeRoute.bind(this, '/dashboard')}>
                <i className="fa fa-dashboard" aria-hidden="true" />
                <span className="menu-title">Tablero</span>
              </a>
            </li>
            {/* //USER */}
            {usersAccess.viewList == false && adminUsersAccess.view == false ? null :
              <li className='nav-item' data-target="#collapseOne" data-toggle="toggle" aria-expanded="true" aria-controls="collapseOne" routerlinkactive="active">
                <a className="nav-link">
                  <i className="fa fa-users" aria-hidden="true" />
                  <span className="menu-title">
                  Los usuarios</span>
                  <i className="fa fa-angle-right drop-arrow"></i>
                </a>
                <div className="nav__submenu collapse" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <ul className="nav__subnav">
                    {usersAccess.viewList ?
                      <li className={path === 'usersList' ? 'nav-item active' : 'nav-item'} >
                        <a className="nav-link" onClick={this.changeRoute.bind(this, '/usersList')}>
                          <i className="fa fa-users" aria-hidden="true" />
                          <span className="menu-title">Los usuarios</span>
                        </a>
                      </li> : null}
                    {adminUsersAccess.view ?
                      <li className={path === '/adminUsers' ? 'nav-item active' : 'nav-item'} >
                        <a className="nav-link" onClick={this.changeRoute.bind(this, '/adminUsers')}>
                          <i className="fa fa-user-plus" aria-hidden="true" />
                          <span className="menu-title">Administración</span>
                        </a>
                      </li>
                      : null}
                  </ul>
                </div>
              </li>}
            {/* //PRODUCT */}
            {productCategoryAccess.viewList == false && productsAccess.viewList == false && productsAccess.addBulkProduct == false && productsAccess.bulkimageUpload == false ? null :
              <li className="nav-item" data-target="#collapseTwo" data-toggle="toggle" aria-expanded="true" aria-controls="collapseTwo" routerlinkactive="active">
                <a className="nav-link">
                  <i className="fa fa-tag" aria-hidden="true" />
                  <span className="menu-title">
                  Catalogo de producto</span>
                  <i className="fa fa-angle-right drop-arrow"></i>
                </a>
                <div className="nav__submenu collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordionExample" >
                  <ul className="nav__subnav">
                    {productCategoryAccess.viewList ?
                      <li className={path === '/categorylist' ? 'nav-item active' : 'nav-item'} >
                        <a className="nav-link" href="javascript:;" onClick={this.changeRoute.bind(this, '/categorylist')} >
                          <i className="fa fa-sitemap" aria-hidden="true" />
                          <span className="menu-title">Categorías</span>
                        </a>
                      </li> : null}
                    {productsAccess.viewList ?
                      <li className={path == '/productlist' ? 'nav-item active' : 'nav-item'} >
                        <a className="nav-link" href="javascript:;" onClick={this.changeRoute.bind(this, '/productlist')}>
                          <i className="fa fa-tags" />
                          <span className="menu-title">Productos</span>
                        </a>
                      </li>
                      : null}
                    {productsAccess.addBulkProduct == false && productsAccess.bulkimageUpload == false ? null :
                      <li className={path == '/bulkuploads' ? 'nav-item active' : 'nav-item'} >
                        <a className="nav-link" href="javascript:;" onClick={this.changeRoute.bind(this, '/bulkuploads')}>
                          <i className="fa fa-folder" aria-hidden="true" />
                          <span className="menu-title">Subidas masivas</span>
                        </a>
                      </li>}
                  </ul>
                </div>
              </li>}
            {/* //SALES */}
            {ordersAccess.viewList == false ? null :
              <li className="nav-item" data-target="#collapseThree" data-toggle="toggle" aria-expanded="true" aria-controls="collapseThree" routerlinkactive="active">
                <a className="nav-link">
                  <i className="fa fa-shopping-cart" aria-hidden="true" />
                  <span className="menu-title">
                  Ventas</span>
                  <i className="fa fa-angle-right drop-arrow"></i>
                </a>
                <div className="nav__submenu collapse" id="collapseThree" aria-labelledby="headingThree" data-parent="#accordionExample" >
                  <ul className="nav__subnav">
                    {ordersAccess.viewList ?
                      <li className={path === '/orderlisting' ? 'nav-item active' : 'nav-item'} >
                        <a className="nav-link" onClick={this.changeRoute.bind(this, '/orderlisting')}>
                          <i className="fa fa-shopping-cart" aria-hidden="true" />
                          <span className="menu-title">Pedidos</span>
                        </a>
                      </li> : null}
                    {ordersAccess.viewInvoice ?
                      <li className={path === '/orderlisting' ? 'nav-item active' : 'nav-item'} >
                        <a className="nav-link" onClick={this.changeRoute.bind(this, '/invoices')}>
                          <i className="fa fa-money" aria-hidden="true" />
                          <span className="menu-title">Facturas</span>
                        </a>
                      </li> : null}
                    {ordersAccess.viewShipment ?
                      <li className={path === '/orderlisting' ? 'nav-item active' : 'nav-item'} >
                        <a className="nav-link" onClick={this.changeRoute.bind(this, '/shipments')}>
                          <i className="fa fa-truck" aria-hidden="true" />
                          <span className="menu-title">Envíos</span>
                        </a>
                      </li> : null}
                    {ordersAccess.viewMemo ?
                      <li className={path === '/orderlisting' ? 'nav-item active' : 'nav-item'} >
                        <a className="nav-link" onClick={this.changeRoute.bind(this, '/creditmemo')}>
                          <i className="fa fa-credit-card" aria-hidden="true" />
                          <span className="menu-title">Notas de crédito</span>
                        </a>
                      </li> : null}
                    {ordersAccess.viewTransaction ?
                      <li className={path === '/orderlisting' ? 'nav-item active' : 'nav-item'} >
                        <a className="nav-link" onClick={this.changeRoute.bind(this, '/transactions')}>
                          <i className="fa fa-usd" aria-hidden="true" />
                          <span className="menu-title">Actas</span>
                        </a>
                      </li> : null}

                  </ul>
                </div>
              </li>}

            {/* //PRODUCT REVIEWS */}
            <li className='nav-item' >
              <a className="nav-link" onClick={this.changeRoute.bind(this, '/reviewlist')}>
                <i className="fa fa-star" aria-hidden="true" />
                <span className="menu-title">Revisiones de productos</span>
              </a>
            </li>

            {/* //CMS */}
            {!homeBannerAccess.view && !cmsPagesAccess.view ? null :
              <li className="nav-item" data-target="#collapseFour" data-toggle="toggle" aria-expanded="true" aria-controls="collapseFour" routerlinkactive="active">
                <a className="nav-link">
                  <i className="fa fa-edit" aria-hidden="true" />
                  <span className="menu-title">
                  Gestión de contenido</span>
                  <i className="fa fa-angle-right drop-arrow"></i>
                </a>
                <div className="nav__submenu collapse" id="collapseFour" aria-labelledby="headingFour" data-parent="#accordionExample">
                  <ul className="nav__subnav">
                    {homeBannerAccess.view ?
                      <li className={path === '/homesection' ? 'nav-item active' : 'nav-item'} >
                        <a className="nav-link" onClick={this.changeRoute.bind(this, '/homesection')}>
                          <i className="fa fa-home" aria-hidden="true" />
                          <span className="menu-title">Casa</span>
                        </a>
                      </li>
                      : null}
                    {cmsPagesAccess.view ?
                      <li className={path == '/cmsListing' ? 'nav-item active' : 'nav-item'} onClick={this.changeRoute.bind(this, '/cmsListing')}>
                        <a className="nav-link" >
                          <i className="fa fa-edit" />
                          <span className="menu-title">Gestión de CMS</span>
                        </a>
                      </li>
                      : null}
                  </ul>
                </div>
              </li>}

            {/* //ROLES */}
            {rolesAccess.viewList ?
              <li className='nav-item' >
                <a className="nav-link" onClick={this.changeRoute.bind(this, '/roleslist')}>
                  <i className="fa fa-check-circle" aria-hidden="true" />
                  <span className="menu-title">Gestionar roles</span>
                </a>
              </li>
              : null}
            {/* //EMAIL */}
            {emailSettingsAccess.viewEmailTemplates ?
              <li className="nav-item" data-target="#collapseFive" data-toggle="toggle" aria-expanded="true" aria-controls="collapseFive" routerlinkactive="active">
                <a className="nav-link">
                  <i className="fa fa-envelope" aria-hidden="true" />
                  <span className="menu-title">
                  Ajustes del correo electrónico</span>
                  <i className="fa fa-angle-right drop-arrow"></i>
                </a>
                <div className="nav__submenu collapse" id="collapseFive" aria-labelledby="headingFive" data-parent="#accordionExample">
                  <ul className="nav__subnav">
                    <li className={path === '/adminEmails' ? 'nav-item active' : 'nav-item'} >
                      <a className="nav-link" onClick={this.changeRoute.bind(this, '/adminEmails')}>
                        <i className="fa fa-envelope" aria-hidden="true" />
                        <span className="menu-title">Correos electrónicos de administrador</span>
                      </a>
                    </li>

                    <li className={path === '/smtpSettings' ? 'nav-item active' : 'nav-item'} >
                      <a className="nav-link" onClick={this.changeRoute.bind(this, '/smtpSettings')}>
                        <i className="fa fa-server" aria-hidden="true" />
                        <span className="menu-title">Configuraciones SMTP</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              : null}
            {/* //MASTER */}
            {this.state.role === 'Super Admin'?
            <li className="nav-item" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix" routerlinkactive="active">
              <a className="nav-link">
                <i className="fa fa-edit" aria-hidden="true" />
                <span className="menu-title">
                Master Management</span>
                <i className="fa fa-angle-right drop-arrow"></i>
              </a>
              <div className="nav__submenu collapse" id="collapseSix" data-toggle="toggle" aria-labelledby="headingSix" data-parent="#accordionExample">
                <ul className="nav__subnav">
                  <li className={path === '/shippingMethods' ? 'nav-item active' : 'nav-item'} >
                    <a className="nav-link" onClick={this.changeRoute.bind(this, '/shippingMethods')} >
                      <i className="fa fa-truck" aria-hidden="true" />
                      <span className="menu-title">Transporte</span>
                    </a>
                  </li>
                  <li className={path === '/supplier' ? 'nav-item active' : 'nav-item'} >
                    <a className="nav-link" onClick={this.changeRoute.bind(this, '/supplier')} >
                      <i className="fa fa-users" aria-hidden="true" />
                      <span className="menu-title">Proveedores</span>
                    </a>
                  </li>
                  <li className={path === '/shippingMethods' ? 'nav-item active' : 'nav-item'} >
                    <a className="nav-link" onClick={this.changeRoute.bind(this, '/states')} >
                      <i className="fa fa-flag" aria-hidden="true" />
                      <span className="menu-title">Provincia</span>
                    </a>
                  </li>
                  <li className={path === '/brand' ? 'nav-item active' : 'nav-item'} >
                    <a className="nav-link" onClick={this.changeRoute.bind(this, '/brand')} >
                      <i className="fa fa-tag" aria-hidden="true" />
                      <span className="menu-title">Marca</span>
                    </a>
                  </li>
                  <li className={path === '/usersOptions' ? 'nav-item active' : 'nav-item'} >
                    <a className="nav-link" onClick={this.changeRoute.bind(this, '/usersOptions')} >
                      <i className="fa fa-bars" aria-hidden="true" />
                      <span className="menu-title">Opciones para clientes</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
            : null} 
            {/* //SETTINGS */}
            {this.state.role === 'Super Admin'?
            <li className="nav-item" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven" routerlinkactive="active">
              <a className="nav-link">
                <i className="fa fa-cog" aria-hidden="true" />
                <span className="menu-title">
                Configuraciones</span>
                <i className="fa fa-angle-right drop-arrow"></i>
              </a>
              <div className="nav__submenu collapse" id="collapseSeven" data-toggle="toggle" aria-labelledby="headingSeven" data-parent="#accordionExample">
                <ul className="nav__subnav">
                  <li className={path === '/defaultMeta' ? 'nav-item active' : 'nav-item'} >
                    <a className="nav-link" onClick={this.changeRoute.bind(this, '/defaultMeta')} >
                      <i className="fa fa-globe" aria-hidden="true" />
                      <span className="menu-title">Configuración global</span>
                    </a>
                  </li>
                  <li className={path === '/payments' ? 'nav-item active' : 'nav-item'} >
                    <a className="nav-link" onClick={this.changeRoute.bind(this, '/payments')} >
                      <i className="fa fa-money" aria-hidden="true" />
                      <span className="menu-title">Configuración de pago</span>
                    </a>
                  </li>
                  <li className={path === '/notifications' ? 'nav-item active' : 'nav-item'} >
                    <a className="nav-link" onClick={this.changeRoute.bind(this, '/notifications')} >
                      <i className="fa fa-bell" aria-hidden="true" />
                      <span className="menu-title">Configuración de las notificaciones</span>
                    </a>
                  </li>

                  <li className={path === '/discountsettings' ? 'nav-item active' : 'nav-item'} >
                    <a className="nav-link" onClick={this.changeRoute.bind(this, '/discountsettings')} >
                      <i className="fa fa-cog" aria-hidden="true" />
                      <span className="menu-title">Configuraciones de descuento</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li> :null}
          </ul>
        </div>
      </nav>
    );
  }
}
const mapStateToProps = state => ({
  admindata: state.admindata.admindata,
  permissionsList: state.admindata.rolePermissions,
  editAdminProfileData: state.commonReducers.edited1Details
});
export default withRouter(connect(mapStateToProps, actions)(Sidebar));
