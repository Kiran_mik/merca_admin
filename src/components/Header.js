import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import * as actions from '../actions'
class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      emailId: '',
      photo: ''
    }
  }
  logout = () => {
    // localStorage.setItem('token', null)
    this.props.Logout(this.props);
    // this.props.history.push('/')
  }
  componentDidMount() {
    if (this.props.admindata.data) {
      if (this.props.editAdminProfileData && this.props.editAdminProfileData.data) {
        var { firstName, emailId, photo } = this.props.editAdminProfileData.data;
        this.setState({ userName: firstName, emailId: emailId, photo: photo })
      } else {
        var { firstName, email, photo } = this.props.admindata.data;
        this.setState({ userName: firstName, emailId: email, photo: photo })
      }
    }
  } 



  render() {
    let { userName } = this.state
    return (
      <nav className='navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row'>
        <div className='text-center navbar-brand-wrapper d-flex align-items-top justify-content-center'>
          <a className='navbar-brand brand-logo' href='javascript:void(0)'>
            <img src='/assets/images/logo.svg' alt='logo' />
          </a>
        </div>
        <div className='navbar-menu-wrapper d-flex align-items-center'>
          <a id='sidebarCollapse'>
            <img src='/assets/images/toggle-menu.png' alt='menu' />
          </a>
          <ul className='navbar-nav navbar-nav-right'>
            <li className='nav-item dropdown profile-dropdown'>
              <a className='nav-link count-indicator dropdown-toggle' id='messageDropdown' data-toggle='dropdown' aria-expanded='false' >
                {userName}
              </a>
              <div className='dropdown-menu dropdown-menu-right navbar-dropdown preview-list' aria-labelledby='messageDropdown' >
                <div className='profile-bg'>
                  <div className='img-user'>
                    <div className='btn-section'>
                      <span className='btn btn-sm' onClick={() => this.props.history.push('/adminprofilepage')} >
                        <i className="fa fa-user"></i>  My Profile
                      </span>
                      <span className='btn btn-sm' onClick={() => this.props.history.push('/changePassword')} >
                        <i className="fa fa-cog"></i> Change Password
                      </span>
                      <div className="dropdown-divider mt-2"></div>
                      <span className='btn btn-sm' onClick={this.logout.bind(this)} >
                        <i className="fa fa-power-off"></i>  Logout
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}

const mapStateToProps = state => ({
  admindata: state.admindata.admindata,
  editAdminProfileData: state.commonReducers.edited1Details
});


export default withRouter(connect(mapStateToProps, actions)(Header));